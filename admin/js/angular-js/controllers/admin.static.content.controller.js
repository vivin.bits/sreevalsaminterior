llmAdminApp.controller('adminStaticContentController', function ($scope, $http, $compile, $rootScope, $location) {
    $scope.edit = false;
    
    $scope.toggleEdit = function ($event) {
        $scope.edit = $scope.edit ? false : true;
    };
    
    $scope.saveMe = function ($event) {
        $event.preventDefault();
        var $target = $event.target;
        var $form = $target.closest('form');
        $http.post($form.attributes['action'].value,
            {file : $($form).find('#file').val(),field: $($form).find('#field').val(),value: $($form).find('#value').val()}
        ).then(function (response) {
            if (response.data.status) {
                successMsg(response.data.msg);
            }
        }, function (response) {
            errorMsg(response.data.msg);
        });
    };
    
    $scope.toggleDetails = function ($event) {
        var $target = $event.target;
        var $text = $($target).html();
        $($target).closest('thead').next().find('table').toggle("slow", function(){
            if($($target).html() === 'Show'){
                $($target).removeClass('btn btn-warning');
                $($target).addClass('btn btn-secondary');
                $($target).html('Hide');
            }else{
                $($target).removeClass('btn btn-secondary');
                $($target).addClass('btn btn-warning');
                $($target).html('Show');
            }
        });
    };
});