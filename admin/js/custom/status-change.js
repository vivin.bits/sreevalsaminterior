

$('body').on('click', '.change-status', function () {
    var $this = this;
    var $status =  $(this).is(":checked");
    var id = $(this).data("id") ;
   
    var content = $(this).data("content") ;
    var url = content+'/change-status';

    $.ajax({
        url: url,
        type: 'POST',  // user.destroy
        data:{
            'id':id
        },
        beforeSend: function(){
        },
            success: function (data) {
                successMsg(data.msg);
                location.reload();
            },
            error: function (data) {
                if($status){
                    $($this).prop('checked', false);
                    window.location.reload;
                }else{
                    $($this).prop('checked', true);
                    window.location.reload;
                }
                var data = $.parseJSON(data.responseText);
                errorMsg(data.msg);
            },
            complete: function() {
                window.location.reload;
            }
        });
    });
// $('.change-status').change(function(){
//     var $this = this;
//     var $status =  $(this).is(":checked");
//     var $token = $('meta[name="csrf-token"]').attr("content");
//     var $data ={ id:$(this).data('id'), _token:$token};
//     var url = base_url+'/'+content+'/change-status';
//     $.ajax({
//         url: $url,
//         type: 'POST',
//         data: $data,
//         beforeSend: function() {

//         },
//         success: function (data) {
//             successMsg(data.msg);
//         },
//         error: function (data) {
//             if($status){
//                 $($this).prop('checked', false);
//             }else{
//                 $($this).prop('checked', true);
//             }
//             var data = $.parseJSON(data.responseText);
//             errorMsg(data.msg);
//         },
//         complete: function() {

//         }
//     });
// });


