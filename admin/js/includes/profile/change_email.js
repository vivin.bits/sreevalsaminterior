function callmodal(e) {
    $id = e;

    $('#btn-yes').attr("data-myval", $id);
    $('#dModal').modal('show');
}
function callUsermodal(e) {
    var id = e;
    $('#myModal').modal('show');
    var data = {
        id: id
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: base_url + '/fetch/list-userss',
        data: data,
        type: 'POST',
        beforeSend: function(){
         $('.rx56').html('loading....');
        },
        success: function (data) {

           $('.rx56').html(data.html);

        }
    });
   
}
function callConmodal(e) {
    var id = e;
    $('#cModal').modal('show');
    var data = {
        id: id
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: base_url + '/fetch/notification-content',
        data: data,
        type: 'POST',
        beforeSend: function(){
         $('.cmodal56').html('loading....');
        },
        success: function (data) {

           $('.cmodal56').html(data.html);

        }
    });
   
}


function delNotification(e) {
    var id = $('#btn-yes').attr("data-myval");
    $this = $(this);
    var data = {
        id: id
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: base_url + '/notification/delete',
        data: data,
        type: 'POST',
        success: function (data) {

            table.ajax.reload();
            $('#dModal').modal('hide');

        }
    });
}


function changeBlock(e, type) {

    if (e.checked) {

        var s = 1;

    } else {
        var s = 0;

    }

    var url = "ajax/changeBlockStatus";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: { id: e.id, status: s },
        success: function (result) {
      
            if (result.s == false) {
                table.ajax.reload();
                iziToast.success({
                    title: 'Block Status',
                    message: result.msg
                });

            }
            else {
                table.ajax.reload();
                iziToast.error({
                    title: 'Block Status',
                    message: result.msg
                });
            }


        }
    });
}
