<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $table = 'album';
    public $timestamps = true;
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
    public function gallery()
    {
        return $this->hasMany('App\Gallery');
    }
}
