<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'department';
    public $timestamps = true;
    
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
    public function course()
    {
        return $this->hasMany('App\course');
    }

}
