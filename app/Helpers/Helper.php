<?php

if (!function_exists('permission_checked')) {

    function permission_checked($role_id, $perm_id) {
        $matchThese = ['role_id' => $role_id, 'permission_id' => $perm_id];
        $permission_entry = \App\Permission_role::where($matchThese)->get();
        if ($permission_entry->count() == 0) {
            return false;
        } else {
            return true;
        }
    }

}

if (!function_exists('check_page_url')) {

    function check_page_url($path) {
        $path = ltrim($path, '/');
        $slug_check = \App\Page::where('slug', $path)->get();
        if ($slug_check->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('has_permission')) {

    function has_permission($permission) {
     //return true;
     $flag = false;
     $permissions = DB::table('permissions')->join('permission_role', function($join) {
        $join->on('permissions.id', '=', 'permission_role.permission_id')->where('permission_role.role_id', '=', Auth::user()->role_id);
    })->select('permissions.name')
     ->get();
     foreach ($permissions as $p) {
        if ($p->name == $permission)
            $flag = true;
    }
    return($flag);

}

}

if (!function_exists('get_image_admin')) {

 function get_image_admin($db_path, $dimension = 'default') {
     $file_path = pathinfo($db_path);
     $output = "";
     $dir_name = $file_path['dirname'];
     $base_name = $file_path['basename'];
     $extension = $file_path['extension'];
     $file_name = $file_path['filename'];
     $image_sizes = Config::get('constants.image_sizes');
     $key_exist = array_key_exists($dimension, $image_sizes);

     if ($key_exist) {
         $size = $image_sizes[$dimension];
         $width = $size[0];
         $height = $size[1];
         $allowed = true;
     } else {
         $allowed = false;
     }
     if ($allowed) {
         $destination_img_name = $dir_name . '/' . $file_name . '-' . $width . 'x' . $height . '.' . $extension;
         $output = url('media') . '/' . $destination_img_name;
         return $output;
     }
 }

}
if (!function_exists('get_banner')) {

   function get_banner($key) {

    $img = \App\BannerImage::where('img_key',$key)->first();
    return $img;

   }
 }


if (!function_exists('get_current_user_details')) {

    function get_current_user_details() {

        if (\Auth::check()) {
            return Auth::user();
        } else {
            return false;
        }
    }

}

if (!function_exists('make_slug')) {
function make_slug($string, $separator = '-')
{
  $string = trim($string);
  $string = mb_strtolower($string, 'UTF-8');
  $string = preg_replace("/[^a-z0-9_\s-ءاآؤئبپتثجچحخدذرزژسشصضطظعغفقكکگلمنوهی]/u", '', $string);
  $string = preg_replace("/[\s-_]+/", ' ', $string);
  $string = preg_replace("/[\s_]/", $separator, $string);

  return $string;
}
}



if (!function_exists('url_sanitize')) {

    function url_sanitize($str) {

        $title = strip_tags($str);
// Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
// Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);
// Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);



        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = str_replace('.', '-', $title);
        $title = str_replace('/', '-', $title);

// Convert nbsp, ndash and mdash to hyphens
        $title = str_replace(array('%c2%a0', '%e2%80%93', '%e2%80%94'), '-', $title);

// Strip these characters entirely
        $title = str_replace(array(
// iexcl and iquest
            '%c2%a1', '%c2%bf',
            // angle quotes
            '%c2%ab', '%c2%bb', '%e2%80%b9', '%e2%80%ba',
            // curly quotes
            '%e2%80%98', '%e2%80%99', '%e2%80%9c', '%e2%80%9d',
            '%e2%80%9a', '%e2%80%9b', '%e2%80%9e', '%e2%80%9f',
            // copy, reg, deg, hellip and trade
            '%c2%a9', '%c2%ae', '%c2%b0', '%e2%80%a6', '%e2%84%a2',
            // acute accents
            '%c2%b4', '%cb%8a', '%cc%81', '%cd%81',
            // grave accent, macron, caron
            '%cc%80', '%cc%84', '%cc%8c',
            ), '', $title);

// Convert times to x
        $title = str_replace('%c3%97', 'x', $title);

        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }

}
if (!function_exists('randomPassword')) {

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}
if (!function_exists('get_subcategory')) {

    function get_subcategory($id) {
        return \App\Category::descendantsOf($id)->toTree($id);
        ;
    }

}

if (!function_exists('get_users_count')) {

    function get_users_count() {
        $users =  App\User::where('active',1)->count();
       return $users;
    }

}

if (!function_exists('get_social_medias')) {

    function get_social_medias() {
        $socials =  App\Settings::where('is_social',1)->get();
        foreach($socials as $social)
        {
            $data[$social->name] = $social->value;
        }
       return $data;
    }

}



if (!function_exists('get_smtp_settings')) {

    function get_smtp_settings() {
        $smtpsettings =  \App\SmtpSettings::get();
        $data = [];
        foreach($smtpsettings as $setting)
        {
            $data[$setting->name] = $setting->value;
        }
       return $data;
    }

}

if (!function_exists('get_google_api_settings')) {

    function get_google_api_settings() {
        $smtpsettings =  \App\GoogleApiSettings::get();
        $data = [];
        foreach($smtpsettings as $setting)
        {
            $data[$setting->name] = $setting->value;
        }
       return $data;
    }

}

if (!function_exists('get_courses_count')) {

    function get_courses_count() {
        $domains =  App\Domains::where('status',1)->count();
       return $domains;
    }

}

if (!function_exists('get_subscribers_count')) {

    function get_subscribers_count() {
        $users =  App\User::leftJoin('membership','users.id','=','membership.user_id')->where('users.status',1)->count();
       return $users;
    }

}



if (!function_exists('get_all_categories')) {

    function get_all_categories() {
        // return \App\Category::whereIsRoot()->defaultOrder()->get();
        $categories =  App\Category::whereIsRoot()->defaultOrder()->get();
        // $categories = Cache::remember($categories, 50);
       return $categories;
    }

}

if (!function_exists('showCategories')) {

    function showCategories() {

        if (! Cache::has('categories')) {
             Cache::remember('categories',60, function() {// 60 minutes
               $categories =  App\Category::whereIsRoot()->defaultOrder()->get();
                $view =  View::make('include.categories')
                ->with('categories', $categories)
                ->render();
                 return   $view ;
            });
        }

         return  Cache::get('categories');
    }

}

if (!function_exists('set_file_name')) {

    function set_file_name($path, $filename, $ext) {
        //

        $file_extention = $ext;

        $filename = str_replace($file_extention, '', $filename);

        $file_name = str_slug($filename) . '.' . $file_extention; // preg_replace("/\s+/", "_", $filename);
        $month = date("n");
        $year = date("Y");

        if (!is_dir($path . $year . '/' . $month)) {
            mkdir($path . $year . '/' . $month, 0775, true);
        }
        $filename = $year . '/' . $month . '/' . $file_name;
        if (!file_exists($path . $filename)) {

            return $filename;
        }

        $filename = str_replace('.' . $file_extention, '', $filename);

        $new_filename = '';
        for ($i = 1; $i < 100; $i++) {
            if (!file_exists($path . $filename . $i . '.' . $file_extention)) {
                $new_filename = $filename . $i . '.' . $file_extention;
                break;
            }
        }
        if ($new_filename == '') {
            $this->set_error('upload_bad_filename');
            return FALSE;
        } else {
            return $new_filename;
        }
    }

}
if (!function_exists('get_settings')) {

    function get_settings() {
        $settings_all = array();
        $settings = \App\Settings::all();
        foreach ($settings as $setting) {
            $settings_all[$setting->name] = $setting->value;
        }
        return $settings_all;
    }

} 

if (!function_exists('getMetaTags')) {

    function getMetaTags() {
        $metaTags = array();
        $tags = \App\Settings::all();
        foreach ($tags as $tag) {
            $metaTags[$tag->name] = $tag->value;
        }
        return $metaTags;
    }

} 
if (!function_exists('getNotifications')) {

    function getNotifications() {
        $notification_all = array();
        $notification_all = \App\UserNotification::leftJoin('notified_users','user_notifications.id','=','notified_users.notification_id')
        ->where('notified_users.user_id','=',Auth::user()->id)
        ->orWhere('user_notifications.type','=',0)
        ->select('user_notifications.*')
        ->orderBy('user_notifications.is_read','ASC')
        ->take(5)
        ->get();
           return $notification_all;
    }

} 

if (!function_exists('getAllNotifications')) {

    function getAllNotifications() {
        $notification_all = array();
        $notification_all = \App\UserNotification::leftJoin('notified_users','user_notifications.id','=','notified_users.notification_id')
        ->where('notified_users.user_id','=',Auth::user()->id)
        ->orWhere('user_notifications.type','=',0)
        ->select('user_notifications.*')
        ->orderBy('user_notifications.created_at','DESC')
        ->simplepaginate(4);
       // ->get();
           return $notification_all;
    }

} 
if (!function_exists('get_notifications_count')) {

    function get_notifications_count() {
        $notification_count = array();
        $notification_count = \App\UserNotification::leftJoin('notified_users','user_notifications.id','=','notified_users.notification_id')
        ->where(function($query){
          $query->orWhere('notified_users.user_id','=',Auth::user()->id)
           ->orWhere('user_notifications.type','=',0);
          
        })
       ->where('user_notifications.is_read','=',0)
        ->select('user_notifications.*')
        
        ->count();
           return $notification_count;
    }

} 


if (!function_exists('get_google_ads')) {

    function get_google_ads() {
     return \App\GoogleAd::all()->first();

 }

}


if (!function_exists('checkUserFavourite')) {

    function checkUserFavourite($ad_id) {
        // if (Auth::check()) {
        //     return "<div class='fav pop-1' id='$ad_id' style='cursor: pointer;' class='rht'><b>Add to favourites</b> <i class='ion-android-star-outline'></i> </div>";

        // }


     if(!empty($ad_id) && Auth::check() ){
        $user_id = Auth::id();
        $ad= \App\UserFavourite::where('user_id',$user_id)->where('ad_id',$ad_id)->first();
       
        if(!empty($ad))
        {
            return "<div class='fav' id='$ad_id' style='cursor: pointer;' class='rht'><b>Remove from favourites</b><i class='ion-android-star-outline custom-icon'></i> </div>";
        }
        else
        {
  return "<div class='fav' id='$ad_id' style='cursor: pointer;' class='rht'><b>Add to favourites</b> <i class='ion-android-star-outline'></i> </div>";
        }
    }
  
}

}
if (!function_exists('limit_text')) {

    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

}
if (!function_exists('getAllPages')) {

    function getAllPages() {
      $pages = \App\Page::where('status',1)->orderBy('display_order','asc')->get();
      return $pages;
  }

}

if (!function_exists('getAllPagesWithOrder')) {

    function getAllPagesWithOrder() {
      $pages = \App\Page::where('status',1)->where('display_order','!=',0)->orderBy('display_order','asc')->get();
      return $pages;
  }

}
if (!function_exists('getAllFooterMenu')) {

    function getAllFooterMenu($cat=null) {
        if($cat)
           $footer_menu_list = \App\FooterMenu::where('status','active')->where('category',$cat)->orderBy('listing_order','asc')->get();
       return $footer_menu_list;
   }

}


if (!function_exists('trackActivity')) {

    function trackActivity($activity=null, $type=null) {
      $report = new \App\Activity();

      if (!Auth::guest() && !empty($activity) && !empty($type))
      {

         $user_name =  Auth::user()->firstname." ".Auth::user()->lastname;
         $statement = null;

       if(Auth::check() && Session::get('Admin_loged_in'))//admin loged in as user
       {
        $report->user_id = 1;
        $statement= "$activity in $user_name's account";
    }
    else
    { 

       $report->user_id = Auth::user()->id ;
       $statement = "$user_name has $activity";

   }

   $report->activity = $statement;
   $report->type = $type;
   $report->ip = $_SERVER['REMOTE_ADDR'];
   $report->save();
}
}

}



if (!function_exists('get_ad_detail_value')) {

    function get_ad_detail_value($ad_detail_id) {
        $ad_detail = \App\AdDetail::find($ad_detail_id);
        $value = '';
        if(count($ad_detail)>0){
            $ad_field_id = $ad_detail->ad_field_id;
            $ad_field_value = $ad_detail->ad_field_value;
            $ad_field_multiple_val = $ad_detail->ad_field_multiple_val;
            $ad_field_opt_status = $ad_detail->ad_opt_status;
            if(($ad_field_multiple_val=='no' && $ad_field_opt_status=='not_referred') || ($ad_field_multiple_val=='' && $ad_field_opt_status=='not_referred')){
                $value = $ad_field_value;
            }elseif($ad_field_multiple_val=='no' && $ad_field_opt_status=='referred'){
                $attr_detail = \App\AttributeValue::find($ad_field_value);
                if(count($attr_detail)>0){
                    $value = $attr_detail->attr_value;
                }
            }elseif($ad_field_multiple_val=='yes' && $ad_field_opt_status=='referred'){
                $attr_value_name_array = array();
                $attr_relation_records = \App\AdAttributesRelation::where('ad_detail_id',$ad_detail_id)->get();
                if(count($attr_relation_records)>0){
                 foreach($attr_relation_records as $row){
                     $attr_detail = \App\AttributeValue::find($row->ad_attr_value_id);
                     if(count($attr_detail)>0){
                        $value .= $attr_detail->attr_value.',';
                    }

                }
                if($value!=''){
                    $value=   rtrim($value, ',');
                }
            }

        }
    }
    return $value;    
}

}
if (!function_exists('get_field_value_edit')) {

    function get_field_value_edit($field_id,$ad_id) {
        $ad_detail = \App\AdDetail::where(array('ad_field_id'=>$field_id,'ad_main_id'=>$ad_id))->first();
        $value = '';
        if(count($ad_detail)>0){
            $ad_field_id = $ad_detail->ad_field_id;
            $ad_detail_id = $ad_detail->id;
            $ad_field_value = $ad_detail->ad_field_value;
            $ad_field_multiple_val = $ad_detail->ad_field_multiple_val;
            $ad_field_opt_status = $ad_detail->ad_opt_status;
            if(($ad_field_multiple_val=='no' && $ad_field_opt_status=='not_referred') || ($ad_field_multiple_val=='' && $ad_field_opt_status=='not_referred')){
                $value = $ad_field_value;
            }elseif($ad_field_multiple_val=='no' && $ad_field_opt_status=='referred'){
                $attr_detail = \App\AttributeValue::find($ad_field_value);
                if(count($attr_detail)>0){
                    $value = $attr_detail->id;
                }
            }elseif($ad_field_multiple_val=='yes' && $ad_field_opt_status=='referred'){
                $attr_value_name_array = array();
                $value = array();
                $attr_relation_records = \App\AdAttributesRelation::where('ad_detail_id',$ad_detail_id)->get();
                if(count($attr_relation_records)>0){
                 foreach($attr_relation_records as $row){
                     $value[] = $row->ad_attr_value_id;
//               $attr_detail = \App\AttributeValue::find($row->ad_attr_value_id);
//               if(count($attr_detail)>0){
//            $value .= $attr_detail->attr_value.',';
//        }

                 }
//           if($value!=''){
//            $value=   rtrim($value, ',');
//           }
             }

         }
     }
     return $value;    
 }

}

/********** To Check Whether Package exist **********/
if (!function_exists('check_package_exist')) {
  function check_package_exist($user_id) {
      $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->first();
      if($userMembership){
        return true;
      }else{
        return false;
      }
  }
}
// Check access to category while ad posting
if (!function_exists('check_access_for_category')) {
    function check_access_for_category($user_id, $category1, $category2) {
        $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->get();
        $data = null;
        if(count($userMembership)>0){
            $access = false;
           foreach($userMembership as $single){
               if($single->category1==0){//all category
                  // $access=true;
                   $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                   //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                   $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;//take values from master table
                   $balance = (int)$allowed_listings - $total_listings_user ;
                   if($balance>0)
                       $access=true;
               }elseif($single->category1==$category1 && ($single->category2=='' || $single->category2==NULL || $single->category2==0)){
                      // $access=true;
                   $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                   //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                   $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;
                   $balance = (int)$allowed_listings - $total_listings_user ;
                   if($balance>0)
                       $access=true;
               }elseif($single->category1==$category1 && $single->category2==$category2 ){
                      // $access=true;
                   $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                   //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                   $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;
                   $balance = (int)$allowed_listings - $total_listings_user ;
                   if($balance>0)
                       $access=true;

               }
//               elseif($single->category1==$category1 && $single->category2>0 ){
//                   $ancestorsAndSelf = \App\Category::ancestorsAndSelf($category2)->pluck('id')->toArray();
//                   if(in_array($single->category2, $ancestorsAndSelf)){
//                       $access=true;
//                   }
//
//               }
               if($access){
                   return $single;
               }




           }
        }
        return $data;
    }
}
if (!function_exists('getMembership')) {
    function getMembership($user_id, $category1, $category2) {
        $ancestorsAndSelf = [];
        $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->get();
        $data = null;
        $access = false;
        if(count($userMembership)>0){
            foreach($userMembership as $single){
                if($single->category1==0){//0=>all main categories
                    $access=true;
                }elseif($single->category1==$category1 && ($single->category2=='' || $single->category2==0)){
                    $access=true;
                }elseif($single->category1==$category1 && $single->category2>0 ){
                    $ancestorsAndSelf = \App\Category::ancestorsAndSelf($category2)->pluck('id')->toArray();
                    if(in_array($single->category2, $ancestorsAndSelf)){
                        $access=true;
                    }

                }

                if($access){
                    return $single;
                }
            }
        }
        return $data;
    }
}
if (!function_exists('getMembershipChk')) {
    function getMembershipChk($user_id, $category1, $category2) {
        $ancestorsAndSelf = [];
        $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->get();
        $data = null;
        $access = false;
        if(count($userMembership)>0){
            foreach($userMembership as $single){
                if($single->category1==0){//0=>all main categories
                   // $access=true;
                    $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                    //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                    $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;// take values from master table
                    $balance = (int)$allowed_listings - $total_listings_user ;
                    if($balance>0)
                        $access=true;
                }elseif($single->category1==$category1 && ($single->category2=='' || $single->category2==0)){
                   // $access=true;
                    $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                    //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                    $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;
                    $balance = (int)$allowed_listings - $total_listings_user ;
                    if($balance>0)
                        $access=true;
                }elseif($single->category1==$category1 && $single->category2>0 ){
                    $ancestorsAndSelf = \App\Category::ancestorsAndSelf($category2)->pluck('id')->toArray();
                    if(in_array($single->category2, $ancestorsAndSelf)){
                        //$access=true;
                        $total_listings_user =  getAdsCountOfUserByMembership($user_id,$single->id);
                        //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_listings : 0;
                        $allowed_listings = $single->getPackage ? $single->getPackage->num_listings : 0;
                        $balance = (int)$allowed_listings - $total_listings_user ;
                        if($balance>0)
                            $access=true;
                    }

                }

                if($access){
                    return $single;
                }
            }
        }
        return $data;
    }
}
if (!function_exists('getMembershipChkFeatured')) {
    function getMembershipChkFeatured($user_id, $category1, $category2) {
        $ancestorsAndSelf = [];
        $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->get();
        $data = null;
        $access = false;
        if(count($userMembership)>0){
            foreach($userMembership as $single){
                if($single->category1==0){//0=>all main categories
                    // $access=true;
                    //$total_listings_user =  getFeaturedAdsCountOfUserByMembership($user_id,$single->id);
                    $total_listings_user =  getFeaturedAdsCountOfUser($user_id);
                   // $allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_featured_listings : 0;
                    $allowed_listings = $single->getPackage ? $single->getPackage->num_featured_listings : 0;//take values from master table
                    $balance = (int)$allowed_listings - $total_listings_user ;
                    if($balance>0)
                        $access=true;
                }elseif($single->category1==$category1 && ($single->category2=='' || $single->category2==0)){
                    // $access=true;
                    //$total_listings_user =  getFeaturedAdsCountOfUserByMembership($user_id,$single->id);
                    $total_listings_user =  getFeaturedAdsCountOfUser($user_id, $category1);
                    //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_featured_listings : 0;
                    $allowed_listings = $single->getPackage ? $single->getPackage->num_featured_listings : 0;
                    $balance = (int)$allowed_listings - $total_listings_user ;
                    if($balance>0)
                        $access=true;
                }elseif($single->category1==$category1 && $single->category2>0 ){
                    $ancestorsAndSelf = \App\Category::ancestorsAndSelf($category2)->pluck('id')->toArray();
                    if(in_array($single->category2, $ancestorsAndSelf)){
                        //$access=true;
                       // $total_listings_user =  getFeaturedAdsCountOfUserByMembership($user_id,$single->id);
                        $total_listings_user =  getFeaturedAdsCountOfUser($user_id, $category1, $category2,$single->category2);
                        //$allowed_listings = $single->membershipDetail ? $single->membershipDetail->num_featured_listings : 0;
                        $allowed_listings = $single->getPackage ? $single->getPackage->num_featured_listings : 0;
                        $balance = (int)$allowed_listings - $total_listings_user ;
                        if($balance>0)
                            $access=true;
                    }

                }

                if($access){
                    return $single;
                }
            }
        }
        return $data;
    }
}
if (!function_exists('getUserFeaturedAdCount')) {

    function getUserFeaturedAdCount()
    {
        $count = \App\Ads::leftJoin('categories', 'categories.id', '=', 'ads.ad_main_category')
            ->leftJoin('users', 'ads.vendor_id', '=', 'users.id')

            ->where('ads.vendor_id', Auth::user()->id)
            ->where('users.active', 1)
            ->where('ads.featured_status', 'active')->count();


        return $count;
    }

}
/************ Check Banner,MPU Ad Category Access ************/
if (!function_exists('check_access_for_banner_mpu_category')) {
    function check_access_for_banner_mpu_category($user_id, $category1, $category2) {
      $ancestorsAndSelf = [];
        $userMembership = \App\UserMembership::whereUser_idAndExpiredAndStatus($user_id,0,1)->where('package_id','<>',NULL)->get();
        //dd($userMembership);
        $data = null;
        $access = false;
        if(count($userMembership)>0){
           foreach($userMembership as $single){
               if($single->category1==0){
                  $access=true;
               }elseif($single->category1==$category1 && ($single->category2=='' || $single->category2==0)){
                  $access=true;
               }elseif($single->category1==$category1 && $single->category2>0 ){
                  $ancestorsAndSelf = \App\Category::ancestorsAndSelf($category2)->pluck('id')->toArray();
                  if(in_array($single->category2, $ancestorsAndSelf)){
                    $access=true;
                  }

               }

               if($access){
                  return $single;
               }
           }
        }
        return $data;
    }
}
if (!function_exists('getAdsCountOfUser')) {

    function getAdsCountOfUser($user_id) {
        return \App\Ads::where(array('vendor_id' => $user_id,'listing_type'=>'paid'))->count();
    }

}
if (!function_exists('getAdsCountOfUserByMembership')) {

    function getAdsCountOfUserByMembership($user_id, $membership_id) {
        $cnt =  \App\Ads::join('ad_membership_relation', 'ads.id', '=', 'ad_membership_relation.ad_id')
            ->join('user_memberships', 'user_memberships.id', '=', 'ad_membership_relation.membership_id')
            ->select('ads.*','ad_membership_relation.membership_id','ad_membership_relation.user_id')
            ->where(array('ads.vendor_id' => $user_id,'ads.listing_type'=>'paid','ad_membership_relation.membership_id'=>$membership_id,'user_memberships.user_id'=>$user_id))->count();



        return $cnt;
    }

}
if (!function_exists('getFeaturedAdsCountOfUserByMembership')) {

    function getFeaturedAdsCountOfUserByMembership($user_id, $membership_id) {
        return \App\Ads::join('user_memberships', 'user_memberships.user_id', '=', 'ads.vendor_id')
            ->join('ad_membership_relation', 'ads.id', '=', 'ad_membership_relation.ad_id')
            ->where(array('ads.vendor_id' => $user_id,'ads.listing_type'=>'paid','ad_membership_relation.membership_id'=>$membership_id,'user_memberships.user_id'=>$user_id,'ads.featured_status'=>'active'))->count();
    }

}
if (!function_exists('getFeaturedAdsCountOfUser')) {

    function getFeaturedAdsCountOfUser($user_id,$ad_main_category=NULL, $ad_sub_category=NULL, $membership_category2=NULL ) {
		$query = \App\Ads::where(array('ads.vendor_id' => $user_id,'ads.featured_status'=>'active'));
		if(!empty($ad_main_category)){
			$query = $query->where('ads.ad_main_category',$ad_main_category);
		}
		if(!empty($ad_sub_category) && !empty($membership_category2)){
		    $category_detail = \App\Category::find($membership_category2);
			$descendants = $category_detail->descendants()->pluck('id')->toArray();
			$query = $query->whereIn('ads.ad_subcategory_id',$descendants);
		}
		return $count = $query->count();
        //return \App\Ads::where(array('ads.vendor_id' => $user_id,'ads.featured_status'=>'active'))->count();
    }

}

if (!function_exists('getCities')) {

    function getCities($parent = 1) {
        return \App\City::where(array('parent_id' => $parent, 'status' => 1))->orderBy('city_order')->get();
    }

}
function test() {
    for ($i = 224; $i <= 224; $i++) {
        if($i!=108 && $i!=115){
        $obj =  \App\Category::find($i);
        $obj->has_attributes = 0;
        $obj->save();
        }
    }
}
if (!function_exists('getCategoryIdFromSlug')) {
    function getCategoryIdFromSlug($slug)
    {
        $obj = \App\Category::where('slug',$slug)->first();
        if($obj)
            return $obj->id;
        else
            return false;

    }
}
function get_enum_values($table, $field) {

    $type = \DB::select(\DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$field}'"))[0]->Type;
    preg_match('/^enum\((.*)\)$/', $type, $matches);
    $enum = array();
    foreach (explode(',', $matches[1]) as $value) {
        $v = trim($value, "'");
        $enum = array_add($enum, $v, $v);
    }

    return $enum;
}
if (!function_exists('getUserMembershipCount')) {

    function getUserMembershipCount($user_id)
    {

        return  \App\UserMembership::Join('packages', 'packages.id', '=', 'user_memberships.package_id')
            ->where('user_memberships.status','<>',0)
            ->where('user_memberships.user_id',$user_id)

            ->count();
    }
}
if (!function_exists('getUserAdvertCount')) {

    function getUserAdvertCount($user_id)
    {

//        return  \App\Advert::where('ad_id','<>',NULL)
//            ->where('user_id',$user_id)
//
//
//            ->count();
        return  \App\Advert::where('user_id',$user_id)


            ->count();
    }
}
 function setUserMembershipExpiry()
{
    //scheduler
    $list = \App\UserMembership::where(array('status' => 1))->get();
    $today = \Carbon\Carbon::today()->toDateString();
    if (count($list) > 0) {
        foreach ($list as $row) {
            if ($row->to_date < $today) {
                $row->expired = 1;
                $row->save();
                $ads = isset($row->user) ? $row->user->get_ads : '';
                if($ads){//set ad listing order
                    foreach($ads as $ad){
                        $ad->order_status = 2;//paid ad expired,1=>paid ad,3=>free ad
                        $ad->featured_status = 'inactive';//paid ad expired,1=>paid ad,3=>free ad
                        $ad->save();

                    }
                }
            }
        }
    }
}

 function setListingAdExpiry()
{// set in cron
    // free plus expired paaid ad
    // $settings = new \App\Settings();
    // $validity_in_days = ($settings->getSetting('listing_ad_validity') && !empty($settings->getSetting('listing_ad_validity'))) ? $settings->getSetting('listing_ad_validity') : 0;

    $list = \App\Ads::where(array('expired'=>0))->get();
    $today = \Carbon\Carbon::today()->toDateString();

    if (count($list) > 0) {

        foreach ($list as $row) {
            //$obj = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at);
            // $expired_at = $obj->addDays($validity_in_days)->toDateString();

            if($row->listing_type=='free'){
                if (!empty($row->expired_at)){
                    if ($row->expired_at < $today) {
                        $row->expired = 1;
                        $row->save();
                    }
                }
            }
            elseif(count($row->getAdMembership)) {
                $membership_id = $row->getAdMembership->membership_id;
                $user_membership = \App\UserMembership::find($membership_id);
                if ($user_membership) {
                    if ($user_membership->expired == 1) {
                        if (!empty($row->expired_at)){
                            if ($row->expired_at < $today) {
                                $row->expired = 1;
                                $row->save();
                            }
                        }
                    }

                }
            }



//                $expired_date =$row->expired_at;
//                if(!empty($expired_date)) {
//                    if ($expired_date < $today) {
//                        $row->expired = 1;
//                        $row->save();
//
//                    }
//                }


        }

    }
}

if (!function_exists('service')) {

    function service()
    {
        $service = App\Album::where('status',1)->get();
        return $service;
    }
}

if (!function_exists('product')) {

    function product()
    {
        $service = App\Packages::where('status',1)->get();
        return $service;
    }
}

