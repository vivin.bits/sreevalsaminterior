<?php

namespace App\Http\Controllers\Admin;

use App\About;
use App\Mission;
use App\Vission;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutViewPage()
    {
        $mission = About::get();
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('title', function ($model) {
                return strip_tags($model->title);

            })
            ->editColumn('short_description', function ($model) {
                return strip_tags($model->short_description);

            })
            ->editColumn('description', function ($model) {
                return strip_tags($model->main_description);

            })
            ->editColumn('action', function ($model) {
                return '<a href="' . url('about/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="about" data-id="' . $model->id . '"></i></a>';

            })

            ->rawColumns(['sl#', 'title', 'short_description','description', 'action'])
            ->make(true);
    
    }
    public function index()
    {
        $data = About::All();
        return view('admin.about.index', ['abouts' => $data]);
    }

    public function indexmission()
    {
        $data = Mission::All();
        return view('admin.mission.index', ['missions' => $data]);
    }

    public function indexvission()
    {
        $data = Vission::All();
        return view('admin.vission.index', ['missions' => $data]);
    }

    public function indexmessage()
    {
        $data = Message::All();
        return view('admin.message.index', ['messages' => $data]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.about.create');
    }

    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'short_desc' => 'required',
            'main_desc' => 'required',
            'meta_title' => 'required',
            'meta_content' => 'required',
            'meta_description' => 'required',
            'meta_key' => 'required',
            'author' => 'required',


        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $about = new About();
        $about->title = $request->input('title');
        $about->slug = $request->input('slug');
        $about->short_description = $request->input('short_desc');
        $about->main_description = $request->input('main_desc');
        $about->meta_title = $request->input('meta_title');
        $about->meta_description = $request->input('meta_description');
        $about->meta_content = $request->input('meta_content');
        $about->meta_key = $request->input('meta_key');
        $about->author = $request->input('author');
        if ($request->hasFile('image')) {


            if ($request->file('image') != null) {
                $about->image = ImageUploadWithPath($request->image, 'profile-images');
            }
        }
        $about->save();
        if ($about->id) {
            return redirect()->back()
                ->with('message', '  added successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }

    public function editForm($id)
    {
        // dd($slug);
        $data = About::where('id', $id)->first();

        return view('admin.about.edit')->with(['about' => $data]);


    }

    public function edit($id, Request $request)
    {

        $v = Validator::make($request->all(), [
            // 'title' => 'required',
            // 'short_desc' => 'required',
            // 'main_desc' => 'required',
            // 'meta_title' => 'required',
            // 'meta_content' => 'required',
            //  'meta_description' => 'required',
            //  'meta_key' => 'required',
            //  'author' => 'required',


        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $about = About::find($id);
        $about->title = $request->input('title');
        $about->short_description = $request->input('short_desc');
        $about->main_description = $request->input('main_desc');
        $about->meta_title = $request->input('meta_title');
        $about->meta_description = $request->input('meta_description');
        $about->meta_content = $request->input('meta_content');
        $about->meta_key = $request->input('meta_key');
        $about->author = $request->input('author');
        if ($request->hasFile('image')) {


            if ($request->file('image') != null) {
                $about->image = ImageUploadWithPath($request->image, 'profile-images');
            }
        }
        $about->save();
        if ($about->id) {
            return redirect()->back()
                ->with('message', '  added successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\About $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    public function missionForm()
    {
        return view('admin.mission.create');
    }

    public function createmission(Request $request)
    {

        $v = Validator::make($request->all(), [
            'mission' => 'required',
            'vission' => 'required',

        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $mission = new Mission();
        $mission->mission = $request->input('mission');
        $mission->vission = $request->input('vission');
        $mission->save();
        if ($mission->id) {
            return redirect()->back()
                ->with('message', '  added successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }

    public function missioneditForm($id)
    {
        $data = Mission::find($id);
        return view('admin.mission.edit')->with(['mission' => $data]);
    }

    public function missionedit($id, Request $request)
    {
        $v = Validator::make($request->all(), [
            // 'content' => 'required',


        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $mission = Mission::find($id);
        $mission->mission = $request->input('mission');
        $mission->vission = $request->input('vission');
        $mission->save();
        if ($mission->id) {
            return redirect()->back()
                ->with('message', '  updated successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }

    public function messageForm()
    {
        return view('admin.message.create');
    } 

    public function createmessage(Request $request)
    {
        $v = Validator::make($request->all(), [
            'content' => 'required',
            'title' => 'required',


        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $file=$request->image;
      
        $mission = new Message();
        $mission->message = $request->input('content');
        $mission->title = $request->input('title');
        if ($request->hasFile('image')) {
            if ($request->file('image') != null) {
                $mission->image = ImageUploadWithPath($file, 'profile-images');
            }
        }
        $mission->save();
        if ($mission->id) {
            return redirect()->back()
                ->with('message', '  added successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }

    public function msgeditForm($id)
    {

        $data = Message::find($id);
        return view('admin.message.edit')->with(['news' => $data]);


    }

    public function editmsg($id, Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',


        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $message = Message::find($id);
        // $mission->message = $request->input('content');
        $message->title = $request->input('title');
        $message->message = $request->input('content');
        $message->save();
        if ($request->hasFile('image')) {

        
        
          if ($request->file('image') != null) {
            Message::where('id',$id)->update(['image'=>ImageUploadWithPath($request->image,'profile-images')]);
            }
        }
        if ($message->id) {
            return redirect()->back()
                ->with('message', '  added successfully.');
        }
        return redirect()->back()->with('message', 'Failed to save.');


    }


    public function destroy(Request $request)
    {

        $dd = About::find($request->get('id'))->delete();
        
    }

    public function msgdestroy(Request $request)
    {
        Message::find($request->id)->delete();



    }

    public function missiondestroy(Request $request)
    {


        Mission::find($request->get('id'))->delete();


    }

    public function missionList()
    {
        $mission = Mission::get();
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('vission', function ($model) {
                return strip_tags($model->vission);

            })
            ->editColumn('mission', function ($model) {
                return strip_tags($model->mission);

            })

            ->editColumn('action', function ($model) {
                return '<a href="' . url('mission/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="mission" data-id="' . $model->id . '"></i></a>';

            })
//
            ->rawColumns(['sl#', 'title', 'image', 'action'])
            ->make(true);
    }

    public function  getMessage()
    {

        $mission = Message::get();
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('name', function ($model) {
                return strip_tags($model->title);

            })
            ->editColumn('message', function ($model) {
                return strip_tags($model->message);

            })

            ->editColumn('action', function ($model) {
                return '<a href="' . url('message/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="message" data-id="' . $model->id . '"></i></a>';

            })

            ->rawColumns(['sl#', 'name', 'message', 'action'])
            ->make(true);
    }
}
