<?php

namespace App\Http\Controllers\Admin;
use App\SchoolActivity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ActivityController extends Controller
{
     /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function getAlbum()
    {
        $mission = SchoolActivity::get();
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('title', function ($model) {
                return strip_tags($model->title);

            })
            ->editColumn('image', function ($model) {
                //return 'file_path <i class="fa fa-eye show_images"></i>';
                if ($model->main_image) {
                    $imag = getImageByPath($model->main_image, '20x20', 'school-activity');
                } else {
                    $imag = "admin/images/pre-img.png";
                }
                return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
            })  
//             ->editColumn('gallery_image', function ($model) {
//                 return '<a href="' . url('album/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
// </a>';

//             })
            ->editColumn('status', function ($model) {
                
                $checked = '';
                if ($model->status == 1){
                    $checked = 'checked';
                }
                return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-content="album" data-id="'.$model['id'].'" '.$checked.'><div class="state p-success" ><label ></label ></div></div>';

        })

            ->editColumn('action', function ($model) {
                return '<a href="' . url('activity/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="activity" data-id="' . $model->id . '"></i></a>';

            })

            ->rawColumns(['sl#', 'title', 'image','gallery_image','status', 'action'])
            ->make(true);
    }

    public function index()
     {
          
         return view('admin.schoolActivity.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.schoolActivity.create');
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            
            'image'=>'required|max:4096'
    
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $album = new SchoolActivity();
            $album->title = $request->input('title');
            $album->slug = $request->input('slug');
            if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $album->main_image = ImageUploadWithPath($request->image,'school-activity');
                }
            }
        
           
            $album->save();
            if ($album->id) {
                return redirect()->back()
                    ->with('message', 'Photo  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
     
        $data = SchoolActivity::find($id);
        return view('admin.schoolActivity.edit')->with(['school' => $data]);
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            'title' => 'required',
           
             
             //'image'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $album = SchoolActivity::find($id);
            $album->title = $request->input('title');
        
         
             if ($request->hasFile('image')) {
                
                flushImage($album,'school-activity',1,'main_image');
                if ($request->file('image') != null) {
                    $album->main_image = ImageUploadWithPath($request->image,'school-activity');
                }
            }
            $album->save();
            if ($album->id) {
                return redirect()->back()
                    ->with('message', 'image added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function upload($id)
    {
        
        $data['gallery'] = SchoolActivity::find($id);

        $data['all_images'] = SchoolActivity::where( 'album_id', $id)->get();
       
        return view('admin.schoolActivity.upload',$data);
        
    }
    public function fileStore(Request $request)
    {
        // $image = $request->file('file');
        // $imageName = $image->getClientOriginalName();
        // $image->move(public_path('images'),$imageName);
        
        // $imageUpload = new Gallery();
        // $imageUpload->filename = $imageName;
        // $imageUpload->save();
        // return response()->json(['success'=>$imageName]);

        $gallery = new SchoolActivity();
        $gallery->album_id = $request->input('albumid');
        if ($request->hasFile('file')) {
                
            
            if ($request->file('file') != null) {
                $gallery->filename = ImageUploadWithPath($request->file,'school-activity');
            }
        }
        $gallery->save();
    }

    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        Gallery::where('filename',$filename)->delete();
        //$path=asset('media/profile-images/'.$filename);
       $path=('/media/school-activity/').$filename;
       if (file_exists($path)) {
           unlink($path);
       }
       return $filename;
        
    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    
    public function uploaddestroy( $id)
    {
    Gallery::where('id', $id)->delete();
    return redirect()->back();
    

    }
    public function destroy( Request $request)
    {
    $album=SchoolActivity::find($request->get('id'));
if($album)
{
    flushImage($album,'school-activity',1,'main_image');
    $album->delete();
}
   
    

    }
    public function changeStatus(Request $request)
    {
        $change=SchoolActivity::find($request->get('id'));
        if($change)
        {
        if($change->status == 1)
        {
            $change->status= 0;
            $change->save();
            return response()->json([
                'status'=>true,
                'message'=>"successfully updated",

            ],200);
        }
        else{
            $change->status=1;
            $change->save();
            return response()->json([
                'status'=>true,
                'message'=>"successfully updated",
            ],200);
        }
    }
    }
}