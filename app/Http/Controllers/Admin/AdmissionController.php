<?php

namespace App\Http\Controllers\Admin;

use App\Admission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Admission::All();
        return view('admin.admission.index',['admissions'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function show(admission $admission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function edit(admission $admission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, admission $admission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\admission  $admission
     * @return \Illuminate\Http\Response
     */
    public function destroy(admission $admission)
    {
        //
    }
}
