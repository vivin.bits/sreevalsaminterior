<?php
/**
 * Ajax Controller
 * 
 * Handle all ajax call to Admin module
 * 
 * PHP version 5
 * 
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1 
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\User;
use App\Domains;
use App\Certification;
use App\Paper;
use App\Chapter;
use App\SubChapter;
use App\Faq;
use App\Subscription;
use App\FaqCategories;
use App\PromoCode;
use App\Page;
use App\EmailTemplates;
use App\Exam;
use Validator;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * AjaxController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class AjaxController extends Controller
{
    /**
     * Change the user staus active or inactive
     * 
     * @param Requeust $request Request parameters
     * 
     * @return Response
     */
    public function changeUserStatus(Request $request)
    {
        $id = $request->get('id');
        $cstatus = $request->get('status');

        $rules = array(
            'id' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $record = User::find($id);
            $record->active = $cstatus;
            $record->save();
            if ($record->active == "1") {
                return response(['s' => true, 'msg' => 'activated'], 200);
            } 
            return response(['s' => false, 'msg' => 'deactivated'], 200);
        }
        return response(['s' => false, 'msg' => 'something went wrong!'], 400);

    }

    /**
     * Change the page status active or inactive
     *
     * @param Request $request Request parameters
     * 
     * @return Response
     */
    public function changePageStatus(Request $request)
    {
        $id = $request->get('id');
        $cstatus = $request->get('status');

        $rules = array(
            'id' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $record = Page::find($id);
            $record->status = $cstatus;
            $record->save();
            if ($record->status == "1") {
                return response(['s' => true, 'msg' => 'activated'], 200);
            }
            return response(['s' => false, 'msg' => 'deactivated'], 200);
        }
        return response(['s' => false, 'msg' => 'something went wrong!'], 400);
    }

    /**
     * Change the email status active or inactive
     * 
     * @param Request $request Request parameters
     * 
     * @return Response
     */
    public function changeEmailStatus(Request $request)
    {
        $id = $request->get('id');
        $cstatus = $request->get('status');

        $rules = array(
            'id' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $record = EmailTemplates::find($id);
            $record->status = $cstatus;
            $record->save();
            if ($record->status == "1") {
                return response(['s' => true, 'msg' => 'activated'], 200);
            } 
             return response(['s' => false, 'msg' => 'deactivated'], 200);
        }
        return response(['s' => false, 'msg' => 'something went wrong!'], 400);
    }

    /**
     * Change user block status
     * 
     * @param Request $request Request parameters
     * 
     * @return Response
     */
    public function changeBlockStatus(Request $request)
    {
        $id = $request->get('id');
        $cstatus = $request->get('status');

        $rules = array(
            'id' => 'required',
            'status' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $record = User::find($id);
            $record->blocked = $cstatus;
            $record->save();
            if ($record->blocked == "1") {
                $response = [
                    's' => true,
                    'msg' => $record->firstname . ' ' 
                    . $record->lastname . ' has been blocked'
                ];
                return response($response, 200);
            } else {
                $response1 = 1;
                $response = [
                    's' => false, 
                    'msg' => $record->firstname . ' ' 
                    . $record->lastname . ' has been unblocked'
                ];
                return response($response, 200);
            }
        }
          return response(['s' => false, 'msg' => 'something went wrong!'], 400);
    }
}
