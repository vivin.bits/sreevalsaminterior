<?php
/**
 * Ajax Controller
 * 
 * Handle authentication to Admin module
 * 
 * PHP version 5
 * 
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1 
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
/**
 * AuthController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class AuthController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo;
    /**
     * Function Usage : Construcor
     *
     * @return void
     */
    public function __construct()
    {

        if (Auth::check()) {
            return Redirect::to('admin/dashboard')->send();
        }
    }

    /**
     * Function Usage : Show login form
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {

        if (Auth::check()) {
//            \Auth::logout();
            return Redirect::to('admin/dashboard')->send();
        }
        if (Auth::check()) {
            \Auth::logout();
            \Session::flush();
            return view('admin.login');
        }

        return view('admin.login');
    }

    
    /**
     * Function Usage : Login
     *
     * @param Request $request Request parameters
     *
     * @return Response
     */
    public function login(Request $request)
    {


        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, true, true)) {
            /* Check user role  */


            /* Check user Active */


            $auth = true; //Success
            Auth::user()->last_login = new \DateTime;
            Auth::user()->save();
        } else {
            $auth = false; //failed
            return redirect()->back()->with('emessage', 'Invalid Credentials!');
        }

        if ($request->ajax()) {
//            dd('jj');
            return response()->json([
                'auth' => $auth,
                'intended' => URL::previous()
            ]);
        } else {
            return Redirect::to('admin/dashboard');
        }
    }
    /**
     *  Function Usage : Logout
     *
     * @return mixed
     */
    public function logout()
    {
        \Auth::logout();
        \Session::flush();
        return Redirect::to('admin/login')->with('message', 'Your are now logged out!');
    }

    /**
     *  Function Usage :Login as user
     *
     * @param Request $request Request parameters
     *
     * @return string
     */
    public function userLogin(Request $request)
    {
        $userKey = str_random(60);
        \App\User::where('id', $request->get('id'))->update(['token' => $userKey]);
        $url = env('APP_URL') . "/loginas/$userKey";

        if ($request->get('url')) {
            $url .= "?url=" . urlencode($request->get('url'));
        }

        return $url;
    }
    public function register()
    {
        dd('kai');
    }


}
