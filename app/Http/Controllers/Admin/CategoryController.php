<?php
/**
 * Category Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Event;
use App\Category;
use App\Media;
use App\WhereToGoFeatures;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use DB;

/**
 * CategoryController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class CategoryController extends Controller
{

    /**
     * Function Usage : Index
     *
     * @param Request $request Request parameters
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $txt_name = $request->get('txt_name') ? $request->get('txt_name') : null;
        $categories = Category::where('parent_id', 0)->orderBy('created_at', 'DESC');
        $categories = $categories->get();
        return view('admin.category.index')
            ->with(['categories' => $categories, 'txt_name' => $txt_name]);
    }

    /**
     * Function Usage : Create
     *
     * @return void
     */
    public function create()
    {
        $parentCategories = Category::where('parent_id', '=', '0')
            ->selectRaw('name,id')->get();
        return view('admin.category.create')
            ->with(['parentCategories' => $parentCategories]);
    }

    /**
     * Function Usage : Check Category delete
     *
     * @param Request $request Request parameters
     *
     * @return Response
     */
    public function checkCategoryDelete(Request $request)
    {
        $rules = ['id' => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response(
                ['success' => false, 'message' => 'Invalid Request'], 400
            );
        }
        $data = $request->all();
        $whereToGoCategory = Category::find($data['id']);
        $countOfSubCategories = count($whereToGoCategory->getSubcategory) ?
            count($whereToGoCategory->getSubcategory) : 0;
        $countOfPlaces = 0;

        return response(
            ['success' => true,
                'countOfSubCategories' => $countOfSubCategories,
                'countOfPlaces' => $countOfPlaces,
                'category_id' => $whereToGoCategory->id], 200
        );
    }

    /**
     * Function Usage : Delete Category
     *
     * @param Request $request Request parameters
     *
     * @return Response
     */
    public function deleteCategory(Request $request)
    {
        $rules = ['id' => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response(['success' => false, 'message' => 'Invalid Request']);
        } else {
            $bestInCityCount = 0;
            $data = $request->all();
            $dCategory = Category::where(
                ['id' => $data['id'], 'status' => 0]
            )->first();
            if ($dCategory) {
                $dCategory->delete();
                return response(
                    ['success' => true, 'message' => 'Successfully Deleted']
                );
            } 
                return response(
                    ['success' => false,
                     'message' => 'Invalid Data']
                );       
        }
    }

    /**
     * Function Usage : Save
     *
     * @param Request $request Request parameters
     *
     * @return Response
     */
    public function save(Request $request)
    {
        $rules = ['title' => 'required']; //
        //'slug' => 'unique:name',);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $data = $request->all();

            $count = 0;
            do {
                if ($count == 0) {
                    $slug = str_slug($data['title']);
                } else {
                    $slug = str_slug($data['title'] . $count);
                }
                $slug_check = Category::where('slug', $slug)->get();
                if ($slug_check->count() > 0) {
                    $count++;
                } else {
                    $count = 0;
                }
            } while ($count > 0);

            $wheretogo = new Category();
            $wheretogo->name = $data['title'];
            $wheretogo->description = $data['description'];
            $wheretogo->parent_id = $data['parent_category']
                ? $data['parent_category']
                : 0;
            $wheretogo->slug = $slug;
            $wheretogo->save();

            if ($wheretogo->id) {
                if ($wheretogo->parent_id == 0) {
                    $items = $data['items'];
                    if (count($items)) {
                        foreach ($items as $key => $item) {
                            if ($item) {
                                $count = 0;
                                do {
                                    if ($count == 0) {
                                        $slug = str_slug($item);
                                    } else {
                                        $slug = str_slug($item . $count);
                                    }
                                    $slug_check = Category::where('slug', $slug)
                                        ->get();
                                    if ($slug_check->count() > 0) {
                                        $count++;
                                    } else {
                                        $count = 0;
                                    }
                                } while ($count > 0);

                                $subCategory = new Category();
                                $subCategory->name = $item;
                                $subCategory->parent_id = $wheretogo->id;
                                $subCategory->slug = $slug;
                                $subCategory->save();
                            }
                        }
                    }
                }

                return redirect()->back()
                    ->with(array('message' => 'Successfully Added'));
            } 
         
                return redirect()->back()->with(array('error' => 'Request Failed!'));
         
        } 
     
            return redirect()->back()->withErrors($validator)->withInput();
    
    }

    /**
     * Edit category
     *
     * @param int $page_id an integer
     *
     * @return $this
     */
    public function edit($page_id)
    {
        $parentCategories = Category::where('parent_id', '=', '0')
            ->pluck('name', 'id');
        $category = Category::find($page_id);
        return view('admin.category.edit')
            ->with(['data' => $category, 'parentCategories' => $parentCategories]);
    }

    /**
     * Category update
     *
     * @param Request $request    for request
     * @param int     $categoryId an integer
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $categoryId)
    {
        $rules = ['title' => 'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $data = $request->all();

            $wheretogo = Category::find($categoryId);
            $wheretogo->name = $request->input('title');
            $wheretogo->description = $request->input('description');
            $wheretogo->parent_id = $data['parent_category']
                ? $data['parent_category']
                : 0;
            $wheretogo->save();
            if ($wheretogo->id) {
                $items = isset($data['items']) ? $data['items'] : null;
                $arrayToDelete = [];
                if (count($items) && $wheretogo->parent_id == 0) {
                    $i = 0;
                    foreach ($items as $key => $items_single) {
                        if ($items_single) {
                            $subCatObjs = Category::where(
                                ['parent_id' => $categoryId,
                                    'id' => @$request->input('hdn_id')[$i]]
                            )->get();

                            if (count($subCatObjs)) {
                                $subCatObj = $subCatObjs->first();
                            } else {
                                $subCatObj = new Category();
                                $count = 0;
                                do {
                                    if ($count == 0) {
                                        $slug = str_slug($items_single);
                                    } else {
                                        $slug = str_slug($items_single . $count);
                                    }

                                    $slug_check = Category::where('slug', $slug)
                                        ->get();
                                    if ($slug_check->count() > 0) {
                                        $count++;
                                    } else {
                                        $count = 0;
                                    }
                                } while ($count > 0);
                                $subCatObj->slug = $slug;
                            }

                            $subCatObj->name = $items_single;
                            $subCatObj->parent_id = $wheretogo->id;
                            $subCatObj->save();
                            $arrayToDelete[] = $subCatObj->id;
                            $i++;
                        }
                    }
                }

                Category::where('parent_id', $categoryId)
                    ->whereNotIn('id', $arrayToDelete)->delete();

                if ($wheretogo->parent_id > 0) {
                    Category::where('parent_id', $wheretogo->id)->delete();
                }

                return redirect()->back()
                    ->with(array('message' => 'Successfully Updated'));
            } 
         
                return redirect()->back()->with(array('error' => 'Request Failed!'));
        
        } 

            return redirect()->back()
                ->with(['validation_fail' => 'Please fill the form!']);
       
    }

}
