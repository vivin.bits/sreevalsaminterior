<?php
/**
 * Cms Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

/**
 * CmsController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class CmsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.index');
    }

    /**
     * Get data
     *
     * @param Request $request for request
     *
     * @return mixed
     */
    public function cmsGetdata(Request $request)
    {
        $pages = Page::orderBy('created_at', 'ASC');

        if ($request->input('pageName')) {
            $pages = $pages
                ->where('title', 'like', '%' . $request->input('pageName') . '%');
        }

        $pages = $pages->get();
        return Datatables::of($pages)
            ->editColumn(
                'id', function ($model) {
                    return '<span class="si_no"></span> ';
                }
            )
            ->editColumn(
                'title', function ($model) {
                    $title = $model->title;
                    if ($model->status == 1) {
                        $title = $title . ' ' .
                        '<a href="http://' . env('DOMAIN') . '/' .
                        $model->slug .
                        '" class="preview-btn" target="_blank">Quick View</a> ';
                    }
                    return $title;
                }
            )
            ->editColumn(
                'created_at', function ($model) {
                    return $model->created_at != null
                    ? date('d F Y', strtotime($model->created_at))
                    : 'N/A';
                }
            )->editColumn(
                'status', function ($model) {
                    if ($model->status == 1) {
                        return
                        '<div class="pretty p-switch p-fill">
            <input type="checkbox" class="userStatus" .
             data-type="activate" id="' . $model->id . '" .
             data-id="' . $model->id . '" .
              data-status="' . $model->status . '" checked  .
              onchange="changePageStatus(this,\'inactivate\')" >
            <div class="state p-success">
            <label></label>
            </div>
            </div>';
                    } else {
                        return
                        '<div class="pretty p-switch p-fill">
            <input type="checkbox"  class="userStatus" .
               data-type="inactivate" id="' . $model->id . '" .
                data-id="' . $model->id . '" .
                 data-status="' . $model->status . '" .
                  onchange="changePageStatus(this,\'activate\')">.
                  <div class="state p-success"><label></label>
            </div>
            </div>';

                    }
                }
            )
            ->editColumn(
                'tblaction', function ($model) {

                    return '<a href="' .
                    url(
                        "pages/view/" .
                        $model->id
                    ) . '"><i class="fa fa-edit"></i></a>';

                }
            )
            ->rawColumns(
                [
                    'id', 'title', 'status', 'created_at', 'tblaction'
                ]
            )
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        return view('admin.pages.page-create')->with('user', $user);
    }

    /**
     * Create page
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function doCreate(Request $request)
    {

        $user = Auth::user();

        $rules = array(
            'title' => 'required|min:2',
            'url_slug' => 'required|unique:pages,slug',
            'content' => 'required|min:2',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $pages = new Page();
            $pages->title = $request->input('title');
            $pages->content = $request->input('content');
            $pages->meta_key = $request->input('meta_key');
            $pages->meta_description = $request->input('meta_desc');
            $pages->slug = $request->input('url_slug');
            $pages->status = $request->input('select_status');
            $pages->save();

            //Adding to activity table

            if ($pages->id) {
                $report = new Activity();
                $report->user_id = Auth::user()->id;
                $report->type = 'new page';
                $report->save();

                return redirect()->back()
                    ->with(array('message' => 'Pages Successfully Added'));
            } 
          
                return redirect()->back()
                    ->with(array('error' => 'Request Failed!'));
           
        } 
                   return redirect()->back()
                       ->withErrors($validator)->withInput();
       
    }

    /**
     * View
     *
     * @param int $page_id for view
     *
     * @return $this
     */
    public function view($page_id)
    {
        $user = Auth::user();
        $data = Page::find($page_id);
        return view('admin.pages.page_view')
            ->with(array('data' => $data, 'user' => $user));
    }

    /**
     * Edit page
     *
     * @param int     $page_id for page id
     * @param Request $request for request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($page_id, Request $request)
    {
        $user = Auth::user();
        if ($request->input('delete')) {
            $page = Page::find($page_id);
            $page->delete();

            $report = new Activity();
            $report->user_id = Auth::user()->id;
            $report->activity = 'Page deleted with title "' . $page->title;
            $report->type = 'delete page';
            $report->ip = $request->getClientIp();
            $report->save();

            return redirect('pages')->with('user', $user);
        } else {
            $rules = array(
                'title' => 'required|min:2',
                //'url_slug' => 'required|unique:pages,slug,'.$page_id,
                'content' => 'required|min:2',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                $page = Page::find($page_id);
                $page->title = $request->input('title');
                $page->content = $request->input('content');
                $page->meta_key = $request->input('meta_key');
                $page->meta_description = $request->input('meta_desc');
                //$page->slug = $request->input('url_slug');
                $page->status = $request->input('select_status');
                $page->save();

                if ($page->id) {

                    return redirect()->back()
                        ->with(array('message' => 'Page Successfully Updated'));
                }
            }
                return redirect()->back()
                    ->withErrors($validator)->with('user', $user);
         
        }
    }

    /**
     * Show page
     *
     * @param string $slug for show
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function show($slug)
    {
        $user = Auth::user();
        $data = Page::where('slug', $slug)->get();
        if ($data->count() > 0) {
            return view('page_display')
                ->with(array('data' => $data, 'user' => $user));
        } 
            return redirect('error')->with('user', $user);
    }

    /**
     * Url slug
     *
     * @param Request $request for request
     *
     * @return void
     */
    public function urlSlug(Request $request)
    {
        if ($request->input('title')) {
            echo sanitize_url($request->input('title'));
        } else {
            echo null;
        }
    }


}
