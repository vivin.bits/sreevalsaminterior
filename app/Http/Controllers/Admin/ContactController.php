<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
     public function index()
    {
        $data = Contact::All();
        return view('admin.contact.index',['contacts'=>$data]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.contact.create');
    }
   
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            // 'content' => 'required',
            // 'facebook' => 'required',
            // 'linkedin' => 'required',
            // 'insta' => 'required',
            // 'email' => 'required',
            // 'youtube' => 'required',
            // 'meta_title' => 'required',
            // 'meta_content' => 'required',
            // 'meta_description' => 'required',
            // 'meta_key' => 'required',
            // //'phone' => 'required',
            // 'twiter' => 'required',
            // 'address' => 'required',
            // 'map' => 'required',

            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $contact = new Contact();
            $contact->title = $request->input('title');
            $contact->address = $request->input('content');
            $contact->email = $request->input('email');
            $contact->facebook = $request->input('facebook');
            $contact->youtube = $request->input('youtube');
            $contact->insta = $request->input('insta');
            $contact->linkedin= $request->input('linkedin');
            $contact->map = $request->input('map');
            $contact->meta_title = $request->input('meta_title');
            $contact->meta_description = $request->input('meta_description');
            $contact->meta_content = $request->input('meta_content');
            $contact->meta_key = $request->input('meta_key');
            $contact->author = $request->input('author');
            $contact->phone = $request->input('phone');
            $contact->twitter = $request->input('twiter');
            $contact->save();
            if ($contact->id) {
                return redirect()->back()
                    ->with('message', 'Contact name  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
     
        $data = Contact::find($id);
        return view('admin.contact.edit')->with(['contact' => $data]);
        
    }
    public function edit($id,Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            // 'content' => 'required',
            // 'facebook' => 'required',
            // 'linkedin' => 'required',
            // 'insta' => 'required',
            // 'email' => 'required',
            // 'youtube' => 'required',
            // 'meta_title' => 'required',
            // 'meta_content' => 'required',
            // 'meta_description' => 'required',
            // 'meta_key' => 'required',
            // //'phone' => 'required',
            // 'twiter' => 'required',
            // 'address' => 'required',
            // 'map' => 'required',

            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }

            $contact = Contact::find($id);
            $contact->title = $request->input('title');
            $contact->address = $request->input('content');
            $contact->email = $request->input('email');
            $contact->facebook = $request->input('facebook');
            $contact->youtube = $request->input('youtube');
            $contact->insta = $request->input('insta');
            $contact->linkedin= $request->input('linkedin');
            $contact->map = $request->input('map');
            $contact->meta_title = $request->input('meta_title');
            $contact->meta_description = $request->input('meta_description');
            $contact->meta_content = $request->input('meta_content');
            $contact->meta_key = $request->input('meta_key');
            $contact->author = $request->input('author');
            $contact->phone = $request->input('phone');
            $contact->twitter = $request->input('twiter');
            $contact->save();
            if ($contact->id) {
                return redirect()->back()
                    ->with('message', 'Contact name  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
