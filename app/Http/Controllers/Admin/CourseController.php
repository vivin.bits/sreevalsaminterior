<?php

namespace App\Http\Controllers\Admin;
use App\Department;
use App\Course;
use App\Subject;
use App\Scheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $course = Course::with('department')->get();
        return view('admin.course.index')->with('course',$course);
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        $department = Department::where('id', '!=', 1)->get();
        $subject = Subject::where('id', '!=', 1)->get();
        $scheme = Scheme::where('id', '!=', 1)->get();
        
        return view('admin.course.create')->with(['department'=>$department, 'subject'=>$subject,'scheme'=>$scheme]);
    }

    public function create(Request $request)
    {
        $v = Validator::make(
            $request->all(), [
                'title' => 'required|max:99',
                'content' => 'required',
                'dept_name'=>'required',
                'description'=>'required',
                'image'=>'required',
                'slug'=>'required|unique:course'
              
            ]
        );
        if ($v->passes()) {
            $course = new course();
            $course->course = $request->input('title');
            $course->slug = $request->input('slug');
            $course->departments_id = $request->input('dept_name');
        
        
            $course->content = $request->input('content');
            
            $course->description = $request->input('description');
            if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $course->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $course->save();
            if ($course->id) {
                return redirect()->back()
                    ->with('message', 'course added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        }
        return redirect()->back()->with('message', '')->withErrors($v)
            ->withInput($request->except('password', 'cpassword'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\course  $course
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
        
        $data = course::find($id);
        $department = Department::where('id', '!=', 1)->get();
        $subject = Subject::where('id', '!=', 1)->get();
        $scheme = Scheme::where('id', '!=', 1)->get();
        return view('admin.course.edit')->with(['course' => $data,'department'=>$department, 'subject'=>$subject,'scheme'=>$scheme]);
        
        
    }
    public function edit($id,Request $request)
    {
       
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'dept_name'=>'required',
            'description'=>'required',
            'image'=>'required',
          
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $course = course::find($id);
            $course->course = $request->input('title');
            
            $course->content = $request->input('content');
            $course->description = $request->input('description');
            if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $course->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            
            $course->save();
            if ($course->id) {
                return redirect()->back()
                    ->with('message', '  updated successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        }
              
            

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        
        $dd=course::find($id);
        $dd->delete();
        return back();
    }
}
