<?php
/**
 * Dashboard Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Album;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Packages;
use Illuminate\Http\Request;
use App\User;
use App\Project;
use App\Career;
use App\Enquiry;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * DashboardController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class DashboardController extends Controller
{ 
    /**
     * For load page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $admission=Album::get()->count();
        $career=Packages::get()->count();
        $users=Project::get()->count();
        $news=Enquiry::get()->count();
    
        return view('admin.dashboard.dashboard')->with(['admission'=>$admission, 'career'=>$career,'users'=>$users,'news'=>$news]);
    }
    
    /**
     * For load page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword()
    {
        return view('admin.change_password');
    }

    /**
     * For update password
     *
     * @param Request $request for request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateChangePassword(Request $request)
    {
        $rules = array(
            'current_password' => 'required',
            'password' => 'required|between:6,12|confirmed',
            'password_confirmation' => 'required|between:6,12'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(
                array(
                    'fail' => true,
                    'errors' => $validator->getMessageBag()->toArray()
                )
            );
        } else {

            $user = User::find(Auth::user()->id);
            if (Auth::attempt(
                [
                    'email' => $user->email,
                    'password' => $request->input('current_password')
                ]
            )
            ) {

                $user->password = \Hash::make($request->input('password'));
                $user->save();

                if ($user->id) {
                    return response()->json(
                        array(
                        'success' => true
                        )
                    );
                }
            } 
                return response()->json(
                    array(
                    'fail' => true,
                    'errors' => array(
                        'current_password' => 'Incorrect Current password'
                    )
                    )
                );
        
        }
    }

}
