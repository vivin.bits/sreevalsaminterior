<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Department::All();
        return view('admin.department.index',['departments'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.department.create');
    }
    /** */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
  

    /**
     * Create user
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $department = new Department();
            $department->name = $request->input('name');
            $department->save();
            if ($department->id) {
                return redirect()->back()
                    ->with('message', 'Department name  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
    }
       
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
        
        $data = Department::find($id);
        return view('admin.department.edit')->with(['department' => $data]);
        
        
    }
    public function edit($id,Request $request)
    {
      
        $v = Validator::make(
            $request->all(), [
                'name' => 'required',
            
            ]
        );
        if ($v->passes()) {
            $department = Department::find($id);
            $department->name = $request->input('name');
            $department->save();
            if ($department->id) {
                return redirect()->back()
                    ->with('message', 'Department name  changed successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        
        $dd=Department::find($id);
        $dd->delete();
        return back();
    }
}
