<?php
/**
 * EmailTemplates Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\EmailTemplates;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
/**
 * EmailTemplatesController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class EmailTemplatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = EmailTemplates::all();
        return view('admin.email_template.email_templates')
            ->with('records', $templates);
    }

    /**
     * Display email template
     *
     * @param int $id for email template
     *
     * @return $this
     */
    public function emailTemplate($id)
    {
        $template = EmailTemplates::find($id);
        return view('admin.email_template.email_template')
            ->with('template', $template);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.email_template.email_templates_create');
    }

    /**
     * To create email template
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $rules = array(
            'title' => 'required|min:2',
            'subject' => 'required|min:2',
            'content' => 'required|min:2',
            'from_name' => 'required',
            'from_email' => 'required|email',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $emailTemplates = new EmailTemplates();
            $emailTemplates->title = $request->input('title');
            $emailTemplates->from_name = $request->input('from_name');
            $emailTemplates->from_email = $request->input('from_email');
            $emailTemplates->to_name = $request->input('to_name');
            $emailTemplates->to_email = $request->input('to_email');
            $emailTemplates->subject = $request->input('subject');
            $emailTemplates->cc_name = $request->input('cc_name');
            $emailTemplates->cc_email = $request->input('cc_email');
            $emailTemplates->email_body = $request->input('content');
            $emailTemplates->status = $request->input('select_status');
            $emailTemplates->created_by = Auth::user()->id;
            $emailTemplates->save();
            return redirect()->back()
                ->with('message', 'Template added successfully.');
        }
            return redirect()->back()->withErrors($validator)->withInput();
       
    }

    /**
     * Display edit form
     *
     * @param int $id for edit form
     *
     * @return $this
     */
    public function editForm($id)
    {
        $templateDetails = EmailTemplates::find($id);
        return view('admin.email_template.email_templates_edit')
            ->with('template_details', $templateDetails);
    }

    /**
     * Display the specified resource.
     *
     * @param int     $id      for  edit
     * @param Request $request for request
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $v = Validator::make(
            $request->all(), [
            'title' => 'required',
            'subject' => 'required',
            'content' => 'required|min:2',
            'from_name' => 'required',
            'from_email' => 'required|email',

            ]
        );
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        } else {

            $emailTemplates = EmailTemplates::find($id);
            $emailTemplates->title = $request->input('title');
            $emailTemplates->from_name = $request->input('from_name');
            $emailTemplates->from_email = $request->input('from_email');
            $emailTemplates->to_name = $request->input('to_name');
            $emailTemplates->to_email = $request->input('to_email');
            $emailTemplates->cc_name = $request->input('cc_name');
            $emailTemplates->cc_email = $request->input('cc_email');
            $emailTemplates->subject = $request->input('subject');
            $emailTemplates->email_body = $request->input('content');
            $emailTemplates->status = $request->input('select_status');
            $emailTemplates->created_by = Auth::user()->id;
            $emailTemplates->save();
            return redirect()->back()
                ->with('message', 'Template updated successfully.');
        }
    }

    /**
     * Delete functionality
     *
     * @param Request $request for request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {

        if (count($request->input('ids')) >= 1) {
            foreach ($request->input('ids') as $id) {
                EmailTemplates::where(array('id' => $id))->delete();
            }
            return redirect()->back()->with('message', 'Deleted successfully.');
        } 
            return redirect()->back()->with('message', 'No item selected.');
       
    }

}
