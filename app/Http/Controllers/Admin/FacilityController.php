<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Facility;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Facility::All();
        return view('admin.facility.index',['facility'=>$data]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.facility.create');
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'content'=>'required',
            'image'=>'required'
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $facility = new Facility();
            $facility->title = $request->input('title');
            $facility->description = $request->input('content');
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $facility->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $facility->save();
            if ($facility->id) {
                return redirect()->back()
                    ->with('message', ' added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
     
        $data = Facility::find($id);
        return view('admin.facility.edit')->with(['facility' => $data]);
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            // 'category' => 'required',
             
             //'image'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $test = Facility::find($id);
            $test->title = $request->input('title');
            $test->description = $request->input('content');
        
         
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $test->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $test->save();
            if ($test->id) {
                return redirect()->back()
                    ->with('message', 'updated successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $id=$request->get('id');
    Facility::where('id', $id)->delete();
    return redirect()->back();
    

    }
    public function getFacility()
    {
        // dd('hh');
    $slider=Facility::select('id','title','description','image')->get();
    return DataTables::of($slider)
        ->editColumn('sl#', function ($model) {
            return '<span class="si_no"></span>';
        })
        ->editColumn('name', function ($model) {
            return $model->title;
        })
        ->editColumn('image', function ($model) {
            //return 'file_path <i class="fa fa-eye show_images"></i>';
            if ($model->image) {
                $imag = getImageByPath($model->image, '150x150', 'profile-images');
            } else {
                $imag = "admin/images/pre-img.png";
            }
            return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
        })
       
        ->editColumn('action', function ($model) {

            return '<a href="'.url('facility/edit/'.$model->id).'" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="facility" data-id="'.$model->id.'"></i></a>';

        })
//
        ->rawColumns(['sl#', 'name', 'image', 'action'])
        ->make(true);


    }
}
