<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Gallery::get();
        return view('admin.gallery.index',['galleries'=>$data]);
    }

    public function createForm()
    {
        $album = Album::where('id', '!=', 1)->get();
        return view('admin.gallery.create')->with(['album'=>$album]);
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
    
            'image'=>'required'
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $gallery = new Gallery();
            $gallery->title = $request->input('title');
        
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $gallery->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $gallery->save();
            if ($gallery->id) {
                return redirect()->back()
                    ->with('message', 'Photo  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
        
        $data = Gallery::find($id);
        return view('admin.gallery.edit')->with(['gallery' => $data]);
        
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            'title' => 'required',
            'image'=>'required'
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $gallery = Gallery::find($id);
            $gallery->title = $request->input('title');
            
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $gallery->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $gallery->save();
            if ($gallery->id) {
                return redirect()->back()
                    ->with('message', 'Photo  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gallery $gallery)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        
        $dd=Gallery::find($id);
        $dd->delete();
        return back();
    }
}
