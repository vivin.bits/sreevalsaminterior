<?php
/**
 * Module Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */
namespace App\Http\Controllers\Admin;

use App\Activity;
use App\AdminModule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Permission;
/**
 * ModuleController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class ModuleController extends Controller
{

    /**
     * Display index
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $modules = AdminModule::get();
        return view('admin.modules.index')->with(['modules' => $modules]);
    }

    /**
     * Display create form
     *
     * @return $this
     */
    public function createForm()
    {
        $modules = AdminModule::get();
        return view('admin.modules.create')->with(['modules' => $modules]);
    }

    /**
     * To create module
     *
     * @param Request $request for create module
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $r = array('name' => 'required', 'perifix' => 'required');

        $v = Validator::make($request->all(), $r);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                ->withInput($request->all());
        }
        $adminModule = new AdminModule();
        $adminModule->type = 'parent';
        $adminModule->name = $request->get('name');
        $adminModule->perifix = $request->get('perifix');
        $adminModule->save();

        $permission_types = array('browse_', 'read_', 'edit_', 'add_', 'delete_');
        foreach ($permission_types as $type) {
            $permission = new Permission();
            $permission->key = $type . $adminModule->perifix;
            $permission->table_name = $adminModule->perifix;
            $permission->module_id = $adminModule->id;
            $permission->save();
        }

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $adminModule->id;
        $activity->type = 'module';
        $activity->activity = 'New module has been created by:';
        $activity->save();

        return redirect()->back()->with('smessage', 'New module added.');
    }

    /**
     * Display update form
     *
     * @param int $id for update form
     *
     * @return $this
     */
    public function updateForm($id)
    {
        $module = AdminModule::find($id);
        return view('admin.modules.edit')->with(['module' => $module]);
    }

    /**
     * Update module
     *
     * @param int     $id      for update form
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $r = array('name' => 'required');

        $v = Validator::make($request->all(), $r);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $adminModule = new AdminModule();
        $adminModule->name = $request->get('name');
        $adminModule->save();

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $adminModule->id;
        $activity->type = 'module';
        $activity->activity = 'New module has been edited by:';
        $activity->save();

        return redirect()->back()->with('smessage', 'Module updated.');
    }
}