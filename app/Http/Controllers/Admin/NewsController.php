<?php

namespace App\Http\Controllers\Admin;

use App\News;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNews()
    {
        $slider=News::select('id','title','content')->get();
        return DataTables::of($slider)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })

            ->editColumn('title', function ($model) {
                return $model->title;
            })
            ->editColumn('description', function ($model) {
                return $model->content;
            })

            ->editColumn('action', function ($model) {

                return '<a href="'.url('news/edit/'.$model->id).'" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="news" data-id="'.$model->id.'"></i></a>';

            })
//
            ->rawColumns(['sl#', 'title', 'description', 'action'])
            ->make(true);
//
    }
    public function index()
    {
        $data = News::All();
        return view('admin.news.index',['newses'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.news.create');
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'content'=>'required',

            'slug'=>'required|unique:news'
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $news = new News();
            $news->title = $request->input('title');
            $news->slug= $request->input('slug');
            $news->content = $request->input('content');
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $news->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $news->save();
            if ($news->id) {
                return redirect()->back()
                    ->with('message', 'news  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
     
        $data = News::find($id);
        return view('admin.news.edit')->with(['news' => $data]);
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            // 'title' => 'required',
            //  'content'=>'required',
            //  'image'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $news = News::find($id);
            $news->title = $request->input('title');
        
            $news->content = $request->input('content');
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $news->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $news->save();
            if ($news->id) {
                return redirect()->back()
                    ->with('message', 'News  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        $dd=News::find($request->get('id'));
        $dd->delete();

    }
}
