<?php

namespace App\Http\Controllers\Admin;

use App\Packages;
use App\PackagesMedia;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPackages()
    {

        $mission = Packages::get(); 
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('title', function ($model) {
                return strip_tags($model->title);

            })
            ->editColumn('image', function ($model) {
                //return 'file_path <i class="fa fa-eye show_images"></i>';
                if ($model->main_image) {
                    $imag = getImageByPath($model->main_image, '10000x100000', 'packages');
                } else {
                    $imag = "admin/images/pre-img.png";
                }
                return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
            })  
            ->editColumn('gallery_image', function ($model) {
                return '<a href="' . url('package/media/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
</a>';

            })
            ->editColumn('status', function ($model) {
                
                $checked = '';
                if ($model->status == 1){
                    $checked = 'checked';
                }
                return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-content="package" data-id="'.$model['id'].'" '.$checked.'><div class="state p-success" ><label ></label ></div></div>';

        })

            ->editColumn('action', function ($model) {
                return '<a href="' . url('package/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="package" data-id="' . $model->id . '"></i></a>';

            })

            ->rawColumns(['sl#', 'title', 'image','gallery_image','status', 'action'])
            ->make(true);
    }
    public function index()
    {
        $packages=Packages::get();
        return view('admin.package.index')->with(['packages'=>$packages]);
    }


    public function createForm()
    {
        return view('admin.package.create');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $v = Validator::make($request->all(), [
           
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        else
        {
            $file=$request->image;
            $package=new Packages();
            $package->title=$request->input('title');
            $package->slug=$request->input('slug');
            $package->description=$request->input('description');
            
            if($request->hasFile('image'))
            {
                if($request->hasFile('image') !=null)
                {

                    $package->main_image = ImageUploadWithPath($file,'packages');

                }
            }
$package->save();
return redirect()->back()
                    ->with('message', 'News  added successfully.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function media(Request $request,$id)
    {
      
        $packages=Packages::find($id);
    
    
        $media=PackagesMedia::where('packages_id',$id)->get();
     

        
        return view('admin.package.media')->with(['media'=>$media,'packages'=>$packages]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function mediaStore(Request $request)
    {

        $v = Validator::make($request->all(), [
         
            'file'=>'required|max:4096'
         ]);
          
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }

        $gallery = new PackagesMedia();
        $gallery->packages_id = $request->input('albumid');
        if ($request->hasFile('file')) {
                
            
            if ($request->file('file') != null) {
                $gallery->filename = ImageUploadWithPath($request->file,'packages-media');
            }
        }
        $gallery->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function deleteMedia(Request $request)
    {
        $gallery=PackagesMedia::find($request->get('id'));
        if($gallery)
        {
        flushImage($gallery,'package_media',1,'filename');
    
        
        $gallery->delete();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function statusChange(Request $request)
    {
        
        $change=Packages::find($request->get('id'));
        if($change)
        {
        if($change->status == 1)
        {
            $change->status= 0;
            $change->save();
            return response()->json([
                'status'=>true,
                'message'=>"successfully updated",

            ],200);
        }
        else{
            $change->status=1;
            $change->save();
            return response()->json([
                'status'=>true,
                'message'=>"successfully updated",
            ],200);
        }
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    
    {
        $package=Packages::find($id);
        return view('admin.package.edit')->with(['album'=>$package]);
    }

    public function update(Request $request,$id)
    {
      
        $v = Validator::make($request->all(), [
            'title' => 'required',
             
             //'image'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $album = Packages::find($id);
            $album->title = $request->input('title');
            $album->description = $request->input('description');
         
             if ($request->hasFile('image')) {
                
                flushImage($album,'packages',1,'main_image');
                if ($request->file('image') != null) {
                    $album->main_image = ImageUploadWithPath($request->image,'packages');
                }
            }
            $album->save();
            if ($album->id) {
                return redirect()->back()
                    ->with('message', 'image added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
    }

    public function delete(Request $request)
    {
       
        $album=Packages::find($request->get('id'));
       
        if($album)
        {
            $media=PackagesMedia::where('packages_id',$id)->delete();
            flushImage($album,'packages',1,'main_image');
            $album->delete();
        }
    }
}
