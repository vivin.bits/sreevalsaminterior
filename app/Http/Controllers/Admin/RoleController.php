<?php
/**
 * Role Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\AdminModule;
use App\Http\Controllers\Controller;
use App\PermissionRole;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * RoleController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class RoleController extends Controller
{
    
    /**
     * Display index
     *
     * @return all roles
     */
    public function index()
    {

        $roles = Role::all();
        return view('admin.roles.index')->with('roles', $roles);
    }

    /**
     * Permission index
     *
     * @return $this
     */
    public function permissionIndex()
    {
        $roles = Role::all();
        return view('admin.roles.permission-index')->with('roles', $roles);
    }

    /**
     * To create form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm()
    {
        return view('admin.roles.create');
    }

    /**
     * Display create
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        //
        $v = Validator::make(
            $request->all(), [
            'display_name' => 'required|min:3|max:50',
            'name' => 'required|min:3|max:50',
            ]
        );

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                ->withInput($request->all());
        }
        $roles = new Role();
        $roles->display_name = $request->input('display_name');
        $roles->name = $request->input('name');
        $roles->save();

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $roles->id;
        $activity->type = 'role';
        $activity->activity = 'New role has be created by:';
        $activity->save();

        return redirect()->back()->with('smessage', 'New role added.');
    }

    /**
     * Update permission form
     *
     * @param int $id for update permission
     *
     * @return $this
     */
    public function updatePermissionForm($id)
    {
        $role = Role::find($id);
        $modules = AdminModule::get();
        return view('admin.roles.permission-edit')
            ->with(['role' => $role, 'modules' => $modules]);
    }

    /**
     * Update form
     *
     * @param int $id for update form
     *
     * @return $this
     */
    public function updateForm($id)
    {
        $role = Role::find($id);
        $modules = AdminModule::get();
        return view('admin.roles.edit')
            ->with(['role' => $role, 'modules' => $modules]);
    }

    /**
     * Update role
     *
     * @param int     $id      for update role
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateRole($id, Request $request)
    {
        $v = Validator::make(
            $request->all(), [
            'display_name' => 'required|min:3|max:50',
            ]
        );

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }

        $roles = Role::find($id);
        $roles->display_name = $request->input('display_name');
        $roles->save();

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $roles->id;
        $activity->type = 'role';
        $activity->activity = 'New role has been edited by:';
        $activity->save();

        return redirect()->back()->with('smessage', 'Role Updated.');
    }

    /**
     * Update permission role
     *
     * @param int     $id      for update
     * @param Request $request for request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        PermissionRole::where(['role_id' => $id])->delete();

        if ($request->get('permissions')) {
            foreach ($request->get('permissions') as $per) {

                $permissionRole = new PermissionRole();
                $permissionRole->permission_id = $per;
                $permissionRole->role_id = $id;
                $permissionRole->save();
            }
        }

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $id;
        $activity->type = 'role';
        $activity->activity = 'New role has been edited by:';
        $activity->save();

        return redirect()->back()->with('smessage', 'Permission Updated.');
    }
}