<?php

namespace App\Http\Controllers\Admin;

use App\Scheme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\SchoolStudent;
use Yajra\DataTables\DataTables;
class SchoolStudentController extends Controller
{
    public function getSchool()
    {
        $slider=SchoolStudent::select('id','name','status','image')->get();
        return DataTables::of($slider)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('name', function ($model) {
                return $model->name;
            })
            ->editColumn('image', function ($model) {
                //return 'file_path <i class="fa fa-eye show_images"></i>';
                if ($model->image) {
                    $imag = getImageByPath($model->image, '20x20', 'school-student');
                } else {
                    $imag = "admin/images/pre-img.png";
                }
                return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
            })
            ->editColumn('actve', function ($model) {
                
                    $checked = '';
                    if ($model->status == 1){
                        $checked = 'checked';
                    }
                    return '<div class="pretty p-switch p-fill" ><input type="checkbox" class="change-status" data-content="school/students" data-id="'.$model['id'].'" '.$checked.'><div class="state p-success" ><label ></label ></div></div>';

            })
            ->editColumn('action', function ($model) {

                return '<a href="'.url('school/students/edit/'.$model->id).'" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="school/students" data-id="'.$model->id.'"></i></a>';

            })
//
            ->rawColumns(['sl#', 'name', 'image','actve', 'action'])
            ->make(true);
    }
   public function index()
   {
       
return view('admin.school.index');
   }

   public function createForm()
   {
       return view('admin.school.create');
   }
   public function create(Request $request)
   {
    //    dd($request->all());
    $v = Validator::make($request->all(), [
        'name' => 'required',
        'image'=>'required|max:4096'
       


    ]);
    if ($v->fails()) {
        return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
    }
    $school = new SchoolStudent();
    
    $school->name = $request->input('name');

    
    if ($request->hasFile('image')) {
        // flushImage($school,'school-student',1,'image');
        if ($request->file('image') != null) {
            $school->image = ImageUploadWithPath($request->image, 'school-student');
        }
    }
    $school->save();
    return redirect()->back()->with('message', 'Succssfully Saved');


}

   


public function edit($id)
{
   
   $school= SchoolStudent::find($id);
    return view('admin.school.edit')->with(['album'=>$school]);
}
public function update($id,Request $request)
{
  
    $v = Validator::make($request->all(), [
        'name' => 'required',
        
        


    ]);
    if ($v->fails()) {
        return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
    }
    $school =  SchoolStudent::find($id);
    
    $school->name = $request->input('name');
 
    
    if ($request->hasFile('image')) {
        flushImage($school,'school-student',1,'image');
        if ($request->file('image') != null) {
            $school->image = ImageUploadWithPath($request->image, 'school-student');
        }
    }
    $school->save();
    return redirect()->back()->with('message', 'Failed to save.');
}


public function destroy(Request $request)
{ 
    $school=SchoolStudent::find($request->get('id'));
    if($school)
    {
    flushImage($school,'school-student',1,'image');
    $school->delete();
    }
}

public function changeStatus(Request $request)
{
    $school=SchoolStudent::find($request->get('id'));
    if($school->status==1)
    {
        $school->status=0;
        $school->save();
    }
   else
   {
        $school->status=1;
        $school->save();
   }
        return redirect()->back();
    
}
}