<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Settings;
use App\SmtpSettings;
use App\GoogleApiSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    private static $cache = array();

    public function index()
    {
        $settings = Settings::all();
        foreach ($settings as $setting) {
            self::$cache[$setting->name] = $setting->value;
        }
        return view('admin.settings.index')->with(array('settings' => self::$cache));
    }

    public function socialIndex()
    {
        $settings = Settings::all();
        foreach ($settings as $setting) {
            self::$cache[$setting->name] = $setting->value;
        }
        return view('admin.social_media.index')->with(array('settings' => self::$cache));
    }

    public function smtpIndex()
    {

        $settings = SmtpSettings::all();
        foreach ($settings as $setting) {
            self::$cache[$setting->name] = $setting->value;
        }
        return view('admin.settings.smtp_index')->with(array('settings' => self::$cache));
    }
      public function googleApiIndex()
    {

        $settings = GoogleApiSettings::all();
        foreach ($settings as $setting) {
            self::$cache[$setting->name] = $setting->value;
        }
        return view('admin.settings.google_api_index')->with(array('settings' => self::$cache));
    }

    public function update(Request $request)
    {
        $rules = array(
            'admin_email' => 'required',
            'google_analytics' => 'required',
            'meta_title' => 'required',
            'meta_key' => 'required',
            'meta_description' => 'required',
        );
        $v = Validator::make($request->all(), $rules);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        } else {
            $data = array(
                array('name' => 'admin_email', 'value' => $request->input('admin_email')),
                array('name' => 'google_analytics', 'value' => $request->input('google_analytics')),
                array('name' => 'renew_days', 'value' => $request->input('renew_days')),
                array('name' => 'upgrade_days', 'value' => $request->input('upgrade_days')),
                array('name' => 'min_days', 'value' => $request->input('min_days')),
                array('name' => 'custom_test_min_questions', 'value' => $request->input('custom_test_min_questions')),
                array('name' => 'meta_title', 'value' => $request->input('meta_title')),
                array('name' => 'meta_key', 'value' => $request->input('meta_key')),
                array('name' => 'meta_description', 'value' => $request->input('meta_description')),
            );

            foreach ($data as $key => $val) {
                \DB::update('update settings set value= "' . addslashes($val["value"]) . '" where name = "' . $val["name"] . '"');
            }
            return redirect()->back()->with(array('message' => 'Settings Updated.'));
        }
    }

    public function socialUpdate(Request $request)
    {
        $rules = array();
        $v = Validator::make($request->all(), $rules);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v)->withInput();
        } else {
            $data = array(
                array('name' => 'facebook_link', 'value' => $request->input('facebook_link')),
                array('name' => 'twitter_link', 'value' => $request->input('twitter_link')),
                array('name' => 'google_link', 'value' => $request->input('google_link')),
                array('name' => 'linkedin_link', 'value' => $request->input('linkedin_link')),
            );

            foreach ($data as $key => $val) {
                \DB::update('update settings set value= "' . addslashes($val["value"]) . '" where name = "' . $val["name"] . '"');
            }
            return redirect()->back()->with(array('message' => 'Social Media Links Updated.'));
        }
    }

    public function smtpUpdate(Request $request)
    {

        $rules = array(
            'host' => 'required',
            'port' => 'required',
            'username' => 'required',
            'from_address' => 'required',
            'from_name' => 'required',
            'password' => 'required',
            'encryption' => 'required',
        );
        $v = Validator::make($request->all(), $rules);

        if ($v->fails()) {

            return redirect()->back()->withErrors($v)->withInput();
        } else {
            $data = array(
                array('name' => 'host', 'value' => $request->input('host')),
                array('name' => 'port', 'value' => $request->input('port')),
                array('name' => 'username', 'value' => $request->input('username')),
                array('name' => 'from_address', 'value' => $request->input('from_address')),
                array('name' => 'from_name', 'value' => $request->input('from_name')),
                array('name' => 'password', 'value' => $request->input('password')),
                array('name' => 'encryption', 'value' => $request->input('encryption')),
            );

            foreach ($data as $key => $val) {
                \DB::update('update smtp_settings set value= "' . addslashes($val["value"]) . '" where name = "' . $val["name"] . '"');
            }

            return redirect()->back()->with(array('message' => 'SMTP Settings Updated.'));
        }
    }

    public function googleApiUpdate(Request $request)
    {

        $rules = array(
            'site_key' => 'required',
            'secret_key' => 'required',
          
        );
        $v = Validator::make($request->all(), $rules);

        if ($v->fails()) {

            return redirect()->back()->withErrors($v)->withInput();
        } else {
            $data = array(
                array('name' => 'site_key', 'value' => $request->input('site_key')),
                array('name' => 'secret_key', 'value' => $request->input('secret_key')),
              
            );

            foreach ($data as $key => $val) {
                \DB::update('update googleapi_settings set value= "' . addslashes($val["value"]) . '" where name = "' . $val["name"] . '"');
            }

            return redirect()->back()->with(array('message' => 'Google API Settings Updated.'));
        }
    }
}
