<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  function  indexList()
    {
        $slider=Slider::select('id','title','image')->get();
        return DataTables::of($slider)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            
            ->editColumn('image', function ($model) {
                //return 'file_path <i class="fa fa-eye show_images"></i>';
                if ($model->image) {
                    $imag = getImageByPath($model->image, '150x150', 'banner');
                } else {
                    $imag = "admin/images/pre-img.png";
                }
                return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
            })
//
//            ->editColumn('image', function ($model) {
//                return $model->image;
//            })
            ->editColumn('action', function ($model) {

                return '<a href="'.url('slide/edit/'.$model->id).'" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="slider" data-id="'.$model->id.'"></i></a>';

            })
//
            ->rawColumns(['sl#', 'title', 'image', 'action'])
            ->make(true);
//
//        return datatables()->of($slider)->addIndexColumn()->toJson();
        }
    public function index()
    {
        $data = Slider::get();
        return view('admin.slider.index',['slides'=>$data]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createForm()
    {
        return view('admin.slider.create');
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'image'=>'required|max:4096'
         
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $news = new Slider();
            $news->title = $request->input('title');
      
           
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $news->image = ImageUploadWithPath($request->image,'banner');
                }
            }
            $news->save();
            if ($news->id) {
                return redirect()->back()
                    ->with('message', 'news  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
  
        $data = Slider::find($id);
        return view('admin.slider.edit')->with(['slider' => $data]);
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            // 'title' => 'required',
            'image'=>'required|max:4096'
         
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $slider = Slider::find($id);
            $slider->title = $request->input('title');
              
   
             if ($request->hasFile('image')) {

                 flushImage($slider,'banner',1,'image');
            
                if ($request->file('image') != null) {
                    $slider->image = ImageUploadWithPath($request->image,'banner');
                }
            }
            $slider->save();
            if ($slider->id) {
                return redirect()->back()
                    ->with('message', 'updated successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request)
    {
        

        $slider= Slider::find($request->get('id'));
        if($slider)
        {
            flushImage($slider,'banner',1,'image');
            $slider->delete();
        }

    }
}
