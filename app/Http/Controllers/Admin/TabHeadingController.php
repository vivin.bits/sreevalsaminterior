<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\MultipleGallery;
use App\TabHeading;
use Yajra\DataTables\DataTables;
class TabHeadingController extends Controller
{
   public function createForm()
   {
       $tab=TabHeading::pluck('name','id');
       return view('admin.tabHeading.create')->with(['tab'=>$tab]);
   }

   public function index()
   {
       return view('admin.tabHeading.index');
   }
   public function create(Request $request)
   {
     
       $tab= new MultipleGallery();
       $tab->tab_id=$request->tab;
       $tab->content=$request->content;
       if($request->hasFile('image'))
       {
        $tab->image = ImageUploadWithPath($request->image,'tab-images');
       }
       $tab->save();
       return redirect()->back()
                    ->with('message', 'Photo  added successfully.');
   }
public function getTab()
{

    $slider=MultipleGallery::
    leftjoin('tab_heading','tab_heading.id','multiple_gallery.tab_id')
    ->select('multiple_gallery.id as id','multiple_gallery.content as content','multiple_gallery.image as image',
    'tab_heading.name as tab')->get();
    return DataTables::of($slider)
        ->editColumn('sl#', function ($model) {
            return '<span class="si_no"></span>';
        })
        ->editColumn('title', function ($model) {
            return $model->tab;
        })
        ->editColumn('image', function ($model) {
            //return 'file_path <i class="fa fa-eye show_images"></i>';
            if ($model->image) {
                $imag = getImageByPath($model->image, '150x150', 'tab-images');
            } else {
                $imag = "admin/images/pre-img.png";
            }
            return "<img src='" . $imag . "'class='img-fluid img-thumbnail' alt='San Fran' >";
        })
//
//            ->editColumn('image', function ($model) {
//                return $model->image;
//            })
        ->editColumn('action', function ($model) {

            return '<a href="'.url('slide/edit/'.$model->id).'" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="tab-heading" data-id="'.$model->id.'"></i></a>';

        })
        
//
        ->rawColumns(['sl#', 'tab', 'image', 'action'])
        ->make(true);
}

public function delete(Request $request)
{
    $id=$request->get('id');
    MultipleGallery::find($id)->delete();
}

}
