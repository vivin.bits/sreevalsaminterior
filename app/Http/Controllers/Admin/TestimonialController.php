<?php

namespace App\Http\Controllers\Admin;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Testimonial::All();
        return view('admin.testimonial.index',['test'=>$data]);
    }
    public function createForm()
    {
        return view('admin.testimonial.create');
    }
    public function create(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'content'=>'required',
            'image'=>'required'
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $news = new Testimonial();
            $news->title = $request->input('title');
            $news->content = $request->input('content');
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $news->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $news->save();
            if ($news->id) {
                return redirect()->back()
                    ->with('message', 'news  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }
  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function editForm($id)
    {
     
        $data = Testimonial::find($id);
        return view('admin.testimonial.edit')->with(['test' => $data]);
        
    }
    public function edit($id,Request $request)
    {

      $v = Validator::make($request->all(), [
            // 'category' => 'required',
             
             //'image'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
         
        
            $test = Testimonial::find($id);
            $test->title = $request->input('title');
            $test->content = $request->input('content');
        
         
             if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $test->image = ImageUploadWithPath($request->image,'profile-images');
                }
            }
            $test->save();
            if ($test->id) {
                return redirect()->back()
                    ->with('message', 'image added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
    Testimonial::where('id', $id)->delete();
    return redirect()->back();
    

    }
    
}
