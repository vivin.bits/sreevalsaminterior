<?php
/**
 * User Controller
 *
 * Handle authentication to Admin module
 *
 * PHP version 5
 *
 * @category   PHP
 * @package    LaravelLib
 * @subpackage Admin
 * @author     Acodez <developer@acodez.in>
 * @license    http://acodez.com/licence.txt Licence
 * @version    GIT: 0.1
 * @link       http://acodez.com/
 * @since      1.0.0
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Activity;
use Carbon;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;
use App\Mail\UserRegistrationMail;

/**
 * UserController
 *
 * @category Class
 * @package  Admin
 * @author   Acodez <developer@acodez.in>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.hashbangcode.com/
 */
class UserController extends Controller
{
    /**
     * Display index
     *
     * @return $this
     */
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->get();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Get users data
     *
     * @param Request $request for request
     *
     * @return mixed
     */
    public function getUsersData(Request $request)
    {
        $value = $request->get('name');
        $list = User::where('users.id', '<>', 0)->select('users.*');
        $data = $list->get();
        return Datatables::of($data)
            ->editColumn(
                's#', function ($model) {
                    return '<span class="si_no"></span> ';
                }
            )
            ->editColumn(
                'last_login', function ($model) {
                    return Carbon\Carbon::parse($model->last_login)
                        ->format('d-M-Y H:i A');
                }
            )
            ->editColumn(
                'email', function ($model) {
                    return $model->email;
                }
            )
            ->editColumn(
                'phone', function ($model) {
                    return $model->phone;
                }
            )
            ->editColumn(
                'name', function ($model) {
                    return '<a style="cursor: pointer;" .
                      class="user-title link adminuser-login" id="adminuser-login" .
                        data-url="' . env('APP_URL') . '/profile" .
                           data-user="' . $model->id . '" > ' .
                        $model->firstname . ' ' .
                        $model->lastname . '</a>';
                }
            )
            ->editColumn(
                'status', function ($model) {
                    if ($model->role_id != '1') {
                        if ($model->active == '1') {
                            return '<div class="pretty p-switch p-fill"> 
<input type="checkbox" class="userStatus" .
 data-type="activate" id="' . $model->id . '" data-id="' .
                            $model->id . '"  data-status="' .
                                $model->status . '" checked 
                            onchange="changeStatus(this,\'inactivate\')" > 
                            <div class="state p-success"><label></label> 
                            </div></div>';
                        } else {
                            return '<div class="pretty p-switch p-fill"> 
<input type="checkbox"  class="userStatus"  .
 data-type="inactivate" id="' . $model->id . '"  data-id="' .
                                $model->id . '" data-status="' . $model->status .
                                '"  onchange="changeStatus(this,\'activate\')"> 
                                <div class="state p-success"> .
                                <label></label></div></div>';
                        }
                    } else {
                        return "Admin";
                    }

                }
            )
            ->editColumn(
                'block', function ($model) {
                    if ($model->role_id != '1') {
                        if ($model->blocked == '1') {
                            return '<div class="pretty p-switch p-switch"> 
<input type="checkbox" class="userStatus"  . 
data-type="activate" id="' . $model->id . '" data-id="' .
                                $model->id . '"  data-status="' . $model->status .
                                '" checked  . 
                                onchange="changeBlock(this,\'inactivate\')" > 
                                <div class="state p-danger"><label></label> 
                                </div></div>';
                        } else {
                            return '<div class="pretty p-switch p-switch"> 
<input type="checkbox"  class="userStatus"  .
 data-type="inactivate" id="' . $model->id .
                            '"  data-id="' . $model->id . '" data-status="' .
                                $model->status . '" .
                                 onchange="changeBlock(this,\'activate\')"> 
                                <div class="state p-danger"><label> 
                                 </label></div></div>';
                        }
                    } else {
                        return "Admin";
                    }
                }
            )
            ->editColumn(
                'action', function ($model) {
                    return '<a href="' . url('users/edit/' . $model->id) .
                        '" type="" class="" title="Edit"> 
                    <i class="fa fa-pencil" aria-hidden="true"></i></a>';
                }
            )
            ->rawColumns(
                ['s#', 'name', 'email', 'block', 'subscription', 'status', 'action']
            )
            ->make(true);
    }

    /**
     * Display profile
     *
     * @return $this
     */
    public function profile()
    {
        $user = User::find(1);
        return view('admin.users.profile')->with('user', $user);
    }

    /**
     * User login
     *
     * @param Request $request for request
     *
     * @return string
     */
    public function userLogin(Request $request)
    {
        $id = $request->get('id');
        $user = User::find($id);
        if ($user != null) {
            $user_key = $this->_getToken(12, $user->id);
        }
        $user->token = $user_key;
        $user->save();
        return env('APP_URL') . '/loginas/' . $user_key;
    }

    /**
     * Get token
     *
     * @param int     $length for length
     * @param varchar $seed   for seed
     *
     * @return string
     */
    private function _getToken($length, $seed)
    {
        $token = "";
        $codeAlphabet = USER_KEY_ALPHA;
        $codeAlphabet .= USER_KEY_NUM;
        mt_srand($seed); // Call once. Good since $application_id is unique.
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[mt_rand(0, strlen($codeAlphabet) - 1)];
        }
        return $token;
    }

    /**
     * Display form for create
     *
     * @return $this
     */
    public function createForm()
    {
        $roles = Role::where('id', '!=', 1)->get();
        return view('admin.users.create')->with('roles', $roles);
    }

    /**
     * Create user
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $v = Validator::make(
            $request->all(), [
                'firstname' => 'required|min:2|max:30',
                'lastname' => 'max:30',
                'email' => 'required|email|min:6|max:75|unique:users',
                'phone' => 'required|unique:users',
                'role' => 'required',
                'password' => 'required|min:8|max:30|confirmed',
                'password_confirmation' => 'required|min:8|max:30'
            ]
        );
        if ($v->passes()) {
            $user = new User();
            $user->firstname = $request->input('firstname');
            $user->lastname = $request->input('lastname');
            $user->email = trim($request->input('email'));
            $user->phone = $request->input('phone');
            $user->password = \Hash::make($request->input('password'));
            $user->role_id = $request->input('role');
            $user->active = 1;
            $user->save();
            if ($user->id) {
                return redirect()->back()
                    ->with('message', 'User added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        }
        return redirect()->back()->with('message', '')->withErrors($v)
            ->withInput($request->except('password', 'cpassword'));
    }

    /**
     * Edit form
     *
     * @param int $id for edit form
     *
     * @return $this
     */
    public function editForm($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        return view('admin.users.edit')
            ->with(array('user' => $user, 'roles' => $roles));
    }

    /**
     * Edit user
     *
     * @param Request $request for request
     * @param int     $id      for edit
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function edit(Request $request, $id)
    {
        $v = Validator::make(
            $request->all(), [
            'firstname' => 'required|min:2|max:30',
            'lastname' => 'max:30',
            'phone' => 'required|unique:users,phone,' . $id,
            ]
        );

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors());
        }
        $user = User::find($id);
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->phone = $request->input('phone');
        $user->save();
        return redirect()->back()
            ->with('message', 'User updated successfully.');
    }

    /**
     * Update profile
     *
     * @param Request $request for request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request)
    {
        $v = Validator::make(
            $request->all(), [
            'firstname' => 'required|min:3|max:50',
            'lastname' => 'max:50',
            'image' => 'mimes:jpeg,bmp,png|max:20480'
            ]
        );

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())
                ->withInput($request->all());
        }
        $file = $request->image;

        $user = User::find(Auth::id());
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->phone = $request->input('phone');
        if ($request->hasFile('image')) {
            if ($request->file('image') != null) {
                $user->avatar = ImageUploadWithPath($file, 'profile-images');
            }
        }
        $user->save();

        $activity = new Activity();
        $activity->user_id = Auth::id();
        $activity->relation_id = $user->id;
        $activity->type = 'user';
        $activity->save();

        return redirect()->back()
            ->with('message', 'Your profile has been updated successfully.');
    }

    public function updateEmail(Request $request)
    {
        $email = $request->input('email');
        $newEmail = $request->input('new_email');
        $messages = [
            'new_email.unique' => 'This Email has already been taken.',
            'new_email.required' => 'The New Email field is required.',
        ];
        $rules = array('new_email' => 'required|email|unique:users,email');

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $authen = User::where('email', $email)->first();
        if ($authen) {
            $authen->temp_email = trim($request->input('new_email'));
            $authen->activation_token = csrf_token();
            $authen->save();

            Mail::to($authen->email)->queue(new UserRegistrationMail('email-reset-user', $authen));
            $message = 'Thank You! Please check your email for the verification!';
            return response()->json([
                'success' => true,
                'message' => $message,
            ]);
        }
        $message = 'No such email id!!';
        return response()->json([
            'fail' => true,
            'errors' => array('message' => $message)
        ]);
    }
}
