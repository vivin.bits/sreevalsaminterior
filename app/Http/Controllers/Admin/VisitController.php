<?php

namespace App\Http\Controllers\Admin;

use App\Visit;
use App\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class VisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = visit::All();
        return view('admin.visit.index',['visits'=>$data]);
    }
    public function careerindex()
    {
        $data = Career::All();
        return view('admin.career.index',['careers'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function show(visit $visit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function edit(visit $visit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, visit $visit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function destroy(visit $visit)
    {
        //
    }
}
