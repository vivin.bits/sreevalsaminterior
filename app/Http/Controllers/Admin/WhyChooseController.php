<?php
//model and datatable pending
namespace App\Http\Controllers\Admin;

use App\Message;
use App\Visit;
use App\WhyChoose;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class WhyChooseController extends Controller
{
    public  function  getwhychoose()
    {
        $mission = WhyChoose::get();
        return DataTables::of($mission)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('title', function ($model) {
                return strip_tags($model->title);

            })
            ->editColumn('description', function ($model) {
                return strip_tags($model->message);

            })

            ->editColumn('action', function ($model) {
                return '<a href="' . url('whychoose/edit/' . $model->id) . '" ><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
<i class="fa fa-trash-o delete" data-content="whychoose" data-id="' . $model->id . '"></i></a>';

            })

            ->rawColumns(['sl#', 'title', 'description', 'action'])
            ->make(true);
    }

public function index()
{

    $list=WhyChoose::get();

return view('admin.whychoose.index')->with(['list'=>$list]);
}
    public function missionForm()
    {
        return view('admin.whychoose.create');
    }
    public function createmission(Request $request)
    {
        $v = Validator::make($request->all(), [
            'title' => 'required',
            'message' => 'required',
           
        
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $mission = new WhyChoose();
            $mission->title = $request->input('title');
            $mission->message = $request->input('message');

            if ($request->hasFile('image')) {
                
            
                if ($request->file('image') != null) {
                    $mission->image = ImageUploadWithPath($request->image,'banner');
                }
            }
            $mission->save();
            if ($mission->id) {
                return redirect()->back()
                    ->with('message', '  added successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }
    public function missioneditForm($id)
    {
        $data = WhyChoose::find($id);
        return view('admin.whychoose.edit')->with(['whychoose' => $data]);
    }
    public function missionedit($id,Request $request)
    {

        $v = Validator::make($request->all(), [
            // 'content' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
           $mission = WhyChoose::find($id);
            $mission->title = $request->input('title');
            $mission->message = $request->input('message');
            $mission->save();
            if ($request->hasFile('image')) {
            
              if ($request->file('image') != null) {
                WhyChoose::where('id',$id)->update(['image'=>ImageUploadWithPath($request->image,'banner')]);
                }
            }
           
            if ($mission->id) {
                return redirect()->back()
                    ->with('message', '  updated successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
            
        
       
    }
    public  function  missiondestroy(Request $request)
    {
        WhyChoose::find($request->id)->delete();
    }
}
