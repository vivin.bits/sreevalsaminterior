<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Settings;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\EmailTemplates;
use Illuminate\Support\Facades\Input;
use Mail;
use App\Mail\UserRegistrationMail;

class LoginController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    protected $redirectTo;

       /**
     * Function Usage : redirect according to authentication
     *
     * @return \Illuminate\Contracts\View\Factory|Redirect|\Illuminate\View\View
     */
    public function index()
    {
        if (!Auth::check()) {
            return view('auth.login');
        }
        return redirect(route('user.profile'));
    }

    /**
     * Function usage : Load Registration
     *
     * @return \Illuminate\Contracts\View\Factory|Redirect|\Illuminate\View\View
     */
    public function register()
    {
        if (!Auth::check()) {
            return view('auth.register');
        } else {
            return redirect(route('user.profile'));
        }
    }

    /**
     * Function Usage : Save user registration details
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {

        $data = $request->all();
        $fullNumber = $request->input('full_number');
        $data['phone'] = $fullNumber;
        $messages = [
            'phone.unique' => trans('profile.text_mobile_already_taken'),
            'firstname.required' => trans('errors.required_firstname'),
            'email.required' => trans('errors.required_email'),
            'email.email' => trans('errors.email_email'),
            'password.required' => trans('errors.required_password'),
            'phone.required' => trans('errors.required_enter_phone'),
             'gender.required' => trans('errors.required_select_gender'),
        ];
        $rules = array(
            'firstname' => 'required|min:2|max:30',
            'lastname' => 'max:30',
            'email' => 'required|email|unique:users',
            'gender' => 'required',
            'password' => 'required|min:8|max:30|confirmed',
            'phone' => 'required|unique:users,phone',
            'dobday' => 'required',
            'dobmonth' => 'required',
            'dobyear' => 'required',

        );
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return redirect('register')->withErrors($validator)->withInput();
        } else {

$dob = $request->input('dobyear') . '-' . $request->input('dobmonth') . '-' . $request->input('dobday');
            $today = date("Y-M-d");

      
   if($dob >= $today )
   {
      return redirect('register')->with('datemessage', 'The date of birth may not be greater than today');
   }
            $authen = new User;
            $authen->firstname = $request->input('firstname');
            $authen->lastname = $request->input('lastname');
            $authen->email = trim($request->input('email'));
            $authen->gender = $request->input('gender');
            $authen->dob = $dob;
            $authen->phone = $request->input('full_number');
            $authen->activation_token = csrf_token();
            $authen->role_id = 3;
            $authen->password = \Hash::make($request->input('password'));
            $authen->blocked = 0;
            $authen->active = 0;
            $authen->save();

           $newPhone = $authen->phone;

            if ($authen->id) {
                $report = new Activity();
                $report->user_id = $authen->id;
                $report->activity = 'New user registration by' . $authen->firstname . ' from IP ' . $request->getClientIp();
                $report->type = 'register';
                $report->ip = $request->getClientIp();
                $report->save();
                Mail::to($authen->email)->queue(new UserRegistrationMail('registration-activation-user', $authen));
                $adminDtl = EmailTemplates::where('key', 'registration-notification-admin')->first();

                if ($adminDtl) {
                    Mail::to($adminDtl->to)->queue(new UserRegistrationMail('registration-notification-admin', $authen));
                }

                $message = trans('authentication.msg_check_mail_activate');
                $newreg = 1;
                $authen->otp = mt_rand(1000, 9999);
                $authen->save();

                 if(env('MOBILY_WS_SEND_SMS')==true)
            {

                $textMsg = trans('notifications.otp_verify_msg').$authen->otp.trans('notifications.otp_msg_continue');
                $status = sendSMS($textMsg,$newPhone);
                $responseCode = $status->result();
   
               if($responseCode['statusCode'] != '1')
               {
                 $message = $responseCode['message'];
        return redirect(route('user.login'))->with('errorMsg', $message);
               }
            }

                $emailId = encrypt($authen->email);
                return Redirect::to('register-success')->with(['message' => $message, 'newreg' => $newreg, 'emailid' => $emailId]);
            } else {
                return redirect()->back()->with('message', trans('authentication.msg_reg_failed'));
            }
        }
    }

    /**
     * Function Usage : Login as admin
     *
     * @param $key
     * @return Redirect
     */
    public function loginasadmin($key)
    {
        $user = \App\User::where('token', '=', $key)->first();

        if ($user) {
            Auth::loginUsingId($user->id, true);
            Auth::user()->save();
            $user->token = null;
            $user->save();
            return redirect('/profile');
        }

        return redirect(ADMIN_BASE_URL);
    }

    /**
     * Function Usage : Registration Success
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function registerSuccess(Request $request)
    {

        if ($request->get('email')) {
            $emailId = $request->get('email');
            $loginMsg = 1;
            return Redirect::to('otp-verification')->with(['emailid' => $emailId, 'loginMsg' => $loginMsg]);
        }

        return view('pages.register-success');
    }

    /**
     * Function Usage : User Account activation success page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function accountActiveSuccess()
    {
        if (Session::get('message')) {
            return view('pages.account-active');
        }

        return Redirect::to('login');
    }

    /**
     * Function Usage : To confirm activation token
     *
     * @param $userId
     * @param $confirmationCode
     * @return mixed
     */
    public function confirm($userId, $confirmationCode)
    {
        if (!$confirmationCode) {
            $message = trans('authentication.msg_acivation_code_missing');
            return Redirect::to('login')->with('message', $message);
        }

        $user = User::where(array('id' => $userId, 'activation_token' => $confirmationCode))->first();
        if (count($user) !== 1) {
            $q = User::where(array('id' => $userId))->first();
            if ($q && $q->active == 1) {
                $message = trans('authentication.msg_account_active');

                return Redirect::to('login')->with('message', $message);
            }

            return Redirect::to('resend-activation');
        } else {
            $user->active = 1;
            $user->activation_token = null;
            $user->save();
            $message = trans('authentication.msg_account_verified');
            return redirect(route('account-activated'))->with('message', $message);
        }

        return Redirect::to('login')->with('message', $message);
    }

    /**
     * Function Usage : Check authentication details
     *
     * @param Request $request
     * @return Redirect
     */
    public function doLogin(Request $request)
    {
        $email = encrypt($request->get('email'));
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, true, true)) {
            $row = User::find(Auth::user()->id);
            if ($row->otp_status == '0') {
                Auth::logout();
                $message = trans('authentication.msg_otp_not_verified');
                return redirect(route('user.login'))->with(['otpErrorMsg' => $message, 'emailid' => $email]);
            }

            if ($row->active == '0') {
                Auth::logout();
                $message = trans('authentication.msg_account_not_verified');
                return redirect(route('user.login'))->with('errorMsg', $message);
            }
        } else {
            $errorMsg = trans('authentication.msg_credential_failed');
            return redirect(route('user.login'))->with('errorMsg', $errorMsg);
        }

        if ($row->blocked == 1) {
            Auth::logout();
            $errorMsg = trans('authentication.msg_user_blocked');
            return redirect(route('user.login'))->with('errorMsg', $errorMsg);
        }

        $setting = Settings::where('name', 'otp_check_login')->first();

        if ($setting == null) {
            Auth::logout();
            return redirect(route('user.login'));
        }

        if (strtolower($setting->value) == 'yes') {
            //$row->login_otp = mt_rand(1000, 9999);
            $row->login_otp = '1111';
            $row->save();
            $mob = $row->phone;
            $newPhone = str_replace('+', '', $mob);
         if(env('MOBILY_WS_SEND_SMS')==true)
            {
           
                $textMsg = trans('notifications.otp_verify_msg').$row->login_otp.trans('notifications.otp_msg_continue');
                $status = sendSMS($textMsg,$newPhone);
                $responseCode = $status->result();
                if($responseCode['statusCode'] != '1')
               {
                  Auth::logout();
                 $message = $responseCode['message'];
        return redirect(route('user.login'))->with('errorMsg', $message);
               }
            }

            Auth::logout();
            Session::put('log_email', encrypt($request->get('email')));
            Session::put('timestamp', Carbon::now());
            return Redirect::to('login-otp-verify');
        }

        if ($request->session()->has('reqUrl')) {
            $domain = Session::get('domain');
            $url = Session::get('reqUrl');
            Session::forget('domain');
            Session::forget('reqUrl');
            return \Redirect::route($url, $domain);
        }

        return redirect()->to('dashboard');
    }

    /**
     * Function Usage : Verify login OTP
     *
     * @return $this|Redirect
     */
    public function loginOtpVarify()
    {
        if (Session::has('log_email')) {
            $email = decrypt(Session::get('log_email'));
            return view('auth.login-otp')->with(['log_email' => encrypt($email)]);
        }

        return redirect(route('user.login'));
    }

    /**
     * Function Usage : Login OTP verification
     *
     * @param Request $request
     * @return mixed
     */
    public function doLoginOtpVarify(Request $request)
    {
        $rules = ['phone_otp' => 'required'];
        $messages = ['phone_otp.required' => trans('authentication.error_otp_required')];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($request->has('log_email')) {

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors());
            $email = decrypt($request->get('log_email'));
            $user = User::where(['email' => $email, 'login_otp' => $request->get('phone_otp')])->first();

            if ($user == null) {
                return redirect()->back()->withErrors(['err_message' => trans('authentication.login_valide_otp')]);
            }
            $current_time = Carbon::now();
            $timeDiff = $current_time->diffInMinutes(Session::get('timestamp'));
            if ($timeDiff <= 15) {
                if (Auth::loginUsingId($user->id, true)) {

                    $user = User::find(Auth::id());
                    $user->login_otp = null;
                    $user->save();
                    Session::forget('log_email');
                    Session::forget('timestamp');

                    if ($request->session()->has('reqUrl')) {
                        $domain = Session::get('domain');
                        $url = Session::get('reqUrl');
                        Session::forget('domain');
                        Session::forget('reqUrl');
                        return \Redirect::route($url, $domain);
                    }
                    return redirect()->to('dashboard');
                }
            }
            return redirect()->back()->withErrors(['err_message' => trans('authentication.login_valide_otp')]);
        }
        return redirect()->back()->withErrors(['err_message' => trans('authentication.login_valide_otp')]);
    }

    /**
     * Function Usage : Logout
     *
     * @return mixed
     */
    public function logout()
    {
        \Auth::logout();
        trackActivity('Logged out', 'Login');
        return Redirect::to('/login')->with('message', '', trans('authentication.msg_logged_out'));
    }

    /**
     * Function Usage : Resend activation
     *
     * @param Request $request
     * @return mixed
     */
    public function resend_activation(Request $request)
    {
        $rules = array(
            'email' => 'required|email'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $email = $request->input('email');
            $user = User::where('email', $email)->first();

            if (!$user) {
                return redirect()->back()->with('message', trans('authentication.msg_email_not_found'));
            } else {
                $activation_code = $user->set_activation($user->id);
                Mail::to($user->email)->queue(new UserRegistrationMail('resend-activation-user', $user));

                if (count(Mail::failures()) == 0) {
                    $message = trans('authentication.msg_activation_link');
                } else {
                    $message = trans('authentication.msg_error');
                }
                return Redirect::to('resend-activation')->with('message', $message);
            }
        }
        return redirect()->back()->withErrors($validator->errors());
    }

    /**
     * Function Usage : Check OTP
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkOtp()
    {
        return view('auth.otp_verification');
    }

    /**
     * Function Usage : OTP Verification
     *
     * @param Request $request
     * @param $email
     * @return mixed
     */
    public function verifyOtp(Request $request, $email)
    {
        $newemail = decrypt($email);

        $otp = $request->get('phone_otp');
        $rules = ['phone_otp' => 'required'];
        $messages = ['phone_otp.required' => trans('profile.text_otp_required')];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->with(['emailid' => $email]);
        }

        if (!$otp) {
            $message = trans('profile.text_otp_missing');
            return Redirect::to('login')->with('message', $message);
        }

        $user = User::where(array('email' => $newemail, 'otp' => $otp))->first();
        if ($user) {
            $user->otp_status = 1;
            $user->otp = null;
            $user->save();

            if ($user->active == 1) {
                $message = trans('authentication.msg_account_verified');
                return redirect(route('account-activated'))->with('message', $message);
            } elseif ($user->otp_status == 1) {
                $message = trans('authentication.msg_otp_verified');
                return redirect(route('account-activated'))->with('message', $message);
            }
        } else {
            $errormessage = trans('profile.text_otp_wrong');
            return redirect()->back()->with(['errormessage' => $errormessage, 'emailid' => $email]);
        }
    }
}
