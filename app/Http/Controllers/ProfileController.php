<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\UserRegistrationMail;
use App\EmailTemplates;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    
    /**
     * Function Usage : Load user profile
     *
     * @return $this
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);
        return view('user.profile')->with(['user' => $user, 'page' => "profile"]);
    }

    /**
     * Function Usage : Update profile
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request)
    {
        $response = array();
        $messages = array();
        $rules = array('firstname' => 'required|min:2|max:30');
        $validator = Validator::make($request->all(), $rules, $messages);
        $response['status'] = false;

        if ($validator->fails()) {
            $response['status'] = false;
            $response['errors'] = $validator->getMessageBag()->toArray();
            return response()->json($response);
        }

        $user = User::find(Auth::user()->id);
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->save();

        if ($user->id) {
            trackActivity("Edited profile", 'Profile Update');
            $response['status'] = true;
        }

        return response()->json($response);
    }

    /**
     * Function Usage : Change password page
     *
     * @return $this
     */
    public function changePasswordPage()
    {
        return view('user.change_password')->with(['page' => 'profile']);
    }

    /**
     * Function Usage : Update change password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateChangePassword(Request $request)
    {
        $rules = array(
            'current_password' => 'required',
            'password' => 'required|between:6,12|confirmed',
            'password_confirmation' => 'required|between:6,12'
        );
        $messages = array(
            'current_password.required' => trans('profile.validate_current_password_required'),
            'password.required' => trans('profile.validate_password_required'),
            'password.between' => trans('profile.validate_password_between'),
            'password.confirmed' => trans('profile.validate_password_confirmed'),
            'password_confirmation.required' => trans('profile.validate_password_confirm_required'),
            'password_confirmation.between' => trans('profile.validate_password_confirm_between'),

        );
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(array(
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }

        $user = User::find(Auth::user()->id);

        if (Auth::attempt(['email' => $user->email, 'password' => $request->input('current_password')])) {
            $user->password = \Hash::make($request->input('password'));
            $user->save();
            Mail::to($user->email)->queue(new UserRegistrationMail('password-change-to-user', $user));

            if ($user->id) {
                return response()->json(array('success' => true));
            }
        }

        return response()->json(array('fail' => true, 'errors' => array('current_password' => trans('profile.msg_incorrect_password'))));
    }

    /**
     * Function Usage : Change mail
     *
     * @return $this
     */
    public function mailChange()
    {
        $user = User::find(Auth::user()->id);
        return view('user.changemail')->with(['user' => $user]);
    }

    /**
     * Function Usage : Update email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateEmail(Request $request)
    {
      $rules = [
        'new_email' => 'required|unique:users,email',
        'confirm_email' => 'required|email'
    ];
    $messages = [
        'new_email.unique' => trans('profile.label_email_taken'),
        'new_email.required' => trans('profile.text_email_required'),
        'confirm_email.required' => trans('profile.text_confirm_email_required'),
        'confirm_email.email' => trans('profile.text_confirm_email_check'),

    ];
    $validator = Validator::make($request->all(), $rules, $messages);

    if ($validator->fails()) {
        return response()->json([
            'fail' => true,
            'errors' => $validator->getMessageBag()->toArray()
        ]);
    }

    if ($request->input('new_email') != $request->input('confirm_email')) {
        return response()->json([
            'fail' => true,
            'errors' => [
                'confirm_email' => [
                    trans('validation.confirmed', ['attribute' => 'confirm email'])
                ]
            ]
        ]);
    }

    $authen = Auth::user();

    if ($authen) {
        $authen->temp_email = trim($request->input('new_email'));
        $authen->activation_token = csrf_token();
        $authen->save();

        Mail::to($authen->email)->queue(new UserRegistrationMail('email-reset-user', $authen));
        $adminDtl = EmailTemplates::where('key', 'email-reset-admin')->first();

        if ($adminDtl) {
            Mail::to($adminDtl->to)->queue(new UserRegistrationMail('email-reset-admin', $authen));
        }

        $message = trans('profile.msg_verify_email');
        return response()->json(array('success' => true, 'message' => $message));
    }

    $message = trans('profile.text_no_such_id');
    return response()->json(array('fail' => true, 'errors' => array('message' => $message)));
}

    /**
     * Function Usage : Confirm mail
     *
     * @param $userId
     * @param $confirmationCode
     * @return mixed
     */
    public function confirmEmail($userId, $confirmationCode)
    {
        if (!$confirmationCode) {
            $message = trans('profile.msg_activation_code_missing');
            return Redirect::to('login')->with('message', $message);
        }

        if (Auth::user()->id == $userId) {
            $user = User::where(array('id' => $userId, 'activation_token' => $confirmationCode))->first();
            $user->activation_token = null;
            $user->email = $user->temp_email;
            $user->temp_email = null;
            $user->save();

            Auth::logout();
            $message = trans('profile.msg_update_email');
            return redirect(route('account-activated'))->with('message', $message);
        }

        Auth::logout();
        $message = trans('profile.msg_update_email');
        return Redirect::to('login')->with('message', $message);
    }

    /**
     * Function Usage : Change phone number
     *
     * @return $this
     */
    public function phoneChange()
    {
        $user = User::find(Auth::user()->id);
        return view('user.change_phone')->with(['user' => $user]);
    }

    /**
     * Function Usage : OTP for mobile number
     *
     * @return $this
     */
    public function phoneOtp()
    {
        $user = User::find(Auth::user()->id);
        return view('user.phone_otp')->with(['user' => $user]);
    }

    /**
     * Function Usage : Upadate mobile number
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePhone(Request $request)
    {

        $data = $request->all();
        $newPhone = $request->input('full_number');
        $fullNumber = "+" . $newPhone;
        $data['phone'] = $fullNumber;
        $messages = [
            'phone.unique' => trans('profile.text_mobile_already_taken'),
            'new_phone.required' => trans('profile.text_mob_required'),
        ];

        $rules = [
            'new_phone' => 'required',
            'phone' => 'unique:users,phone',
        ];
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        $authen = Auth::user();

        if ($authen) {
            $authen->otp = mt_rand(1000, 9999);
            $authen->temp_phone = $fullNumber;
            $authen->save();


            if(env('MOBILY_WS_SEND_SMS')==true)
            {
                $textMsg = trans('notifications.otp_verify_msg').$authen->otp.trans('notifications.otp_msg_continue');
               $status = sendSMS($textMsg,$newPhone);
               $responseCode = $status->result();
   
               if($responseCode['statusCode'] != '1')
               {
                 $message = $responseCode['message'];
        return response()->json(array('failure' => true, 'message' =>$message ));
               }
            }
     
            $message = trans('profile.text_verify_otp');
            return response()->json(array('success' => true, 'message' => $message));
        }

        $message = trans('profile.text_no_such_mobile');
        return response()->json(array('fail' => true, 'errors' => array('message' => $message)));
    }

    /**
     * Function Usage : Confirm OTP
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmOtp(Request $request)
    {
        $otp = $request->get('phone_otp');
        $rules = [
            'phone_otp' => 'required',
        ];
        $messages = [
            'phone_otp.required' => trans('profile.text_otp_required'),
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray()
            ]);
        }

        if (!$otp) {
            $message = trans('profile.text_otp_missing');
            return Redirect::to('login')->with('message', $message);
        }

        if (Auth::check()) {
            $userId = Auth::user()->id;
            $user = User::where(array('id' => $userId, 'otp' => $otp))->first();
            if ($user) {
                $user->phone = $user->temp_phone;
                $user->temp_phone = null;
                $user->otp = null;
                $user->save();
                $message = trans('profile.text_number_updated');
                return response()->json(array('success' => true, 'new_mobile' => $user->phone, 'message' => $message));
            } else {
                $errormessage = trans('profile.text_otp_wrong');
                return response()->json([
                    'otpfail' => true,
                    'errormessage' => $errormessage,
                ]);
            }
        }

        Auth::logout();
        $message = trans('profile.msg_update_email');
        return Redirect::to('login')->with('message', $message);
    }
}
