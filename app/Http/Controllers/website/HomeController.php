<?php
 
namespace App\Http\Controllers\website;
use App\Course;
use App\About;
use App\Contact;
use App\Slider;
use App\WhyChoose;
use App\Gallery;
use App\Album;
use App\Message;
use App\Mission;
use App\Project;
use App\SchoolActivity;
use App\Testimonial;
use App\Facility;
use App\News;
use App\Enquiry;
use App\Packages;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\MultipleGallery;
use App\TabHeading;
use Yajra\DataTables\DataTables;
class HomeController extends Controller
{
    public function index()
    { 
       $banner=Slider::get();
       $whychoose=WhyChoose::first();
       $activity=SchoolActivity::get();
       $testmonial=Testimonial::get();
       $project=Project::get();
       $service=Message::get();
        return view('home.home')->with(['banner'=>$banner,'whychoose'=>$whychoose,
        'activity'=>$activity,'testmonial'=>$testmonial,'project'=>$project,'service'=>$service]);
    }
    public function about()
    {
       $whychoose=WhyChoose::first();

        $about=About::first();
          return view('home.about')->with(['about'=>$about,'whychoose'=>$whychoose,]);
  
    
        

    }
    public function aboutDetails($slug)
    {
      $aboutusDetails=About::where('slug',$slug)->first();
      $schoolStudent=SchoolStudent::get();
        return view('home.aboutDetails')->with(['aboutusDetails'=>$aboutusDetails,'school'=>$schoolStudent]);

    }

    public function gallery()
    {
        $gallery=Album::get();
        return view('home.gallery')->with(['gallery'=>$gallery]);


    }
public function activity()
{
    $activity=SchoolActivity::get();
    return view('home.activity')->with(['activity'=>$activity]);   
}
    public function course($slug)
    { $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        $data['course_details']  = Course::where('slug', '=', $slug)->first();
        return view('home.course',$data);
        
    }
    public function coy()
    {
        $data['contact'] = Contact::first();
        $album_name =  'General';
        $get_album['album']  = Album::where('category', '=', $album_name)->first();
        $get_album_id =  $get_album['album']['id'];
        $data['courses'] = Course::pluck('course','slug');
        $data['gallery'] = Gallery::where( 'album_id', $get_album_id)->get();
        return view('home.gallery')->with($data);
    }
    public function message()
    {
        $data['about']   = About::first();
        $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        $data['message']   = Message::first();
        return view('home.message',$data);
    }
    public function mission()
    {
        $data['about']   = About::first();
        $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        $data['mission']   = Mission::first();
        return view('home.mission',$data);
    }

    public function facility()
    {   
        $data['facility'] = Facility::orderBy('id')->get();
        $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        return view('home.facility',$data);
    }
    public function contact()
    {
    $contact = Contact::first();
       
       
        return view('home.contact')->with(['contact'=>$contact]);
    }
    public function application()
    {
        $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        return view('home.application',$data);
    }
    public function career()
    {
        $data['contact'] = Contact::first();
        $data['courses'] = Course::pluck('course','slug');
        return view('home.career',$data);
    }
    public function applicationcreate(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required',
            'lname'=>'required',
            'street'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $admission = new Admission();
            $admission->fname = $request->input('fname');
            $admission->lname = $request->input('lname');
            $admission->street = $request->input('street');
            $admission->city = $request->input('city');
            $admission->pincode = $request->input('pincode');
            $admission->state = $request->input('state');
            $admission->country = $request->input('country');
            $admission->phone = $request->input('phone');
            $admission->course = $request->input('course');
            $admission->email = $request->input('email');
            $admission->save();
            if ($admission->id) {
                return redirect()->back()
                    ->with('message', 'application send   successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
    }
    public function careercreate(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fname' => 'required',
            'lname'=>'required',
            'street'=>'required',
            
            
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
            $career = new  Career();
            $career->fname = $request->input('fname');
            $career->lname = $request->input('lname');
            $career->phone = $request->input('phone');
            $career->email = $request->input('email');
            $career->city = $request->input('city');
            $career->street = $request->input('street');
            $career->state = $request->input('state');
            $career->country = $request->input('country');
            $career->qualification = $request->input('qualification');
            $career->experience= $request->input('experience');
            $career->category = $request->input('category');
            $career->pincode = $request->input('pincode');
            $career->save();
            if ($career->id) {
                return redirect()->back()
                    ->with('message', 'application send   successfully.');
            } 
                return redirect()->back()->with('message', 'Failed to save.');
    }
   
public function allFacility()
{
    $facility=Facility::get();
    return view('home.facility')->with(['facility'=>$facility]);
}
 public function contactView()
    {
        $contact = Contact::first();
        return view('home.contact')->with(['contact' => $contact]);
    }

 
    public function contactStore(Request $request)
    {
        
         $v = Validator::make($request->all(), [
            'name' => 'required',
            'message' => 'required',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput()->with('wmessage', 'Fill out The required Fields');
        }
        $contact = new  Enquiry();
        $contact->first_name = $request->input('name');
//        $contact->last_name = $request->input('lname');
//        $contact->subject = $request->input('message');
        $contact->email = $request->input('email');
        $contact->message = $request->input('message');
        $contact->save();

        return redirect()->back()
            ->with('message', 'application send   successfully.');
    }

    public function enquiryList()
    {
        return view('home.enquiry');
    }

    public  function  enquiryIndex()
    {
        $enquiry = Enquiry::get();
        return DataTables::of($enquiry)
            ->editColumn('sl#', function ($model) {
                return '<span class="si_no"></span>';
            })
            ->editColumn('first_name', function ($model) {
                return $model->first_name;

            })
            // ->editColumn('last_name', function ($model) {
            //     return $model->last_name;

            // })
            ->editColumn('email', function ($model) {
                return $model->email;

            })
            // ->editColumn('subject', function ($model) {
            //     return $model->subject;

            // })
            ->editColumn('message', function ($model) {
                return strip_tags($model->message);

            })

            ->rawColumns(['sl#', 'first_name', 'email','message'])
            ->make(true);
    }




    public function service($slug)
    {
        $service=Album::where('slug',$slug)->where('status',1)->first();
        $servicelist=Album::get();
        return view('home.service')->with(['service'=>$service,'servicelist'=>$servicelist]);
    }


 
    public function products($slug)
    {
        $package=Packages::where('slug',$slug)->first();
        $servicelist=Album::get();
        return view('home.products')->with(['package'=>$package,'servicelist'=>$servicelist]);
    }


    public function packagesList()
    {
        return view('home.packages');
    }

    public function galleryListImage()
    {
        $galleryTabs =  TabHeading::select('id','name')->get();
        return view('home.galleryimage')->with(['gallery'=>$galleryTabs]);
    }

    public function allGallery(Request $request)
    {
        $id=$request->input('id');
     
        $galler_image= MultipleGallery::where('tab_id', $id)->get();
        $html= view('home.multipleimages',['galler_images'=>$galler_image])->render();
        return response()->json($html);
    }
}
