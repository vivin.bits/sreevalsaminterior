<?php
/**
 * Created by PhpStorm.
 * User: COMPUTER
 * Date: 3/27/2018
 * Time: 3:30 PM
 */

namespace App\Http\Middleware;

use Closure;

class AdminPermissionsRequired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // Check if a user is logged in.
        $route = $request->route();
        $actions = $route->getAction();

        if (isset($actions['module']) && $actions['module'] && !empty($actions['module'])) {
            if (module_permission($actions['module'])) {

                if (isset($actions['permission']) && $actions['permission'] && !empty($actions['permission'])) {

                    if(can($actions['permission'].'_'.$actions['module'])){
                        return $next($request);
                    }
                    return $request->ajax() ? response('No access') : redirect('/no-access');
                }
                return $next($request);
            }
            return $request->ajax() ? response('No access') : redirect('/no-access');
        }
        return $next($request);
    }
}