<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistrationMail extends Mailable
{
  use Queueable, SerializesModels;

  private $_user;
  private $_key;
  private $_tokens;
  
  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($key, $user,$tokens = null){
    $this->_user = $user;
    $this->_key = $key;
    $this->_tokens = $tokens;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build(){
    switch ($this->_key) {
      case 'registration-activation-user':
        return $this->_sendActivationMail();
      break;
      case 'registration-notification-admin':
        return $this->_sendRegistrationMailToAdmin();
      break;
      case 'resend-activation-user':
        return $this->_sendResendActivationMail();
      break;
      case 'user-password-reset':
        return $this->_sendPasswordResetMail();
      break;
      case 'email-reset-user':
        return $this->_sendResetMail();
      break;
      case 'email-reset-admin':
        return $this->_sendResetMailToAdmin();
      break;
      case 'contact-acknowledge-user':
        return $this->_sendContactinfoMail();
      break;
      case 'contact-notification-admin':
        return $this->_sendContactinfoMailToAdmin();
      case 'password-change-to-user':
        return $this->_sendPasswordChangeToUser();
      break;
    }
  }

  private function _sendActivationMail(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]', $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[activation_token]', url('account-activation/'.$this->_user->id.'/'.$this->_user->activation_token), $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendRegistrationMailToAdmin(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]',  $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[phone]', $this->_user->phone, $template);
      $template = str_replace('[email]', $this->_user->email, $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendResendActivationMail(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]', $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[activation_token]', url('account-activation/'.$this->_user->id.'/'.$this->_user->activation_token), $template);
      return $this->view('emails.user-resend-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendPasswordResetMail(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[fullname]', $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[activation_token]', url('password/reset/'.$this->_tokens.'?email='.$this->_user->email), $template);
      return $this->view('emails.user-password-reset')->with(['template' => $template]);
    }
  }

  private function _sendResetMail(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]', $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[activation_token]', url('email-reset/'.$this->_user->id.'/'.$this->_user->activation_token), $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendResetMailToAdmin(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]',  $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[phone]', $this->_user->phone, $template);
      $template = str_replace('[email]', $this->_user->temp_email, $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendContactinfoMail(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]', $this->_user->firstname.' '.$this->_user->lastname, $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendContactinfoMailToAdmin(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[username]',  $this->_user->firstname.' '.$this->_user->lastname, $template);
      $template = str_replace('[phone]', $this->_user->phone, $template);
      $template = str_replace('[email]', $this->_user->email, $template);
      $template = str_replace('[message]', $this->_user->message, $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }

  private function _sendPasswordChangeToUser(){
    $emailTemplate = \App\EmailTemplates::where('key', $this->_key)->first();
    $this->subject(CNF_APPNAME.' - '.$emailTemplate->subject);
    if($emailTemplate){
      $template = $emailTemplate->email_body;
      $template = str_replace('[user]',  $this->_user->firstname.' '.$this->_user->lastname, $template);
      return $this->view('emails.user-activation-template')->with(['template' => $template]);
    }
  }
}