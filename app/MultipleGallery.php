<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleGallery extends Model
{
    protected $table='multiple_gallery';
   protected $fillable=['tab_id','content','image'];
}
