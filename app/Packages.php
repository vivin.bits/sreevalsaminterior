<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $table="packages";
    public $timestamps=true;
    public function media()
    {
        return $this->hasMany('App\PackagesMedia','packages_id','id');
    }
}
