<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackagesMedia extends Model
{
    protected $table="packages_media";
    public $fillable=true;
}
