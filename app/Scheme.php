<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
    
    protected $table = 'scheme';
    public $timestamps = true;
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
