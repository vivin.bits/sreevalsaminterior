<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolActivity extends Model
{
    protected $table = 'school_activitys';
    public $timestamps = true;
}
