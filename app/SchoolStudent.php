<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolStudent extends Model
{
    public $timestamps=true;
}
