<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Settings extends Authenticatable
{
    use Notifiable;
      protected $table = 'settings';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
    'title_ar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

     protected $dates = ['deleted_at'];





}
