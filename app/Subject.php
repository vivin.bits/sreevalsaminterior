<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';
    public $timestamps = true;
    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
