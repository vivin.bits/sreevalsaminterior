<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TabHeading extends Model
{
    protected $table="tab_heading";
    public $timestamp=true;
    
}
