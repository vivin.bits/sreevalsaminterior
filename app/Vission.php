<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vission extends Model
{
    protected $table = 'vission';
    public $timestamps = true;
}
