<?php
// define('CNF_APPNAME','Leisure Last Minute');
define('CNF_EMAIL','vasanthan.rk@acodez.co.in');
//define('CNF_EMAIL','info@leisurelastminute.com');
define('USER_BASE_URL','http://leisurelastminute.vasanthan.acodez.ca/');
define('AGENT_BASE_URL','http://leisurelastminute.vasanthan.acodez.ca/');
define('ADMIN_BASE_URL','http://admin.leisurelastminute.vasanthan.acodez.ca/');

define('USER_KEY_ALPHA','UVWXYZABCDEFNOPQRSTGHIJKLM');
define('USER_KEY_NUM','963258741');

define('CURRENCY','$');
define('CURRENCY_CODE','USD');
define('LAT', 11.258567);
define('LNG', 75.778554);
define('COUNTRY', 'IN');

return [ 
	"image_sizes" => [
	    "default" => array(500, 261),
	    "profile_image" => array(150, 110),
	    "media_preview_image" => array(200, 150),
		 "banner_preview_image" => array(200, 100),
		 "10x10"=>array(10,10),
		 "400x400"=>array(400,400),
		"30x30"=>array(30,30),
		"10000x1000"=>array(10000,1000),
		"459x192"=>array(459,192),
		"429x71"=>array(429,71),
		"325x319"=>array(325,319),
		"459x266.67"=>array(459,266.67),
		"429x286"=>array(429,286),
		"370x230"=>array(370,230),  
		"429x267"=>array(429,267),
		"429x369"=>array(429,369),
		"504.16x349"=>array(504.16,349),
		"440x290"=>array(440,290),
		"1284x722"=>array(1284,722),
		"642x556"=>array(642,556),
		"364x236"=>array(364,236),
		"354x198"=>array(354,198),
		"240x133"=>array(240,133),
		"190x105"=>array(190,105),
		"370x504"=>array(370,504),
		"503x326"=>array(503,326),
		"900x500"=>array(900,500),
		"354x198"=>array(354,198),
		"247x164"=>array(247,164),
		"20x20"=>array(20,20),
		"354.03x198.35"=>array(354.03,198.35),
		"150x150"=>array(150,150),
		"2000x1000"=>array(2000,1000)
	]
];


?>
