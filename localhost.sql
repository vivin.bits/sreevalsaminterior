-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 13, 2019 at 08:23 AM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dummy`
--
CREATE DATABASE IF NOT EXISTS `dummy` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `dummy`;

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `main_description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `meta_tag` varchar(200) DEFAULT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `mission` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `main_description`, `image`, `slug`, `meta_tag`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `mission`, `created_at`, `updated_at`) VALUES
(3, 'Ayyappa College', '<p>What you become depends on what you know, which again depends on what learn, and is ultimately a function of what you are taught. And who is bigger teacher than experience? At Ayyappa, we take pride in our sprawling a decade pedagogy experience, as year after year we have produced great learners who went on to become outstanding individuals, to leave a mark on their respective domains.</p>', '<p>Our focus is not just learning but the process of learning, for learning is a phase which begins and gradually blooms into a beautiful flower and hence every stage of this metamorphosis requires intense care, sincerity, and dedication. Our style of teaching knows no discrimination as we impart knowledge and wisdom just the way it’s expected of us.</p><p>Ayyappa institute of management studies was started in 2017 with an aim to impart quality education to students in the country. We offer highly demanded UG degree courses and one PG course in different streams. Ayyappa college is unique in the university which offers rare and highly job oriented courses in a serene and picturesque campus</p>', '2019/8/about-pic1.png', 'ayyappacollege', NULL, 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', NULL, '2019-08-16 12:07:12', '2019-08-17 05:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` enum('facility','order','user','setting','role','classification','category','email','cms','module') DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', NULL, '2018-08-31 00:54:24', '2018-08-31 00:54:24'),
(2, 1, 1, 'user', NULL, '2018-08-31 00:54:48', '2018-08-31 00:54:48'),
(3, 1, 1, 'user', NULL, '2018-08-31 00:56:22', '2018-08-31 00:56:22'),
(4, 1, 1, 'user', NULL, '2018-08-31 00:56:33', '2018-08-31 00:56:33'),
(5, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:30:44', '2018-09-12 04:30:44'),
(6, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:31:03', '2018-09-12 04:31:03'),
(7, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 04:56:59', '2018-09-12 04:56:59'),
(8, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 05:55:38', '2018-09-12 05:55:38'),
(9, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:46', '2018-09-12 06:01:46'),
(10, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:57', '2018-09-12 06:01:57'),
(11, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:03', '2018-09-12 06:04:03'),
(12, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:23', '2018-09-12 06:04:23'),
(13, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:35', '2018-09-12 06:04:35'),
(14, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:32:50', '2018-09-12 06:32:50'),
(15, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:33:01', '2018-09-12 06:33:01'),
(16, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 22:59:25', '2018-09-12 22:59:25'),
(17, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:10', '2018-09-12 23:01:10'),
(18, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:20', '2018-09-12 23:01:20'),
(19, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:27', '2018-09-12 23:02:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:47', '2018-09-12 23:02:47'),
(21, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:15', '2018-09-12 23:03:15'),
(22, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:40', '2018-09-12 23:03:40'),
(23, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:54', '2018-09-12 23:03:54'),
(24, 1, 1, 'user', NULL, '2018-09-13 04:46:30', '2018-09-13 04:46:30'),
(25, 1, 1, 'user', NULL, '2018-09-13 04:47:20', '2018-09-13 04:47:20'),
(26, 1, 1, 'user', NULL, '2018-09-13 04:48:46', '2018-09-13 04:48:46'),
(27, 1, 1, 'user', NULL, '2018-09-13 04:52:00', '2018-09-13 04:52:00'),
(28, 1, 1, 'user', NULL, '2018-09-13 04:52:17', '2018-09-13 04:52:17'),
(29, 1, 18, 'module', 'New module has been created by:', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(30, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 02:56:44', '2019-07-29 02:56:44'),
(31, 1, 19, 'module', 'New module has been created by:', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(32, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:50:28', '2019-07-29 06:50:28'),
(33, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:53:26', '2019-07-29 06:53:26'),
(34, 1, 20, 'module', 'New module has been created by:', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(35, 1, 1, 'role', 'New role has been edited by:', '2019-07-30 22:58:34', '2019-07-30 22:58:34'),
(36, 1, 21, 'module', 'New module has been created by:', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(37, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 04:35:01', '2019-08-06 04:35:01'),
(38, 1, 22, 'module', 'New module has been created by:', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(39, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 05:32:55', '2019-08-06 05:32:55'),
(40, 1, 23, 'module', 'New module has been created by:', '2019-08-12 22:58:03', '2019-08-12 22:58:03'),
(41, 1, 1, 'role', 'New role has been edited by:', '2019-08-12 22:58:25', '2019-08-12 22:58:25'),
(42, 1, 24, 'module', 'New module has been created by:', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(43, 1, 1, 'role', 'New role has been edited by:', '2019-08-13 04:36:06', '2019-08-13 04:36:06'),
(44, 1, 25, 'module', 'New module has been created by:', '2019-08-14 00:16:42', '2019-08-14 00:16:42'),
(45, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 00:17:05', '2019-08-14 00:17:05'),
(46, 1, 26, 'module', 'New module has been created by:', '2019-08-14 05:56:10', '2019-08-14 05:56:10'),
(47, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 05:56:32', '2019-08-14 05:56:32'),
(48, 1, 27, 'module', 'New module has been created by:', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(49, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 07:52:44', '2019-08-14 07:52:44'),
(50, 1, 28, 'module', 'New module has been created by:', '2019-08-20 04:26:05', '2019-08-20 04:26:05'),
(51, 1, 1, 'role', 'New role has been edited by:', '2019-08-20 04:26:20', '2019-08-20 04:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `perifix` varchar(200) NOT NULL,
  `type` enum('parent','child') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 'User', 'user', 'parent', '2018-03-26 05:44:20', '2018-03-26 05:44:20'),
(2, 0, 'Roles', 'roles', 'parent', '2018-03-27 01:07:21', '2018-03-27 01:07:21'),
(3, 0, 'Module', 'module', 'parent', '2018-03-27 01:07:42', '2018-03-27 01:07:42'),
(4, 0, 'Admin User', 'admin_user', 'parent', '2018-03-27 23:00:33', '2018-03-27 23:00:33'),
(5, 0, 'Classification', 'classification', 'parent', '2018-03-29 00:06:51', '2018-03-29 00:06:51'),
(6, 0, 'Category', 'category', 'parent', '2018-03-29 07:09:16', '2018-03-29 07:09:16'),
(7, 0, 'CMS', 'cms', 'parent', '2018-03-29 22:48:40', '2018-03-29 22:48:40'),
(8, 0, 'Facility Management', 'facility_management', 'parent', '2018-05-06 23:21:17', '2018-05-06 23:21:17'),
(9, 0, 'Order Management', 'order', 'parent', '2018-05-07 23:47:47', '2018-05-07 23:47:47'),
(10, 0, 'Email Setting', 'email_template', 'parent', '2018-05-08 23:49:01', '2018-05-08 23:49:01'),
(11, 0, 'User Report', 'user_report', 'parent', '2018-05-13 22:40:52', '2018-05-13 22:40:52'),
(12, 0, 'Facility Report', 'facility_report', 'parent', '2018-05-14 22:02:29', '2018-05-14 22:02:29'),
(13, 0, 'Order Report', 'order_report', 'parent', '2018-05-15 00:45:33', '2018-05-15 00:45:33'),
(14, 0, 'Settings', 'setting', 'parent', '2018-05-16 22:17:17', '2018-05-16 22:17:17'),
(15, 0, 'Admin User', 'admin_user', 'parent', '2018-05-22 05:50:07', '2018-05-22 05:50:07'),
(16, 0, 'Static Content', 'static_content', 'parent', '2018-05-30 03:38:32', '2018-05-30 03:38:32'),
(17, 0, 'Testimonial', 'testimonial', 'parent', '2018-06-01 05:45:03', '2018-06-01 05:45:03'),
(18, 0, 'department', 'department', 'parent', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(19, 0, 'subject', 'subject', 'parent', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(20, 0, 'news', 'news', 'parent', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(21, 0, 'scheme', 'scheme', 'parent', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(22, 0, 'course', 'course', 'parent', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(23, 0, 'gallery', 'gallery', 'parent', '2019-08-12 22:58:02', '2019-08-12 22:58:02'),
(24, 0, 'album', 'album', 'parent', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(25, 0, 'about', 'about', 'parent', '2019-08-14 00:16:41', '2019-08-14 00:16:41'),
(26, 0, 'contact', 'contact', 'parent', '2019-08-14 05:56:09', '2019-08-14 05:56:09'),
(27, 0, 'slider', 'slider', 'parent', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(28, 0, 'facility', 'facility', 'parent', '2019-08-20 04:26:05', '2019-08-20 04:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `street` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`id`, `fname`, `lname`, `street`, `city`, `pincode`, `state`, `country`, `phone`, `email`, `course`, `created_at`, `updated_at`) VALUES
(5, 'SUBINA', 'KS', 'IRITTY', 'KANNUR', '670706', 'Kerala', 'INDIA', '9061395796', 'subinakandoth@gmail.com', 'B.Sc.Petrochemical', '2019-08-19 03:56:58', '2019-08-19 03:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `category` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `category`, `slug`, `main_image`, `created_at`, `updated_at`) VALUES
(11, 'Onam cellebration', '', '2019/8/onam.jpg', '2019-08-13 22:30:10', '2019-08-13 22:30:10'),
(12, 'General', 'general', '2019/8/dpm-83141.jpg', '2019-08-17 01:41:51', '2019-08-17 01:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `qualification` longtext NOT NULL,
  `experience` longtext NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(50) NOT NULL,
  `category` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `fname`, `lname`, `city`, `street`, `phone`, `email`, `qualification`, `experience`, `country`, `state`, `pincode`, `category`, `updated_at`, `created_at`) VALUES
(4, 'shijina', 'k', 'kannur', 'mattannur', '9446395796', 'shijina@gmail.com', 'MSc geology', 'fresher', 'INDIA', 'Kerala', '670709', 'Faculty', '2019-08-19 10:31:48', '2019-08-19 10:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `slug` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1. Default Item',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `slug`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'THREE', 'Three', 'next2', 0, 0, '2018-09-10', '2018-09-11'),
(10, 'gg', '', 'dd', 9, 0, '2018-09-10', '2018-09-10'),
(13, 'fgdgdgdsfgs', '', 'fgdgdgdsfgs', 9, 0, '2018-09-10', '2018-09-10'),
(15, '4', '', 'two', 12, 0, '2018-09-10', '2018-09-11'),
(16, '222', 'test', 'thhree', 1, 0, '2018-09-11', '2018-09-12'),
(26, 'ONE', 'one', 'sdfsdfsd', 0, 0, '2018-09-11', '2018-09-11'),
(30, '11', NULL, 'dummy', 26, 0, '2018-09-11', '2018-09-11'),
(31, '44', NULL, '44', 12, 0, '2018-09-11', '2018-09-11'),
(32, '444', NULL, '444', 12, 0, '2018-09-11', '2018-09-11'),
(35, 'htyyt', 'tjytjyjyj', 'htyyt', 0, 0, '2019-08-01', '2019-08-01'),
(36, 'jyjytjyt', NULL, 'jyjytjyt', 35, 0, '2019-08-01', '2019-08-01'),
(37, 'jyutyuytj', NULL, 'jyutyuytj', 35, 0, '2019-08-01', '2019-08-01'),
(38, 'Bachelor of Computer Application', 'zdxczxv', 'bachelor-of-computer-application', 1, 0, '2019-08-01', '2019-08-01'),
(39, 'Bsc. Geology', 'fghfh', 'bsc-geology', 35, 0, '2019-08-01', '2019-08-01'),
(40, 'we', 'wewre', 'we', 0, 0, '2019-08-01', '2019-08-01'),
(41, 'vc', NULL, 'vc', 40, 0, '2019-08-01', '2019-08-01'),
(42, 'vm', NULL, 'vm', 40, 0, '2019-08-01', '2019-08-01'),
(43, 'vk', NULL, 'vk', 40, 0, '2019-08-01', '2019-08-01'),
(45, 'ffgfg', 'gfgfg', 'ffgfg', 0, 0, '2019-08-01', '2019-08-01'),
(46, 'cvcvcv', NULL, 'cvcvcv', 45, 0, '2019-08-01', '2019-08-01'),
(47, 'cvgfhgh', 'fgh', 'cv', 35, 0, '2019-08-01', '2019-08-14'),
(49, 'xfd', NULL, 'xfd', 0, 0, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `phone` longtext NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `map` longtext NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `insta` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `meta_title` varchar(50) DEFAULT NULL,
  `meta_content` varchar(50) DEFAULT NULL,
  `meta_description` varchar(50) DEFAULT NULL,
  `meta_key` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `phone`, `email`, `address`, `map`, `facebook`, `twitter`, `linkedin`, `insta`, `youtube`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `created_at`, `updated_at`) VALUES
(6, 'Ayyappa College', '<p>04869 252999</p>', 'principal.aims1@gmail.com', '<p>Ayyappa College Of Management Studies</p><p>55th Mile, Peermade, Idukki District, Kerala</p>', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7868.548572652532!2d77.056052!3d9.571608!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb65e73f9454c0f0!2sAyyappa+Institute+of+Management+Studies%2C+Peermade!5e0!3m2!1sen!2sin!4v1566212709483!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'ayyappa', 'ayyappacollege', 'ayyappacollege', 'ayyappa', 'ayyappacollege', 'ayyappa', 'ayyappa', 'ayyappa college', 'ayyappa', 'ayyappa', '2019-08-19 11:06:32', '2019-08-19 05:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `course` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `departments_id`, `course`, `slug`, `content`, `description`, `image`, `created_at`, `updated_at`) VALUES
(17, 10, 'B.Sc. Geology', 'bsc-geology', '<p>Geology is an Earth science concerned with the solid earth, the rock of which it is composed and the processes by which they change over time. Geology can refer to the study of the solid features of any planet or natural satellite(Such as Mars or the Moon).</p><p>Geology describes the structure of the earth beneath its surface, and the processes that have shaped structure. An important of geology is the study of how earth’s materials, structures processes and organisms have changed overtime.\r\n\r\n</p><h3>JOB OPPORTUNITIES</h3><p></p><p>Variety of companies offer job to geologist like oil and natural gas corporation (ONGC), Geological survey of India (GSI), CPWD, TWAD, PWD, DGM, and many private and government organizations with interests in Natural Resource Exploitation.</p><p>The research degrees provide a higher level of training often in a geology specially areas such as paleontology mineralogy hydrology or volcano logy. Employment opportunities for geologists very good.</p>', '', '2019/8/dpm-8444-1-768x5124.jpg', '2019-08-16 05:16:01', '2019-08-15 23:46:01'),
(18, 11, 'B.Sc.Petrochemical', 'bscpetrochemical', '<p>BSC Petrochemicals is a UG course which prepares an aspiring student for a bright future in the petrochemical industry. This is a very rare course. AIMS, Ayyappa Institute of Management Studies is one of the top institutions providing this course under MG University Kottayam.</p><p>Petrochemical industries are at great importance in the context at present business scenario. Petrochemical products are obtained from refining crude oil and it is needless to mention that the entire procedure is quite extensive. This is why petrochemicals or petroleum products have a high price in the market. Newer ending opportunities are waiting for a petrochemical student. 1000’s of jobs are awaiting qualified candidates as the expansion of cochin Refineries is nearing completion.\r\n\r\n</p>', '<h3>Eligibility</h3>\r\n<p>\r\nA pass in Plus Two / Equivalent examination with Chemistry as one of the optional subjects</p><h3>B.Sc.Petrochemical + Advanced diploma in</h3><ol><li>Oil & Gas Safety Management</li><li>Process & Applications of Petroleum Products</li></ol><h3>JOB OPPORTUNITIES</h3><ul><li>Petroleum Refineries</li><li>Polymer Industries</li><li>Paint Industries</li><li>Food Industry</li><li>Pharmaceutical Industry</li><li>Auto Mobile Industry</li></ul><h3>Research Areas</h3><ul><li>Nanotechnology</li><li>Polymers</li><li>Research on fuel technology</li></ul>', '2019/8/dpm-8068.jpg', '2019-08-16 10:26:53', '2019-08-16 04:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'Department of PetroChecmicals', '2019-08-19 10:43:34', '2019-08-19 05:13:34'),
(10, 'Department of Geology', '2019-08-06 06:28:17', '2019-08-06 06:28:17'),
(13, 'Department of commerce', '2019-08-19 05:13:59', '2019-08-19 05:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `eligibility` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(500) DEFAULT NULL,
  `to_name` varchar(500) DEFAULT NULL,
  `to_email` varchar(500) DEFAULT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_email` varchar(500) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email_body` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `key`, `from_name`, `from_email`, `to_name`, `to_email`, `cc_name`, `cc_email`, `subject`, `email_body`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Registration notification to admin', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.raju@gmail.com', 'Sanoop Saji', 'sanoopsaji@gmail.com', 'Registration notification', '<div>\r\n<h1>NEW USER REGISTRATION</h1>\r\n\r\n<h3 style=\"font-size:30px;font-weight:400;color:#000;\"><span style=\"font-size: 13px;\">Hi ,&nbsp;<br>\r\nNew user has been registered with following details:</span></h3><p>Name&nbsp; &nbsp;: [username]</p><p>Email&nbsp; &nbsp;: [email]</p><p>Phone&nbsp; : [phone]</p>\r\n<br>\r\nThanks,<br><strong>\r\n<a href=\"#\">Admin Library</a></strong>&nbsp;</div>', 1, 1, '2018-09-11 04:38:25', '2018-09-14 10:57:36'),
(2, 'Test', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.rk@acodez.co.in', 'Sanoop V', 'sanoop.v@acodez.in', 'Testing', '<p>This is just a <strong>dummy </strong>content...</p>', 0, 1, '2018-09-12 07:13:55', '2019-08-01 05:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 'content not available', '<p>content not available</p>', '2019/8/img-not-available.png', '2019-08-20 10:32:39', '2019-08-20 10:41:54'),
(3, 'content not available', '<p>content not available</p>', '2019/8/img-not-available1.png', '2019-08-20 10:47:19', '2019-08-20 10:47:19');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `album_id`, `filename`, `created_at`, `updated_at`) VALUES
(10, 10, '2019/8/1565750958935it.jpg', '2019-08-13 21:19:19', '2019-08-13 21:19:19'),
(11, 10, '2019/8/1565750993356its.jpg', '2019-08-13 21:19:53', '2019-08-13 21:19:53'),
(22, 11, '2019/8/1565757302324it.jpg', '2019-08-13 23:05:02', '2019-08-13 23:05:02'),
(25, 11, '2019/8/1565759217536it.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(26, 11, '2019/8/1565759217539its.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(28, 10, '2019/8/1565961995368about-pic.png', '2019-08-16 07:56:35', '2019-08-16 07:56:35'),
(29, 10, '2019/8/1565962200607dpm-8068.jpg', '2019-08-16 08:00:00', '2019-08-16 08:00:00'),
(30, 12, '2019/8/1566025936417dpm-8130.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(31, 12, '2019/8/1566025936419dpm-8191.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(32, 12, '2019/8/1566025936420dpm-8291.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(33, 12, '2019/8/1566025936421dpm-8314.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(34, 12, '2019/8/1566025936422dpm-8370.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(35, 12, '2019/8/1566025936424dpm-8398.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(36, 12, '2019/8/1566025936426dpm-8418.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(37, 12, '2019/8/1566025936427dpm-8438.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(40, 12, '2019/8/15660372184301111.jpg', '2019-08-17 04:50:18', '2019-08-17 04:50:18'),
(39, 12, '2019/8/1566025936429dpm-9119.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(2, 'Jayesh J Kumar', '<p>I feel enthused. A million accomplished.</p><p>\r\n\r\nAlways wanted a worry-free environment, up in the hills to establish a college, away from the hassles of the set pattern of most campuses today, free from congestion and pollution.\r\n\r\n</p><p>\r\n\r\nThe college is snuggled in the hill station of Peermade, high up in the Western Ghats in Kerala, amidst spectacular waterfalls with uncompromising forest, open grasslands, pine forest, tea, coffee and spice plantations around. Guaranteed to offer a fulfilling standard of academic life. The college provides besides studies, extensive opportunities for continued personal growth in a compassionate milieu. Education with health, in the cool climates of Peermade with its stunning areas of natural beauty; what more can you ask for!\r\n\r\n</p><p>\r\n\r\nThe college is intended for students preparing for a career to move forward on the strength of intellect and imagination. We are committed to providing students with the freedom and care to go all out for their personal best.\r\n\r\n</p><p>\r\n\r\nIt has all the facilities and offers a broad range of services, catering to the needs of students; excellent and well-equipped Labs where students obtain valuable practical familiarity. Business students benefit by obtaining career-related practical experience in the well-established simulated enterprise offices.\r\n\r\n</p><p>\r\n\r\nIt is our endeavor to nurture the college to a preeminent academy of the nation and strive to sustain and augment its quality in teaching, research, public service, and economic development. We will have the best guest and visiting lecturers and speakers from various industries and from an assortment of renounced organizations imparting intellectual lessons and classes to our students. This is not to mention our planned roadmap to put our institution within the radar of all major Industries and Organizations and put in place a worldwide campus selection mechanism.\r\n\r\n</p><p>\r\n\r\nWe know how parents from middle-class background scarify all their base personal needs to the education of their children – what do they expect in return? Nothing short of a well-disciplined education and remunerative employment – thus we account for each rupee they spend.\r\n\r\n</p><p>\r\n\r\nWell, what more can I say to trust parents and earnest students? AIMS has been, is and always will be at your service.\r\n\r\n</p><p>\r\n\r\nI wish you all the best.\r\n\r\n</p>', '2019-08-19 05:13:41', '2019-08-19 12:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `vission` longtext NOT NULL,
  `mission` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `vission`, `mission`, `created_at`, `updated_at`) VALUES
(2, '<p>To emerge as a premier world-class college educating and creating knowledge and positioning knowledge on a level of excellence. To come forward as a brilliant school of learning in which the students, faculty, and staff thrive and the nation and the world benefit.</p>', '<p>To create a center of excellence in different streams; impart skills and competencies to shape global citizens; establish aims as an institution.</p>', '2019-08-19 12:33:39', '2019-08-19 07:03:39');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`) VALUES
(8, 'Admission', 'admission', '<p>Admission \r\n\r\nStarted\r\n\r\n for the year 2019&nbsp;</p>', '2019/8/about-pic3.png', '2019-08-16 23:30:28', '2019-08-16 23:46:59'),
(9, 'Admission', 'admission-2', '<p>Admission Started for the year 2019&nbsp;</p>', '2019/8/about-pic4.png', '2019-08-16 23:48:21', '2019-08-16 23:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `meta_key` text CHARACTER SET utf8,
  `meta_description` text CHARACTER SET utf8,
  `slug` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `meta_key`, `meta_description`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>This is just a dummy content</p>', NULL, NULL, 'about-us', 1, '2018-09-11 06:02:45', '2018-09-12 06:25:00'),
(2, 'Services', '<p>This is just a dummy content.</p>', NULL, NULL, 'services', 1, '2018-09-11 06:05:29', '2018-09-12 06:18:06'),
(3, 'contact', '<p>information</p>', 'contact', 'contact', 'contact', 1, '2019-08-13 05:01:33', '2019-08-13 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(1, 'browse_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(2, 'read_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(3, 'edit_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(4, 'add_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(5, 'delete_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(6, 'browse_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(7, 'read_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(8, 'edit_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(9, 'add_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(10, 'delete_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(11, 'browse_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(12, 'read_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(13, 'edit_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(14, 'add_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(15, 'delete_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(16, 'browse_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(17, 'read_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(18, 'edit_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(19, 'add_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(20, 'delete_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(21, 'browse_classification', 'classification', '2018-03-29 00:06:51', '2018-03-29 00:06:51', 5),
(22, 'read_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(23, 'edit_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(24, 'add_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(25, 'delete_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(26, 'browse_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(27, 'read_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(28, 'edit_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(29, 'add_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(30, 'delete_category', 'category', '2018-03-29 07:09:17', '2018-03-29 07:09:17', 6),
(31, 'browse_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(32, 'read_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(33, 'edit_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(34, 'add_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(35, 'delete_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(36, 'browse_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(37, 'read_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(38, 'edit_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(39, 'add_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(40, 'delete_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(41, 'browse_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(42, 'read_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(43, 'edit_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(44, 'add_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(45, 'delete_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(46, 'browse_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(47, 'read_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(48, 'edit_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(49, 'add_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(50, 'delete_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(51, 'browse_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(52, 'read_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(53, 'edit_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(54, 'add_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(55, 'delete_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(56, 'browse_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(57, 'read_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(58, 'edit_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(59, 'add_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(60, 'delete_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(61, 'browse_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(62, 'read_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(63, 'edit_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(64, 'add_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(65, 'delete_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(66, 'browse_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(67, 'read_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(68, 'edit_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(69, 'add_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(70, 'delete_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(71, 'browse_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(72, 'read_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(73, 'edit_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(74, 'add_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(75, 'delete_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(76, 'browse_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(77, 'read_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(78, 'edit_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(79, 'add_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(80, 'delete_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(81, 'browse_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(82, 'read_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(83, 'edit_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(84, 'add_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(85, 'delete_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(86, 'browse_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(87, 'read_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(88, 'edit_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(89, 'add_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(90, 'delete_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(91, 'browse_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(92, 'read_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(93, 'edit_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(94, 'add_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(95, 'delete_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(96, 'browse_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(97, 'read_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(98, 'edit_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(99, 'add_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(100, 'delete_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(101, 'browse_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(102, 'read_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(103, 'edit_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(104, 'add_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(105, 'delete_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(106, 'browse_gallery', 'gallery', '2019-08-12 22:58:02', '2019-08-12 22:58:02', 23),
(107, 'read_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(108, 'edit_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(109, 'add_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(110, 'delete_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(111, 'browse_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(112, 'read_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(113, 'edit_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(114, 'add_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(115, 'delete_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(116, 'browse_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(117, 'read_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(118, 'edit_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(119, 'add_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(120, 'delete_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(121, 'browse_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(122, 'read_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(123, 'edit_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(124, 'add_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(125, 'delete_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(126, 'browse_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(127, 'read_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(128, 'edit_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(129, 'add_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(130, 'delete_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(131, 'browse_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(132, 'read_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(133, 'edit_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(134, 'add_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(135, 'delete_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'System Administrator', '2018-02-07 06:39:33', '2018-09-12 06:01:57'),
(2, 'user', 'Normal User', '2018-02-07 06:39:33', '2018-09-12 04:31:00'),
(3, 'sub_admin', 'Sub admin', '2018-03-28 01:38:44', '2018-03-28 01:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biology', '2019-08-06 04:42:18', '2019-08-06 04:42:18'),
(2, 'computer science', '2019-08-06 04:47:06', '2019-08-06 04:47:06'),
(3, 'humanities', '2019-08-06 04:47:15', '2019-08-06 04:47:15'),
(5, 'any', '2019-08-12 09:36:28', '2019-08-12 09:36:28'),
(6, 'commerce', '2019-08-12 09:36:43', '2019-08-12 09:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Affiliated to M.G.University, Kottayam', '2019/8/banner21.jpg', '2019-08-17 05:31:11', '2019-08-17 05:32:03'),
(3, 'Affiliated to M.G.University, Kottayam', '2019/8/banner32.jpg', '2019-08-17 05:31:24', '2019-08-17 05:31:24'),
(4, 'Affiliated to M.G.University, Kottayam', '2019/8/banner4.jpg', '2019-08-17 05:31:49', '2019-08-17 05:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `created_at`, `updated_at`) VALUES
(24, 'Mathematics', '2019-08-15 05:49:56', '2019-08-15 05:49:56'),
(20, 'physcics', '2019-08-09 05:53:37', '2019-08-09 05:53:37'),
(21, 'Chemistry', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(22, 'Geology', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(23, 'Biology', '2019-08-15 05:47:35', '2019-08-15 05:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Jayesh J Kumar', '<p>Always wanted a worry-free environment, up in the hills to establish a college, away from the hassles of the set pattern of most campuses today, free from congestion and pollution.</p>', '2019/8/dpm-9119.jpg', '2019-08-19 11:40:09', '2019-08-19 11:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `temp_phone` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `login_otp` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `otp_status` tinyint(2) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  `blocked` tinyint(2) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `temp_email`, `password`, `phone`, `temp_phone`, `otp`, `login_otp`, `role_id`, `avatar`, `remember_token`, `activation_token`, `active`, `otp_status`, `token`, `blocked`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin@admin.com', NULL, '$2y$10$j9iaCUOR3tQHAnOlh2TKquxESTmhCITukHeGQR6dnyHacKxBr1YS.', '94247760947', '+96698989898989', 4799, NULL, 1, '2018/8/screenshot-iytimgcom-2016-12-02-12-07-35.png', '7DaIsPwylKRl4c8jHpkABJ6e1jj2jPR7v5HO8xYsY7Hw1ISMtIo1JCmDGf0w', NULL, 1, 1, '', 0, 'active', '2019-10-24 13:20:34', '2018-03-23 06:50:19', '2019-10-24 07:50:34', '0000-00-00 00:00:00'),
(2, 'Vasanthan', 'R K', 'vasanthan.rk@acodez.co.in', NULL, '$2y$10$/2RMCfB7LDOmXrFPwtbwduntpqYtfDaY3qqUekozhdpBpUQWyz5/m', '15454545', NULL, NULL, NULL, 3, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-29 00:44:20', '2019-10-24 08:07:25', NULL),
(3, 'Nandul', 'Das', 'nandul.das@acodez.co.in', NULL, '$2y$10$yElsIKQuA3i6TQuCW6OTyOd0F3S4KihLqepjXbw7masvm21UhKuyy', '123456789', NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-30 05:54:53', '2019-10-24 08:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`id`, `name`, `address`, `phone`, `email`, `reason`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth (h)\r\nperinkary(po)\r\nkannur', '9061395796', 'subinakandoth@gmail.com', 'Admission', '2019-07-10', '03:25:00', '2019-07-29 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `vission`
--

CREATE TABLE `vission` (
  `id` int(11) NOT NULL,
  `vission` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vission`
--

INSERT INTO `vission` (`id`, `vission`, `created_at`, `updated_at`) VALUES
(1, '<p>ghhsasa</p>', '2019-08-15 04:26:57', '2019-08-15 04:26:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vission`
--
ALTER TABLE `vission`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vission`
--
ALTER TABLE `vission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"sreevalsam\",\"table\":\"school_activitys\"},{\"db\":\"sreevalsam\",\"table\":\"activities\"},{\"db\":\"sreevalsam\",\"table\":\"album\"},{\"db\":\"sreevalsam\",\"table\":\"gallery\"},{\"db\":\"sreevalsam\",\"table\":\"slider\"},{\"db\":\"sreevalsam\",\"table\":\"school_students\"},{\"db\":\"sreevalsam\",\"table\":\"admission\"},{\"db\":\"sreevalsam\",\"table\":\"about\"},{\"db\":\"sreevalsam\",\"table\":\"whychoose\"},{\"db\":\"sreevalsam\",\"table\":\"message\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2019-09-25 17:44:16', '{\"collation_connection\":\"utf8mb4_unicode_ci\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;--
-- Database: `sreevalsam`
--
CREATE DATABASE IF NOT EXISTS `sreevalsam` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sreevalsam`;

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `main_description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `meta_tag` varchar(200) DEFAULT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `mission` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `main_description`, `image`, `slug`, `meta_tag`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `mission`, `created_at`, `updated_at`) VALUES
(5, 'Welcome to Bethany Balikamadhom', '<p>Bethany Balikamadhom High School for Girls was established in the year 1931 May 18 by the Servant of God, Arch Bishop Mar Ivanios for the Sisters of the Imitation of Christ, Pathanamthitta Province with a vision of women empowerment.</p><p>Bethany Sisters , the gem of Syro Malankara Catholic Church, moulding the young generation with a view to reach out to the oppressed and the needy and to liberate them through education for the better citizenship.</p>', '<p>Sisters of the Imitation of Christ(Bethany Sisters), a Religious Congregation was founded by the Servant of God, Archbishop Mar Ivanios in 1925. It was the first monastic community for women in the Syro Malankara Catholic Church aimed at the women empowerment in particular and the spiritual and social renewal of the society in general. BBHS for Girls is a ChristianMinority Institution, an ever burning lamp of Nangiarkulangara locality. It has a unique vision andmission. We are standing at the brim of Navathi, shining like a star focusing on the Motto and Charism.</p><p>The prime goal of this age old Institution is Caracter Formation and Knowledge Enhance ment. The today’s gems of BBHS becomes tomorrow’s 916 carot jewels of the family, society and in the world at large. They acquire the values and skills because of the dedicated and selfless service of the Religious Sisters, Teachers and the Non teaching Staff.</p>', '2019/11/navya-nair.jpg', 'welcome-to-bethany-balikamadhom', NULL, 'about us', 'about us', 'about us', 'about us', 'about us', NULL, '2019-11-17 16:27:47', '2019-11-24 09:22:25');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` enum('facility','order','user','setting','role','classification','category','email','cms','module') DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', NULL, '2018-08-31 00:54:24', '2018-08-31 00:54:24'),
(2, 1, 1, 'user', NULL, '2018-08-31 00:54:48', '2018-08-31 00:54:48'),
(3, 1, 1, 'user', NULL, '2018-08-31 00:56:22', '2018-08-31 00:56:22'),
(4, 1, 1, 'user', NULL, '2018-08-31 00:56:33', '2018-08-31 00:56:33'),
(5, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:30:44', '2018-09-12 04:30:44'),
(6, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:31:03', '2018-09-12 04:31:03'),
(7, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 04:56:59', '2018-09-12 04:56:59'),
(8, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 05:55:38', '2018-09-12 05:55:38'),
(9, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:46', '2018-09-12 06:01:46'),
(10, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:57', '2018-09-12 06:01:57'),
(11, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:03', '2018-09-12 06:04:03'),
(12, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:23', '2018-09-12 06:04:23'),
(13, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:35', '2018-09-12 06:04:35'),
(14, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:32:50', '2018-09-12 06:32:50'),
(15, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:33:01', '2018-09-12 06:33:01'),
(16, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 22:59:25', '2018-09-12 22:59:25'),
(17, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:10', '2018-09-12 23:01:10'),
(18, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:20', '2018-09-12 23:01:20'),
(19, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:27', '2018-09-12 23:02:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:47', '2018-09-12 23:02:47'),
(21, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:15', '2018-09-12 23:03:15'),
(22, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:40', '2018-09-12 23:03:40'),
(23, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:54', '2018-09-12 23:03:54'),
(24, 1, 1, 'user', NULL, '2018-09-13 04:46:30', '2018-09-13 04:46:30'),
(25, 1, 1, 'user', NULL, '2018-09-13 04:47:20', '2018-09-13 04:47:20'),
(26, 1, 1, 'user', NULL, '2018-09-13 04:48:46', '2018-09-13 04:48:46'),
(27, 1, 1, 'user', NULL, '2018-09-13 04:52:00', '2018-09-13 04:52:00'),
(28, 1, 1, 'user', NULL, '2018-09-13 04:52:17', '2018-09-13 04:52:17'),
(29, 1, 18, 'module', 'New module has been created by:', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(30, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 02:56:44', '2019-07-29 02:56:44'),
(31, 1, 19, 'module', 'New module has been created by:', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(32, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:50:28', '2019-07-29 06:50:28'),
(33, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:53:26', '2019-07-29 06:53:26'),
(34, 1, 20, 'module', 'New module has been created by:', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(35, 1, 1, 'role', 'New role has been edited by:', '2019-07-30 22:58:34', '2019-07-30 22:58:34'),
(36, 1, 21, 'module', 'New module has been created by:', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(37, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 04:35:01', '2019-08-06 04:35:01'),
(38, 1, 22, 'module', 'New module has been created by:', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(39, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 05:32:55', '2019-08-06 05:32:55'),
(40, 1, 23, 'module', 'New module has been created by:', '2019-08-12 22:58:03', '2019-08-12 22:58:03'),
(41, 1, 1, 'role', 'New role has been edited by:', '2019-08-12 22:58:25', '2019-08-12 22:58:25'),
(42, 1, 24, 'module', 'New module has been created by:', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(43, 1, 1, 'role', 'New role has been edited by:', '2019-08-13 04:36:06', '2019-08-13 04:36:06'),
(44, 1, 25, 'module', 'New module has been created by:', '2019-08-14 00:16:42', '2019-08-14 00:16:42'),
(45, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 00:17:05', '2019-08-14 00:17:05'),
(46, 1, 26, 'module', 'New module has been created by:', '2019-08-14 05:56:10', '2019-08-14 05:56:10'),
(47, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 05:56:32', '2019-08-14 05:56:32'),
(48, 1, 27, 'module', 'New module has been created by:', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(49, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 07:52:44', '2019-08-14 07:52:44'),
(50, 1, 28, 'module', 'New module has been created by:', '2019-08-20 04:26:05', '2019-08-20 04:26:05'),
(51, 1, 1, 'role', 'New role has been edited by:', '2019-08-20 04:26:20', '2019-08-20 04:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `perifix` varchar(200) NOT NULL,
  `type` enum('parent','child') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 'User', 'user', 'parent', '2018-03-26 05:44:20', '2018-03-26 05:44:20'),
(2, 0, 'Roles', 'roles', 'parent', '2018-03-27 01:07:21', '2018-03-27 01:07:21'),
(3, 0, 'Module', 'module', 'parent', '2018-03-27 01:07:42', '2018-03-27 01:07:42'),
(4, 0, 'Admin User', 'admin_user', 'parent', '2018-03-27 23:00:33', '2018-03-27 23:00:33'),
(5, 0, 'Classification', 'classification', 'parent', '2018-03-29 00:06:51', '2018-03-29 00:06:51'),
(6, 0, 'Category', 'category', 'parent', '2018-03-29 07:09:16', '2018-03-29 07:09:16'),
(7, 0, 'CMS', 'cms', 'parent', '2018-03-29 22:48:40', '2018-03-29 22:48:40'),
(8, 0, 'Facility Management', 'facility_management', 'parent', '2018-05-06 23:21:17', '2018-05-06 23:21:17'),
(9, 0, 'Order Management', 'order', 'parent', '2018-05-07 23:47:47', '2018-05-07 23:47:47'),
(10, 0, 'Email Setting', 'email_template', 'parent', '2018-05-08 23:49:01', '2018-05-08 23:49:01'),
(11, 0, 'User Report', 'user_report', 'parent', '2018-05-13 22:40:52', '2018-05-13 22:40:52'),
(12, 0, 'Facility Report', 'facility_report', 'parent', '2018-05-14 22:02:29', '2018-05-14 22:02:29'),
(13, 0, 'Order Report', 'order_report', 'parent', '2018-05-15 00:45:33', '2018-05-15 00:45:33'),
(14, 0, 'Settings', 'setting', 'parent', '2018-05-16 22:17:17', '2018-05-16 22:17:17'),
(15, 0, 'Admin User', 'admin_user', 'parent', '2018-05-22 05:50:07', '2018-05-22 05:50:07'),
(16, 0, 'Static Content', 'static_content', 'parent', '2018-05-30 03:38:32', '2018-05-30 03:38:32'),
(17, 0, 'Testimonial', 'testimonial', 'parent', '2018-06-01 05:45:03', '2018-06-01 05:45:03'),
(18, 0, 'department', 'department', 'parent', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(19, 0, 'subject', 'subject', 'parent', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(20, 0, 'news', 'news', 'parent', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(21, 0, 'scheme', 'scheme', 'parent', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(22, 0, 'course', 'course', 'parent', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(23, 0, 'gallery', 'gallery', 'parent', '2019-08-12 22:58:02', '2019-08-12 22:58:02'),
(24, 0, 'album', 'album', 'parent', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(25, 0, 'about', 'about', 'parent', '2019-08-14 00:16:41', '2019-08-14 00:16:41'),
(26, 0, 'contact', 'contact', 'parent', '2019-08-14 05:56:09', '2019-08-14 05:56:09'),
(27, 0, 'slider', 'slider', 'parent', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(28, 0, 'facility', 'facility', 'parent', '2019-08-20 04:26:05', '2019-08-20 04:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `street` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`id`, `fname`, `lname`, `street`, `city`, `pincode`, `state`, `country`, `phone`, `email`, `course`, `created_at`, `updated_at`) VALUES
(5, 'SUBINA', 'KS', 'IRITTY', 'KANNUR', '670706', 'Kerala', 'INDIA', '9061395796', 'subinakandoth@gmail.com', 'B.Sc.Petrochemical', '2019-08-19 03:56:58', '2019-08-19 03:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `title`, `slug`, `main_image`, `status`, `created_at`, `updated_at`) VALUES
(19, 'Annual Event Party 2019', 'annual-event-partys-2019', '2019/11/kids3.jpg', 1, '2019-11-17 11:36:48', '2019-11-17 11:36:48'),
(17, 'sports ssday', 'sports-day', '2019/11/kids1.jpg', 1, '2019-12-08 15:51:29', '2019-12-08 10:21:29'),
(18, 'Annual Event Party 2019', 'annual-event-party-2019', '2019/11/kids2.jpg', 1, '2019-11-17 11:35:00', '2019-11-17 11:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `qualification` longtext NOT NULL,
  `experience` longtext NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(50) NOT NULL,
  `category` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `fname`, `lname`, `city`, `street`, `phone`, `email`, `qualification`, `experience`, `country`, `state`, `pincode`, `category`, `updated_at`, `created_at`) VALUES
(4, 'shijina', 'k', 'kannur', 'mattannur', '9446395796', 'shijina@gmail.com', 'MSc geology', 'fresher', 'INDIA', 'Kerala', '670709', 'Faculty', '2019-08-19 10:31:48', '2019-08-19 10:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `slug` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1. Default Item',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `slug`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'THREE', 'Three', 'next2', 0, 0, '2018-09-10', '2018-09-11'),
(10, 'gg', '', 'dd', 9, 0, '2018-09-10', '2018-09-10'),
(13, 'fgdgdgdsfgs', '', 'fgdgdgdsfgs', 9, 0, '2018-09-10', '2018-09-10'),
(15, '4', '', 'two', 12, 0, '2018-09-10', '2018-09-11'),
(16, '222', 'test', 'thhree', 1, 0, '2018-09-11', '2018-09-12'),
(26, 'ONE', 'one', 'sdfsdfsd', 0, 0, '2018-09-11', '2018-09-11'),
(30, '11', NULL, 'dummy', 26, 0, '2018-09-11', '2018-09-11'),
(31, '44', NULL, '44', 12, 0, '2018-09-11', '2018-09-11'),
(32, '444', NULL, '444', 12, 0, '2018-09-11', '2018-09-11'),
(35, 'htyyt', 'tjytjyjyj', 'htyyt', 0, 0, '2019-08-01', '2019-08-01'),
(36, 'jyjytjyt', NULL, 'jyjytjyt', 35, 0, '2019-08-01', '2019-08-01'),
(37, 'jyutyuytj', NULL, 'jyutyuytj', 35, 0, '2019-08-01', '2019-08-01'),
(38, 'Bachelor of Computer Application', 'zdxczxv', 'bachelor-of-computer-application', 1, 0, '2019-08-01', '2019-08-01'),
(39, 'Bsc. Geology', 'fghfh', 'bsc-geology', 35, 0, '2019-08-01', '2019-08-01'),
(40, 'we', 'wewre', 'we', 0, 0, '2019-08-01', '2019-08-01'),
(41, 'vc', NULL, 'vc', 40, 0, '2019-08-01', '2019-08-01'),
(42, 'vm', NULL, 'vm', 40, 0, '2019-08-01', '2019-08-01'),
(43, 'vk', NULL, 'vk', 40, 0, '2019-08-01', '2019-08-01'),
(45, 'ffgfg', 'gfgfg', 'ffgfg', 0, 0, '2019-08-01', '2019-08-01'),
(46, 'cvcvcv', NULL, 'cvcvcv', 45, 0, '2019-08-01', '2019-08-01'),
(47, 'cvgfhgh', 'fgh', 'cv', 35, 0, '2019-08-01', '2019-08-14'),
(49, 'xfd', NULL, 'xfd', 0, 0, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `phone` longtext NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `map` longtext NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `insta` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `meta_title` varchar(50) DEFAULT NULL,
  `meta_content` varchar(50) DEFAULT NULL,
  `meta_description` varchar(50) DEFAULT NULL,
  `meta_key` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `phone`, `email`, `address`, `map`, `facebook`, `twitter`, `linkedin`, `insta`, `youtube`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `created_at`, `updated_at`) VALUES
(6, 'B.B.H.S Nangiarkulangara', '+0479 241 0750', 'principal.aims1@gmail.com', '<p>Mavelikara Road, Nangiarkulangara, Kerala 690513</p>', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7868.548572652532!2d77.056052!3d9.571608!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb65e73f9454c0f0!2sAyyappa+Institute+of+Management+Studies%2C+Peermade!5e0!3m2!1sen!2sin!4v1566212709483!5m2!1sen!2sin', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', '2019-08-19 11:06:32', '2019-11-17 11:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `course` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `departments_id`, `course`, `slug`, `content`, `description`, `image`, `created_at`, `updated_at`) VALUES
(17, 10, 'B.Sc. Geology', 'bsc-geology', '<p>Geology is an Earth science concerned with the solid earth, the rock of which it is composed and the processes by which they change over time. Geology can refer to the study of the solid features of any planet or natural satellite(Such as Mars or the Moon).</p><p>Geology describes the structure of the earth beneath its surface, and the processes that have shaped structure. An important of geology is the study of how earth’s materials, structures processes and organisms have changed overtime.\r\n\r\n</p><h3>JOB OPPORTUNITIES</h3><p></p><p>Variety of companies offer job to geologist like oil and natural gas corporation (ONGC), Geological survey of India (GSI), CPWD, TWAD, PWD, DGM, and many private and government organizations with interests in Natural Resource Exploitation.</p><p>The research degrees provide a higher level of training often in a geology specially areas such as paleontology mineralogy hydrology or volcano logy. Employment opportunities for geologists very good.</p>', '', '2019/8/dpm-8444-1-768x5124.jpg', '2019-08-16 05:16:01', '2019-08-15 23:46:01'),
(18, 11, 'B.Sc.Petrochemical', 'bscpetrochemical', '<p>BSC Petrochemicals is a UG course which prepares an aspiring student for a bright future in the petrochemical industry. This is a very rare course. AIMS, Ayyappa Institute of Management Studies is one of the top institutions providing this course under MG University Kottayam.</p><p>Petrochemical industries are at great importance in the context at present business scenario. Petrochemical products are obtained from refining crude oil and it is needless to mention that the entire procedure is quite extensive. This is why petrochemicals or petroleum products have a high price in the market. Newer ending opportunities are waiting for a petrochemical student. 1000’s of jobs are awaiting qualified candidates as the expansion of cochin Refineries is nearing completion.\r\n\r\n</p>', '<h3>Eligibility</h3>\r\n<p>\r\nA pass in Plus Two / Equivalent examination with Chemistry as one of the optional subjects</p><h3>B.Sc.Petrochemical + Advanced diploma in</h3><ol><li>Oil & Gas Safety Management</li><li>Process & Applications of Petroleum Products</li></ol><h3>JOB OPPORTUNITIES</h3><ul><li>Petroleum Refineries</li><li>Polymer Industries</li><li>Paint Industries</li><li>Food Industry</li><li>Pharmaceutical Industry</li><li>Auto Mobile Industry</li></ul><h3>Research Areas</h3><ul><li>Nanotechnology</li><li>Polymers</li><li>Research on fuel technology</li></ul>', '2019/8/dpm-8068.jpg', '2019-08-16 10:26:53', '2019-08-16 04:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'Department of PetroChecmicals', '2019-08-19 10:43:34', '2019-08-19 05:13:34'),
(10, 'Department of Geology', '2019-08-06 06:28:17', '2019-08-06 06:28:17'),
(13, 'Department of commerce', '2019-08-19 05:13:59', '2019-08-19 05:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `eligibility` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(500) DEFAULT NULL,
  `to_name` varchar(500) DEFAULT NULL,
  `to_email` varchar(500) DEFAULT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_email` varchar(500) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email_body` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `key`, `from_name`, `from_email`, `to_name`, `to_email`, `cc_name`, `cc_email`, `subject`, `email_body`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Registration notification to admin', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.raju@gmail.com', 'Sanoop Saji', 'sanoopsaji@gmail.com', 'Registration notification', '<div>\r\n<h1>NEW USER REGISTRATION</h1>\r\n\r\n<h3 style=\"font-size:30px;font-weight:400;color:#000;\"><span style=\"font-size: 13px;\">Hi ,&nbsp;<br>\r\nNew user has been registered with following details:</span></h3><p>Name&nbsp; &nbsp;: [username]</p><p>Email&nbsp; &nbsp;: [email]</p><p>Phone&nbsp; : [phone]</p>\r\n<br>\r\nThanks,<br><strong>\r\n<a href=\"#\">Admin Library</a></strong>&nbsp;</div>', 1, 1, '2018-09-11 04:38:25', '2018-09-14 10:57:36'),
(2, 'Test', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.rk@acodez.co.in', 'Sanoop V', 'sanoop.v@acodez.in', 'Testing', '<p>This is just a <strong>dummy </strong>content...</p>', 0, 1, '2018-09-12 07:13:55', '2019-08-01 05:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 'content not available', '<p>content not available</p>', '2019/8/img-not-available.png', '2019-08-20 10:32:39', '2019-08-20 10:41:54'),
(3, 'content not available', '<p>content not available</p>', '2019/8/img-not-available1.png', '2019-08-20 10:47:19', '2019-08-20 10:47:19');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `album_id`, `filename`, `created_at`, `updated_at`) VALUES
(10, 10, '2019/8/1565750958935it.jpg', '2019-08-13 21:19:19', '2019-08-13 21:19:19'),
(11, 10, '2019/8/1565750993356its.jpg', '2019-08-13 21:19:53', '2019-08-13 21:19:53'),
(57, 19, '2019/11/15745738324771.jpg', '2019-11-24 00:07:12', '2019-11-24 00:07:12'),
(56, 19, '2019/11/1574573832481about-tab.png', '2019-11-24 00:07:12', '2019-11-24 00:07:12'),
(28, 10, '2019/8/1565961995368about-pic.png', '2019-08-16 07:56:35', '2019-08-16 07:56:35'),
(29, 10, '2019/8/1565962200607dpm-8068.jpg', '2019-08-16 08:00:00', '2019-08-16 08:00:00'),
(30, 12, '2019/8/1566025936417dpm-8130.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(31, 12, '2019/8/1566025936419dpm-8191.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(32, 12, '2019/8/1566025936420dpm-8291.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(33, 12, '2019/8/1566025936421dpm-8314.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(34, 12, '2019/8/1566025936422dpm-8370.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(35, 12, '2019/8/1566025936424dpm-8398.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(36, 12, '2019/8/1566025936426dpm-8418.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(37, 12, '2019/8/1566025936427dpm-8438.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(40, 12, '2019/8/15660372184301111.jpg', '2019-08-17 04:50:18', '2019-08-17 04:50:18'),
(39, 12, '2019/8/1566025936429dpm-9119.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(54, 17, '2019/11/1574573810519screenshot-from-2019-11-17-12-56-34.png', '2019-11-24 00:06:50', '2019-11-24 00:06:50'),
(58, 19, '2019/11/1574573832483babitha.jpg', '2019-11-24 00:07:12', '2019-11-24 00:07:12'),
(59, 19, '2019/11/1574573832485chithra.jpg', '2019-11-24 00:07:12', '2019-11-24 00:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(3, 'Principal\'s Message', '<p>“Little dreams can lead to greatness, Little victories to success. ‘it’s the little things that bring the greatest happiness”&nbsp;It’s my earnest wish to convey the feelings of appreciation, gratitude and joy as I write this message. Bethany Balikamadhom High School steps in to the 89th year of its establishment and service to the humanity with the Motto and Charism of women empowerment. First, I would like to express my sincere gratitude to Almighty God, who strengthen, guide this Institution and instill the grace of Holy Spirit. BBHS, while admiring and keeping its motto and charism, always aspiring to aim High in its quest for Excellence in imparting human, intellectual, spiritual and moral formation to the students.</p>', '2019-11-17 16:25:35', '2019-11-17 16:25:35');

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `vission` longtext NOT NULL,
  `mission` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `vission`, `mission`, `created_at`, `updated_at`) VALUES
(4, '<p>The Vision of BBHS for Girls is to form young generation who care , serve and love the downtrodden and the marginalized. Founder Mar Ivanios, the icon of the Teaching profession depicts as a “Friend & Guide” , one who devotes himself completely to the upliftment of the humanity.</p>', '<p>The Mission of BBHS for Girls is to continue the Teaching ministry of JESUS CHRIST in this world. BBHS aims at providing excellent academic formation along with artistic skills of International Standards, concentrates both in efficiency and value orientation and enable the students to face the emerging challenges.</p>', '2019-11-17 10:54:16', '2019-11-17 10:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`) VALUES
(8, 'Admission', 'admission', '<p>Admission \r\n\r\nStarted\r\n\r\n for the year 2019&nbsp;</p>', '2019/8/about-pic3.png', '2019-08-16 23:30:28', '2019-08-16 23:46:59'),
(9, 'Admission', 'admission-2', '<p>Admission Started for the year 2019&nbsp;</p>', '2019/8/about-pic4.png', '2019-08-16 23:48:21', '2019-08-16 23:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `meta_key` text CHARACTER SET utf8,
  `meta_description` text CHARACTER SET utf8,
  `slug` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `meta_key`, `meta_description`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>This is just a dummy content</p>', NULL, NULL, 'about-us', 1, '2018-09-11 06:02:45', '2018-09-12 06:25:00'),
(2, 'Services', '<p>This is just a dummy content.</p>', NULL, NULL, 'services', 1, '2018-09-11 06:05:29', '2018-09-12 06:18:06'),
(3, 'contact', '<p>information</p>', 'contact', 'contact', 'contact', 1, '2019-08-13 05:01:33', '2019-08-13 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(1, 'browse_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(2, 'read_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(3, 'edit_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(4, 'add_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(5, 'delete_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(6, 'browse_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(7, 'read_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(8, 'edit_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(9, 'add_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(10, 'delete_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(11, 'browse_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(12, 'read_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(13, 'edit_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(14, 'add_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(15, 'delete_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(16, 'browse_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(17, 'read_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(18, 'edit_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(19, 'add_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(20, 'delete_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(21, 'browse_classification', 'classification', '2018-03-29 00:06:51', '2018-03-29 00:06:51', 5),
(22, 'read_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(23, 'edit_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(24, 'add_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(25, 'delete_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(26, 'browse_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(27, 'read_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(28, 'edit_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(29, 'add_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(30, 'delete_category', 'category', '2018-03-29 07:09:17', '2018-03-29 07:09:17', 6),
(31, 'browse_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(32, 'read_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(33, 'edit_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(34, 'add_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(35, 'delete_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(36, 'browse_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(37, 'read_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(38, 'edit_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(39, 'add_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(40, 'delete_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(41, 'browse_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(42, 'read_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(43, 'edit_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(44, 'add_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(45, 'delete_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(46, 'browse_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(47, 'read_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(48, 'edit_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(49, 'add_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(50, 'delete_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(51, 'browse_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(52, 'read_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(53, 'edit_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(54, 'add_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(55, 'delete_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(56, 'browse_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(57, 'read_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(58, 'edit_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(59, 'add_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(60, 'delete_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(61, 'browse_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(62, 'read_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(63, 'edit_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(64, 'add_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(65, 'delete_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(66, 'browse_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(67, 'read_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(68, 'edit_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(69, 'add_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(70, 'delete_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(71, 'browse_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(72, 'read_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(73, 'edit_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(74, 'add_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(75, 'delete_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(76, 'browse_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(77, 'read_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(78, 'edit_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(79, 'add_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(80, 'delete_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(81, 'browse_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(82, 'read_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(83, 'edit_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(84, 'add_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(85, 'delete_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(86, 'browse_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(87, 'read_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(88, 'edit_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(89, 'add_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(90, 'delete_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(91, 'browse_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(92, 'read_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(93, 'edit_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(94, 'add_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(95, 'delete_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(96, 'browse_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(97, 'read_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(98, 'edit_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(99, 'add_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(100, 'delete_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(101, 'browse_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(102, 'read_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(103, 'edit_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(104, 'add_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(105, 'delete_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(106, 'browse_gallery', 'gallery', '2019-08-12 22:58:02', '2019-08-12 22:58:02', 23),
(107, 'read_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(108, 'edit_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(109, 'add_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(110, 'delete_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(111, 'browse_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(112, 'read_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(113, 'edit_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(114, 'add_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(115, 'delete_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(116, 'browse_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(117, 'read_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(118, 'edit_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(119, 'add_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(120, 'delete_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(121, 'browse_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(122, 'read_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(123, 'edit_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(124, 'add_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(125, 'delete_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(126, 'browse_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(127, 'read_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(128, 'edit_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(129, 'add_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(130, 'delete_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(131, 'browse_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(132, 'read_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(133, 'edit_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(134, 'add_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(135, 'delete_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'System Administrator', '2018-02-07 06:39:33', '2018-09-12 06:01:57'),
(2, 'user', 'Normal User', '2018-02-07 06:39:33', '2018-09-12 04:31:00'),
(3, 'sub_admin', 'Sub admin', '2018-03-28 01:38:44', '2018-03-28 01:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biology', '2019-08-06 04:42:18', '2019-08-06 04:42:18'),
(2, 'computer science', '2019-08-06 04:47:06', '2019-08-06 04:47:06'),
(3, 'humanities', '2019-08-06 04:47:15', '2019-08-06 04:47:15'),
(5, 'any', '2019-08-12 09:36:28', '2019-08-12 09:36:28'),
(6, 'commerce', '2019-08-12 09:36:43', '2019-08-12 09:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `school_activitys`
--

CREATE TABLE `school_activitys` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_activitys`
--

INSERT INTO `school_activitys` (`id`, `title`, `slug`, `main_image`, `status`, `created_at`, `updated_at`) VALUES
(17, 'activity 2', 'activity-2', '2019/11/2.jpg', 1, '2019-11-17 11:41:17', '2019-11-17 11:41:17'),
(16, 'activity 1', 'activity-1', '2019/11/3.jpg', 1, '2019-11-17 11:40:59', '2019-11-17 11:40:59'),
(18, 'activity 2', 'activity-2', '2019/11/1.jpg', 1, '2019-11-17 11:41:32', '2019-11-17 11:41:32');

-- --------------------------------------------------------

--
-- Table structure for table `school_students`
--

CREATE TABLE `school_students` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `image` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_students`
--

INSERT INTO `school_students` (`id`, `name`, `status`, `image`, `created_at`, `updated_at`) VALUES
(20, 'Navya Nair', 1, '2019/11/navya-nair.jpg', '2019-11-17 16:55:17', '2019-11-17 16:55:17'),
(21, 'Dr. Chithra', 1, '2019/11/chithra1.jpg', '2019-11-17 16:59:51', '2019-11-17 16:59:51'),
(22, 'Babitha Jayan', 1, '2019/11/babitha.jpg', '2019-11-17 17:00:03', '2019-11-17 17:00:03');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(19, 'baner 2', '2019/12/h1-s11.jpg', '2019-12-08 19:00:35', '2019-12-08 19:00:35'),
(18, 'banner', '2019/12/h1-s1.jpg', '2019-12-08 15:16:57', '2019-12-08 15:16:57');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `created_at`, `updated_at`) VALUES
(24, 'Mathematics', '2019-08-15 05:49:56', '2019-08-15 05:49:56'),
(20, 'physcics', '2019-08-09 05:53:37', '2019-08-09 05:53:37'),
(21, 'Chemistry', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(22, 'Geology', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(23, 'Biology', '2019-08-15 05:47:35', '2019-08-15 05:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Jayesh J Kumar', '<p>Always wanted a worry-free environment, up in the hills to establish a college, away from the hassles of the set pattern of most campuses today, free from congestion and pollution.</p>', '2019/8/dpm-9119.jpg', '2019-08-19 11:40:09', '2019-08-19 11:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `temp_phone` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `login_otp` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `otp_status` tinyint(2) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  `blocked` tinyint(2) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `temp_email`, `password`, `phone`, `temp_phone`, `otp`, `login_otp`, `role_id`, `avatar`, `remember_token`, `activation_token`, `active`, `otp_status`, `token`, `blocked`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin@admin.com', NULL, '$2y$10$j9iaCUOR3tQHAnOlh2TKquxESTmhCITukHeGQR6dnyHacKxBr1YS.', '94247760947', '+96698989898989', 4799, NULL, 1, '2018/8/screenshot-iytimgcom-2016-12-02-12-07-35.png', 'LaCWJA4HKqRhX3eH2f9hbDryxSBJQ6sbGRODVZX22JiZqM9P51fxORBW7YdZ', NULL, 1, 1, '', 0, 'active', '2019-11-17 06:05:35', '2018-03-23 06:50:19', '2019-11-17 00:35:35', '0000-00-00 00:00:00'),
(2, 'Vasanthan', 'R K', 'vasanthan.rk@acodez.co.in', NULL, '$2y$10$/2RMCfB7LDOmXrFPwtbwduntpqYtfDaY3qqUekozhdpBpUQWyz5/m', '15454545', NULL, NULL, NULL, 3, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-29 00:44:20', '2019-10-24 08:07:25', NULL),
(3, 'Nandul', 'Das', 'nandul.das@acodez.co.in', NULL, '$2y$10$yElsIKQuA3i6TQuCW6OTyOd0F3S4KihLqepjXbw7masvm21UhKuyy', '123456789', NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-30 05:54:53', '2019-10-24 08:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`id`, `name`, `address`, `phone`, `email`, `reason`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth (h)\r\nperinkary(po)\r\nkannur', '9061395796', 'subinakandoth@gmail.com', 'Admission', '2019-07-10', '03:25:00', '2019-07-29 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `vission`
--

CREATE TABLE `vission` (
  `id` int(11) NOT NULL,
  `vission` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vission`
--

INSERT INTO `vission` (`id`, `vission`, `created_at`, `updated_at`) VALUES
(1, '<p>ghhsasa</p>', '2019-08-15 04:26:57', '2019-08-15 04:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `whychoose`
--

CREATE TABLE `whychoose` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `whychoose`
--

INSERT INTO `whychoose` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(3, 'Great Faculties', '<p>Loving & caring teachers who are experienced with great knowledge and modern teaching methods.</p>', '2019-11-17 16:21:52', '2019-11-17 16:21:52'),
(4, 'Creative Lessons', '<p>Lessons are taken in audio visual class rooms using information technology.</p>', '2019-11-17 16:22:13', '2019-11-17 16:22:13'),
(5, 'Club Activities', '<p>About 25+ clubs are functioning in this school. Each student can be a member of any of this club</p>', '2019-11-17 16:22:33', '2019-11-17 16:22:33'),
(6, 'Best Environment', '<p>Purely eco friendly school where plastic products are strictly not allowed and bio friendly waste management system.</p>', '2019-11-17 16:23:07', '2019-11-17 16:23:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_activitys`
--
ALTER TABLE `school_activitys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_students`
--
ALTER TABLE `school_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vission`
--
ALTER TABLE `vission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whychoose`
--
ALTER TABLE `whychoose`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `school_activitys`
--
ALTER TABLE `school_activitys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `school_students`
--
ALTER TABLE `school_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vission`
--
ALTER TABLE `vission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `whychoose`
--
ALTER TABLE `whychoose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;--
-- Database: `sreevalsam-old`
--
CREATE DATABASE IF NOT EXISTS `sreevalsam-old` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sreevalsam-old`;

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `main_description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `meta_tag` varchar(200) DEFAULT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `mission` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `main_description`, `image`, `slug`, `meta_tag`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `mission`, `created_at`, `updated_at`) VALUES
(3, 'Ayyappa College', '<p>What you become depends on what you know, which again depends on what learn, and is ultimately a function of what you are taught. And who is bigger teacher than experience? At Ayyappa, we take pride in our sprawling a decade pedagogy experience, as year after year we have produced great learners who went on to become outstanding individuals, to leave a mark on their respective domains.</p>', '<p>Our focus is not just learning but the process of learning, for learning is a phase which begins and gradually blooms into a beautiful flower and hence every stage of this metamorphosis requires intense care, sincerity, and dedication. Our style of teaching knows no discrimination as we impart knowledge and wisdom just the way it’s expected of us.</p><p>Ayyappa institute of management studies was started in 2017 with an aim to impart quality education to students in the country. We offer highly demanded UG degree courses and one PG course in different streams. Ayyappa college is unique in the university which offers rare and highly job oriented courses in a serene and picturesque campus</p>', '2019/8/about-pic1.png', 'ayyappacollege', NULL, 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', 'Ayyappa College', NULL, '2019-08-16 12:07:12', '2019-08-17 05:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` enum('facility','order','user','setting','role','classification','category','email','cms','module') DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', NULL, '2018-08-31 00:54:24', '2018-08-31 00:54:24'),
(2, 1, 1, 'user', NULL, '2018-08-31 00:54:48', '2018-08-31 00:54:48'),
(3, 1, 1, 'user', NULL, '2018-08-31 00:56:22', '2018-08-31 00:56:22'),
(4, 1, 1, 'user', NULL, '2018-08-31 00:56:33', '2018-08-31 00:56:33'),
(5, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:30:44', '2018-09-12 04:30:44'),
(6, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:31:03', '2018-09-12 04:31:03'),
(7, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 04:56:59', '2018-09-12 04:56:59'),
(8, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 05:55:38', '2018-09-12 05:55:38'),
(9, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:46', '2018-09-12 06:01:46'),
(10, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:57', '2018-09-12 06:01:57'),
(11, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:03', '2018-09-12 06:04:03'),
(12, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:23', '2018-09-12 06:04:23'),
(13, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:35', '2018-09-12 06:04:35'),
(14, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:32:50', '2018-09-12 06:32:50'),
(15, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:33:01', '2018-09-12 06:33:01'),
(16, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 22:59:25', '2018-09-12 22:59:25'),
(17, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:10', '2018-09-12 23:01:10'),
(18, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:20', '2018-09-12 23:01:20'),
(19, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:27', '2018-09-12 23:02:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:47', '2018-09-12 23:02:47'),
(21, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:15', '2018-09-12 23:03:15'),
(22, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:40', '2018-09-12 23:03:40'),
(23, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:54', '2018-09-12 23:03:54'),
(24, 1, 1, 'user', NULL, '2018-09-13 04:46:30', '2018-09-13 04:46:30'),
(25, 1, 1, 'user', NULL, '2018-09-13 04:47:20', '2018-09-13 04:47:20'),
(26, 1, 1, 'user', NULL, '2018-09-13 04:48:46', '2018-09-13 04:48:46'),
(27, 1, 1, 'user', NULL, '2018-09-13 04:52:00', '2018-09-13 04:52:00'),
(28, 1, 1, 'user', NULL, '2018-09-13 04:52:17', '2018-09-13 04:52:17'),
(29, 1, 18, 'module', 'New module has been created by:', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(30, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 02:56:44', '2019-07-29 02:56:44'),
(31, 1, 19, 'module', 'New module has been created by:', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(32, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:50:28', '2019-07-29 06:50:28'),
(33, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:53:26', '2019-07-29 06:53:26'),
(34, 1, 20, 'module', 'New module has been created by:', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(35, 1, 1, 'role', 'New role has been edited by:', '2019-07-30 22:58:34', '2019-07-30 22:58:34'),
(36, 1, 21, 'module', 'New module has been created by:', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(37, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 04:35:01', '2019-08-06 04:35:01'),
(38, 1, 22, 'module', 'New module has been created by:', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(39, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 05:32:55', '2019-08-06 05:32:55'),
(40, 1, 23, 'module', 'New module has been created by:', '2019-08-12 22:58:03', '2019-08-12 22:58:03'),
(41, 1, 1, 'role', 'New role has been edited by:', '2019-08-12 22:58:25', '2019-08-12 22:58:25'),
(42, 1, 24, 'module', 'New module has been created by:', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(43, 1, 1, 'role', 'New role has been edited by:', '2019-08-13 04:36:06', '2019-08-13 04:36:06'),
(44, 1, 25, 'module', 'New module has been created by:', '2019-08-14 00:16:42', '2019-08-14 00:16:42'),
(45, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 00:17:05', '2019-08-14 00:17:05'),
(46, 1, 26, 'module', 'New module has been created by:', '2019-08-14 05:56:10', '2019-08-14 05:56:10'),
(47, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 05:56:32', '2019-08-14 05:56:32'),
(48, 1, 27, 'module', 'New module has been created by:', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(49, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 07:52:44', '2019-08-14 07:52:44'),
(50, 1, 28, 'module', 'New module has been created by:', '2019-08-20 04:26:05', '2019-08-20 04:26:05'),
(51, 1, 1, 'role', 'New role has been edited by:', '2019-08-20 04:26:20', '2019-08-20 04:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `perifix` varchar(200) NOT NULL,
  `type` enum('parent','child') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 'User', 'user', 'parent', '2018-03-26 05:44:20', '2018-03-26 05:44:20'),
(2, 0, 'Roles', 'roles', 'parent', '2018-03-27 01:07:21', '2018-03-27 01:07:21'),
(3, 0, 'Module', 'module', 'parent', '2018-03-27 01:07:42', '2018-03-27 01:07:42'),
(4, 0, 'Admin User', 'admin_user', 'parent', '2018-03-27 23:00:33', '2018-03-27 23:00:33'),
(5, 0, 'Classification', 'classification', 'parent', '2018-03-29 00:06:51', '2018-03-29 00:06:51'),
(6, 0, 'Category', 'category', 'parent', '2018-03-29 07:09:16', '2018-03-29 07:09:16'),
(7, 0, 'CMS', 'cms', 'parent', '2018-03-29 22:48:40', '2018-03-29 22:48:40'),
(8, 0, 'Facility Management', 'facility_management', 'parent', '2018-05-06 23:21:17', '2018-05-06 23:21:17'),
(9, 0, 'Order Management', 'order', 'parent', '2018-05-07 23:47:47', '2018-05-07 23:47:47'),
(10, 0, 'Email Setting', 'email_template', 'parent', '2018-05-08 23:49:01', '2018-05-08 23:49:01'),
(11, 0, 'User Report', 'user_report', 'parent', '2018-05-13 22:40:52', '2018-05-13 22:40:52'),
(12, 0, 'Facility Report', 'facility_report', 'parent', '2018-05-14 22:02:29', '2018-05-14 22:02:29'),
(13, 0, 'Order Report', 'order_report', 'parent', '2018-05-15 00:45:33', '2018-05-15 00:45:33'),
(14, 0, 'Settings', 'setting', 'parent', '2018-05-16 22:17:17', '2018-05-16 22:17:17'),
(15, 0, 'Admin User', 'admin_user', 'parent', '2018-05-22 05:50:07', '2018-05-22 05:50:07'),
(16, 0, 'Static Content', 'static_content', 'parent', '2018-05-30 03:38:32', '2018-05-30 03:38:32'),
(17, 0, 'Testimonial', 'testimonial', 'parent', '2018-06-01 05:45:03', '2018-06-01 05:45:03'),
(18, 0, 'department', 'department', 'parent', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(19, 0, 'subject', 'subject', 'parent', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(20, 0, 'news', 'news', 'parent', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(21, 0, 'scheme', 'scheme', 'parent', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(22, 0, 'course', 'course', 'parent', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(23, 0, 'gallery', 'gallery', 'parent', '2019-08-12 22:58:02', '2019-08-12 22:58:02'),
(24, 0, 'album', 'album', 'parent', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(25, 0, 'about', 'about', 'parent', '2019-08-14 00:16:41', '2019-08-14 00:16:41'),
(26, 0, 'contact', 'contact', 'parent', '2019-08-14 05:56:09', '2019-08-14 05:56:09'),
(27, 0, 'slider', 'slider', 'parent', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(28, 0, 'facility', 'facility', 'parent', '2019-08-20 04:26:05', '2019-08-20 04:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `street` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`id`, `fname`, `lname`, `street`, `city`, `pincode`, `state`, `country`, `phone`, `email`, `course`, `created_at`, `updated_at`) VALUES
(5, 'SUBINA', 'KS', 'IRITTY', 'KANNUR', '670706', 'Kerala', 'INDIA', '9061395796', 'subinakandoth@gmail.com', 'B.Sc.Petrochemical', '2019-08-19 03:56:58', '2019-08-19 03:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `category` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `category`, `slug`, `main_image`, `created_at`, `updated_at`) VALUES
(11, 'Onam cellebration', '', '2019/8/onam.jpg', '2019-08-13 22:30:10', '2019-08-13 22:30:10'),
(12, 'General', 'general', '2019/8/dpm-83141.jpg', '2019-08-17 01:41:51', '2019-08-17 01:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `qualification` longtext NOT NULL,
  `experience` longtext NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(50) NOT NULL,
  `category` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `fname`, `lname`, `city`, `street`, `phone`, `email`, `qualification`, `experience`, `country`, `state`, `pincode`, `category`, `updated_at`, `created_at`) VALUES
(4, 'shijina', 'k', 'kannur', 'mattannur', '9446395796', 'shijina@gmail.com', 'MSc geology', 'fresher', 'INDIA', 'Kerala', '670709', 'Faculty', '2019-08-19 10:31:48', '2019-08-19 10:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `slug` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1. Default Item',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `slug`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'THREE', 'Three', 'next2', 0, 0, '2018-09-10', '2018-09-11'),
(10, 'gg', '', 'dd', 9, 0, '2018-09-10', '2018-09-10'),
(13, 'fgdgdgdsfgs', '', 'fgdgdgdsfgs', 9, 0, '2018-09-10', '2018-09-10'),
(15, '4', '', 'two', 12, 0, '2018-09-10', '2018-09-11'),
(16, '222', 'test', 'thhree', 1, 0, '2018-09-11', '2018-09-12'),
(26, 'ONE', 'one', 'sdfsdfsd', 0, 0, '2018-09-11', '2018-09-11'),
(30, '11', NULL, 'dummy', 26, 0, '2018-09-11', '2018-09-11'),
(31, '44', NULL, '44', 12, 0, '2018-09-11', '2018-09-11'),
(32, '444', NULL, '444', 12, 0, '2018-09-11', '2018-09-11'),
(35, 'htyyt', 'tjytjyjyj', 'htyyt', 0, 0, '2019-08-01', '2019-08-01'),
(36, 'jyjytjyt', NULL, 'jyjytjyt', 35, 0, '2019-08-01', '2019-08-01'),
(37, 'jyutyuytj', NULL, 'jyutyuytj', 35, 0, '2019-08-01', '2019-08-01'),
(38, 'Bachelor of Computer Application', 'zdxczxv', 'bachelor-of-computer-application', 1, 0, '2019-08-01', '2019-08-01'),
(39, 'Bsc. Geology', 'fghfh', 'bsc-geology', 35, 0, '2019-08-01', '2019-08-01'),
(40, 'we', 'wewre', 'we', 0, 0, '2019-08-01', '2019-08-01'),
(41, 'vc', NULL, 'vc', 40, 0, '2019-08-01', '2019-08-01'),
(42, 'vm', NULL, 'vm', 40, 0, '2019-08-01', '2019-08-01'),
(43, 'vk', NULL, 'vk', 40, 0, '2019-08-01', '2019-08-01'),
(45, 'ffgfg', 'gfgfg', 'ffgfg', 0, 0, '2019-08-01', '2019-08-01'),
(46, 'cvcvcv', NULL, 'cvcvcv', 45, 0, '2019-08-01', '2019-08-01'),
(47, 'cvgfhgh', 'fgh', 'cv', 35, 0, '2019-08-01', '2019-08-14'),
(49, 'xfd', NULL, 'xfd', 0, 0, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `phone` longtext NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `map` longtext NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `insta` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `meta_title` varchar(50) DEFAULT NULL,
  `meta_content` varchar(50) DEFAULT NULL,
  `meta_description` varchar(50) DEFAULT NULL,
  `meta_key` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `phone`, `email`, `address`, `map`, `facebook`, `twitter`, `linkedin`, `insta`, `youtube`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `created_at`, `updated_at`) VALUES
(6, 'Ayyappa College', '<p>04869 252999</p>', 'principal.aims1@gmail.com', '<p>Ayyappa College Of Management Studies</p><p>55th Mile, Peermade, Idukki District, Kerala</p>', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7868.548572652532!2d77.056052!3d9.571608!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb65e73f9454c0f0!2sAyyappa+Institute+of+Management+Studies%2C+Peermade!5e0!3m2!1sen!2sin!4v1566212709483!5m2!1sen!2sin\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'ayyappa', 'ayyappacollege', 'ayyappacollege', 'ayyappa', 'ayyappacollege', 'ayyappa', 'ayyappa', 'ayyappa college', 'ayyappa', 'ayyappa', '2019-08-19 11:06:32', '2019-08-19 05:43:48');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `course` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `departments_id`, `course`, `slug`, `content`, `description`, `image`, `created_at`, `updated_at`) VALUES
(17, 10, 'B.Sc. Geology', 'bsc-geology', '<p>Geology is an Earth science concerned with the solid earth, the rock of which it is composed and the processes by which they change over time. Geology can refer to the study of the solid features of any planet or natural satellite(Such as Mars or the Moon).</p><p>Geology describes the structure of the earth beneath its surface, and the processes that have shaped structure. An important of geology is the study of how earth’s materials, structures processes and organisms have changed overtime.\r\n\r\n</p><h3>JOB OPPORTUNITIES</h3><p></p><p>Variety of companies offer job to geologist like oil and natural gas corporation (ONGC), Geological survey of India (GSI), CPWD, TWAD, PWD, DGM, and many private and government organizations with interests in Natural Resource Exploitation.</p><p>The research degrees provide a higher level of training often in a geology specially areas such as paleontology mineralogy hydrology or volcano logy. Employment opportunities for geologists very good.</p>', '', '2019/8/dpm-8444-1-768x5124.jpg', '2019-08-16 05:16:01', '2019-08-15 23:46:01'),
(18, 11, 'B.Sc.Petrochemical', 'bscpetrochemical', '<p>BSC Petrochemicals is a UG course which prepares an aspiring student for a bright future in the petrochemical industry. This is a very rare course. AIMS, Ayyappa Institute of Management Studies is one of the top institutions providing this course under MG University Kottayam.</p><p>Petrochemical industries are at great importance in the context at present business scenario. Petrochemical products are obtained from refining crude oil and it is needless to mention that the entire procedure is quite extensive. This is why petrochemicals or petroleum products have a high price in the market. Newer ending opportunities are waiting for a petrochemical student. 1000’s of jobs are awaiting qualified candidates as the expansion of cochin Refineries is nearing completion.\r\n\r\n</p>', '<h3>Eligibility</h3>\r\n<p>\r\nA pass in Plus Two / Equivalent examination with Chemistry as one of the optional subjects</p><h3>B.Sc.Petrochemical + Advanced diploma in</h3><ol><li>Oil & Gas Safety Management</li><li>Process & Applications of Petroleum Products</li></ol><h3>JOB OPPORTUNITIES</h3><ul><li>Petroleum Refineries</li><li>Polymer Industries</li><li>Paint Industries</li><li>Food Industry</li><li>Pharmaceutical Industry</li><li>Auto Mobile Industry</li></ul><h3>Research Areas</h3><ul><li>Nanotechnology</li><li>Polymers</li><li>Research on fuel technology</li></ul>', '2019/8/dpm-8068.jpg', '2019-08-16 10:26:53', '2019-08-16 04:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'Department of PetroChecmicals', '2019-08-19 10:43:34', '2019-08-19 05:13:34'),
(10, 'Department of Geology', '2019-08-06 06:28:17', '2019-08-06 06:28:17'),
(13, 'Department of commerce', '2019-08-19 05:13:59', '2019-08-19 05:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `eligibility` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(500) DEFAULT NULL,
  `to_name` varchar(500) DEFAULT NULL,
  `to_email` varchar(500) DEFAULT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_email` varchar(500) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email_body` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `key`, `from_name`, `from_email`, `to_name`, `to_email`, `cc_name`, `cc_email`, `subject`, `email_body`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Registration notification to admin', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.raju@gmail.com', 'Sanoop Saji', 'sanoopsaji@gmail.com', 'Registration notification', '<div>\r\n<h1>NEW USER REGISTRATION</h1>\r\n\r\n<h3 style=\"font-size:30px;font-weight:400;color:#000;\"><span style=\"font-size: 13px;\">Hi ,&nbsp;<br>\r\nNew user has been registered with following details:</span></h3><p>Name&nbsp; &nbsp;: [username]</p><p>Email&nbsp; &nbsp;: [email]</p><p>Phone&nbsp; : [phone]</p>\r\n<br>\r\nThanks,<br><strong>\r\n<a href=\"#\">Admin Library</a></strong>&nbsp;</div>', 1, 1, '2018-09-11 04:38:25', '2018-09-14 10:57:36'),
(2, 'Test', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.rk@acodez.co.in', 'Sanoop V', 'sanoop.v@acodez.in', 'Testing', '<p>This is just a <strong>dummy </strong>content...</p>', 0, 1, '2018-09-12 07:13:55', '2019-08-01 05:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 'content not available', '<p>content not available</p>', '2019/8/img-not-available.png', '2019-08-20 10:32:39', '2019-08-20 10:41:54'),
(3, 'content not available', '<p>content not available</p>', '2019/8/img-not-available1.png', '2019-08-20 10:47:19', '2019-08-20 10:47:19');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `album_id`, `filename`, `created_at`, `updated_at`) VALUES
(10, 10, '2019/8/1565750958935it.jpg', '2019-08-13 21:19:19', '2019-08-13 21:19:19'),
(11, 10, '2019/8/1565750993356its.jpg', '2019-08-13 21:19:53', '2019-08-13 21:19:53'),
(22, 11, '2019/8/1565757302324it.jpg', '2019-08-13 23:05:02', '2019-08-13 23:05:02'),
(25, 11, '2019/8/1565759217536it.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(26, 11, '2019/8/1565759217539its.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(28, 10, '2019/8/1565961995368about-pic.png', '2019-08-16 07:56:35', '2019-08-16 07:56:35'),
(29, 10, '2019/8/1565962200607dpm-8068.jpg', '2019-08-16 08:00:00', '2019-08-16 08:00:00'),
(30, 12, '2019/8/1566025936417dpm-8130.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(31, 12, '2019/8/1566025936419dpm-8191.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(32, 12, '2019/8/1566025936420dpm-8291.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(33, 12, '2019/8/1566025936421dpm-8314.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(34, 12, '2019/8/1566025936422dpm-8370.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(35, 12, '2019/8/1566025936424dpm-8398.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(36, 12, '2019/8/1566025936426dpm-8418.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(37, 12, '2019/8/1566025936427dpm-8438.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(40, 12, '2019/8/15660372184301111.jpg', '2019-08-17 04:50:18', '2019-08-17 04:50:18'),
(39, 12, '2019/8/1566025936429dpm-9119.jpg', '2019-08-17 01:42:16', '2019-08-17 01:42:16'),
(44, 11, '2019/11/1573147116022download.jpeg', '2019-11-07 11:48:36', '2019-11-07 11:48:36');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(2, 'Jayesh J Kumar', '<p>I feel enthused. A million accomplished.</p><p>\r\n\r\nAlways wanted a worry-free environment, up in the hills to establish a college, away from the hassles of the set pattern of most campuses today, free from congestion and pollution.\r\n\r\n</p><p>\r\n\r\nThe college is snuggled in the hill station of Peermade, high up in the Western Ghats in Kerala, amidst spectacular waterfalls with uncompromising forest, open grasslands, pine forest, tea, coffee and spice plantations around. Guaranteed to offer a fulfilling standard of academic life. The college provides besides studies, extensive opportunities for continued personal growth in a compassionate milieu. Education with health, in the cool climates of Peermade with its stunning areas of natural beauty; what more can you ask for!\r\n\r\n</p><p>\r\n\r\nThe college is intended for students preparing for a career to move forward on the strength of intellect and imagination. We are committed to providing students with the freedom and care to go all out for their personal best.\r\n\r\n</p><p>\r\n\r\nIt has all the facilities and offers a broad range of services, catering to the needs of students; excellent and well-equipped Labs where students obtain valuable practical familiarity. Business students benefit by obtaining career-related practical experience in the well-established simulated enterprise offices.\r\n\r\n</p><p>\r\n\r\nIt is our endeavor to nurture the college to a preeminent academy of the nation and strive to sustain and augment its quality in teaching, research, public service, and economic development. We will have the best guest and visiting lecturers and speakers from various industries and from an assortment of renounced organizations imparting intellectual lessons and classes to our students. This is not to mention our planned roadmap to put our institution within the radar of all major Industries and Organizations and put in place a worldwide campus selection mechanism.\r\n\r\n</p><p>\r\n\r\nWe know how parents from middle-class background scarify all their base personal needs to the education of their children – what do they expect in return? Nothing short of a well-disciplined education and remunerative employment – thus we account for each rupee they spend.\r\n\r\n</p><p>\r\n\r\nWell, what more can I say to trust parents and earnest students? AIMS has been, is and always will be at your service.\r\n\r\n</p><p>\r\n\r\nI wish you all the best.\r\n\r\n</p>', '2019-08-19 05:13:41', '2019-08-19 12:14:13');

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `vission` longtext NOT NULL,
  `mission` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `vission`, `mission`, `created_at`, `updated_at`) VALUES
(2, '<p>To emerge as a premier world-class college educating and creating knowledge and positioning knowledge on a level of excellence. To come forward as a brilliant school of learning in which the students, faculty, and staff thrive and the nation and the world benefit.</p>', '<p>To create a center of excellence in different streams; impart skills and competencies to shape global citizens; establish aims as an institution.</p>', '2019-08-19 12:33:39', '2019-08-19 07:03:39');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`) VALUES
(8, 'Admission', 'admission', '<p>Admission \r\n\r\nStarted\r\n\r\n for the year 2019&nbsp;</p>', '2019/8/about-pic3.png', '2019-08-16 23:30:28', '2019-08-16 23:46:59'),
(9, 'Admission', 'admission-2', '<p>Admission Started for the year 2019&nbsp;</p>', '2019/8/about-pic4.png', '2019-08-16 23:48:21', '2019-08-16 23:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `meta_key` text CHARACTER SET utf8,
  `meta_description` text CHARACTER SET utf8,
  `slug` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `meta_key`, `meta_description`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>This is just a dummy content</p>', NULL, NULL, 'about-us', 1, '2018-09-11 06:02:45', '2018-09-12 06:25:00'),
(2, 'Services', '<p>This is just a dummy content.</p>', NULL, NULL, 'services', 1, '2018-09-11 06:05:29', '2018-09-12 06:18:06'),
(3, 'contact', '<p>information</p>', 'contact', 'contact', 'contact', 1, '2019-08-13 05:01:33', '2019-08-13 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(1, 'browse_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(2, 'read_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(3, 'edit_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(4, 'add_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(5, 'delete_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(6, 'browse_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(7, 'read_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(8, 'edit_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(9, 'add_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(10, 'delete_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(11, 'browse_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(12, 'read_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(13, 'edit_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(14, 'add_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(15, 'delete_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(16, 'browse_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(17, 'read_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(18, 'edit_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(19, 'add_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(20, 'delete_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(21, 'browse_classification', 'classification', '2018-03-29 00:06:51', '2018-03-29 00:06:51', 5),
(22, 'read_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(23, 'edit_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(24, 'add_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(25, 'delete_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(26, 'browse_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(27, 'read_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(28, 'edit_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(29, 'add_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(30, 'delete_category', 'category', '2018-03-29 07:09:17', '2018-03-29 07:09:17', 6),
(31, 'browse_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(32, 'read_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(33, 'edit_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(34, 'add_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(35, 'delete_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(36, 'browse_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(37, 'read_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(38, 'edit_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(39, 'add_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(40, 'delete_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(41, 'browse_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(42, 'read_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(43, 'edit_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(44, 'add_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(45, 'delete_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(46, 'browse_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(47, 'read_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(48, 'edit_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(49, 'add_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(50, 'delete_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(51, 'browse_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(52, 'read_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(53, 'edit_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(54, 'add_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(55, 'delete_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(56, 'browse_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(57, 'read_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(58, 'edit_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(59, 'add_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(60, 'delete_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(61, 'browse_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(62, 'read_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(63, 'edit_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(64, 'add_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(65, 'delete_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(66, 'browse_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(67, 'read_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(68, 'edit_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(69, 'add_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(70, 'delete_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(71, 'browse_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(72, 'read_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(73, 'edit_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(74, 'add_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(75, 'delete_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(76, 'browse_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(77, 'read_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(78, 'edit_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(79, 'add_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(80, 'delete_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(81, 'browse_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(82, 'read_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(83, 'edit_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(84, 'add_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(85, 'delete_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(86, 'browse_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(87, 'read_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(88, 'edit_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(89, 'add_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(90, 'delete_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(91, 'browse_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(92, 'read_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(93, 'edit_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(94, 'add_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(95, 'delete_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(96, 'browse_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(97, 'read_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(98, 'edit_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(99, 'add_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(100, 'delete_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(101, 'browse_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(102, 'read_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(103, 'edit_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(104, 'add_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(105, 'delete_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(106, 'browse_gallery', 'gallery', '2019-08-12 22:58:02', '2019-08-12 22:58:02', 23),
(107, 'read_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(108, 'edit_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(109, 'add_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(110, 'delete_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(111, 'browse_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(112, 'read_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(113, 'edit_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(114, 'add_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(115, 'delete_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(116, 'browse_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(117, 'read_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(118, 'edit_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(119, 'add_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(120, 'delete_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(121, 'browse_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(122, 'read_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(123, 'edit_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(124, 'add_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(125, 'delete_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(126, 'browse_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(127, 'read_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(128, 'edit_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(129, 'add_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(130, 'delete_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(131, 'browse_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(132, 'read_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(133, 'edit_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(134, 'add_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(135, 'delete_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'System Administrator', '2018-02-07 06:39:33', '2018-09-12 06:01:57'),
(2, 'user', 'Normal User', '2018-02-07 06:39:33', '2018-09-12 04:31:00'),
(3, 'sub_admin', 'Sub admin', '2018-03-28 01:38:44', '2018-03-28 01:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biology', '2019-08-06 04:42:18', '2019-08-06 04:42:18'),
(2, 'computer science', '2019-08-06 04:47:06', '2019-08-06 04:47:06'),
(3, 'humanities', '2019-08-06 04:47:15', '2019-08-06 04:47:15'),
(5, 'any', '2019-08-12 09:36:28', '2019-08-12 09:36:28'),
(6, 'commerce', '2019-08-12 09:36:43', '2019-08-12 09:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Affiliated to M.G.University, Kottayam', '2019/8/banner21.jpg', '2019-08-17 05:31:11', '2019-08-17 05:32:03'),
(3, 'Affiliated to M.G.University, Kottayam', '2019/8/banner32.jpg', '2019-08-17 05:31:24', '2019-08-17 05:31:24'),
(4, 'Affiliated to M.G.University, Kottayam', '2019/8/banner4.jpg', '2019-08-17 05:31:49', '2019-08-17 05:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `created_at`, `updated_at`) VALUES
(24, 'Mathematics', '2019-08-15 05:49:56', '2019-08-15 05:49:56'),
(20, 'physcics', '2019-08-09 05:53:37', '2019-08-09 05:53:37'),
(21, 'Chemistry', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(22, 'Geology', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(23, 'Biology', '2019-08-15 05:47:35', '2019-08-15 05:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Jayesh J Kumar', '<p>Always wanted a worry-free environment, up in the hills to establish a college, away from the hassles of the set pattern of most campuses today, free from congestion and pollution.</p>', '2019/8/dpm-9119.jpg', '2019-08-19 11:40:09', '2019-08-19 11:40:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `temp_phone` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `login_otp` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `otp_status` tinyint(2) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  `blocked` tinyint(2) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `temp_email`, `password`, `phone`, `temp_phone`, `otp`, `login_otp`, `role_id`, `avatar`, `remember_token`, `activation_token`, `active`, `otp_status`, `token`, `blocked`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin@admin.com', NULL, '$2y$10$j9iaCUOR3tQHAnOlh2TKquxESTmhCITukHeGQR6dnyHacKxBr1YS.', '94247760947', '+96698989898989', 4799, NULL, 1, '2018/8/screenshot-iytimgcom-2016-12-02-12-07-35.png', '7DaIsPwylKRl4c8jHpkABJ6e1jj2jPR7v5HO8xYsY7Hw1ISMtIo1JCmDGf0w', NULL, 1, 1, '', 0, 'active', '2019-11-07 15:49:29', '2018-03-23 06:50:19', '2019-11-07 10:19:29', '0000-00-00 00:00:00'),
(2, 'Vasanthan', 'R K', 'vasanthan.rk@acodez.co.in', NULL, '$2y$10$/2RMCfB7LDOmXrFPwtbwduntpqYtfDaY3qqUekozhdpBpUQWyz5/m', '15454545', NULL, NULL, NULL, 3, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-29 00:44:20', '2019-10-24 08:07:25', NULL),
(3, 'Nandul', 'Das', 'nandul.das@acodez.co.in', NULL, '$2y$10$yElsIKQuA3i6TQuCW6OTyOd0F3S4KihLqepjXbw7masvm21UhKuyy', '123456789', NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-30 05:54:53', '2019-10-24 08:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`id`, `name`, `address`, `phone`, `email`, `reason`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth (h)\r\nperinkary(po)\r\nkannur', '9061395796', 'subinakandoth@gmail.com', 'Admission', '2019-07-10', '03:25:00', '2019-07-29 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `vission`
--

CREATE TABLE `vission` (
  `id` int(11) NOT NULL,
  `vission` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vission`
--

INSERT INTO `vission` (`id`, `vission`, `created_at`, `updated_at`) VALUES
(1, '<p>ghhsasa</p>', '2019-08-15 04:26:57', '2019-08-15 04:26:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vission`
--
ALTER TABLE `vission`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vission`
--
ALTER TABLE `vission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
