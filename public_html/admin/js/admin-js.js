/**
 * Created by COMPUTER on 3/28/2018.
 */

 // Notification messages START::
function successMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "Update Complete : ",
            message: $msg,
            icon: 'fa fa-check',
        },{
            type: "success",
            delay: 500,
            placement: {
                align: 'right',
                from: "bottom"
            }
        });
    });
}

function errorMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "error : ",
            message: $msg,
            icon: 'fa fa-ban',
        },{
            type: "danger",
            delay: 700,
            placement: {
                align: 'right',
                from: "bottom"
            }
        });
    });
}

function warningMsg($msg) {
    $(document).ready(function () {
        $.notify({
            title: "Warning : ",
            message: $msg,
            icon: 'fa fa-exclamation-triangle'
        },{
            type: "warning",
            delay: 500,
            placement: {
                align: 'right'
            }
        });
    });
}
// Notification messages END::
/*Create slug */
var createSlug = function(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
    var to = "aaaaaeeeeeiiiiooooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
};

/*Create slug on change in title*/
$(document).on('change', '#title', function() {
    $slug = createSlug($(this).val());
    $('#slug').val($slug);
});
/*Create slug on change in title*/
$(document).on('change', '#name', function() {
    $slug = createSlug($(this).val());
    $('#nameslug').val($slug);
});
