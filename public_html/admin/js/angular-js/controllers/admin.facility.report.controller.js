/**
 * Created by COMPUTER on 5/14/2018.
 */
llmAdminApp.controller('adminFacilityReportController', function ($scope, $http, $compile, $timeout, $parse, $window) {

    $scope.firstLoad = true;
    $scope.filterView = true;
    $scope.loading = false;

    $scope.filters = {};
    $scope.filters.status = [];
    $scope.filters.column = [];

    $scope.reportDatas = {};

    $scope.showFilters = function ($event) {
        $event.preventDefault();
        if($scope.filterView == true){
            $scope.filterView = false;
        }else{
            $scope.filterView = true;
        }
    };

    $scope.generateReport = function($event){
        var url = base_url + '/get-facility-report';
        $scope.loading = true;

        $http.post(url, $scope.filters).then(function (response) {
            $scope.loading = false;
            $scope.firstLoad = false;
            if (response.data.status) {
                $scope.reportDatas = response.data.reportDatas;
                $scope.req = response.data.req;
            }
        }, function (response) {
            $scope.loading = false;
            $scope.firstLoad = false;
        });
    };

    $scope.getUrlParameters = function () {
        var flag = 0;
        var queryStr = window.location.search,
            queryArr = queryStr.replace('?','').split('&');
        for (var q = 0, qArrLength = queryArr.length; q < qArrLength; q++) {
            var qArr = queryArr[q].split('=');
            if(qArr.length > 1){
                if(decodeURIComponent(qArr[0]) == 'status[]'){
                    $scope.filters.status.push(qArr[1]);
                }else if(decodeURIComponent(qArr[0]) == 'column[]'){
                    $scope.filters.column.push(qArr[1]);
                }else{
                    $scope.filters[decodeURIComponent(qArr[0])] = decodeURIComponent(qArr[1]);
                }
                flag = 1;
            }
        }

        if(flag == 1){
            setTimeout(function () {
                $scope.generateReport();
            }, 500);
        }
    };

    $scope.filterData = function($event){
        $event.preventDefault();
        $scope.tableloading = true;
        $scope.pagination_url = '';

        var url = base_url+'/facility-report?';
        $scope.pageurl = url + $.param($scope.filters);

        setTimeout(function(){
            window.history.pushState({path:$scope.pageurl},'',$scope.pageurl);
        },200);

        $scope.generateReport();
    };

    $scope.selectAdminStatus = function (val) {
        var result = $scope.filters.status.indexOf(val);
        if(result != -1){
            $scope.filters.status.splice(result, 1);
        }else{
            $scope.filters.status.push(val);
        }
    };

    $scope.downloadPdf = function($event){
        var url = base_url+'/get-facility-report-pdf?';
        $scope.pageurl = url + $.param($scope.filters);
        $window.open($scope.pageurl, '_blank');
    };

    $scope.downloadExcel = function($event){
        var url = base_url+'/get-facility-report-exl?';
        $scope.pageurl = url + $.param($scope.filters);
        $window.open($scope.pageurl, '_blank');
    };

    $scope.getUrlParameters();
});