$(document).ready(function(){

    var table = $('#sampleTable').DataTable({
        "aaSorting": [],
        'order': [[1, 'desc']],
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
                j = 0;
                for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
                    j++;
                }
            }
        },
        serverSide: true,
        scrollX: true,
        oLanguage: {
            sProcessing: '<svg width="30px"  height="30px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: transparent;"><circle cx="50" cy="50" fill="none" stroke="#0fb963" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(228 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>'
        },
        processing: true,
        "pageLength": 10,
        "columnDefs": [{'orderable': false, 'targets': [0,2,2 ]}],
        mark: true,
        fixedColumns: {},
        ajax: {
            url:   '/admin/missionList',
        },
        columns: [
            
            {data: 'sl#', name: 'sl#'},
            {data: 'vission', name: 'vission'},

            {data: 'mission', name: 'mission'},
            {data: 'action', name: 'action'}
        ]
    });
    $('body').on('click', '.delete-featurestype', function () {

        var id = $(this).data("id") ;
        var url = base_url+'/featurestype/delete';

        swal({
            title: "Are you sure ?",
            text: "Once deleted, it will be removed from tourpakage!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: url,
                        type: 'POST',  // user.destroy
                        data:{
                            'id':id,
                        },
                        beforeSend: function(){
                            $("#if_loading").html("loading");
                        },
                        success: function(result) {
                            table.draw();

                            // Do something with the result
                        },
                        complete: function(){
                            $('#if_loading').html("");
                        }
                    });

                } else {
                    //
                }
            });

    });
});

