function callmodal(e) {
    $id = e;
    $('#btn-yes').attr("data-myval", $id);
    $('#myModal').modal('show');
}

function delPage(e) {
    var id = $('#btn-yes').attr("data-myval");
    $this = $(this);
    var data = {
        id: id
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: base_url + '/page/delete',
        data: data,
        type: 'POST',
        success: function (data) {

            table.ajax.reload();
            $('#myModal').modal('hide');

        }
    });
}
function changeEmailStatus(e, type) {

    if (e.checked) {

        var s = "1";

    } else {
        var s = "0";

    }

    var url = "ajax/changeEmailStatus";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: { id: e.id, status: s },
        success: function (result) {
            console.log(result);
            if (result.s == false) {
                iziToast.error({
                    title: 'Status',
                     targetFirst: true,
                     toastOnce:true,
                    message: result.msg
                });

            }
            else {
                iziToast.success({
                    title: 'Status',
                     targetFirst: true,
                   toastOnce:true,
                    message: result.msg
                });
            }

        },
        error: function (e) {
            console.log(e);
        }

    });
}


function changeBlock(e, type) {

    if (e.checked) {

        var s = 1;

    } else {
        var s = 0;

    }

    var url = "ajax/changeBlockStatus";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "POST",
        url: url,
        data: { id: e.id, status: s },
        success: function (result) {
      
            if (result.s == false) {
                table.ajax.reload();
                iziToast.success({
                    title: 'Block Status',
                    message: result.msg
                });

            }
            else {
                table.ajax.reload();
                iziToast.error({
                    title: 'Block Status',
                    message: result.msg
                });
            }


        }
    });
}
