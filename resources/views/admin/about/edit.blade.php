@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
                About Us Edit
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                        <a href="{!! URL::to('admin/about') !!}" class="btn btn-primary pull-right">Back </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{{url('about/edit',$about->id)}}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <div class="box-body">
                          
                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="{!! $about->title !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>

                            <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                    <label for="description">short description</label>
                                    <textarea name="short_desc" id="content">{{$about->short_description}}</textarea>
                                    <span>{{$errors->first('content')}}</span>
                                   
                                    
                                </div>
                                <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                    <label for="description">Main description</label>
                                    <textarea name="main_desc" id="content">{{$about->main_description}}</textarea>
                                    <span>{{$errors->first('content')}}</span>
                                   
                                    
                                </div>
                                <div class="form-group @if($errors->first('meta_title')) has-danger @endif">
                                    <label for="meta_title">Meta Title</label>
                                    <input class="form-control @if($errors->first('meta_title')) is-invalid @endif"
                                           id="meta_title" name="meta_title" type="text" placeholder="Enter meta name"
                                           autocomplete="off"  value="{!! $about->meta_title !!}">
                                    @if($errors->first('meta_title'))
                                    <label class="alert-danger">{{$errors->first('meta_title')}}</label>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('meta_title')) has-danger @endif">
                                        <label for="meta_title">Meta Content</label>
                                        <input class="form-control @if($errors->first('meta_title')) is-invalid @endif"
                                               id="meta_title" name="meta_content" type="text" placeholder="Enter meta name"
                                               autocomplete="off" value="{!! $about->meta_content !!}">
                                        @if($errors->first('meta_title'))
                                        <label class="alert-danger">{{$errors->first('meta_title')}}</label>
                                        @endif
                                    </div>
                                <div class="form-group @if($errors->first('meta_key')) has-danger @endif">
                                    <label for="meta_key">Meta Key</label>
                                    <input class="form-control @if($errors->first('meta_key')) is-invalid @endif"
                                           id="meta_key" name="meta_key" type="text" placeholder="Enter meta key"
                                           autocomplete="off"  value="{!! $about->meta_key !!}">
                                    @if($errors->first('meta_key'))
                                    <label class="alert-danger">{{$errors->first('meta_key')}}</label>
                                    @endif
                                </div>

                                <div class="form-group @if($errors->first('meta_description')) has-danger @endif">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea
                                            class="form-control @if($errors->first('meta_description')) is-invalid @endif"
                                            id="meta_description" name="meta_description" type="text"
                                            placeholder="Enter meta description" autocomplete="off"> {{$about->meta_description}}</textarea>
                                    @if($errors->first('meta_description'))
                                    <label class="alert-danger">{{$errors->first('meta_description')}}</label>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('meta_key')) has-danger @endif">
                                        <label for="meta_key">Author</label>
                                        <input class="form-control @if($errors->first('meta_key')) is-invalid @endif"
                                               id="meta_key" name="author" type="text" placeholder="Enter author"
                                               autocomplete="off"  value="{!! $about->author !!}">
                                        @if($errors->first('meta_key'))
                                        <label class="alert-danger">{{$errors->first('meta_key')}}</label>
                                        @endif
                                    </div>

                     
                           
                         
                        
                            
                        
                            <div>
                                    <div class="form-group @if($errors->first('image')) has-danger @endif">
                                            <label for="image">Image</label><br>
                                            <input   id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                            @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                                        </div>
                                        <img src="{{asset('media/profile-images/'.$about->image)}}" id="uploadPreview" width="150px">

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection
