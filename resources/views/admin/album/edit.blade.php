@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
        Service Edit
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Photo Category </h3>
                        <a href="{!! URL::to('admin/album') !!}" class="btn btn-primary pull-right">Back to  Album</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{{url('admin/album/edit/'.$album->id)}}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <div class="box-body">
                            
                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="name" class="form-control" placeholder="Enter Title" value="{!! $album->title !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>
                            
                            <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                            <label for="description">Description</label>
                                        <textarea class="form-control @if($errors->first('message')) is-invalid @endif" id="message" name="description"
                                                  placeholder="Enter description" autocomplete="off" value="{{old('message')}}" 
                                                  rows="10" style="height: 100px;">{!!$album->description!!}</textarea>
                                        </div>
                                       
                            <div class="form-group @if($errors->first('image')) has-danger @endif">
                                <label for="image">Image</label><br>
                                <input   id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                            </div>
                            <img src="{{asset('media/album/'.$album->main_image)}}" id="uploadPreview" width="150px">
                  

                            <div class="form-group @if($errors->first('image')) has-danger @endif">
                                <label for="image">Image</label><br>
                                <input   id="image" name="image1" type="file" accept="image/x-png,image/gif,image/jpeg">
                                @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                            </div>
                            <img src="{{asset('media/album/'.$album->main_image1)}}" id="uploadPreview" width="150px">


                            <div class="form-group @if($errors->first('image')) has-danger @endif">
                                <label for="image">Image</label><br>
                                <input   id="image" name="image2" type="file" accept="image/x-png,image/gif,image/jpeg">
                                @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                            </div>
                            <img src="{{asset('media/album/'.$album->main_image2)}}" id="uploadPreview" width="150px">
    

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{ asset('admin/custom/slug.js')}}"></script>

<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection
