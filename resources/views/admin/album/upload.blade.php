@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            GALLERY
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{!! $gallery->category !!} </h3>
                        <a href="{!! URL::to('admin/album') !!}" class="btn btn-primary pull-right">Back to  Gallery</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>
           
           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{{url('admin/album/upload/'.$gallery->id)}}" method="post"  enctype="multipart/form-data" class="dropzone" id="dropzone">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <input type="hidden" name="albumid" value="{{ $gallery->id }}">





    
                     </form>
                </div><!-- /.box -->
            
   </div>
   <section class="content-header">
    <h1>
        Uploaded Images

    </h1>
    <div class = "row">
   



            @foreach ($all_images as $images)
         
               
            <div class = "col-sm-6 col-md-3">
                    <div class = "thumbnail">
                    <img src = "{{getImageByPath($images->filename,'440*290','gallery-image')}}" alt = "Generic placeholder thumbnail"> 
                    </div>
                    
                    <div class = "caption">
                       
                       
                       <p>
                          <a   class = "btn btn-primary delete" data-content="upload" data-id={{$images->id}} role = "button">
                             Delete
                          </a> 
                        
                       </p>
                       
                    </div>
                 </div>
                   
            @endforeach
    





        
      
           
           
       
        
     </div>
    {{-- <table id="example2" class="table table-bordered dataTable table-hover" style="width: 100%">
        <thead>
            <tr>
                <th>S#</th>
                
               
                <th>Image</th>
               
                
                <th>Action</th>
            </tr>
      

         
    </table> --}}
  {{--   @include('include.breadcrub') --}}
</section>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->
    <script type="text/javascript">
        Dropzone.options.dropzone =
         {
            maxFilesize: 12,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 5000,
            removedfile: function(file) 
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                    type: 'POST',
                    url: '{{ url("image/delete") }}',
                    data: {filename: name},
                    success: function (data){
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
       
            success: function(file, response) 
            {
                console.log(response);
            },
            error: function(file, response)
            {
               return false;
            }
};
</script>

@endsection

@section('js')
<script src="{{asset('admin/js/custom/delete.js')}}"></script>

<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection