@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Applications

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
               
                <div class="box-body">

    @if(can('browse_module'))
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Category</th>
                            <th>Qualification</th>
                            <th>Experience</th>
                    
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        @foreach($careers as $career)
                        <?php $count++;?>
                        <tr>
                            <td>{!!$count!!}</td>
                            <td>{{$career->fname}}</td>
                            <td>{{$career->lname}}</td>
                            <td>{{$career->email}}</td>
                            <td>{{$career->phone}}</td>
                           <td>{{$career->category}}</td>
                           <td>{{$career->qualification}}</td>
                           <td>{{$career->experience}}</td>

    
                        
                    
                            
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection