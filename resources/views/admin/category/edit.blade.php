@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Category
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box clearfix">
				
				<div class="box-header">
                    <!-- <h3 class="box-title">Category</h3> -->
                  <a href="{!! URL::to('category') !!}" class="btn btn-primary pull-right">
					Back</a>
                </div><!-- /.box-header -->
				


                    <div class="box-header">
                        @if(Session::get('message'))
                            <div class="alert alert-success clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('message') !!}
                            </div>
                        @endif

                        @if(Session::get('error'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('error') !!}
                            </div>
                        @endif

                        @if(Session::has('validation_fail'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('validation_fail') !!}
                            </div>
                        @endif

                    </div><!-- /.box-header -->
					
					
					   <div class="col-xs-12 discussion-detail">   
    <form id="bic_form" action="{!! URL::to('category/edit/'.$data->id) !!}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="box-header">
                               <div class="col-md-4">
                               <h3 class="box-title">Parent Category</h3>
                               </div>    
    <div class="col-md-8">
        <select name="parent_category" class="form-control cls-bic-prnt-cat">
            <option value="">Select parent category</option>
            @if ($parentCategories->count())
                @foreach($parentCategories as $key => $val)
                    <option value="{{ $key }}" {{ $data->parent_id == $key ? 'selected="selected"' : '' }}>{{ $val }}</option>
                @endforeach
            @endif
        </select>
        <span class="required_message">{{$errors->first('parent_category')}}</span>
    </div>
    
    </div>
    
    <div class="box-header">
        <div class="col-md-4">
        <h3 class="box-title">Title</h3>
        </div>
        <div class="col-md-8">
        <input type="text" name="title" id="title" required class="form-control" placeholder="Enter Title" value="{!!$data->name!!}">
            <span class="required_message">{{$errors->first('title')}}</span>
        </div>
    </div>
     <div class="box-header">
        <div class="col-md-4">
        <h3 class="box-title">Description</h3>
        </div>
        <div class="col-md-8">
        <textarea type="text" name="description" id="description" required class="form-control" placeholder="Enter Description">{!!$data->description!!}</textarea>
            <span class="required_message">{{$errors->first('description')}}</span>
        </div>
    </div>
    <div class="box-header cls-cat-parent-empty">
    @if(count($data->getSubcategory)&& $data->parent_id==0)
        <div id="AudioWrapperr_two" class="col-xs-11 p-l-0 p-r-0">
            @php $i=0 @endphp
            @foreach($data->getSubcategory as $category)
                <div class="form-group @if($i>0) {!!'extra_two'!!} @endif clearfix form-group-bg-grey">
                    <div class="col-md-12">
                        <div class="col-md-6 p-l-0">			
                            @if($i==0)<label for="items">Add Sub-Category</label>@endif
                            <input type="text" name="items[{!!$i!!}]" class="form-control locat speciCls cls-sub-cat-title" id="items" placeholder="Title" value="{!!$category->name!!}">
                            <input type="hidden" name="hdn_id[]" class="form-control" value="{!!$category->id!!}">
                        </div>
                                                                                            
                        <div class="col-md-5 p-l-0 p-r-0">			
                            
                        </div>      
											
                        <div class="col-md-1">
                            @if($i>0)
                                <span class="remove_now_two close" style="margin-top:5px;"><i class="fa fa-close"></i></span>
                            @else
                                &nbsp;
                            @endif
                        </div>

                        <input type="hidden" name="base_url" id="base_url" value="{{URL::to('/')}}">
                    </div>
                </div>
                @php $i++ @endphp				
            @endforeach						
        </div>
         <div class="col-xs-1">
            <a href="#" id="AddMoreAudio_two" class="btn btn-success"><i class="fa fa-plus-square"></i></a>
        </div>   
        @else

<div id="AudioWrapperr_two" class="col-xs-11 p-l-0 p-r-0 thsclass" @if($data->parent_id!=0) style="display: none;" @else style="display: block;" @endif>
                <div class="form-group clearfix form-group-bg-grey">                                
                    <div class="col-md-6">
                        <label for="specification">Add Sub-category</label>
                        <input type="text" name="items[0]" class="form-control locat speciCls" id="items" placeholder="Title" value="">
                        <input type="hidden" name="items_type[0]" id="items_orgn" value="">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
             <div class="col-xs-1 thsclass" @if($data->parent_id!=0) style="display: none;" @else style="display: block;"  @endif>
            <a href="#" id="AddMoreAudio_two" class="btn btn-success"><i class="fa fa-plus-square"></i></a>
        </div> 
        @endif

   {{-- @if(!count($data->getSubcategory) && $data->parent_id==0)
            <div id="AudioWrapperr_two" class="col-xs-11 p-l-0 p-r-0">
                <div class="form-group clearfix form-group-bg-grey">         						
                    <div class="col-md-6">
                        <label for="specification">Add Sub-category</label>
                        <input type="text" name="items[0]" class="form-control locat speciCls" id="items" placeholder="Title" value="">
                        <input type="hidden" name="items_type[0]" id="items_orgn" value="">
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
             <div class="col-xs-1">
            <a href="#" id="AddMoreAudio_two" class="btn btn-success"><i class="fa fa-plus-square"></i></a>
        </div>   
@endif --}}

                        
    </div>
                       	<div class="box-header">
                            <div class="col-md-4">
                                <h3 class="box-title">&nbsp;</h3>
                            </div>
                            <div class="col-md-8">
                                <button type="submit" class="pull-right btn btn-primary margin-top cls-bic-updt">Update</button>
                            </div>
                        </div><!-- /.box-header -->
				
                    </form>
					</div>
					
					
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection

@section('additional_scripts')

    <script>

$(document).ready(function(){

    setTimeout(function(){ $('.msgalt').fadeOut(); }, 1000);
    
});

</script>

<script>

    
    // Add Repeater Field : start
    
    console.log($('.speciCls').length-1);
    
    var speci_field_index = $('.speciCls').length-1;
    $(document).ready(function () {


    var MaxInputsAudio = 50; //maximum input boxes allowed
    var InputsWrapper = $("#AudioWrapperr_two input"); //Input boxes wrapper ID
    var AddButton = "#AddMoreAudio_two"; //Add button ID

    var x = InputsWrapper.length; //initlal text box count
    var FieldCount = 1; //to keep track of text box added
    var count = $('.speciCls').length-1;
    $(AddButton).click(function (e) //on add input button click
    {
        count++;

        //console.log(count);

        e.preventDefault();
        InputsWrapper = $("#AudioWrapperr_two input");
        x = InputsWrapper.length;
        //console.log(x + '  ' + MaxInputsAudio);
        if (count < MaxInputsAudio) //max input box allowed
        {
            speci_field_index++;
            FieldCount++; //text box added increment
            //add input box
            $(InputsWrapper).parents('#AudioWrapperr_two').append('<div class="form-group extra_two form-group-bg-grey clearfix"><div class="col-md-12"><div class="col-md-6 p-l-0" style="padding-left:0;"><input type="text" required name="items['+speci_field_index+']" class="form-control cls-sub-cat-title" id="items' + speci_field_index + '" placeholder="Title" ><input type="hidden" name="items_type['+speci_field_index+']" id="items_type' + speci_field_index + '" value=""></div><div class="col-md-5"></div><div class="col-md-1"><span class="remove_now_two close" style="margin-top:5px;"><i class="fa fa-close"></i></span></div></div></div>');
            //x++; //text box increment
        }

    });
      
    $("body").on("click", ".remove_now_two", function (e) { //user click on remove text
        
        if (count >= 1) 
        {
            $(this).parents('.extra_two').remove(); //remove text box
            count--; //decrement textbox
        }
        return false;
    });
        
    // Add Repeater Field : end
        
    });
    // ********* Category Change ********
$('.cls-bic-prnt-cat').on('change',function(){

    if($(this).val()==""){
        $('.cls-sub-cat-title').attr('disabled',false);
        $('.cls-cat-parent-empty').show();
         $('.thsclass').show();
    }else{
        $('.cls-sub-cat-title').attr('disabled',true);
        $('.cls-cat-parent-empty').hide();
         $('.thsclass').hide();
    }
});
</script>
@endsection