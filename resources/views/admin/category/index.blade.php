 @extends('admin.layout.app')

 @section('head_title')
 Categories
 @endsection
 @section('main_content')
 
<section class="content-header">
    <h1>
        Categories

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>

 <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                  
                    <div class="box-header">
                        <!-- <h3 class="box-title">Best in City Category</h3> -->
               
                            <a href="{!! URL::to('category/create') !!}" class="btn btn-primary pull-right" style="margin-left:5px;">Add New</a>
                 
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <div class="col-md-12">
                               <div class="cls-bic-category-container">
                                @if(count($categories))
                                    @foreach($categories as $category)
                                          <ul style="border: 1px solid #ddd;padding: 5px;">
                                            <li style="list-style:none;">
                                                <div style="padding: 10px;list-style: none;background: #ddd;"> {{$category->name}}

                                                    <a href="{{URL::to('category/edit/' . $category->id)}}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    @if(!$category->status)
                                                        <a href="#" data-id="{{$category->id}}" class="cls-remove-category">
                                                            <i class="fa fa-trash-o"></i>
                                                        </a>
                                                    @endif
                                                </div>
                                                @if(count($category->getSubcategory))
                                                    <ul style="padding-top: 10px;">
                                                        @foreach($category->getSubcategory as $subCategory)
                                                            <li style="padding: 10px;list-style: none;background: #bfbcbc;">{{$subCategory->name}}
                                                                <a href="{{URL::to('category/edit/' . $subCategory->id)}}">
                                                                    <i class="fa fa-edit"></i>
                                                                </a>
                                                                {{--  @if(!$bestInCity->status)  --}}
                                                                    <a href="#" data-id="{{$subCategory->id}}" class="cls-remove-category">
                                                                        <i class="fa fa-trash-o"></i>
                                                                    </a>
                                                                {{--  @endif  --}}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        </ul>
                                    @endforeach
                                @endif
                            </div>
                  
</div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection
{{-- @include('admin.layout.popup.wheretogo-delete')
@include('admin.layout.popup.wheretogo-success') --}}

@section('additional_scripts')
        <!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    /*********** Delete Category **********/

    $('.cls-bic-category-container').on('click', '.cls-remove-category', function (e) {
        e.preventDefault();
        var category_id = $(this).data('id');
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "{!! url('/category/delete-check')!!}",
                data: {id: category_id},
                success: function (result) {
                    if(result.success){
                        var category_id = result.category_id;
                        var countOfSubCategories = result.countOfSubCategories;
                        var countOfPlaces = result.countOfPlaces;
                        if(!countOfSubCategories && !countOfPlaces){
                            var act= "Are you sure want to delete this Category?";
                        }else{
                            var act= "Are you sure want to delete this Category. ";
                            if(countOfSubCategories){
                                var act= act + countOfSubCategories + " Sub categories added in to this Category. ";
                            }
                            if(countOfPlaces){
                                var act= act + countOfPlaces + " Places added in to this Category. ";
                            }
                            var act= act + "On clicking confirm this reference will be deleted. Are you sure want to continue?";
                        }

                        //***************
                        alertify.confirm("<img src='{{asset("images/logo.png")}}' alt='Logo' title='Logo'>", act, function () {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
                                }
                            });
                            $.ajax({
                                type: "POST",
                                url: "{!! url('/category/delete')!!}",
                                data: {id: category_id},
                                success: function (result) {
                                    if(result.success==false){
                                        alertify.alert("<img src='{{asset("images/logo.png")}}' alt='Logo'>", result.message);
                                    }else{
                                        location.reload();
                                    }
                                }
                            });
                        },function () {
                            location.reload();
                        });
                        //***************
                    }
                }
            },function(result){
                console.log("Error!");
            });
    });


</script>

@endsection