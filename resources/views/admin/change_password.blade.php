@extends('admin.layout.app')
@section('title','| Change password')
@section('additional_styles')
<style>
    .c_error{ color:#f00;}
</style>
@endsection
@section('main_content')
<section class="content-header">
    <h1>
        Change Password

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="alert alert-success msgalt" style="display:none;">
                         
                                                 </div>
                <div class="box-body">

                            <form  id="changePasswordForm" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                @if (Session::has('message'))
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <span>{!! Session::get('message') !!}</span>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">Current Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" name="current_password" class="form-control" tabindex="1" placeholder="Current Password" autocomplete="off" onkeyup="$(this).next('span').html('');" />
                                        <span class="current_password_error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-3 control-label">New Password</label><div class="col-sm-8">
                                    <input type="password" name="password" class="form-control"  tabindex="2" placeholder="New Password" autocomplete="off" onkeyup="$(this).next('span').html('');" />
                                    <span class="password_error"></span></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-3 control-label">Confirm Password</label>
                                    <div class="col-sm-8">
                                        <input type="password" name="password_confirmation" class="form-control"  tabindex="3" placeholder="Confirm Password" autocomplete="off" onkeyup="$(this).next('span').html('');" />
                                        <span class="password_confirmation_error"></span>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <input type="button" name="submit" value="Change Password"  class="btn btn-primary changePasswordbtn" tabindex="4" />
                                    </div>

                                    <div class="c_loading" style="display:none; text-align: center">
                                        <img src="{{ asset('images/progress.gif')}}" alt=""/>
                                    </div>
                                    <div class="status"></div>
                                </div>
                            </form>
                       </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
    </div>
@endsection

@section('additional_scripts')

<script type="text/javascript">
    $(".changePasswordbtn").click(function(e) {
        var form = $("#changePasswordForm");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "{!! URL::to('change-password') !!}",
            data: form.serialize(),
            beforeSend: function() {

                jQuery(".c_loading", form).show();
                $(".msgalt").hide();
            },
            success: function(data) {

//console.log(data);
                $('.c_error', form).html('');
                if (data.fail) {
                    console.log(data.errors);
                    $.each(data.errors, function(index, value) {
                        var errorDiv = '.' + index + '_error';
                        $(errorDiv, form).addClass('c_error');
                        $(errorDiv).empty().append(value);
                    });
                    $(".c_loading", form).hide();
                }
                if (data.success) {

                    $(".msgalt").html('<a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">x</a>Password Updated Successfully. </p>')
                    $(".c_loading", form).hide();
                    $(".msgalt").show();
                    $('#changePasswordForm')[0].reset();
                }


            } //success

        });
    });
</script>
@endsection