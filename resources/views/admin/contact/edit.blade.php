@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
          Contact Edit
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Contact </h3>
                        <a href="{!! URL::to('admin/contact') !!}" class="btn btn-primary pull-right">Back</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{{url('contact/edit',$contact->id)}}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <div class="box-body">
                            
                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="{!! $contact->title !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>

                            
                           
                         
                            <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                <label for="description">Address</label>
                                <textarea class="form-control @if($errors->first('message')) is-invalid @endif" id="message" name="content"
                                          placeholder="Enter Description" autocomplete="off" value="{{old('phone')}}" maxlength="3000"
                                          rows="10" style="height: 100px;"> {{$contact->address}}</textarea>
                                <span>{{$errors->first('content')}}</span>




                            </div>
                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Phone</label>
                                <input type="text" name="phone" class="form-control" placeholder="Enter Title"  value="{!! $contact->phone !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>

                            
                        </div>
                        <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Location Map</label>
                                <input type="text" name="map" class="form-control" placeholder=""  value="{!! $contact->map !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>
                        <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Facebook</label>
                                <input type="text" name="facebook" class="form-control" placeholder="Enter Facebook Link"  value="{!! $contact->facebook !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>

                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                    <label for="title">Linkedin</label>
                                    <input type="text" name="linkedin" class="form-control" placeholder="Enter Linkedin Link"  value="{!! $contact->linkedin !!}">
                                    <span>{{$errors->first('title')}}</span>
                                </div>
                                <div class="form-group @if($errors->first('title')) has-error @endif">
                                        <label for="title">Twiter</label>
                                        <input type="text" name="twiter" class="form-control" placeholder="Enter Twiter Link"  value="{!! $contact->twitter !!}">
                                        <span>{{$errors->first('title')}}</span>
                                    </div>
                                    <div class="form-group @if($errors->first('title')) has-error @endif">
                                            <label for="title">Youtube</label>
                                            <input type="text" name="youtube" class="form-control" placeholder="Enter youtube Link"  value="{!! $contact->youtube !!}">
                                            <span>{{$errors->first('title')}}</span>
                                        </div>
                                    <div class="form-group @if($errors->first('title')) has-error @endif">
                                            <label for="title">Insta</label>
                                            <input type="text" name="insta" class="form-control" placeholder="Enter Insta Link"  value="{!! $contact->insta !!}">
                                            <span>{{$errors->first('title')}}</span>
                                        </div>
                                         <div class="form-group @if($errors->first('title')) has-error @endif">
                                                <label for="title">Email</label>
                                                <input type="text" name="email" class="form-control" placeholder="Enter Email"  value="{!! $contact->email !!}">
                                                <span>{{$errors->first('title')}}</span>
                                            </div>
                                            <div class="form-group @if($errors->first('meta_title')) has-danger @endif">
                                                    <label for="meta_title">Meta Title</label>
                                                    <input class="form-control @if($errors->first('meta_title')) is-invalid @endif"
                                                           id="meta_title" name="meta_title" type="text" placeholder="Enter meta name"
                                                           autocomplete="off"  value="{!! $contact->meta_title !!}">
                                                    @if($errors->first('meta_title'))
                                                    <label class="alert-danger">{{$errors->first('meta_title')}}</label>
                                                    @endif
                                                </div>
                                                <div class="form-group @if($errors->first('meta_title')) has-danger @endif">
                                                        <label for="meta_title">Meta Content</label>
                                                        <input class="form-control @if($errors->first('meta_title')) is-invalid @endif"
                                                               id="meta_title" name="meta_content" type="text" placeholder="Enter meta name"
                                                               autocomplete="off" value="{!! $contact->meta_content !!}">
                                                        @if($errors->first('meta_title'))
                                                        <label class="alert-danger">{{$errors->first('meta_title')}}</label>
                                                        @endif
                                                    </div>
                                                <div class="form-group @if($errors->first('meta_key')) has-danger @endif">
                                                    <label for="meta_key">Meta Key</label>
                                                    <input class="form-control @if($errors->first('meta_key')) is-invalid @endif"
                                                           id="meta_key" name="meta_key" type="text" placeholder="Enter meta key"
                                                           autocomplete="off"  value="{!! $contact->meta_key !!}">
                                                    @if($errors->first('meta_key'))
                                                    <label class="alert-danger">{{$errors->first('meta_key')}}</label>
                                                    @endif
                                                </div>
                
                                                <div class="form-group @if($errors->first('meta_description')) has-danger @endif">
                                                    <label for="meta_description">Meta Description</label>
                                                    <textarea
                                                            class="form-control @if($errors->first('meta_description')) is-invalid @endif"
                                                            id="meta_description" name="meta_description" type="text"
                                                            placeholder="Enter meta description" autocomplete="off">{{$contact->meta_description}}</textarea>
                                                    @if($errors->first('meta_description'))
                                                    <label class="alert-danger">{{$errors->first('meta_description')}}</label>
                                                    @endif
                                                </div>
                                                <div class="form-group @if($errors->first('meta_key')) has-danger @endif">
                                                        <label for="meta_key">Author</label>
                                                        <input class="form-control @if($errors->first('meta_key')) is-invalid @endif"
                                                               id="meta_key" name="author" type="text" placeholder="Enter author"
                                                               autocomplete="off"  value="{!! $contact->author !!}">
                                                        @if($errors->first('meta_key'))
                                                        <label class="alert-danger">{{$errors->first('meta_key')}}</label>
                                                        @endif
                                                    </div>

                                        {{-- <div class="box-header cls-cat-parent-empty">
                                                <div id="AudioWrapperr_two" class="col-xs-11 p-l-0 p-r-0">
                                                    <div class="form-group clearfix form-group-bg-grey">         						
                                                        <div class="col-md-6">
                                                            <label for="specification">Phone Number</label>
                                                            <input type="text" name="items[]" class="form-control locat cls-sub-cat-title" id="items" placeholder="Enter phone number" value="">
                                                            <input type="hidden" name="items_type[0]" id="items_orgn" value="">
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-xs-1">
                                                    <a href="#" id="AddMoreAudio_two" class="btn btn-success"><i class="fa fa-plus-square"></i></a>
                                                </div>
                                            </div><!-- /.box-header -->
                                          --}}
    
                            

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection
@section('additional_scripts')
<script>
    setTimeout(function(){ $('.clsAlert').fadeOut(); }, 3000);
    $('.fa-close').click(function(){
        $('.clsAlert').fadeOut();
    });
    
        // Add Repeater Field : start
        
        var speci_field_index = 0;
        $(document).ready(function () {

    var MaxInputsAudio = 50; //maximum input boxes allowed
    var InputsWrapper = $("#AudioWrapperr_two input"); //Input boxes wrapper ID
    var AddButton = "#AddMoreAudio_two"; //Add button ID

    var x = InputsWrapper.length; //initlal text box count
    var FieldCount = 1; //to keep track of text box added
    var count = 0;
    $(AddButton).click(function (e) //on add input button click
    {
        count++;

        //console.log(count);

        e.preventDefault();
        InputsWrapper = $("#AudioWrapperr_two input");
        x = InputsWrapper.length;
        //console.log(x + '  ' + MaxInputsAudio);
        if (count < MaxInputsAudio) //max input box allowed
        {
            speci_field_index++;
            FieldCount++; //text box added increment
            //add input box
            $(InputsWrapper).parents('#AudioWrapperr_two').append('<div class="form-group extra_two form-group-bg-grey clearfix"><div class="col-md-12"><div class="col-md-6 p-l-0" style="padding-left:0;"><input type="text" required name="items['+speci_field_index+']" class="form-control cls-sub-cat-title" id="items' + speci_field_index + '" placeholder="Title" ><input type="hidden" name="items_type['+speci_field_index+']" id="items_type' + speci_field_index + '" value=""></div><div class="col-md-5"></div><div class="col-md-1"><span class="remove_now_two close" style="margin-top:5px;"><i class="fa fa-close"></i></span></div></div></div>');
            //x++; //text box increment
        }

    });
    
    $("body").on("click", ".remove_now_two", function (e) {
        if (count >= 1){
            $(this).parents('.extra_two').remove();
            count--;
        }
        return false;
    });
});

// ********* Category Change ********
$('.cls-bic-prnt-cat').on('change',function(){
    if($(this).val()==""){
        $('.cls-sub-cat-title').attr('disabled',false);
        $('.cls-cat-parent-empty').show();
    }else{
        $('.cls-sub-cat-title').attr('disabled',true);
        $('.cls-cat-parent-empty').hide();
    }
});

</script>
@endsection

