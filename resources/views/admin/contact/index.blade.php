@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Contacts

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
@if(can('add_module'))
                    <!-- <a class="btn btn-primary pull-right" href="{{url('contact/create')}}" >Create</a> -->
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">

    @if(can('browse_module'))
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        @foreach($contacts as $contact)
                        <?php $count++;?>
                        <tr>
                            <td>{!!$count !!}</td>
                            <td>{{$contact->title}}</td>
                            <td>{!!$contact->address!!}</td>
                            <td>{!!$contact->phone!!}</td>
                            <td>{{$contact->email}}</td>
                            
                    
                            <td>
                               
                                <a href="{{url('contact/edit',$contact->id)}}" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            </td>
                            
                              
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection