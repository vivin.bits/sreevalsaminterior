@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')


<section class="content-header">
    <h1>
    COURSES

    </h1>
    {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(can('add_roles'))
                    <a class="btn btn-primary pull-right" href="{{url('course/create')}}" >Create</a>
                    @endif
                </div><!-- /.box-header -->
                <div class="box-body">
                 <table id="example2" class="table table-bordered dataTable table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>S#</th>
                            <th>Department Name</th>
                            <th>Course</th>
                           
                            <th>Image</th>
                         
                            
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $count =0;?>
                        @foreach($course as $courses)
                        <?php $count++;?>
                        <tr>
                            <td>{!! $count !!}</td>

                            <td>{{$courses->department->name}}</td>
                            <td>{!! $courses->course !!}</td>
                            
                            
                  
                            <td> <img src="{{asset('media/profile-images/'.$courses->image)}}" 
                                     alt="San Fran" style="width:120px;"></td>
                            
                         <td>
                         {{--  @if(can('edit_module')) --}}
                         <a href="{!! URL::to('courses/edit'.$courses->id) !!}" type="" class=""><i class="fa fa-edit"></i></a>
                         {{--   @endif --}} &nbsp&nbsp&nbsp    

                         {{--  @if(can('delete_module')) --}}
                                <a href="{{url('courses/delete'.$courses->id)}}" title="Edit Module"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                         </td>
                     </tr>
                     @endforeach

                 </tbody>
                 <tfoot>
                   
                </tfoot>
            </table>
            {{--       {!! Form::close() !!}  --}}
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->


@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script type="application/javascript">
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "columnDefs": [{ 'orderable': false, 'targets': [9,10] }],
        "searching": true,
        "ordering": true,
        "info": true,
        "mark": true,
        "scrollX": true,
       "autoWidth": false,
        "lengthMenu": [ [25,10, 50, -1], [25,10, 50, "All"] ]
    });
    
</script>
<script src="{{ asset('admin/js/includes/email_template/email.js')}}"></script>
@endsection