@extends('admin.layout.app')
@section('title','| Users')
@section('additional_styles')
<style type="text/css">
  .error{
    color: #f00;
  }
</style>
@endsection
@section('head_title')
Users
@endsection
@section('main_content')
<!-- Content Header (Page header) -->
 <section class="content-header">
        <h1>
            DEPARTMENTS
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>

<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                     {{--    <h3 class="box-title">Add New </h3> --}}
                        <a href="{!! URL::to('department') !!}" class="btn btn-primary pull-right">Back to Department</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
            <form role="form" name="adduser" action="{{url('department/edit'.$department->id)}}"  method="post">
              <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                <div class="form-group @if($errors->first('firstname')) has-error @endif">
                  <label for="firstname">Department Name</label>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Enter department name"  value="{!! $department->name !!}">
                  <span class="help-block required">{{$errors->first('name')}}</span>
            
    
         
            
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            </form>
          </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->



@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script>
  $(function() {

  $("form[name='adduser']").validate({

    rules: {

     firstname: {
      required: true,
      minlength: 2
    },

     phone: {
      required: true,
      number: true
    },

    role: "required",

    email: {
      required: true,
      email: true
    },
    password: {
      required: true,
      minlength: 8
    }
  },
    // Validation messages
    messages: {

     firstname: {
      required: "Please enter your firstname",
      minlength: "Your password must be at least 2 characters long"
    },

     phone: {
      required: "Please enter your phone",
      number: "Please enter a valid number."
    },
    role: "Please select role",
    password: {
      required: "Please provide a password",
      minlength: "Your password must be at least 8 characters long"
    },
    email: {
      required: "Please enter your email address",
      email: "Please enter a valid email address"
    }
  },

  submitHandler: function(form) {
    form.submit();
  }
});
});
</script>
@endsection
