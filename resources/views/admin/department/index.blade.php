@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Departments

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
@if(can('add_module'))
                    <a class="btn btn-primary pull-right" href="{{url('department/create')}}" >Create</a>
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">

    @if(can('browse_module'))
    
    <table id="example2" class="table table-bordered dataTable table-hover" style="width: 100%">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Department Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        @foreach($departments as $department)
                        <?php $count++?>
                        <tr>
                            <td>{!!$count!!}</td>
                            <td>{{$department->name}}</td>
                        
                    
                            <td>
                               {{--  @if(can('edit_module')) --}}
                                <a href="{{url('department/edit'.$department->id)}}" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                            
                            
                            
                               {{--  @if(can('delete_module')) --}}
                                <a href="{{url('department/delete'.$department->id)}}" title="Delete Module"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                              </td>
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection