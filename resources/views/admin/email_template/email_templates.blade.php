@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')


<section class="content-header">
    <h1>
        Email Templates

    </h1>
    {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(can('add_roles'))
                    <a class="btn btn-primary pull-right" href="{{url('email-templates/create')}}" >Create</a>
                    @endif
                </div><!-- /.box-header -->
                <div class="box-body">
                 {{--     {!! Form::open(array('url'=>'email_templates/delete' )) !!}  --}}

                 <table id="example2" class="table table-bordered dataTable table-hover" style="width: 100%">
                    <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Name&nbsp;(From)</th>  
                            <th>Email&nbsp;(From)</th>  
                            <th>Name&nbsp;(To)</th>
                            <th>Email&nbsp;(To)</th>
                            <th>Name&nbsp;(Cc)</th>
                            <th>Email&nbsp;(Cc)</th>
                            <th>Subject</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $count =0;?>
                        @foreach($records as $templ)
                        <?php $count++;?>
                        <tr>
                            <td>{!! $count !!}</td>


                            <td>{!! $templ->title !!}</td>
                            <td>{!! $templ->from_name !!}</td>
                            <td>{!! $templ->from_email !!}</td>
                            <td>{!! $templ->to_name !!}</td>
                            <td>{!! $templ->to_email !!}</td>
                            <td>{!! $templ->cc_name !!}</td>
                            <td>{!! $templ->cc_email !!}</td>
                            <td>{!! $templ->subject !!}</td>
                            <td>@if ($templ->status == '1') 
                             <div class="pretty p-switch p-fill"><input type="checkbox" class="userStatus"  data-type="activate" id="{{ $templ->id }}" data-id="{{ $templ->id }}"  data-status="{{ $templ->status }}" checked onchange="changeEmailStatus(this,'inactivate')" ><div class="state p-success"><label></label></div></div>
                             @else 
                             <div class="pretty p-switch p-fill"><input type="checkbox"  class="userStatus"   data-type="inactivate" id="{{ $templ->id }}"  data-id="{{ $templ->id }}" data-status="{{ $templ->status }}"  onchange="changeEmailStatus(this,'activate')"><div class="state p-success"><label></label></div></div>
                             @endif
                         </td>

                         <td>
                             <a href="{!! URL::to('email-templates/edit/'.$templ->id) !!}" type="" class=""><i class="fa fa-edit"></i></a>       

                         </td>
                     </tr>
                     @endforeach

                 </tbody>
                 <tfoot>
                   
                </tfoot>
            </table>
            {{--       {!! Form::close() !!}  --}}
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->


@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script type="application/javascript">
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "columnDefs": [{ 'orderable': false, 'targets': [9,10] }],
        "searching": true,
        "ordering": true,
        "info": true,
        "mark": true,
        "scrollX": true,
       "autoWidth": false,
        "lengthMenu": [ [25,10, 50, -1], [25,10, 50, "All"] ]
    });
    
</script>
<script src="{{ asset('admin/js/includes/email_template/email.js')}}"></script>
@endsection