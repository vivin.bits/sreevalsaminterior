@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Email Templates
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add New </h3>
                        <a href="{!! URL::to('email-templates') !!}" class="btn btn-primary pull-right">Back to Email Templates</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
                            <form role="form" action="{!! URL::to('email-templates/edit/'.$template_details->id) !!}" method="post" >

                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                <div class="box-body">
                                    <div class="form-group @if($errors->first('title')) has-error @endif">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" id="title" placeholder="Enter title" value="{!! $template_details->title !!}" readonly="readonly">
                                        <span class="help-block">{{$errors->first('title')}}</span>
                                    </div>
                                    <div class="form-group">
                                    <div class="col-md-6 leftpad form-group @if($errors->first('from_name')) has-error @endif">
                                        <label for="title">From Name </label>
                                        <input type="text" name="from_name" class="form-control" placeholder="Enter From Name" value="{!! $template_details->from_name !!}">
                                        <span>{{$errors->first('from_name')}}</span>
                                    </div>

                                    <div class="col-md-6 rightpad form-group @if($errors->first('from_email')) has-error @endif">
                                        <label for="title">From Email </label>
                                        <input type="text" name="from_email" class="form-control" placeholder="Enter From Email" value="{!! $template_details->from_email !!}">
                                        <span>{{$errors->first('from_email')}}</span>
                                    </div>
                                </div>
 <div class="form-group">
                                    <div class="col-md-6 leftpad form-group @if($errors->first('to_name')) has-error @endif">
                                        <label for="title">To Name </label>
                                        <input type="text" name="to_name" class="form-control" placeholder="Enter To Name" value="{!! $template_details->to_name !!}">
                                        <span>{{$errors->first('to_name')}}</span>
                                    </div>

                                    <div class="col-md-6 rightpad form-group @if($errors->first('to_email')) has-error @endif">
                                        <label for="title">To Email</label>
                                        <input type="text" name="to_email" class="form-control" placeholder="Enter To Email" value="{!! $template_details->to_email !!}">
                                        <span>{{$errors->first('to_email')}}</span>
                                    </div>
                                </div>
                                 <div class="form-group">
                                
                                  <div class="form-group col-md-6 leftpad @if($errors->first('cc_name')) has-error @endif">
                                        <label for="title">Cc Name</label>
                                        <input type="text" name="cc_name" class="form-control" placeholder="Enter Cc" value="{!! $template_details->cc_name !!}">
                                        <span>{{$errors->first('cc_name')}}</span>
                                    </div>
                                    <div class="col-md-6 rightpad form-group @if($errors->first('cc_email')) has-error @endif">
                                        <label for="title">Cc Email</label>
                                        <input type="text" name="cc_email" class="form-control" placeholder="Enter To Email" value="{!! $template_details->cc_email !!}">
                                        <span>{{$errors->first('cc_email')}}</span>
                                    </div>
                                </div>
                                 
                                    <div class="form-group @if($errors->first('subject')) has-error @endif">
                                        <label for="title">Subject</label>
                                        <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter mail subject" value="{!! $template_details->subject !!}">
                                        <span class="help-block">{{$errors->first('subject')}}</span>
                                    </div>

                                    <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                        <label for="description">Email Content</label>
                                        <textarea name="content" id="content">{!! $template_details->email_body !!}</textarea>
                                        <span>{{$errors->first('content')}}</span>
                                        
                                    </div>

 <div class="form-group">

                                            <label class="rtl">Status</label>

                                                <select name="select_status" id="select_status" class="form-control">
                                                   <option value="1" @if($template_details->status==1) {!!'selected'!!} @endif>Active</option>
                                                   <option value="0" @if($template_details->status==0) {!!'selected'!!} @endif>Inactive</option>
                                               </select>

                                           </div>
                                </div><!-- /.box-body -->

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->


            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection
@section('additional_scripts')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection

