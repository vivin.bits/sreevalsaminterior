@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Facilities 

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
    <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">
@if(can('add_module'))
                    <a class="btn btn-primary pull-right" href="{{url('facility/create')}}" >Create</a>
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">

  
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Sl#</th>
                            <th>Title</th>
                            <!-- <th>Description</th> -->
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                       
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/custom/module/facility.js')}}"></script>
    <script src="{{asset('admin/js/custom/delete.js')}}"></script>


@endsection