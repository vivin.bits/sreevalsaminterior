<script>
    @if(Session::has('smessage'))
        successMsg("{!! Session::get('smessage') !!}");
    @endif
    @if(Session::has('emessage'))
        errorMsg("{!! Session::get('emessage') !!}");
    @endif
    @if(Session::has('wmessage'))
        warningMsg("{!! Session::get('wmessage') !!}");
    @endif
</script>
