<?php $menus =  \Illuminate\Support\Facades\Config::get('backend-menu.menus');?>
    {{--{!! dump($menus) !!}--}}
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
    {{--   <li class="header">MAIN NAVIGATION</li> --}}

        @foreach($menus as $key => $value)
      {{--       @if(has_permission($value['permission'])) --}}
                    <?php $flag = false;
                    $new_str = substr(Request::path(), ($pos = strpos(Request::path(), '/')) !== false ? $pos + 1 : 0);
                    ?>

                          <li
                          @if(is_array($value['child']))
                                  @foreach($value['child'] as $k=>$v)
                                  @if($new_str == $v['slug']) <?php $flag = true;?> @endif
                                  @endforeach
                                  @if($flag) class="active current-menu treeview" @else class="treeview" @endif
                          @endif
                                  >
                            <a href="{!! URL::to($value['slug']) !!}">
                              <i class="{!! $value['icon-class'] !!}"></i> <span>{!! $value['Name'] !!}</span>
                               @if($value['child'])
                                    <i class="fa fa-angle-left pull-right"></i>
                                   @endif
                            </a>
                              @if(is_array($value['child']))

                                        <ul class="treeview-menu">
                                        @foreach($value['child'] as $k => $v)
                                         {{--        @if(has_permission($v['permission'])) --}}
                                                    <li @if(Request::path() == $v['slug']) class="active current-menu treeview" @endif><a href="{!! URL::to($v['slug']) !!}"><i class="{!! $v['icon-class'] !!}"></i> {!! $v["Name"] !!}</a></li>
                                          {{--       @endif --}}
                                        @endforeach
                                        </ul>
                              @endif
                          </li>
          {{--   @endif --}}

           
        @endforeach
    </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>