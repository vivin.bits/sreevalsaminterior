@extends('admin.layout.app')
@section('title','| Users')
@section('additional_styles')
@endsection
@section('head_title')
Users
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Users
           
        </h1>
   {{--   @include('include.breadcrub') --}}
    </section>

<!-- Main content -->
<section class="content">
        <div class="row">
            <div class="col-xs-12">
             

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit {!! $user->first_name !!}'s profile</h3>
                        <a href="{!! URL::to('users') !!}" class="btn btn-primary pull-right">Back to Users</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">

                        @if(Session::get('message'))
                    
                    <div class="alert alert-success msgalt">
                         <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
                                                 </div>
                        
                    @endif
                        <div class="col-md-10 p-left-0">
                        <form role="form" action="{!! URL::to('users/edit/'.$user->id) !!}" method="post">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <div class="box-body">
                                <div class="form-group @if($errors->first('firstname')) has-error @endif">
                                    <label for="firstname">First Name</label>
                                    <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Enter first name" minlength="2" maxlength="30" value="{!! $user->firstname !!}">
                                    <span class="help-block required">{{$errors->first('firstname')}}</span>
                                </div>
                                <div class="form-group @if($errors->first('lastname')) has-error @endif">
                                    <label for="lastname">Last Name</label>
                                    <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter last name"  maxlength="30" value="{!! $user->lastname !!}">
                                    <span class="help-block required">{{$errors->first('lastname')}}</span>
                                </div>
                                <div class="form-group @if($errors->first('email')) has-error @endif">
                                    <label for="name">Email</label>
                                    <span class="form-control">{{$user->email}}</span>
                                </div>
                                <div class="form-group @if($errors->first('phone')) has-error @endif">
                                    <label for="phone">Phone</label>
                                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter Phone" minlength="6" maxlength="15" value="{!! $user->phone !!}">
                                    <span class="help-block required">{{$errors->first('phone')}}</span>
                                </div>
                                <!-- <div class="form-group @if($errors->first('role')) has-error @endif">
                                    <label for="name">Role</label>
                                    <select name="role" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($roles as $role)
                                        <option @if($role->id == $user->role_id) selected="selected" @endif value="{!! $role->id !!}">{!! $role->name !!}</option>
                                        @endforeach
                                    </select>
 <span class="help-block required">{{$errors->first('role')}}</span>
                                </div> -->
                            </div><!-- /.box-body -->
                           
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>

                        </form>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->


@endsection

@section('additional_scripts')

@endsection