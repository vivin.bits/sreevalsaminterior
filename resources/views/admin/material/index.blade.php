 @extends('admin.layout.app')

 @section('head_title')
 Users
 @endsection
 @section('main_content')
 
<section class="content-header">
    <h1>
        Users

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                    <a   href="{!! URL::to('users/create') !!}" class="btn btn-primary pull-right">Create</a>
                </div><!-- /.box-header -->
                <div class="box-body">
  <table id="example2" class="table table-striped table-no-bordered table-hover dataTable dtr-inline" style="width: 100%">
    <thead>
      <tr>
        <th>S#</th>
        <!--  <th><input type="checkbox" class="check-all" /></th>  -->
        <th width="200">Name</th>
        <th>Email</th>
        <th>Phone</th>
        {{--  <th>Role</th> --}}

        <th>Block</th>
        <th>Status</th>
        <th>Last login</th>
        <th>Action</th>
        
      </tr>
    </thead>
    

  </table>
</div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
<div class="modal fade mymodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header" style="color: #fff;
    padding: 9px 15px;
    border-bottom: 1px solid #eee;
    background-color: #d9534f;">
    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
    <button type="button" class="close" data-dismiss="modal"><span
      aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

    </div>
    <div class="modal-body">
      Are you sure you want to delete this user?
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline" data-dismiss="modal">No</button>
      <button type="button" class="btn btn-outline" data-myval="" id="btn-yes" onclick="delUser(this)">Yes</button>
    </div>
  </div>
</div>
</div>

@endsection


@section('additional_scripts')

{{-- <script src="{{ asset('admin/js/jquery.dataTables.min.js')}}"></script> --}}






<script type="text/javascript">

  var table = $('#example2').DataTable({
    "aaSorting": [],
    'order': [[1, 'desc']],
    "fnDrawCallback": function (oSettings) {
    

      if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength) {
       j = 0;
       for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++) {
        $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[j]].nTr).find('span').text(i + 1);
        j++;
      }
    }

  },
  serverSide: true,

  oLanguage: {sProcessing: "<img src='{{ asset('admin/images/loading.gif')}}'>"},
    processing: true,
    "pageLength": 25,
    "columnDefs": [{ 'orderable': false, 'targets': [2,3,4,5,6,7] }],
    mark: true,
    fixedColumns: {},
    ajax: {
      url: '{!! URL::to("/users_data") !!}',
    },
      columns: [
      {data: 's#', name: 's#'},
      {data: 'name', name: 'name'},
      {data: 'email', name: 'email'},
      {data: 'phone', name: 'phone'},
                // {data: 'role_id', name: 'role_id'},

                {data: 'block', name: 'block'},
                      {data: 'status', name: 'status'},
                {data: 'last_login', name: 'last_login'},
                {data: 'action',name: 'action'},
                

                ]
              });

  
    $(document).on('click', '.adminuser-login', function (e) {
            e.preventDefault();
            var CSRF_TOKEN = "{!! Session::token() !!}";
            var sendData = {id: $(this).data('user'), '_token': CSRF_TOKEN}
            var url = "{!! url('users/login') !!}";
            $.ajax({
                type: "post",
                url: url,
                crossDomain: true,
                data: sendData,
                success: function (data) {
                    window.open(data, 'Filtr3', 'fullscreen=0');
                }
            });
        });

            </script>
            <script src="{{ asset('admin/js/includes/users/users.js')}}"></script>


            @endsection
