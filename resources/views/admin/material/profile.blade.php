@extends('admin.layout.app')
@section('title','| Profile')
@section('additional_styles')
<style type="text/css">  
input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>
@endsection
@section('main_content')
<section class="content-header">
    <h1>
        Admin Profile

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                
            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
                <div class="box-body">
                  <form method="post" name="editAdminUser" id="editAdminUser" action="{{url('profile')}}" enctype="multipart/form-data">
                          {{csrf_field()}}
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fist Name</label>
                          <input type="text" name="firstname" class="form-control" value="{!! $user->firstname !!}">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" name="lastname" class="form-control" value="{!! $user->lastname !!}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
   
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email address</label>
                          <input type="email" name="email" class="form-control" value="{!! $user->email !!}" readonly="readonly">
                        </div>
                      </div>
                    </div>

                     <div class="form-group">
                                    <label for="image">Image</label>
                                    <div class="fake-input">
                                        <input id="uploadFile" disabled="disabled" type="text"  style="display: none;" >
                                        <div class="fileUpload">
                                           
                                           
<input  id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg"/>
                                        </div>
                                    </div>

                                </div>

                                 <div class="col-lg-4">
                                <div class="prof-img" id="prof_img">
                                    <img src="{{asset('media/profile-images/'.$user->avatar)}}" class=" img-fluid img-thumbnail" alt="San Fran" style="height:200px;">
                                </div>
                                 <label for="image" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Change Profile Picture
</label>
                            </div>

                    
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone</label>
                          <input type="text" class="form-control" value="{!! $user->phone !!}">
                        </div>
                      </div>
                    
                    </div>
                   <div class="col-md-8">
                    <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                  </div>
                    <div class="clearfix"></div>
                  </form>
               </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
       // $("#editAdminUser").validate({
           // rules: {
           //     first_name: "required",
          //  }
      //  });
   $( document ).ready(function() {
        function readURL(input, img_con) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#'+img_con).find('img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function() {
            readURL(this, 'prof_img');
            $(this).parents('.form-group').find("#uploadFile").val(this.value);
        });
      });
    </script>


