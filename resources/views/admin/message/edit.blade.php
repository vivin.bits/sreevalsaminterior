@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
<link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
                Our Service Edit
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                        <a href="{!! URL::to('admin/message') !!}" class="btn btn-primary pull-right">Back </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{!! URL::to('message/edit/'.$news->id) !!}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <div class="box-body">
                                <div class="form-group @if($errors->first('title')) has-error @endif">
                                        <label for="title">Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Enter Title" value="{!! $news->title !!}"">
                                        <span>{{$errors->first('title')}}</span>
                                    </div>
                          
                                  
                                    <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                            <label for="description">Message</label>
                                        <textarea class="form-control @if($errors->first('message')) is-invalid @endif" id="message" name="content"
                                                  placeholder="Enter Message" autocomplete="off" value="{{old('message')}}" maxlength="3000"
                                                  rows="10" style="height: 100px;">{{$news->message}}</textarea>
                                           
                                            
                                        </div>

                     
                                        <div class="form-group @if($errors->first('image')) has-danger @endif">
                                                <label for="image"><strong>icon</strong></label><br>
                                                <input onchange="PreviewImage();"  id="image" name="image" type="file" >
                                                @if($errors->first('image'))<span class="alert-danger">{{$errors->first('image')}}</span> @endif
                                        
                                        </div>

                                        <img src="{{asset('media/profile-images/'.$news->image)}}" id="uploadPreview" width="200px">
                                    </div>
                         
                        
                            
                        
                            <div>

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

<script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery-migrate-3.0.0.min.js')}}"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
@endsection
