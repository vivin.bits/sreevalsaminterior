@extends('admin.layout.app')

@section('additional_styles')

@endsection

@section('main_content')
 <section class="content-header">
        <h1>
            Modules
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add New </h3>
                        <a href="{!! URL::to('module') !!}" class="btn btn-primary pull-right">Back to Modules</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
                    <form method="post" action="{{url('module/create')}}" id="createModule" name="createModule">
                        <div class="row">
                            <div class="col-lg-6">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Module name</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" name="name" type="text" placeholder="Enter module name" required autocomplete="off" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="form-control-feedback">{{$errors->first('name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('perifix')) has-danger @endif">
                                    <label for="perifix">Perifix</label>
                                    <input class="form-control @if($errors->first('perifix')) is-invalid @endif" id="perifix" type="text" name="perifix" placeholder="Enter module perifix" required autocomplete="off" value="{{old('perifix')}}">
                                    @if($errors->first('perifix'))
                                        <sapn class="form-control-feedback">{{$errors->first('perifix')}}</sapn>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                  </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@section('js')

    <!--Notifications Message Section-->
    @include('admin.layout.notifications')

    <script>
        $("#createModule").validate({
            rules: {
                // simple rule, converted to {required:true}
                perifix: "required",
                name: "required",
            }
        });
    </script>
@endsection