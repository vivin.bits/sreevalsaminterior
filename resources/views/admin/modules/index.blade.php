@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Modules

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
@if(can('add_module'))
                    <a class="btn btn-primary pull-right" href="{{url('module/create')}}" >Create</a>
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">

    @if(can('browse_module'))
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Module Name</th>
                            <th>Perifix</th>
                            <th>Created</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($modules as $module)
                        <tr>
                            <td>{{$module->name}}</td>
                            <td>{{$module->perifix}}</td>
                            <td>{{date('d M Y h:i A',strtotime($module->created_at))}}</td>
                            <td>
                               {{--  @if(can('edit_module')) --}}
                                <a href="{{url('module/edit/'.$module->id)}}" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection