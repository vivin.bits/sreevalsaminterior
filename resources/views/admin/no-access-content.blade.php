
    <div class="page-error tile">
        <h1><i class="fa fa-exclamation-circle"></i> Error 401: Permission Denied!</h1>
        <p>You don't have permission to access this content. Please contact admin.</p>
        {{--<p><a class="btn btn-primary" href="{{url('/')}}">Go Back</a></p>--}}
    </div>