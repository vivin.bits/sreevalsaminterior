@extends('admin.layout.app')

@section('additional_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('admin/redactor/redactor/redactor.css')}}" />

@endsection
@section('head_title')
Email Templates
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Product Create
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Photo Category </h3>
                        <a href="{!! URL::to('packagess') !!}" class="btn btn-primary pull-right">Back</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
              
                    <form action="{!! URL::to('package/create') !!}" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        <div class="box-body">
                            
                            <div class="form-group @if($errors->first('title')) has-error @endif">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="name" class="form-control" placeholder="Enter Title" value="{!! old('category') !!}">
                                <span>{{$errors->first('title')}}</span>
                            </div>

                            <div class="form-group @if($errors->first('slug')) has-danger @endif">
                                <label for="slug">Slug</label>
                                <input class="form-control @if($errors->first('slug')) is-invalid @endif" id="slug" name="slug" type="text" placeholder="Enter slug"  value="{{old('slug')}}">
                                @if($errors->first('slug'))
                                    <label class="error">{{$errors->first('slug')}}</label>
                                @endif
                            </div>     
                     
                            <!-- <div class="form-group @if($errors->first('description')) has-danger @endif">
                                            <label for="title"> Desciption</label>
                                            <textarea name="description" id="description" cols="30" rows="10" maxlength ="40" >{{old('description')}}</textarea>
                                            @if($errors->first('description'))
                                                <span class="form-control-feedback">{{$errors->first('description')}}</span>
                                            @endif
                                        </div> -->
                                        <div class="form-group email_temp @if($errors->first('content')) has-error @endif">
                                            <label for="description">Description</label>
                                        <textarea class="form-control @if($errors->first('message')) is-invalid @endif" id="message" name="description"
                                                  placeholder="Enter description" autocomplete="off" value="{{old('message')}}" 
                                                  rows="10" style="height: 100px;"></textarea>
                                        </div>
                            <div  class="form-group @if($errors->first('image')) has-danger @endif">

                            <label  for="image">Image</label><br>

                            <input   id="image"  name="image"  type="file" >

                            @if($errors->first('image'))<span  class="alert-danger">{{$errors->first('image')}}</span>
                             @endif
                           
                            <div>
                         
                </div>

    

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


@endsection

@section('js')
<script src="{{ asset('admin/redactor/redactor/redactor.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/fontsize/fontsize.js')}}"></script>
    <script src="{{ asset('admin/redactor/plugins/imagemanager/imagemanager.js')}}"></script>
 <script src="{{ asset('admin/js/custom/slug.js')}}"></script> 
<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});


$R('#description', {
                maxHeight:"400px",
                minHeight:"400px",
                plugins: ['fontsize','imagemanager'],
                imageUpload: '{!! asset('admin/redactor/demo/scripts/image_upload.php')!!}',
                imageResizable: true,
                imagePosition: true
            });
</script>
@endsection
