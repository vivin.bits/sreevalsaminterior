@extends('admin.layout.app')

@section('additional_styles')

@endsection

@section('main_content')


<section class="content-header">
    <h1>
        Pages

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
  <section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">

                    <a class="btn btn-primary pull-right" href="{{url('pages/create')}}" >Create</a>
         
                </div><!-- /.box-header -->
    <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
            <thead>
                <tr class="clstblheader">
                    <th>Sl No.</th>
                    <th width="400">Title</th>
                    <th>Status</th>
                    <th>Created on</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('admin/js/bootstrap-switch.min.js')}}"></script>
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script src="{{ asset('admin/js/datatables.mark.js')}}"></script>

<script src="{{ asset('admin/js/iziToast.min.js')}}"></script>
<script type="text/javascript">

    var table = $('#example2').DataTable({
        "pageLength": 50,
        //"searching": true,
        /*"columnDefs": [
            { "searchable": false, "targets": 0 },
            { "searchable": true, "targets": 1 }
            ],*/
            "bFilter": false,
            "aaSorting": [],
            "aoColumnDefs": [

            {orderable: false, targets: [0]}],
            "fnDrawCallback": function(oSettings) {

                if (oSettings.bSorted || oSettings.bFiltered || oSettings._iDisplayLength)
                {
                    j = 0;

                    for (var i = oSettings._iDisplayStart; i < oSettings.aiDisplay.length + oSettings._iDisplayStart; i++)
                    {
                        $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[j] ].nTr).find('span').text(i + 1);
                        j++;
                    }
                }

                /********* Switch Status ********/
                $("[name='my-checkbox']").bootstrapSwitch({
                    onText: 'Active',
                    offText: 'Inactive',
                    offColor: 'danger'
                });

            },
            processing: true,
            serverSide: true,
        // fixedHeader: true,
        /*fixedColumns: {
            leftColumns: 2
        },*/
        //scrollX: true,
        ajax: {
            url: '{!! URL::to("pages/get-data") !!}',
            data: function(d) {
                d.pageName = $('#txt_name').val();
            }
        },
        columns: [
        {data: 'id', name: 'id', orderable: false, searchable: false},
        {data: 'title', name: 'title'},
        {data: 'status', name: 'status', orderable: false, searchable: false},
        {data: 'created_at', name: 'created_at'},
        {data: 'tblaction', name: 'tblaction', orderable: false, searchable: false}

        ]});

    $('#txt_name').on('keyup', function () {
        table.draw();
    });

    $('.cls-form-reset').on('click',function(event){
        event.preventDefault();
        $('#txt_name').val('');
        table.draw();
    });
</script>

<script src="{{ asset('admin/js/includes/pages/pages.js')}}"></script>

@endsection