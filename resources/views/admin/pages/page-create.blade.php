@extends('admin.layout.app')
@section('title','| Pages')
@section('additional_styles')
<style type="text/css">
  .error{
    color: #f00;
  }
</style>
 <link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
 <link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Pages
@endsection
@section('main_content')
<!-- Content Header (Page header) -->
 <section class="content-header">
        <h1>
            Pages
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box clearfix">
                    
                    <div class="box-header">
                        <!-- <h3 class="box-title">Add Category</h3> -->
                        <a href="{!! URL::to('pages') !!}" class="btn btn-primary pull-right">
                         Back</a>
                     </div><!-- /.box-header -->
                     
                     <div class="box-header">
                        @if(Session::get('message'))
                            <div class="alert alert-success clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('message') !!}
                            </div>
                        @endif

                        @if(Session::get('error'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('error') !!}
                            </div>
                        @endif

                        @if(Session::has('validation_fail'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('validation_fail') !!}
                            </div>
                        @endif

                    </div><!-- /.box-header -->
           <div class="col-md-8 p-left-0 page-dtl">
                    <form action="{!! URL::to('pages/create') !!}" method="post">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        
						  <div class="form-group @if($errors->first('title')) has-error @endif">
                           
                            <label class="rtl">Title</label>
                       
                            <input type="text" name="title" id="page_title" class="form-control" placeholder="Enter Title">
                                <span class="help-block required">{{$errors->first('title')}}</span>
                            
                        </div><!-- /.box-header -->
						
					  <div class="form-group @if($errors->first('url_slug')) has-error @endif">
                          
                            <label class="rtl">URL Slug</label>
                      
                            <input type="text" name="url_slug" id="url_slug" class="form-control" placeholder="URL Slug">
                                <span class="help-block required">{{$errors->first('url_slug')}}</span>
                      
                        </div><!-- /.box-header -->
						
						  <div class="form-group @if($errors->first('content')) has-error @endif editor_height">
                           
                                <label class="rtl">Page Content</label>
                           
                                <textarea name="content" class="rtl" id="content"></textarea>
								
								<span class="help-block required">{{$errors->first('content')}}</span>
								
                          
                        </div><!-- /.box-header -->
						
                      
                         <div class="form-group">
                          
                                <label class="rtl">Meta Key</label>
                          
                                <input type="text" name="meta_key" class="form-control" placeholder="Enter Meta Key">
                          
                        </div><!-- /.box-header -->
                             <div class="form-group">
                         
                                <label class="rtl">Meta Description</label>
                       
                           
                                <textarea name="meta_desc" class="form-control"></textarea>
                                                  </div><!-- /.box-header -->
						
					       <div class="form-group">
                          
                                <label class="rtl">Status</label>
                            
                                <select name="select_status" id="select_status" class="form-control">
									<option value="1" @if(old('select_status')==1) {!!'selected'!!} @endif>Active</option>
									<option value="0" @if(old('select_status')==0) {!!'selected'!!} @endif>Inactive</option>
								</select>
                          
                        </div><!-- /.box-header -->
						
						       <div class="form-group">
                          
                                <h3 class="box-title">&nbsp;</h3>
                       
                                <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                            </div>

            </form>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</div>
</section><!-- /.content -->

@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script src="{{ asset('redactor/redactor.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
<script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
<script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
<script src="{{ asset('admin/js/additional-methods.js')}}"></script>
<script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 

 <script src="{{ asset('redactor/langs/ar.js')}}"></script>

<script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>

     <script>

		
		$(function(){
			
			$('#page_title').focusout(function(){

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('[name="csrf_token"]').attr('content')
                }
            });
                $.ajax({
                url: "{!! URL::to('pages/url-slug') !!}",
                type: 'POST',
			    data: { title : $(this).val() },
                success: function (data)
			    {
				    $('#url_slug').val(data);
                }
                });	
	        });
			
			// Close Alert Message : start
	        setTimeout(function(){ $('.clsAlert').fadeOut();}, 4000);
	        $('.fa-close').click(function(){$('.clsAlert').fadeOut();});
	
	        // Close Alert Message : end

		});
		
    </script>
    <script>
  $('document').ready(function () {

    $(document).on('change', 'input#page_title', function() {
      var slug = slugify($(this).val());
      //$('#slug').text(slug);
      document.getElementById("url_slug").value = slug;
    $("input.url_slug" ).trigger("focus");
    });
  });
  function slug_check()
  {
    var string = document.getElementById("url_slug").value;
    slugify(string);
     document.getElementById("url_slug").value = slug;
  }

  function slugify(string)
  {
    var string = $.trim(string);
  string = string.toLowerCase('UTF-8');
  string = string.replace("/[^a-z0-9_\s-ءاآؤئبپتثجچحخدذرزژسشصضطظعغفقكکگلمنوهی]/u", '', string);
  string = string.replace("/[\s-_]+/", ' ', string);
  string = string.replace("/[\s_]/", string);
    string = string.replace(/\s+/g, '-');

   // alert(string);

   return string;
  }
</script>
<script>
 
   $R('#content', {
    plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '{!! asset('redactor/scripts/file_upload.php')!!}',
  //  fileManagerJson: '/your-folder/files.json' ,
    imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
 //   imageManagerJson: '/your-folder/images.json',
    // lang: 'ar' ,
  });
</script>
@endsection