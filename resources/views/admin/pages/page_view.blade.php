@extends('admin.layout.app')
@section('title','| Pages')
@section('additional_styles')
<style type="text/css">
  .error{
    color: #f00;
  }
</style>
 <link rel="stylesheet" href="{!! asset('redactor/redactor.css') !!}" />
 <link rel="stylesheet" href="{!! asset('redactor/plugins/filemanager/filemanager.css') !!}" />
@endsection
@section('head_title')
Pages
@endsection
@section('main_content')
<!-- Content Header (Page header) -->
 <section class="content-header">
        <h1>
            Pages
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box clearfix">
                    
                    <div class="box-header">
                        <!-- <h3 class="box-title">Add Category</h3> -->
                        <a href="{!! URL::to('pages') !!}" class="btn btn-primary pull-right">
                         Back</a>
                     </div><!-- /.box-header -->
                     
                     <div class="box-header">
                        @if(Session::get('message'))
                            <div class="alert alert-success clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('message') !!}
                            </div>
                        @endif

                        @if(Session::get('error'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('error') !!}
                            </div>
                        @endif

                        @if(Session::has('validation_fail'))
                            <div class="alert alert-danger clsAlert">
                                <i class="fa fa-close" style="float:right;color:white;cursor:pointer;"></i> {!! Session::get('validation_fail') !!}
                            </div>
                        @endif

                    </div><!-- /.box-header -->
           
                   <div class="col-md-8 p-left-0 page-dtl">     
                    <form action="{!! URL::to('pages/view/'.$data->id) !!}" method="post">
                        <input type="hidden" name="_token" value="{!! Session::token() !!}">
                        
                        <div class="form-group @if($errors->first('title')) has-error @endif">

                            <label class="rtl">Title</label>

                                <input type="text" name="title" id="page_title" class="form-control" placeholder="Enter Title" value="{!! $data->title !!}">
                                <span class="help-block required">{{$errors->first('title')}}</span>

                            </div><!-- /.box-header -->

                            <div class="form-group editor_height @if($errors->first('content')) has-error @endif">

                                <label class="rtl">Page Content</label>


                                    <textarea name="content" class="form-control" id="content">{!! $data->content !!}</textarea>

                                    <span class="help-block required">{{$errors->first('content')}}</span>


                                </div><!-- /.box-header -->

                                <div class="form-group">

                                    <label class="rtl">Meta Key</label>

                                        <input type="text" name="meta_key" class="form-control" placeholder="Enter Meta Key" value="{!! $data->meta_key !!}">
                                        
                                    </div><!-- /.box-header -->
                                    <div class="form-group">

                                        <label class="rtl">Meta Description</label>

                                            <textarea name="meta_desc" class="form-control" placeholder="Enter meta description">{!! $data->meta_description !!}</textarea>
                                            
                                        </div><!-- /.box-header -->

                                        <div class="form-group">

                                            <label class="rtl">Status</label>

                                                <select name="select_status" id="select_status" class="form-control">
                                                   <option value="1" @if($data->status==1) {!!'selected'!!} @endif>Active</option>
                                                   <option value="0" @if($data->status==0) {!!'selected'!!} @endif>Inactive</option>
                                               </select>

                                           </div><!-- /.box-header -->

                                           <div class="box-header">
                                         
                                           <div class="box-footer sub-btn">
                                                <button type="submit" class="pull-right btn btn-primary margin-top" value="update" name="update">Update</button>
                                            </div>

                                        </form>
                                    </div>
                  
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->



                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
            </section><!-- /.content -->

            @endsection

            @section('additional_scripts')

            <script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
            <script src="{{ asset('redactor/redactor.js')}}"></script>
            <script src="{{ asset('redactor/plugins/table/table.js')}}"></script>
            <script src="{{ asset('redactor/plugins/table/table.min.js')}}"></script>
            <script src="{{ asset('redactor/plugins/filemanager/filemanager.js')}}"></script> 
            <script src="{{ asset('redactor/plugins/imagemanager/imagemanager.js')}}"></script> 
            <script src="{{ asset('admin/js/additional-methods.js')}}"></script>
            <script src="{{ asset('redactor/langs/ar.js')}}"></script>
            <script src="{{ asset('admin/js/jquery-ui.min.js')}}"></script>

            <!-- place in header of your html document -->
            <script>

              $(function(){

                 $('#page_title').focusout(function(){

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="csrf_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{!! URL::to('pages/url-slug') !!}",
                        type: 'POST',
                        data: { title : $(this).val() },
                        success: function (data)
                        {
                           $('#url_slug').val(data);
                       }
                   });	
                });

			// Close Alert Message : start

           setTimeout(function(){ 

              $('.clsAlert').fadeOut();

          }, 4000);

           $('.fa-close').click(function(){

              $('.clsAlert').fadeOut();

          });

	        // Close Alert Message : end

      });

  </script>
  <script>

   $R('#content', {
 pasteBlockTags: ['a', 'img', 'br', 'strong', 'ins', 'code', 'del', 'span', 'samp', 'kbd', 'sup', 'sub', 'mark', 'var', 'cite', 'small', 'b', 'u', 'em', 'i'],
     plugins: ['filemanager','table','imagemanager'],
    // fileUpload: '/your-upload-script/',
    // fileManagerJson: '/your-folder/files.json' ,
    imageUpload: '{!! asset('redactor/scripts/image_upload.php')!!}',
    imageManagerJson: '/your-folder/images.json',
    lang: 'ar' ,

});
</script>
@endsection