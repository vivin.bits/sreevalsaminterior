@extends('admin.layout.app')

@section('additional_styles')

@endsection
@section('head_title')
Roles
@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Roles
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add New </h3>
                        <a href="{!! URL::to('roles') !!}" class="btn btn-primary pull-right">Back to Roles</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('smessage'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('smessage') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
                    <form method="post" id="createrRole" name="createrRole" action="{{url('role/create')}}">
                        <div class="row">
                            <div class="col-lg-6">
                                {{csrf_field()}}
                                <div class="form-group @if($errors->first('display_name')) has-danger @endif">
                                    <label for="display_name">Display name</label>
                                    <input class="form-control @if($errors->first('display_name')) is-invalid @endif" id="display_name" name="display_name" type="text" placeholder="Enter display name" autocomplete="off" value="{{old('display_name')}}">
                                    @if($errors->first('display_name'))
                                        <sapn class="form-control-feedback">{{$errors->first('display_name')}}</sapn>
                                    @endif
                                </div>
                                <div class="form-group @if($errors->first('name')) has-danger @endif">
                                    <label for="name">Role</label>
                                    <input class="form-control @if($errors->first('name')) is-invalid @endif" id="name" type="text" name="name" placeholder="Enter role" autocomplete="off" value="{{old('name')}}">
                                    @if($errors->first('name'))
                                        <sapn class="form-control-feedback">{{$errors->first('name')}}</sapn>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
              </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@section('js')
    <!--Notifications Message Section-->
    @include('admin.layout.notifications')

    <script>
        $("#createrRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>
@endsection