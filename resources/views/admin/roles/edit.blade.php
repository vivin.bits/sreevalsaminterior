@extends('admin.layout.app')

@section('additional_styles')

@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Roles
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Edit Role </h3>
                        <a href="{!! URL::to('roles') !!}" class="btn btn-primary pull-right">Back to Roles</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('smessage'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('smessage') !!}
           </div>

           @endif
           <div class="col-md-12 p-left-0">
                    <form method="post" action="{{url('role/edit/'.$role->id)}}" name="editRole" id="editRole">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-6">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="display_name">Display name</label>
                                    <input class="form-control" id="display_name" name="display_name" type="text" placeholder="Enter display name" required autocomplete="off" value="{{$role->display_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Role</label>
                                    <input class="form-control" id="name" type="text" name="name" placeholder="Enter role" autocomplete="off" readonly value="{{$role->name}}">
                                </div>
                            </div>
                        </div>
                       
                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                 </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@section('js')

    <!--Notifications Message Section-->
    @include('admin.layout.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {

            $('.permission-group').on('change', function(){
                $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });
    </script>
@endsection