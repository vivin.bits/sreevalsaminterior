@extends('admin.layout.app')

@section('additional_styles')

@endsection

@section('main_content')


<section class="content-header">
    <h1>
        Roles

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
  <section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
@if(can('add_roles'))
                    <a class="btn btn-primary pull-right" href="{{url('role/create')}}" >Create</a>
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">
        

        @if(can('browse_roles'))
       
                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Role name</th>
                                <th>Role</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($roles as $role)
                            <tr>
                                <td>{{$role->display_name}}</td>
                                <td>{{$role->name}}</td>
                                <td>{{date('d M Y h:i A',strtotime($role->created_at))}}</td>
                                <td>
                                  {{--   @if(can('edit_roles')) --}}
                                    <a href="{{url('role/edit/'.$role->id)}}" title="Edit Role"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                   {{--  @endif --}}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                          @else
            @include('admin.no-access-content')
        @endif
                    </div>
                </div>
            </div>
        </div>
      

@endsection

@section('js')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">

        $('#sampleTable').DataTable({
            bPaginate: false,
            bSort: false,
            bFilter: false,
            bInfo: false
        });
    </script>
@endsection