@extends('admin.layout.app')

@section('additional_styles')

@endsection
@section('main_content')
 <section class="content-header">
        <h1>
            Permissions
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Permissions </h3>
                        <a href="{!! URL::to('permissions') !!}" class="btn btn-primary pull-right">Back to Permissions</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('smessage'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('smessage') !!}
           </div>

           @endif

           <div class="col-md-12 p-left-0">
                    <form method="post" action="{{url('permission/edit/'.$role->id)}}" name="editRole" id="editRole">
                        {{csrf_field()}}
                       
                      {{--   <h5>Permission</h5> --}}

                        <div class="animated-checkbox">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <tbody>

                                <?php
                                $role_permissions = (isset($role)) ? $role->permissions->pluck('key')->toArray() : [];
                                ?>
                                @foreach(App\Permission::all()->groupBy('table_name') as $table => $permission)
                                    <tr>
                                        <th>
                                            <label>

<div class="pretty p-svg p-curve">
              <input type="checkbox" id="{{$table}}" class="permission-group">
        <div class="state p-primary">
            <!-- svg path -->
            <svg class="svg svg-icon" viewBox="0 0 20 20">
                <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path>
            </svg>
            <label> <span class="label-text">{{strtoupper(str_replace('_',' ', $table))}}</span></label>
        </div>
    </div>
                                          </label>
                                        </th>
                                        @foreach($permission as $perm)
                                            <td>

                                                <label>

<div class="pretty p-svg p-curve">
        <input type="checkbox" id="permission-{{$perm->id}}" name="permissions[]" class="the-permission" value="{{$perm->id}}" @if(in_array($perm->key, $role_permissions)) checked @endif/>
        <div class="state p-success">
            <!-- svg path -->
            <svg class="svg svg-icon" viewBox="0 0 20 20">
                <path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z" style="stroke: white;fill:white;"></path>
            </svg>
           <label><span class="label-text">{{ ucfirst(substr($perm->key, 0, strpos( $perm->key, '_'))) }}</span></label>
        </div>
    </div>
  
                                                </label>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Update</button>
                        </div>
                    </form>
                 </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@section('additional_scripts')

    <!--Notifications Message Section-->
    @include('admin.layout.notifications')
    <script>
        $("#editRole").validate({
            rules: {
                // simple rule, converted to {required:true}
                display_name: "required",
                name: "required",
            }
        });
    </script>

    <script>
        $('document').ready(function () {
            $('.permission-group').on('change', function(){
                 $(this).parents('tr').find(".the-permission").prop('checked', this.checked);
            });

            function parentChecked(){
                $('.permission-group').each(function(){
                    var allChecked = true;
                    $(this).parents('tr').find(".the-permission").each(function(){
                        if(!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function(){
                parentChecked();
            });
        });


        $('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('input:checkbox').prop('checked', true);
    } else {
        $('input:checkbox').prop('checked', false);
    }
});

$("input[type='checkbox'].justone").change(function(){
    var a = $("input[type='checkbox'].justone");
    if(a.length == a.filter(":checked").length){
        $('.selectall').prop('checked', true);
    }
    else {
        $('.selectall').prop('checked', false);
    }
});

    </script>
@endsection