@extends('admin.layout.app')
@section('title','| Users')
@section('additional_styles')
<style type="text/css">
  .error{
    color: #f00;
  }
</style>
@endsection
@section('head_title')
Users
@endsection
@section('main_content')
<!-- Content Header (Page header) -->
 <section class="content-header">
        <h1>
            Subject
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box clearfix">
                    
                    <div class="box-header">
                        <!-- <h3 class="box-title">Add Category</h3> -->
                        <a href="{!! URL::to('scheme') !!}" class="btn btn-primary pull-right">
                         Back</a>
                     </div><!-- /.box-header -->
                     
                    <div class="col-xs-12 discussion-detail">   
                    <form action="{!! URL::to('scheme/create/') !!}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{!! Session::token() !!}">
                       
                        <div class="box-header cls-cat-parent-empty">
                            <div id="AudioWrapperr_two" class="col-xs-11 p-l-0 p-r-0">
                                <div class="form-group clearfix form-group-bg-grey">         						
                                    <div class="col-md-6">
                                        <label for="specification">Add Scheme</label>
                                        <input type="text" name="items[]" class="form-control locat cls-sub-cat-title" id="items" placeholder="Title" value="">
                                        <input type="hidden" name="items_type[0]" id="items_orgn" value="">
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div> 
                            <div class="col-xs-1">
                                <a href="#" id="AddMoreAudio_two" class="btn btn-success"><i class="fa fa-plus-square"></i></a>
                            </div>
                        </div><!-- /.box-header -->
                       
                       
                       
                       <div class="box-header">
                        <div class="col-md-4">
                            <h3 class="box-title">&nbsp;</h3>
                        </div>
                        <div class="col-md-8">
                            <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                        </div>
                    </div><!-- /.box-header -->
                </form>
                </div>
            </div><!-- /.box -->

        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

@endsection

@section('additional_scripts')
<script>
    setTimeout(function(){ $('.clsAlert').fadeOut(); }, 3000);
    $('.fa-close').click(function(){
        $('.clsAlert').fadeOut();
    });
    
        // Add Repeater Field : start
        
        var speci_field_index = 0;
        $(document).ready(function () {

    var MaxInputsAudio = 50; //maximum input boxes allowed
    var InputsWrapper = $("#AudioWrapperr_two input"); //Input boxes wrapper ID
    var AddButton = "#AddMoreAudio_two"; //Add button ID

    var x = InputsWrapper.length; //initlal text box count
    var FieldCount = 1; //to keep track of text box added
    var count = 0;
    $(AddButton).click(function (e) //on add input button click
    {
        count++;

        //console.log(count);

        e.preventDefault();
        InputsWrapper = $("#AudioWrapperr_two input");
        x = InputsWrapper.length;
        //console.log(x + '  ' + MaxInputsAudio);
        if (count < MaxInputsAudio) //max input box allowed
        {
            speci_field_index++;
            FieldCount++; //text box added increment
            //add input box
            $(InputsWrapper).parents('#AudioWrapperr_two').append('<div class="form-group extra_two form-group-bg-grey clearfix"><div class="col-md-12"><div class="col-md-6 p-l-0" style="padding-left:0;"><input type="text" required name="items['+speci_field_index+']" class="form-control cls-sub-cat-title" id="items' + speci_field_index + '" placeholder="Title" ><input type="hidden" name="items_type['+speci_field_index+']" id="items_type' + speci_field_index + '" value=""></div><div class="col-md-5"></div><div class="col-md-1"><span class="remove_now_two close" style="margin-top:5px;"><i class="fa fa-close"></i></span></div></div></div>');
            //x++; //text box increment
        }

    });
    
    $("body").on("click", ".remove_now_two", function (e) {
        if (count >= 1){
            $(this).parents('.extra_two').remove();
            count--;
        }
        return false;
    });
});

// ********* Category Change ********
$('.cls-bic-prnt-cat').on('change',function(){
    if($(this).val()==""){
        $('.cls-sub-cat-title').attr('disabled',false);
        $('.cls-cat-parent-empty').show();
    }else{
        $('.cls-sub-cat-title').attr('disabled',true);
        $('.cls-cat-parent-empty').hide();
    }
});

</script>
@endsection