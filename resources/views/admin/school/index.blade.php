@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        ALBUM

    </h1>
 
</section>
<section class="content">
    <div class="row">
    <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">

                    <a class="btn btn-primary pull-right" href="{{url('admin/school/students/create')}}" >Create</a>
                     
                </div><!-- /.box-header -->
                <div class="box-body">

    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Image</th>
                            <!-- <th>Gallery Image</th> -->
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    
                    </table>
     
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/js/custom/school.js')}}"></script>
    <script src="{{asset('admin/js/custom/delete.js')}}"></script>
    <script src="{{asset('admin/js/custom/status-change.js')}}"></script> 
@endsection
