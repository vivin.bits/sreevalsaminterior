@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
    Advertisement

    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">

                    <a class="btn btn-primary pull-right" href="{{url('admin/activity/create')}}" >Create</a>

                </div><!-- /.box-header -->
                <div class="box-body">

                    <table class="table table-hover table-bordered " id="sample">
                        <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')

<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/custom/status-change.js')}}"></script> 
    <script src="{{asset('admin/js/custom/module/schoolActivity.js')}}"></script>
<script src="{{asset('admin/js/custom/delete.js')}}"></script>
@endsection
