 @extends('admin.layout.app')

@section('head_title')
Google API Settings
@endsection
<style type="text/css">
  .field-icon {
  float: right;
  margin-top: -25px;
  position: relative;
  z-index: 2;
  cursor: pointer;
  margin-right: 5px;
}
</style>
@section('main_content')
 <!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Google API Settings </li>
          </ul>
                 </div>

      </div>

       @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif  

      <section class="section-padding">

        <div class="container-fluid">

<div class="row">
 <div class="col-xs-12">
            <div class="box">

  
  <form action="{!! URL::to('settings/googleapi-update') !!}" method="post">
                       <div class="box-body">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
       <div class="form-group col-md-8">
                  <label for="site_key">Site Key</label>
                  <input type="text" name="site_key" class="form-control" id="site_key" value="{!! $settings['site_key'] !!}">
                  <span class="help-block required error">{{$errors->first('site_key')}}</span>
                </div>
 <div class="form-group col-md-8">
                  <label for="secret_key">Secret Key</label>
                  <input type="text" name="secret_key" class="form-control" id="secret_key" value="{!! $settings['secret_key'] !!}">
                  <span class="help-block required error">{{$errors->first('secret_key')}}</span>
                </div>
                
     
                </div><!-- /.box-body -->
            

            <div class="box-footer p-r-0">
              <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div> 

       

         </form>

  </div>
    </div>
  </div>

</div>
      </section>
        @endsection


        @section('additional_scripts')
<script src="{{ asset('admin/js/bootstrap-switch.min.js')}}"></script>
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script src="{{ asset('admin/js/datatables.mark.js')}}"></script>
<script src="{{ asset('admin/js/iziToast.min.js')}}"></script>
<script type="text/javascript">
    $(document).on("click",".toggle-password",function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>



@endsection
