 @extends('admin.layout.app')

@section('head_title')
Settings
@endsection
@section('main_content')
 <!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Settings </li>
          </ul>
                 </div>

      </div>

       @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif  

      <section class="section-padding">

        <div class="container-fluid">

<div class="row">

   <div class="col-xs-12">
            <div class="box">
  
  <form action="{!! URL::to('settings/update') !!}" method="post">
                       <div class="box-body">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
       <div class="form-group col-md-9">
                  <label for="contact_email">Admin Email</label>
                  <input type="text" name="admin_email" class="form-control" id="admin_email" value="{!! $settings['admin_email'] !!}">
                  <span class="help-block required error">{{$errors->first('admin_email')}}</span>
                </div>

                 <div class="form-group col-md-9">
                  <label for="google_analytics">Google Analytics</label>
                  <textarea name="google_analytics" class="form-control" rows="4" cols="6" id="google_analytics" >{!! $settings['google_analytics'] !!}</textarea> 
                  <span class="help-block required error">{{$errors->first('google_analytics')}}</span>
                </div>

   
      <div class="form-group col-xs-9">
                               <label for="contact_email">Meta Title</label>
                               <input type="text" name="meta_title" class="form-control" id="meta_title" value="{!! $settings['meta_title'] !!}">
                               <span class="help-block required error">{{$errors->first('meta_title')}}</span>
                           </div>
                           
                            <div class="form-group col-xs-9">
                               <label for="contact_email">Meta Key</label>
                               <input type="text" name="meta_key" class="form-control" id="meta_key" value="{!! $settings['meta_key'] !!}">
                               <span class="help-block required error">{{$errors->first('meta_key')}}</span>
                           </div>
                            <div class="form-group col-xs-9">
                               <label for="contact_email">Meta Description</label>
                              <textarea name="meta_description" class="form-control" rows="4" cols="6" id="meta_description">{!! $settings['meta_description'] !!}</textarea>
                               <span class="help-block required error">{{$errors->first('meta_description')}}</span>
                           </div>


                           
                       </div><!-- /.box-body -->
         
            <div class="box-footer p-r-0">
              <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div> 

       

         </form>

  </div> </div> </div>
</div>
      </section>
        @endsection


        @section('additional_scripts')
<script src="{{ asset('admin/js/bootstrap-switch.min.js')}}"></script>
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script src="{{ asset('admin/js/datatables.mark.js')}}"></script>
<script src="{{ asset('admin/js/iziToast.min.js')}}"></script>




@endsection
