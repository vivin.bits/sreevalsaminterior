 @extends('admin.layout.app')

@section('head_title')
SMTP Settings
@endsection
<style type="text/css">
  .field-icon {
  float: right;
  margin-top: -25px;
  position: relative;
  z-index: 2;
  cursor: pointer;
  margin-right: 5px;
}
</style>
@section('main_content')
 <!-- Breadcrumb-->
      <div class="breadcrumb-holder">
        <div class="container-fluid">
          <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
            <li class="breadcrumb-item active">SMTP Settings </li>
          </ul>
                 </div>

      </div>

       @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif  

      <section class="section-padding">

        <div class="container-fluid">

<div class="row">
   <div class="col-xs-12">
            <div class="box">
  <form action="{!! URL::to('settings/smtp-update') !!}" method="post">
                       <div class="box-body">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
       <div class="form-group col-md-8">
                  <label for="host">Host</label>
                  <input type="text" name="host" class="form-control" id="host" value="{!! $settings['host'] !!}">
                  <span class="help-block required error">{{$errors->first('host')}}</span>
                </div>
 <div class="form-group col-md-8">
                  <label for="port">Port</label>
                  <input type="text" name="port" class="form-control" id="port" value="{!! $settings['port'] !!}">
                  <span class="help-block required error">{{$errors->first('port')}}</span>
                </div>
                 <div class="form-group col-md-8">
                  <label for="username">Username</label>
                  <input type="text" name="username" class="form-control" id="username" value="{!! $settings['username'] !!}">
                  <span class="help-block required error">{{$errors->first('username')}}</span>
                </div>
                 <div class="form-group col-md-8">
                  <label for="from_address">From Address</label>
                  <input type="text" name="from_address" class="form-control" id="from_address" value="{!! $settings['from_address'] !!}">
                  <span class="help-block required error">{{$errors->first('from_address')}}</span>
                </div>
                 <div class="form-group col-md-8">
                  <label for="from_name">From Name</label>
                  <input type="text" name="from_name" class="form-control" id="from_name" value="{!! $settings['from_name'] !!}">
                  <span class="help-block required error">{{$errors->first('from_name')}}</span>
                </div>

                 <div class="form-group col-md-8">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" id="password" value="{!! $settings['password'] !!}">
                  <span class="help-block required error">{{$errors->first('password')}}</span>
                  <span toggle="#password" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                </div>

                 <div class="form-group col-md-8">
                  <label for="encryption">Encryption</label>
                  <input type="text" name="encryption" class="form-control" id="encryption" value="{!! $settings['encryption'] !!}">
                  <span class="help-block required error">{{$errors->first('encryption')}}</span>
                </div>
                   
                </div><!-- /.box-body -->
         

            <div class="box-footer p-r-0">
              <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div> 

       

         </form>

  </div> </div></div>
</div>
      </section>
        @endsection


        @section('additional_scripts')
<script src="{{ asset('admin/js/bootstrap-switch.min.js')}}"></script>
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script src="{{ asset('admin/js/datatables.mark.js')}}"></script>
<script src="{{ asset('admin/js/iziToast.min.js')}}"></script>
<script type="text/javascript">
    $(document).on("click",".toggle-password",function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>



@endsection
