@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Testimonial 

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
@if(can('add_module'))
                    <a class="btn btn-primary pull-right" href="{{url('testimonial/create')}}" >Create</a>
                      @endif
                </div><!-- /.box-header -->
                <div class="box-body">

    @if(can('browse_module'))
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        @foreach($test as $test)
                        <?php $count++;?>
                        <tr>
                            <td>{!!$count !!}</td>
                            <td>{{$test->title}}</td>
                            <td>{!!$test->content!!}</td>
                            <td> <img src="{{asset('media/profile-images/'.$test->image)}}" 
                                     alt="San Fran" style="width:120px;"></td>
                    
                            <td>
                               {{--  @if(can('edit_module')) --}}
                                <a href="{{url('test/edit/'.$test->id)}}" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                            
                            
                            
                               {{--  @if(can('delete_module')) --}}
                                <a href="{{url('test/delete/'.$test->id)}}" title="Delete  Module"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              {{--   @endif --}}
                              </td>
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('js')
<script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
@endsection