@extends('admin.layout.app')
@section('title','| Users')
@section('additional_styles')
<style type="text/css">
  .error{
    color: #f00;
  }
</style>
@endsection
@section('head_title')
Users
@endsection
@section('main_content')
<!-- Content Header (Page header) -->
 <section class="content-header">
        <h1>
            Users
            
        </h1>
   {{--     @include('include.breadcrub') --}}
    </section>

<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                     {{--    <h3 class="box-title">Add New </h3> --}}
                        <a href="{!! URL::to('users') !!}" class="btn btn-primary pull-right">Back to Users</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            @if(Session::get('message'))

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
           </div>

           @endif
           <div class="col-md-10 p-left-0">
            <form role="form" name="adduser" action="{!! URL::to('users/create/') !!}" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

                <div class="form-group @if($errors->first('firstname')) has-error @endif">
                  <label for="firstname">First Name</label>
                  <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Enter first name" minlength="2" maxlength="30" value="{!! old('firstname') !!}">
                  <span class="help-block required">{{$errors->first('firstname')}}</span>
                </div>
                <div class="form-group @if($errors->first('lastname')) has-error @endif">
                  <label for="lastname">Last Name</label>
                  <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter last name"  maxlength="30" value="{!! old('lastname') !!}">
                  <span class="help-block required">{{$errors->first('lastname')}}</span>
                </div>
                <div class="form-group @if($errors->first('email')) has-error @endif">
                  <label for="name">Email</label>
                  <input type="text" name="email" class="form-control" id="name" placeholder="Enter email" minlength="6" maxlength="75" value="{!! old('email') !!}">
                  <span class="help-block required">{{$errors->first('email')}}</span>
                </div>

                <div class="form-group @if($errors->first('phone')) has-error @endif">
                  <label for="phone">Phone</label>
                  <input type="text" name="phone" class="form-control" id="phone" placeholder="Enter phone" minlength="6" maxlength="15" value="{!! old('phone') !!}">
                  <span class="help-block required">{{$errors->first('phone')}}</span>
                </div>
                <div class="form-group @if($errors->first('role')) has-error @endif"">
                  <label for="name">Role</label>
                  <select name="role" class="form-control">
                    <option value="">Select</option>
                    @foreach($roles as $role)

                    <option value="{!! $role->id !!}" @if(old('role') == $role->id) selected @endif>{!! $role->display_name !!}</option>

                    @endforeach
                  </select>
                  <span class="help-block required">{{$errors->first('role')}}</span>

                </div>
                <div class="form-group @if($errors->first('password')) has-error @endif">
                  <label for="name">Password</label>
                  <input type="password" name="password" class="form-control" minlength="8" maxlength="30" id="password" placeholder="Enter Password">
                  <span class="help-block required">{{$errors->first('password')}}</span>
                </div>
                <div class="form-group @if($errors->first('password_confirmation')) has-error @endif">
                  <label for="password_confirmation">Confirm Password</label>
                  <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" minlength="8" maxlength="30" placeholder="Confirm Password">
                  <span class="help-block required">{{$errors->first('password_confirmation')}}</span>
                </div>
        

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

            </form>
          </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

              

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->



@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/jquery.validate.min.js')}}"></script>
<script>
  $(function() {

  $("form[name='adduser']").validate({

    rules: {

     firstname: {
      required: true,
      minlength: 2
    },

     phone: {
      required: true,
      number: true
    },

    role: "required",

    email: {
      required: true,
      email: true
    },
    password: {
      required: true,
      minlength: 8
    }
  },
    // Validation messages
    messages: {

     firstname: {
      required: "Please enter your firstname",
      minlength: "Your password must be at least 2 characters long"
    },

     phone: {
      required: "Please enter your phone",
      number: "Please enter a valid number."
    },
    role: "Please select role",
    password: {
      required: "Please provide a password",
      minlength: "Your password must be at least 8 characters long"
    },
    email: {
      required: "Please enter your email address",
      email: "Please enter a valid email address"
    }
  },

  submitHandler: function(form) {
    form.submit();
  }
});
});
</script>
@endsection
