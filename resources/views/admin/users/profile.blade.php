@extends('admin.layout.app')
@section('title','| Profile')
@section('additional_styles')
<style type="text/css">  
input[type="file"] {
  display: none;
}
</style>
@endsection
@section('main_content')
<section class="content-header">
  <h1>
    Admin Profile

  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        @if(Session::get('message'))
        <div class="alert alert-success msgalt">
         <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>{!! Session::get('message') !!}
       </div>

       @endif
       <div class="box-body">
        <form method="post" name="editAdminUser" id="editAdminUser" action="{{url('profile')}}" enctype="multipart/form-data">
          {{csrf_field()}}

          <div class="col-md-4">
            <div class="form-group">
              <label for="image">Image</label>
              <div class="fake-input">
                <input id="uploadFile" disabled="disabled" type="text"  style="display: none;" >
                <div class="fileUpload">    
                  <input  id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg"/>
                </div>
              </div>
            </div>
            <div class="col-md-12" style="text-align: center;">
              <div class="prof-img" id="prof_img" style="text-align: center">
                <img src="{{asset('media/profile-images/'.$user->avatar)}}" class=" img-fluid img-thumbnail" alt="San Fran" style="height:200px;">
              </div>
              <label for="image" class="custom-file-upload">
                <i class="fa fa-cloud-upload"></i> Change Profile Picture
              </label>                            </div>
            </div>
            <div class="col-md-8">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Fist Name</label>
                    <input type="text" name="firstname" class="form-control" value="{!! $user->firstname !!}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Last Name</label>
                    <input type="text" name="lastname" class="form-control" value="{!! $user->lastname !!}">
                  </div>
                </div>                    
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email</label>
                    <input type="text" autocomplete="off" readonly="readonly" name="changemail" id="changemail" class="form-control" value="{!! $user->email !!}">
                    <input type="hidden" disabled="disabled" name="email" class="form-control" value="{{ $user->email }}" />
                  </div>
                  <div class="form-group" id="example2">
                    <div class="col-sm-offset-2 col-sm-10">

                      <a class="user-title" style="cursor: pointer" id="change_email">Change Email</a>
                    </div>
                  </div>
                </div>                  
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="bmd-label-floating">Phone</label>
                    <input type="text" name="phone" class="form-control" value="{!! $user->phone !!}">
                  </div>
                </div>                    
              </div>

              <div class="row" style="margin-top: 40px;">
               <div class="col-md-8">
                <button type="submit" class="btn btn-primary pull-left">Update Profile</button>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </form>                 
        <div class="modal fade mymodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header" style="color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #379392;">
            <h4 class="modal-title" id="myModalLabel">List of Users</h4>
            <button type="button" class="close" data-dismiss="modal"><span
              aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body rx56">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button> 
            </div>
          </div>
        </div>
      </div>
      <div id="popup" style="display: none">
        <span class="button b-close">X</span>
        <div class="common-popup">   
          <form name="changeEmail" id="changeEmail">
           {{csrf_field()}}
           <div id="form1" class="col-xs-12">
             <div class="form-group">
              <label class="bmd-label-floating" style="float: left;">Current Email</label>
              <input type="text" autocomplete="off" readonly="readonly" name="changemail" id="changemail" class="form-control" value="{!! $user->email !!}">                
            </div>
            <div class="form-group">
              <label for="inputEmail" class="bmd-label-floating" style="float: left;">New Email</label>           
              <input type="text" name="new_email" class="form-control" value="{!! old('new_email') !!}" />
              <input type="hidden" name="email" class="form-control" readonly="readonly" value="{{ $user->email }}" />
              <span class="new_email_error error"></span>              
            </div>            
            <div class="form-group">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary changeEmailbtn" id="formButton">Update</button>
                <div class="modal-body cmodal56">
                </div>
                <div id="successmsg"><span></span></div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div><!-- /.box-body -->
</div><!-- /.box -->

</div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">

</script>
<script>

 $( document ).ready(function() {
  function readURL(input, img_con) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#'+img_con).find('img').attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#image").change(function() {
    readURL(this, 'prof_img');
    $(this).parents('.form-group').find("#uploadFile").val(this.value);
  });
});
</script>

<script src="{{asset('js/validate.min.js')}}"></script>
<script src="{{ asset('admin/js/jquery.bpopup.min.js')}}"></script>
<script>
  jQuery(document).ready(function($){

 $("form[name='changeEmail']").validate({

  rules: {

   new_email: {
    required: true,
  },

},
    // Specify validation error messages
    messages: {

     new_email: {
      required: "Please enter new email address",
    },

  },

  submitHandler: function(form) {
    form.submit();
  }
});

 $(".changeEmailbtn").click(function(e) {
  var form = $("#changeEmail");
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    type: "POST",
    url: "{!! URL::to('change-adminmail') !!}",
    data: form.serialize(),
    beforeSend: function(){

     $('.cmodal56').html('Sending mail.Please wait....');
   },

   success: function(data) {

    console.log(data);
    $('.error', form).html('');
    if (data.fail) {
      console.log(data.errors);
      $('#successmsg span').html('');
      $('.cmodal56').html('');
      $.each(data.errors, function(index, value) {
        var errorDiv = '.' + index + '_error';
        $(errorDiv, form).addClass('error');
        $(errorDiv).empty().append(value);

      });
    }
    if (data.success) {
       $('.cmodal56').html('');
       $('#successmsg').css('display','block')
       $('#successmsg span').text(data.message);
                 }
            } 
          });
});

 $('#change_email').bind('click', function(e) {
  e.preventDefault();

  $('#popup').bPopup({
    contentContainer: '.common-popup',
    closeClass: 'b-close',
    transition: 'slideDown',
    onClose: function () {
      $('.popup-container').empty();
    }
  });
});
});
</script>
<script type="text/javascript">

</script>

