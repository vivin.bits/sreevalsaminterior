@extends('admin.layout.app')

@section('additional_styles')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        Visit Form

    </h1>
  {{--   @include('include.breadcrub') --}}
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
               
                <div class="box-body">

    @if(can('browse_module'))
    
                
    <table id="example2" class="table table-bordered dataTable table-hover" style="width: 100%">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Purpose of visit</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        @foreach($visits as $visit)
                        <?php $count++;?>
                        <tr>
                            <td>{!! $count!!}</td>
                            <td>{{$visit->name}}</td>
                            <td>{{$visit->address}}</td>
                            <td>{{$visit->phone}}</td>
                            <td>{{$visit->email}}</td>
                            <td>{{$visit->reason}}</td>
                            <td>{{$visit->date}}</td>
                            <td>{{$visit->time}}</td>
                        
                            <td>@if ($visit->status == '1') 
                             <div class="pretty p-switch p-fill"><input type="checkbox" class="userStatus"  data-type="activate" id="{{ $visit->id }}" data-id="{{  $visit->id }}"  data-status="{{  $visit->status }}" checked onchange="changeEmailStatus(this,'inactivate')" ><div class="state p-success"><label></label></div></div>
                             @else 
                             <div class="pretty p-switch p-fill"><input type="checkbox"  class="userStatus"   data-type="inactivate" id="{{  $visit->id }}"  data-id="{{  $visit->id }}" data-status="{{ $visit->status }}"  onchange="changeEmailStatus(this,'activate')"><div class="state p-success"><label></label></div></div>
                             @endif
                         </td>
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                     @else
        @include('admin.no-access-content')
    @endif
                </div>
            </div>
        </div>
    </div>
   
@endsection

@section('additional_scripts')
<script src="{{ asset('admin/js/mark.js(jquery.mark.min.js)')}}"></script>
<script type="application/javascript">

$('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "columnDefs": [{ 'orderable': false, 'targets': [9,10] }],
        "searching": true,
        "ordering": true,
        "info": true,
        "mark": true,
        "scrollX": true,
       "autoWidth": false,
        "lengthMenu": [ [25,10, 50, -1], [25,10, 50, "All"] ]
    });
    
</script>
@endsection

