<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
<style>
   .mc{max-width:580px;width:100%;margin:auto;border:1px solid #f0f0f0;box-shadow:2px 0px 6px #f0f0f0;padding-bottom:25px;}
    table{width:100%;} 
    a.button:hover{background:#EA6847 !important;}
</style>
</head>
<body>
    <div class="mc">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding:25px;"><img src="{{asset('images/email/images/logo-email.png')}}" /></td>
        </tr>
        <tr>
          <td>
            <img src="{{asset('images/email/images/banner-email.jpg')}}" style="max-width:100%;height:auto;" />
          </td>
        </tr>
        <tr>
          <td align="center" valign="middle" style="padding:30px 0;">
            @yield('content')
          </td>
        </tr>
      </table>
    </div>
</body>
</html>