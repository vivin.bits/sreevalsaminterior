@extends('layouts.app')
@section('content')
     
       
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('website/images/background/8.jpg')}})">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                	<h1>About Us</h1>
                </div>
                <div class="pull-right">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>About us</li>
                    </ul>
                </div>
            </div>
            <div class="contact-number text-center"><span class="icon flaticon-phone-call"></span>Call Us: +91 9349 429 212</div>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Success Section-->
    <section class="success-section">
    	<div class="auto-container">
        	<div class="row clearfix">
            @if($about)
                <!--Image Column-->
                <div class="image-column col-md-5 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    	<div class="image">
                        	<img src="{{getImageByPath($about->image,'370x504','profile-images')}}" alt="" />
                        </div>
                        <div class="small-img">
                        	<img src="{{asset('website/images/resource/success-2.jpg')}}" alt="" />
                        </div>
                    </div>
                </div>
                <!--Content Column-->
                <div class="content-column col-md-7 col-sm-12 col-xs-12 tt">
                	<div class="inner-column">
                    	<div class="since-year clearfix">
                        	<span class="title">ABOUT US</span>
                           
                            
                        </div>
                       
                        <div class="text">
                        {!!$about->main_description!!}
                        </div>
                        @endif
                        <div class="fact-counter">
                            <div class="row clearfix">
                            
                                <!--Column-->
                                <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="3500" data-stop="200">0</span>
                                            <h4 class="counter-title">Projects</h4>
                                        </div>
                                    </div>
                                </div>
                        
                                <!--Column-->
                                <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="2500" data-stop="895">0</span>
                                            <h4 class="counter-title">Interior Designs</h4>
                                        </div>
                                    </div>
                                </div>
                        
                                <!--Column-->
                                <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="2200" data-stop="954">0</span>
                                            <h4 class="counter-title">Customers</h4>
                                        </div>
                                    </div>
                                </div>
                        
                                <!--Column-->
                                <div class="column counter-column col-md-3 col-sm-6 col-xs-12">
                                    <div class="inner">
                                        <div class="count-outer count-box">
                                            <span class="count-text" data-speed="2000" data-stop="25">0</span>
                                            <h4 class="counter-title">On Going/h4>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                                
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Success Section-->
    
    <!--Fluid Section One-->
    <section class="fluid-section-one">
    	<div class="outer-container clearfix">
        	<!--Image Column-->
            @if($whychoose)
            <div class="image-column" style="background-image:url({{getImageByPath($whychoose->image,'642x735','banner')}});">
            	<figure class="image-box"><img src="{{getImageByPath($whychoose->image,'642x735','banner')}}" alt=""></figure>
            </div>
        	<!--Content Column-->
            <div class="content-column">
            	<div class="inner-box">
                    <div class="sec-title">
                        <div class="title">We Offer Best Services & Solutions</div>
                        <h2><span class="theme_color">Why </span> Choose Us</h2>
                    </div>
                    <div class="text">{{$whychoose->message}}</div>
                    <ul class="list-style-one clearfix">
                    	<li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-briefcase-1"></span>15 years of Experience</li>
                        <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-diamond-1"></span>Exclusive Partnerships</li>
                        <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-bank-building"></span>Individual Approach</li>
                        <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>Global Best Solutions</li>
						<li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>Best Quality</li>
						<li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>24/7 Online Support</li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
    </section>
    <!--End Fluid Section One-->
    
  
    
    
    <!--Call To Action Two-->
    <section class="call-to-action-section-two" style="background-image:url(images/background/7.jpg)">
    	<div class="auto-container">
        	<div class="row clearfix">
            	
                <div class="column col-md-7 col-sm-12 col-xs-12">
                	<h2><span class="theme_color">Interior</span> Company</h2>
                    <div class="text">If you have any Interior, construction & renovation work need, simply call our 24 hour emergecny number.</div>
                </div>
                <div class="btn-column col-md-5 col-sm-12 col-xs-12">
                	<div class="number">+91 9349 429 212 <span class="theme_color"> or </span> <a href="contact.html" class="theme-btn btn-style-five">Contact Us</a> </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--End Call To Action Two-->
    <section id="contact-btns">
    <div class="container-fluid">
        <a href="tel:+919349429212" class="btn btn-primary">Call Us Now</a>
        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#getForm">GET A FREE QUOTE</a>
    </div>
</section>




    
@endsection
