@extends('layouts.app')
@section('content')
     
        <!-- Breadcrumbs Start -->
        <div class="rs-breadcrumbs">
                <img src="{{asset('website/images/bg-box.jpg')}}" alt="Breadcrumbs Image">
            <div class="container">
                <div class="breadcrumbs-content">
                    <h1 class="title">About Us</h1>
                    <div class="page-path text-center">
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>About Us</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<!-- Breadcrumbs End -->

          
        <!-- About Us Start -->
        @if($aboutusDetails) 
        <div id="rs-about" class="rs-about align-items-center gray-bg sec-spacer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-md-12 mb-md-30">
                            <div class="about-img text-center">
                                <img src="{{getImageByPath($aboutusDetails->image,'429x369','profile-images')}}" alt="img02"/>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-12 mpl-15 pl-40">
                            <div class="about-tab style-1 extra-padding ">
                                <div class="sec-title margin-0 text-left">
    
                                    <h2 class="section-title primary-color"><span class="title-sec">{{$aboutusDetails->title}} </h2> 
    
                                    <h4 class="section-subtitle-right">A  journey  in  moulding... </h4>     
                                    <p>{!!$aboutusDetails->short_description!!} </p>
               
                               
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                        <p>{!!$aboutusDetails->main_description!!} </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Us End -->


@endif


        <!-- Cta Start -->
        <div class="rs-kid-cta primary-bg pt-50 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-12 mb-md-30">
                        <div class="cta-text white-color">
                            <h2 class="margin-0 white-color">Enroll Your Child In Our School</h2>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="cta-button">
                            <a href="#" class="readon hvr-ripple-out readon-cta white uppercase">Enroll Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Cta End -->
        
        <!-- Team Start -->
        <div id="rs-team" class="rs-team style-2 sec-spacer">
            <div class="container">
                <div class="sec-title text-center">
                    <h2 class="section-title primary-color">Anecdotal Achievements since last 15 years:-</h2> 
                    <h4 class="section-subtitle"></h4>
                </div>
				<div class="team-center-carousel owl-carousel extra-padding">

@if($school)
@foreach($school as $schools)
                    <div class="team-item">
                        <div class="team-img">
                            <img src="{{getImageByPath($schools->image,'325x319','school-student')}}" alt="team Image" />
                        </div>
                        
                        <div class="team-content ">
                            <div class="team-top">
                                <h4 class="team-title"><a href="#">{{$schools->name}}</a></h4>
                            </div>
                            <div class="round-shape "></div>
                            <div class="clc">
                                <span class="team-subtitle">Film Actress</span>
                            
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
<!-- 
                    <div class="team-item">
                            <div class="team-img">
                                <img src="{{asset('website/images/ach/chithra .jpg')}}" alt="team Image" />
                            </div>
                            
                            <div class="team-content ">
                                <div class="team-top">
                                    <h4 class="team-title"><a href="#">Dr. Chithra </a></h4>
                                </div>
                                <div class="round-shape "></div>
                                <div class="clc">
                                    <span class="team-subtitle">IAS</span>
                                
                                </div>
                            </div>
                        </div> -->

                        <!-- <div class="team-item">
                                <div class="team-img">
                                    <img src="{{asset('website/images/ach/babitha.jpg')}}" alt="team Image" />
                                </div>
                                
                                <div class="team-content ">
                                    <div class="team-top">
                                        <h4 class="team-title"><a href="#">Babitha Jayan</a></h4>
                                    </div>
                                    <div class="round-shape "></div>
                                    <div class="clc">
                                        <span class="team-subtitle">District Panchayat Member <br>Alappuzha </span>
                                    
                                    </div>
                                </div>
                            </div> -->


                    
           
            
     


				</div>
			</div>
		</div>
        <!-- Team End -->
        




    
@endsection