@extends('layouts.app')
@section('content')
@if($contact)
 
    <!--Page Title-->
    <section class="page-title" style="background-image:url({{asset('website/images/background/8.jpg')}}">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                	<h1>Contact Us</h1>
                </div>
                <div class="pull-right">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
            </div>
            <div class="contact-number text-center"><span class="icon flaticon-phone-call"></span>Call Us: +91 9349 429 212</div>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Map Section-->
    <section class="map-section">
        <!--Map Outer-->
        <div class="map-section">
            <!--Map Outer-->
            <div class="map-outer">
                <!--Map Canvas-->
<iframe src=https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3929.691798835347!2d76.32512821460683!3d9.95957999287887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s49%2F1638-A6+Thykoodam+Vyttila+P.O%2C+Ernakulam+India-82019!5e0!3m2!1sen!2sin!4v1525144531287 width=600 height=450 frameborder=0 style=border:0 allowfullscreen></iframe>
                <div class="map-canvas"
                    data-zoom="15"
                    data-lat="9.959580"
                    data-lng="76.327317"
                    data-type="roadmap"
                    data-hue="#ffc400"
                    data-title="Sreevalsam Interiors"
                    data-icon-path="images/icons/map-marker.png"
                    data-content="49/1638-A6 Thykoodam
Vyttila P.O, Ernakulam
India-82019<br><a href='mailto:info@youremail.com'>Mail: info@sreevelsaminteriors.com</a>">
                </div>
            </div>
        </div>
    </section>
    <!--End Map Section-->
    
    <!--Contact Section-->
    <section class="contact-section tt">
    	<div class="auto-container">
        	
            <h2><span class="theme_color">Get</span> in Touch</h2>
            <div class="text">You can talk to our online representative at any time. Please use our Live Chat System on our website or Fill up below instant messaging programs. <br> Please be patient, We will get back to you. Our 24/7 Support, General Inquireies Phone: +91 9349 429 212</div>
            <div class="row clearfix">
            	<div class="form-column col-lg-9 col-md-8 col-sm-12 col-xs-12">
                	<div class="inner-column">
                    <div class="alert "  role="alert"><h3 id="message" style="color: #2ECC40"></h3></div>
                    
                        <!--Contact Form-->
                        <div class="contact-form style-two">
                            <form method="post" action="_email/contact-form.php" id="contact-form">
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="text" name="name" value="" placeholder="Name" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="email" name="email" value="" placeholder="Email" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="text" name="subject" value="" placeholder="Subject" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="text" name="phone" value="" placeholder="Phone" required>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 co-xs-12">
                                        <textarea name="message" placeholder="Your Massage"></textarea>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 co-xs-12">
                                        <button type="submit" class="theme-btn btn-style-one">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--End Contact Form-->
                        
                    </div>
                </div>
                <div class="info-column col-lg-3 col-md-4 col-sm-12 col-xs-12">
					<h2><span class="theme_color">COCHIN</span></h2>
                	
                    <ul class="list-style-two">
                        <li><span class="icon flaticon-home-1"></span><strong>{{$contact->title}}, {{$contact->phone}} Thykoodam,</strong> {!!$contact->address!!}</li>
                        <li><span class="icon flaticon-envelope-1"></span><strong>Send your mail at</strong>{{$contact->email}}</li>
                        <li><span class="icon flaticon-technology-2"></span><strong>Have Any Question</strong>+91 9349 429 212, 0484 2306 221</li>
                        <li><span class="icon flaticon-clock-1"></span><strong>Working Hours</strong>Mon-Sat:9.30am to 7.00pm</li>
                    </ul>
					
					<h2><span class="theme_color">THIRUVALLA</span></h2>
                	
                    <ul class="list-style-two">
                        <li><span class="icon flaticon-home-1"></span><strong>First Floor, Keeppallil Building,  </strong> Muthoor P O, Thiruvalla - 07, Pathanamthitta </li>
                        <li><span class="icon flaticon-envelope-1"></span><strong>Send your mail at</strong>{{$contact->email}}</li>
                        <li><span class="icon flaticon-technology-2"></span><strong>Have Any Question</strong>+91 9349 429 212</li>
                       
                    </ul>
						<h2><span class="theme_color">THRISSUR</span></h2>
                	
                    <ul class="list-style-two">
                        <li><span class="icon flaticon-home-1"></span><strong>Sreevalsam interiors,</strong> Museum Road, Chembukav , Thrissur</li>
                        <li><span class="icon flaticon-envelope-1"></span><strong>Send your mail at</strong>{{$contact->email}}</li>
                        <li><span class="icon flaticon-technology-2"></span><strong>Have Any Question</strong>+91 9349 429 212</li>
                       
                    </ul>
					
					
					
					
					
					
					@endif
                    
                </div>
            </div>
        </div>
    </section>
@endsection
