@extends('layouts.app')
@section('content')
<!-- Start Banner Carousel -->
        <!-- Start Banner -->
        <div class="inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-lg-9">
                        <div class="content">
                            @if($course_details)
                            <h1>{{$course_details->course}}</h1>
                            <p>{{$course_details->department->name}}</p>
                        </div>
                    </div>
         
                </div>
            </div>
        </div>
        <!-- End Banner --> 
 
        <!-- Start Course Description -->
        <section class="about inner padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-push-5 left-block">
                        <h2>COURSE DESCRIPTION</h2>
                        {!!$course_details->content!!}
                     
                   
                    </div>
                    <div class="col-md-5 col-md-pull-7">
                        <div class="enquire-wrapper">
                            <figure class="hidden-xs hidden-sm"><img src="{{getImageByPath($course_details->image,'440x290','profile-images')}}" class="img-responsive" alt=""></figure>
                      
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                {!!$course_details->description!!}
                </div>
            </div>
        </section>
        @endif
        <!-- End Course Description --> 




    
@endsection