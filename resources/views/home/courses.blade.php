@extends('layouts.app')
@section('content')
<!-- Start Banner Carousel -->
        <!-- Start Banner -->
        <div class="inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-lg-9">
                        <div class="content">
                      
                            <h1>Our Courses</h1>
                         
                        </div>
                    </div>
         
                </div>
            </div>
        </div>
        <!-- End Banner --> 
 
        <section class="how-study padding-lg">
            <div class="container">
                <h2> <span></h2>
                <ul class="row">

                @foreach ($all_courses as $course)
                    <li class="col-sm-4 mb-20">
                        <div class="overly">
                            <div class="cnt-block">
                                <h3><a href="course/{{$course->slug}}">{{$course->course}}</a></h3>
                                <p>{{$course->department->name}}</p>
                            </div>
                            <a href="course/{{$course->slug}}" class="more"><i class="fa fa-university" aria-hidden="true"></i></a> </div>
                        <figure><img src="{{getImageByPath($course->image,'440x290','profile-images')}}" class="img-responsive" alt=""></figure>
                    </li>
                @endforeach

            

                </ul>
            </div>
        </section>






@endsection