@extends('admin.layout.app')

@section('css')

@endsection

@section('main_content')

<section class="content-header">
    <h1>
        CONTACT ENQUIRY 

    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">





                </div><!-- /.box-header -->
                <div class="box-body">


                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Sl#</th>
                            <th>name</th>
                            <!-- <th>last name</th> -->
                            <th>email</th>
                            <!-- <th>subject</th> -->
                            <th>message</th>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>

    @endsection

    @section('js')
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/js/custom/module/enquiry.js')}}"></script>
    <script src="{{asset('admin/js/custom/delete.js')}}"></script>
    @endsection
