@extends('layouts.app')
@section('content')
<div class="rs-breadcrumbs">
            <img src="{{asset('website/images/bg-box.jpg')}}" alt="Breadcrumbs Image">
            <div class="container">
                <div class="breadcrumbs-content">
                    <h1 class="title">Our Facility</h1>
                    <div class="page-path text-center">
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>Our Facility</li>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
		<!-- Breadcrumbs End -->

        <!-- Services Start -->
        <div class="rs-gallery-section pt-90 pb-70">
            <div class="container">
                                      <div class="row">


@if($facility)
@foreach($facility as $activities)
                                            <div class="col-lg-4 mb-50">
                                                    <div class="event-item blue-color">
                                                        <div class="event-img">
                                                            <a href="">   <img src="{{getImageByPath($activities->image,'370x230','profile-images')}}" alt=""></a>
                                                         
                                                         
                                                        </div>
                                                           <div class="middle-part">
                                            
                                                    <h3 class="title"><a class="title-act" href="#">{{$activities->title}}</a></h3>
                                     
                                                </div>
                                                    </div>
                                                </div>
@endforeach
@endif



                        <!-- <div class="col-lg-4 mb-50">
                            <div class="event-item blue-color">
                                <div class="event-img">
                                    <a href="">   <img src="{{asset('website/images/img_not_available.jpg')}}" alt=""></a>
                                 
                                 
                                </div>
                                   <div class="middle-part">
                    
                            <h3 class="title"><a class="title-act" href="#">Guiding</a></h3>
             
                        </div>
                            </div>
                        </div> -->

                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="{{asset('website/images/img_not_available.jpg')}}" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">DCL</a></h3>
                 
                            </div>
                                </div>
                            </div> -->

                            
                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="{{asset('website/images/activity/kcsl.jpg')}}" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">KCSL</a></h3>
                 
                            </div>
                                </div>
                        </div> -->

<!--                             
                        <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="{{asset('website/images/img_not_available.jpg')}}" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">NCC</a></h3>
                 
                            </div>
                                </div>
                            </div> -->

                            
                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="{{asset('website/images/activity/spc.jpg')}}" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">SPC</a></h3>
                 
                            </div>
                                </div>
                        </div> -->

                            



                    </div>    
            </div>
        </div>
        <!-- Services End -->
        
       
        @endsection




       