@extends('layouts.app')
@section('content')
   <!-- Breadcrumbs Start -->
   <div class="rs-breadcrumbs">
            <img src="{{asset('website/images/bg-box.jpg')}}" alt="Breadcrumbs Image">
            <div class="container">
                <div class="breadcrumbs-content">
                    <h1 class="title">Our Gallery</h1>
                    <div class="page-path text-center">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Our Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<!-- Breadcrumbs End -->

        <!-- Services Start -->
        <div class="rs-gallery-section pt-90 pb-70">
            <div class="container">

                <div class="row grid">
                @if($gallery)
                @foreach($gallery as $galleries)
                    <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter1 filter3">
                        <div class="single-gallery style-2">
                            <img src="{{getImageByPath($galleries->main_image,'459x266.67','album')}}" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="{{getImageByPath($galleries->main_image,'325x319','album')}}" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif 
                    <!-- <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter3">
                        <div class="single-gallery style-2">
                            <img src="{{asset('website/images/gallery/img/2.jpg')}}" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="{{asset('website/images/gallery/img/2.jpg')}}" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter2">
                        <div class="single-gallery style-2">
                            <img src="{{asset('website/images/gallery/img/3.jpg')}}" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="{{asset('website/images/gallery/img/3.jpg')}}" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
       -->
        
                </div>
            </div>
        </div>
        @endsection