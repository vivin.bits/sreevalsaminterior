@extends('layouts.app')
@section('content')
        <!-- Start Banner -->
        <div class="inner-banner blog">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content">
                            <h5>FROM THE DESK OF THE CHAIRMAN</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Banner --> 

        <!-- Start About -->
        <section class="about inner padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 left-block">
                    <h4>{{$message->title}}</h4>
                        {!!$message->message!!}
                    </div>
                    <div class="col-md-5 about-right"> <img src="{{getImageByPath($about->image,'440x290','profile-images')}}" class="img-responsive" alt=""> </div>
                </div>
            </div>
        </section>
        <!-- End About -->
        <!-- End Course Description --> 




    
@endsection