@extends('layouts.app')
@section('content')
        <!-- Start Banner -->
        <div class="inner-banner blog">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content">
                            <h3>MISSION & VISSION</h3>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Banner --> 

        <!-- Start About -->
        <section class="about inner padding-lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 left-block">
                    <h4>Mission</h4>
                        {!!$mission->mission!!}
                        <h4>Vission</h4>
                        {!!$mission->vission!!}

                    </div>
                    <div class="col-md-5 about-right"> <img src="{{getImageByPath($about->image,'440x290','profile-images')}}" class="img-responsive" alt=""> </div>
                </div>
            </div>
        </section>
        <!-- End About -->
        <!-- End Course Description --> 




    
@endsection