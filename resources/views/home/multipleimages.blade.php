



<div class="filter-list row clearfix gallery-thumbs">
    @if($galler_images)
@foreach($galler_images as $gallery )
<div class="gallery-item mix all kitchen col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="{{getImageByPath($gallery->image,'354.03x198.35','tab-images')}}" alt="">
                                <!--Overlay Box-->
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                         
                                            <li><a href="{{getImageByPath($gallery->image,'2000x1000','tab-images')}}" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">{{$gallery->content}}</a></h3>
                                           
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>  
                    
                  
                   