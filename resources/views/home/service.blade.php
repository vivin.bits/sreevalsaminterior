@extends('layouts.app')
@section('content')
<!--Page Title-->
@if($service)
<section class="page-title" style="background-image:url({{asset('website/images/background/8.jpg')}}">
    <div class="auto-container">
        <div class="clearfix">
            <div class="pull-left">
                <h1>{{$service->title}}</h1>
            </div>
            <div class="pull-right">
                <ul class="page-breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li>{{$service->title}}</li>
                </ul>
            </div>
        </div>
        <div class="contact-number text-center"><span class="icon flaticon-phone-call"></span>Call Us: +91 9349 429 212</div>
    </div>
</section>
<!--End Page Title-->

<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">



            <!--Content Side-->
            <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="services-single">
                    <div class="inner-box">

                        <!--Services Gallery-->
                        <div class="services-gallery">
                            <div class="row clearfix">
                                <div class="column col-md-8 col-sm-8 col-xs-12">
                                    <div class="image">
                                        <img src="{{getImageByPath($service->main_image,'503x326','album')}}">
                                    </div>
                                </div>
                               <div class="column col-md-4 col-sm-4 col-xs-12">
                                   <div class="image">
                                       <img src="{{getImageByPath($service->main_image1,'247x164','album')}}" alt="" />
                                   </div>
                                   <div class="image">
                                       <img src="{{getImageByPath($service->main_image2,'247x164','album')}}" alt="" />
                                   </div>
                               </div>
                            </div>
                        </div>
                        <h2>{{$service->title}}</h2>
                        <div class="text">
                            <p>{!!$service->description!!}</p>


                        </div>



                    </div>
                </div>
            </div>

            @endif
            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <aside class="sidebar default-sidebar">

                    <!--Blog Category Widget-->
                    <div class="sidebar-widget sidebar-blog-category">
                        <ul class="blog-cat">
                            @if($servicelist)
                            @foreach($servicelist as $list)
                            <li class="{{ Request::segment(1) === 'categories' ? 'active' : null }}"><a href="{{url('service/'.$list->slug)}}">{{$list->title}}</a></li>
                           @endforeach
                            @endif


                        </ul>
                    </div>

                    <!--Brochure-->
                    <div class="sidebar-widget brochure-widget">
                        <div class="sidebar-title">
                            <h2><span class="theme_color">Download</span> Brochures</h2>
                        </div>

                        <div class="brochure-box">
                            <div class="inner">
                                <span class="icon fa fa-file-pdf-o"></span>
                                <div class="text">Project-One .pdf</div>
                            </div>
                            <a href="#" class="overlay-link"></a>
                        </div>



                    </div>

                    <!--Solution Box-->
                    <div class="solution-box" style="background-image:url(images/resource/image-2.jpg)">
                        <div class="inner">
                            <div class="title">Quick Contact</div>
                            <h2>Get Solution</h2>
                            <div class="text">Contact us at the Interior office nearest to you or submit a business inquiry online.</div>
                            <a class="solution-btn theme-btn" href="{{url('contact')}}">Contact Us</a>
                        </div>
                    </div>

                </aside>
            </div>







        </div>
    </div>
</div>
<!--End Sidebar Page Container-->

<!--Call To Action Two-->
<section class="call-to-action-section-two" style="background-image:url(images/background/7.jpg)">
    <div class="auto-container">
        <div class="row clearfix">

            <div class="column col-md-7 col-sm-12 col-xs-12">
                <h2><span class="theme_color">Interior</span> Company</h2>
                <div class="text">If you have any Interior, construction & renovation work need, simply call our 24 hour emergecny number.</div>
            </div>
            <div class="btn-column col-md-5 col-sm-12 col-xs-12">
                <div class="number">+91 9349 429 212 <span class="theme_color"> or </span> <a href="{{url('contact')}}" class="theme-btn btn-style-five">Contact Us</a> </div>
            </div>

        </div>
    </div>
</section>
<!--End Call To Action Two-->
<section id="contact-btns">
    <div class="container-fluid">
        <a href="tel:+919349429212" class="btn btn-primary">Call Us Now</a>
        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#getForm">GET A FREE QUOTE</a>
    </div>
</section>

@endsection
