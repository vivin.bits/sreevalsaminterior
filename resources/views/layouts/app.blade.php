<<<<<<< HEAD
<!doctype html>
<html lang="en">
<head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Bethany Balikamadhom</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        @yield('css')
        <link rel="shortcut icon" type="{{asset('website/image/x-icon')}}" href="{{asset('website/images/fav.png')}}">
        <!-- bootstrap v4 css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/bootstrap.min.css')}}">
        <!-- Hover CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/hover-min.css')}}">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/font-awesome.min.css')}}">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/animate.css')}}">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/owl.carousel.css')}}">
		<!-- slick css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/slick.css')}}">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/magnific-popup.css')}}">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/fonts/flaticon.css')}}">
        <!-- rsmenu CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/rsmenu-main.css')}}">
        <!-- rsmenu transitions CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/rsmenu-transitions.css')}}">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/style.css')}}">
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="{{asset('website/css/responsive.css')}}">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    </head>
    <body>

        <!-- Start Preloader -->
        <div id="loading">
            <div class="element">
                <div class="sk-folding-cube">
                    <div class="sk-cube1 sk-cube"></div>
                    <div class="sk-cube2 sk-cube"></div>
                    <div class="sk-cube4 sk-cube"></div>
                    <div class="sk-cube3 sk-cube"></div>
                </div>
            </div>
        </div>
        <!-- End Preloader -->
 <!--Full width header Start-->
 <div class="full-width-header">
        <!-- Start Header -->
        <header id="rs-header" class="rs-header style-1">
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-12">
                                <div class="logo-area">
                                    <a href="index.html"><img src="{{asset('website/images/logo.png')}}" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-12">
                                <div class="main-menu clearfix">
                                    <a class="rs-menu-toggle"><i class="fa fa-bars"></i>Menu</a>
                                    <nav class="rs-menu">
                                        <ul class="nav-menu">
                                            <!-- Home -->
                                            <li class="current-menu-item current_page_item menu-item-has-children"> <a href="{{url('/')}}">Home</a>
                                               
                                            </li>
                                            <!-- End Home --> 

                                            <!--About Start-->
                                            <li><a href="{{url('about')}}">About Us</a></li>
                                            <!--About End-->

                                            <!--Teacher Menu Start -->
                                            <li class="menu-item-has-children"> <a href="{{url('allFacility')}}">FACILITIES</a>
                                            
                                            </li>

                                             <!--Teacher Menu Start -->
                                            <li class="menu-item-has-children"> <a href="{{url('gallery')}}">GALLERY</a>
                                            
                                            </li>

                                                  <li><a href="{{url('activity')}}">ACTIVITIES
</a></li>
                                            <!--Teacher Menu End -->

                                         
                                            <!--Class Menu End -->

                            

                                     

                                            <!--Blog Menu End-->

                                            <!--Contact Menu Start-->
                                            <li><a href="{{url('contact')}}">Contact</a></li>
                                            <!--Contact Menu End-->
                                        </ul> <!-- //.nav-menu -->
                                    </nav>
                                </div> <!-- //.main-menu -->
                                <div class="right-bar-icon text-right">
                                    <a class="rs-search" data-target=".search-modal" data-toggle="modal" href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <!-- Menu End -->
            </header>
          <!-- Header End -->
       
  <!-- Toolbar Start -->

@yield('content')
        

        






   <!-- Footer Start -->
   <footer id="rs-footer" class="rs-footer footer-2 footer-bg2  pt-100">
			<!-- Footer Top -->
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-12">
                            <div class="footer-contact">
                                <div class="footer-logo-part">
                                    <img src="{{asset('website/images/footer-logo.png')}}" alt="Footer Logo">
                                </div>
                                <div class="footer-contact-part">
                                    <div class="contact-item mb-15">
                                        <div class="icon-part kinder-pulm">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="address-part">
                                            <span>Mavelikara Road, Nangiarkulangara, Kerala 690513</span>
                                        </div>
                                    </div>
                                    <div class="contact-item mb-15">
                                        <div class="icon-part rotate-icon kinder-orange">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="number-part">
                                            <a href="tel+04792410750">+0479 241 0750</a><br>
                                      
                                        </div>
                                    </div>
                                    <div class="contact-item">
                                        <div class="icon-part kinder-blue">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="email-part">
                                            <a href="mailto:infoname@gmail.com">35046alapuzha@gmail.com</a><br>
                                            <a href="#">www.bethanybluebells.org</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 mb-md-30">
                            <h4 class="footer-title">Useful Links</h4>
                            <ul class="sitemap-widget clearfix">
                                <li class="active"><a href="index.html"><i class="fa fa-angle-right"></i>Home</a></li>
                                <li ><a href=""><i class="fa fa-angle-right"></i>About</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Mission</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Vision</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Facilities</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>News & Events</a></li>                                

                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-12 mb-md-30">
                            <h4 class="footer-title">Google Map</h4>
                            <div class="recent-post-widget">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15751.323416431123!2d76.46604!3d9.2594054!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1e801fd21c858dfe!2sBethany+Balikamadhom+Higher+Secondary+School!5e0!3m2!1sen!2sin!4v1564673612649!5m2!1sen!2sin" width="300" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-md-12">
                            <h4 class="footer-title">Social Media</h4>
                            <div class="newsletter-text">
                           
                             
                                <div class="footer-share">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>    
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>                                
                </div>
            </div>

            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="copyright">
                        <p>© 2019 <a href="#">Bethany Balikamadhom</a>. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-rocket"></i>
        </div>
        
        <!-- Search Modal Start -->
        <div aria-hidden="true" class="modal fade search-modal" role="dialog" tabindex="-1">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true" class="fa fa-close"></span>
	        </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here.." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to top --> 
        <a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> 
        @yield('js')
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="{{asset('website/js/modernizr-2.8.3.min.js')}}"></script>
        <!-- jquery latest version -->
        <script src="{{asset('website/js/jquery.min.js')}}"></script>
        <!-- bootstrap js -->
        <script src="{{asset('website/js/bootstrap.min.js')}}"></script>
        <!-- owl.carousel js -->
        <script src="{{asset('website/js/owl.carousel.min.js')}}"></script>
		<!-- slick.min js -->
        <script src="{{asset('website/js/slick.min.js')}}"></script>
        <!-- isotope.pkgd.min js -->
        <script src="{{asset('website/js/isotope.pkgd.min.js')}}"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="{{asset('website/js/imagesloaded.pkgd.min.js')}}"></script>
        <!-- wow js -->
        <script src="{{asset('website/js/wow.min.js')}}"></script>
        <!-- counter top js -->
        <script src="{{asset('website/js/waypoints.min.js')}}"></script>
        <script src="{{asset('website/js/jquery.counterup.min.js')}}"></script>
        <!-- magnific popup -->
        <script src="{{asset('website/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Water Ripples Effect -->
        <script src="{{asset('website/js/jquery.ripples-min.js')}}"></script>
        <!-- rsmenu js -->
        <script src="{{asset('website/js/rsmenu-main.js')}}"></script>
        <!-- plugins js -->
        <script src="{{asset('website/js/plugins.js')}}"></script>
		 <!-- main js -->
        <script src="{{asset('website/js/main.js')}}"></script>


  <script src="{{asset('website/js/jquery.vticker.min.js')}}"  type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
  $('#newstick').vTicker();
});

</script>

    
    </body>
</html>
=======

<!DOCTYPE html>
<html>


<head>
  <meta charset="UTF-8">
<meta name="description" content="Sreevalsam Interiors is one of classic & the best interior designers in Cochin. We have spread up our interior designing business across the nation including Cochin,Thiruvalla, Thiruvanthapuram,Pathanamthitta,Kollam etc.">
  <meta name="keywords" content="interior in kottayam,pathanamthitta,interior in ernakulam,interior design kerala">
  <meta name="author" content="Sreevalsam Interiors">


<title>sreevalsaminteriors</title>
<!-- Stylesheets -->
<link href="{{asset('website/css/bootstrap.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>


 <!--Main Footer-->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
<link href="{{asset('website/plugins/revolution/css/settings.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="{{asset('website/plugins/revolution/css/layers.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="{{asset('website/plugins/revolution/css/navigation.css')}}" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="{{asset('website/css/style.css')}}" rel="stylesheet">
<link href="{{asset('website/css/responsive.css')}}" rel="stylesheet">

<!--Color Switcher Mockup-->
<link href="{{asset('website/css/color-switcher-design.css')}}" rel="stylesheet">
@yield('head')
<!--Color Themes-->
<link id="theme-color-file" href="{{asset('website/css/color-themes/default-theme.css')}}" rel="stylesheet">

<link rel="shortcut icon" href="{{asset('website/images/favicon.png')}}" type="image/x-icon">
<link rel="icon" href="{{asset('website/images/favicon.png')}}" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120473367-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120473367-1');
</script>
</head>

<body>
<div class="contact-toggle-main" style="display: block;">
      <div class="contact-toggle-btn" id="contact-toggle-btn"></div>
      <div class="contact-course newAdd" id="contact-course">
        Get in Touch<br>
        <!-- <i class="fa fa-phone" aria-hidden="true"></i>  --><a href="tel:919061645457" style="color:#fff;">+91 9349 429 212</a><br>
      </div>

<div class="offers">
<a>
    <img src="{{asset('website/images/get_quote_free.png')}}" width="10%" class="pull-right" data-toggle="modal" data-target="#getForm"></a>
<div class="clear"></div>   

</div>


    </div>





<div class="page-wrapper">
 	
    <!-- Preloader -->

 	
    <!-- Main Header-->
    <header class="main-header">
    
    	<!--Header Top-->
    	<div class="header-top">
            <div class="auto-container">
                <div class="clearfix">
                    <!--Top Left-->
                    <div class="top-left pull-left">
                    	<ul class="clearfix">
                        	<li>Welcome to our Home Interiors</li>
                        </ul>
                    </div>
                    <!--Top Right-->
                    <div class="top-right pull-right">
                    	<ul class="social-nav">
                        	<li><a href="https://www.facebook.com/sreevalsaminteriors/"><span class="fa fa-facebook-f"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                            <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                        </ul>
                    	<ul class="list">
                        	<li>Have any question? +91 9349 429 212</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo">
                            <svg><rect></rect></svg>
                            <a href="index.html"><img src="{{asset('website/images/logo.png')}}" alt="" title=""></a></div>

                    </div>
                    
                    <div class="pull-right upper-right clearfix">
                    	
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-home-1"></span></div>
                            <ul>
                            	<li><strong>49/1638-A6 Thykoodam</strong></li>
                                <li>Vyttila P.O, Ernakulam</li>
								 <li>India-82019</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-note"></span></div>
                            <ul>
                            	<li><strong>Send your mail at</strong></li>
								<li>info@sreevalsaminteriors</li>
								<li>.com</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-clock-1"></span></div>
                            <ul>
                            	<li><strong>Working Hours</strong></li>
                                <li>Mon-Sat:9.30am to 7.00pm</li>
                            </ul>
                        </div>
                        
                    </div>

                    
                </div>

            </div>
        </div>

        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower">
            
        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="{{ Request::segment(1) === 'categories' ? 'active' : null }}"><a href="{{url('/')}}">Home</a>
                                   
                                </li>
                                <li><a href="{{url('about')}}">About us</a></li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                        @if(service())
                                        @foreach(service() as $serviceList)
                                        <li><a href="{{url('service/'.$serviceList->slug)}}">{{$serviceList->title}}</a></li>

                                    @endforeach
                                        @endif
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Products</a>
                                     <ul>
                                     @if(product())
                                     @foreach(product() as $products)
                                        <li><a href="{{url('products',$products->slug)}}">{{$products->title}}</a></li>
                                        @endforeach
                                        @endif
                                        
                                    </ul>
                                </li>
								<li><a href="{{url('all-gallery-images')}}">Gallery</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Blog</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Packages</a>
                                    <ul>
                                        <li><a href="{{url('package-list')}}">INTERIOR 3.99 LAKHS</a></li>
                                        <!-- <li><a href="interior4.39lakhs.html">INTERIOR 4.39 LAKHS</a></li>
                                        <li><a href="interior4.99lakhs.html">INTERIOR 4.99 LAKHS</a></li>
										<li><a href="interior5.39lakhs.html">INTERIOR 5.39 LAKHS</a></li>
                                        <li><a href="interior5.99lakhs.html">INTERIOR 5.99 LAKHS</a></li>
                                        <li><a href="interior6.49lakhs.html">INTERIOR 6.49 LAKHS</a></li> -->
                                    </ul>
                                </li>
                                <li><a href="{{url('contact')}}">Contact us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <div class="outer-box clearfix">
                    	
                        <!--Search Box-->
                   
                        
                        <div class="advisor-box">
                        	<a href="{{url('contact')}}" class="theme-btn advisor-btn">Book Now <span class="fa fa-long-arrow-right"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
              
                	<a href="index.html" class="img-responsive"><img src="{{asset('website/images/logo-small.png')}}" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="{{ Request::segment(1) === 'categories' ? 'active' : null }}"><a href="{{url('/')}}">Home</a>
                                    
                                </li>
                                <li><a href="{{url('about')}}">About us</a></li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                    @if(service())
                                        @foreach(service() as $serviceList)
                                        <li><a href="{{url('service/'.$serviceList->slug)}}">{{$serviceList->title}}</a></li>

                                    @endforeach
                                        @endif
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Products</a>
                                   <ul>
                                   @if(product())
                                     @foreach(product() as $products)
                                        <li><a href="{{url('products',$products->slug)}}">{{$products->title}}</a></li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </li>
								<li d><a href="{{url('all-gallery-images')}}">Gallery</a>
                                <li class="dropdown"><a href="">Blog</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Packages</a>
                                    <ul>
                                        <li><a href="interior3.99lakhs.html">INTERIOR 3.99 LAKHS</a></li>
                                        <!-- <li><a href="interior4.39lakhs.html">INTERIOR 4.39 LAKHS</a></li>
                                        <li><a href="interior4.99lakhs.html">INTERIOR 4.99 LAKHS</a></li>
										<li><a href="interior5.39lakhs.html">INTERIOR 5.39 LAKHS</a></li>
                                        <li><a href="interior5.99lakhs.html">INTERIOR 5.99 LAKHS</a></li>
                                        <li><a href="interior6.49lakhs.html">INTERIOR 6.49 LAKHS</a></li> -->
                                    </ul>
                                </li>
                                <li><a href="{{url('contact')}}">Contact us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->

@yield('content')
        

        






   <!--Main Footer-->
   <footer class="main-footer" style="background-image:url({{asset('website/images/background/3.jpg')}})">
    	<div class="auto-container">
        
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!--big column-->
                    <div class="big-column col-lg-7 col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                            <!--Footer Column-->
                            <div class="footer-column col-md-7 col-sm-6 col-xs-12">
                                <div class="footer-widget logo-widget">
									<div class="logo">
                                    	<a href="index.html"><img src="{{asset('website/images/footer-logo.png')}}" alt="" /></a>
                                    </div>
                                    <div class="text">Sreevalsam Interiors is one of classic &amp; the best interior designers in Cochin. We have spread up our interior designing business across the nation including Cochin,Thiruvalla, Thiruvanthapuram,Pathanamthitta,Kollam etc. </div>
                                    <ul class="social-icon-one">
                                        <li><a href="https://www.facebook.com/sreevalsaminteriors/"><span class="fa fa-facebook-f"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                        <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                        <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-5 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Services</h2>
                                    </div>
                                    <ul class="footer-lists">
                                        <li><a href="{{url('about')}}">About Us</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                        <li><a href="architectinteriordesigning.html">Architect & Interior Designing</a></li>
                                        <li><a href="residentialdesigning.html">Residential Designing</a></li>
                                        <li><a href="officeinteriordesigning.html">Office Interior Designing</a></li>
                                        <li><a href="commercialhospitality.html">Commercial & Hospitality</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!--big column-->
                    <div class="big-column col-lg-5 col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        	
                            <!--Footer Column-->
                            <div class="footer-column col-md-5 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Extra Links</h2>
                                    </div>
                                    <ul class="footer-lists-two">
                                        <li><a href="faq.html">FAQ</a></li>
                                        <li><a href="#">Blogs</a></li>
                                        <li><a href="modularkitchen.html">Modular Kitchen</a></li>
                                        <li><a href="furnituredesigning.html">Furniture Designing</a></li>
                                        <li><a href="vastushastrainteriordesigning.html">Vastu Shastra Interior Designing</a></li>
                                        <li><a href="turnkeyprojects.html">Turnkey Projects</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-7 col-sm-6 col-xs-12">
                                 <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Extra Links</h2>
                                    </div>
                                    <ul class="footer-lists-two">
                                        <li><a href="retailstore.html">Retail Store / Shopping Mall Interior Design</a></li>
                                        <li><a href="resortsinteriordesign.html">Resorts Interior Design</a></li>
                                        <li><a href="hotelinteriordesign.html">Hotel Interior Design</a></li>
                                        <li><a href="conventioncentreinteriordesign.html">Convention Centre Interior Design </a></li>
                                        <li><a href="requestquote.html">Request Quote</a></li>
                                        <li><a href="testimonals.html">Testimonals</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        <!--Footer Bottom-->
        <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="row clearfix">
                	
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	<div class="copyright">Copyright © 2018 <a href="#">Sreevalsam Interiors,</a> All Right Reserved</div>
                    </div>
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	 <p>Designed by <a href="http://verywebby.com/">Very Webby Design & Technologies</a>. All rights reserved</p>

                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->

<!--<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>-->

<!-- Color Palate / Color Switcher -->

<!-- /.End Of Color Palate -->
<meta content="{!! csrf_token() !!}" name="csrf-token">

<script> var base_url ='{{url('')}}'</script>
<script src="{{asset('website/js/jquery.js')}}"></script> 
<!--Revolution Slider-->

<script src="{{asset('website/js/jquery.slides.min.js')}}"></script>



<script src="{{asset('website/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('website/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('website/js/jquery.fancybox.js')}}"></script>
<script src="{{asset('website/js/owl.js')}}"></script>
<script src="{{asset('website/js/mixitup.js')}}"></script>
<script src="{{asset('website/js/wow.js')}}"></script>
<script src="{{asset('website/js/jquery-ui.js')}}"></script>
<script src="{{asset('website/js/script.js')}}"></script>
<!--Google Map APi Key-->

<script src="{{asset('website/js/map-script.js')}}"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<!--End Google Map APi-->

<!--Color Switcher-->
  <script>
    $(function() {
      $('#slides').slidesjs({
      
        play: {
     
          auto: true,
          interval: 10000
   
        }
      });
    });
  </script>

<script>

$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

$(document).ready(function() {

    base_url = "{{url('')}}";

});



$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

</script>
	






<script type="text/javascript">
    
  $(document).ready(function(){
$(".contact-toggle-btn").click(function(){
if($("#contact-course").hasClass( "newAdd" )){
$("#contact-course").removeClass("newAdd");
}
else{
$("#contact-course").addClass("newAdd");
}
});
});
$(document).ready(function(){
$(".contact-toggle-btn").click(function(){
if($("#contact-toggle-btn").hasClass( "btn-r" )){
$("#contact-toggle-btn").removeClass("btn-r");
}
else{
$("#contact-toggle-btn").addClass("btn-r");
}
});
});

$(function(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  $('.menu-a').click( function() {
    //$(".navbar-nav").toggleClass("hide-nav");
    $('.navbar-toggle').click();
  });
}
});


</script>
@yield('js')
<script>
    $(document).ready(function(){


 //This JQuery code will Reset value of Modal item when modal will load for create new records


 //This JQuery code is for Click on Modal action button for Create new records or Update existing records. This code will use for both Create and Update of data through modal
 
 
 
 
 $('#action').click(function(){
  var name = $('#name').val();
  var phone = $('#phone').val();
  var email = $('#email').val();
  var address = $('#address').val(); 
    var pack = $('#pack').val(); 
      var message = $('#message').val(); 


   $.ajax({
    url : "_email/in22.php",   
    data:{name:name, phone:phone,email:email,pack:pack,address:address,message:message}, 
    success:function(data){
     //alert(data);  
                          if(data == 'No')  
                          {  
                               alert("Wrong Data");  
                          }  
                          else  
                          {  
                               
                               
                          }  
     
    }
   });
  

 });




});
</script>


<script>
function submitEnquiry(){

    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    var name = $('#inputName').val();
    var phone = $('#inputPhone').val();
    var email = $('#inputEmail').val();
    var address = $('#inputAddress').val();

    if(name.trim() == '' ){
    
        $('.statusMsg').html('<span style="color:red;">Please enter your name.</span>');
        $('#inputName').focus();
        return false;
    }else if(phone.trim() == '' ){
       
         $('.statusMsg').html('<span style="color:red;">Please enter your Phone No.</span>');
        $('#inputMessage').focus();
        return false;
    }
    else if(email.trim() == '' ){
        
        $('.statusMsg').html('<span style="color:red;">Please enter your email.</span>');
        $('#inputEmail').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
    
           $('.statusMsg').html('<span style="color:red;">Please enter valid email.</span>');
        $('#inputEmail').focus();
        return false;
    }else if(address.trim() == '' ){
          $('.statusMsg').html('<span style="color:red;">Please enter your Address Or Place.</span>');
        $('#inputMessage').focus();
        return false;
    }else{
     
        $.ajax({
            type:'POST',
            url:'_email/submit_enquiry.php',
            data:'name='+name+'&phone='+phone+'&email='+email+'&address='+address,
            beforeSend: function () {
                $('.submitBtn').attr("disabled","disabled");
                $('.modal-body').css('opacity', '.5');
            },
            success:function(msg){
                if(msg == 'ok'){
                    $('#getForm').modal('hide');
                   swal("Submitted Successfully.", "We Will Contact You Soon.", "success");
        
            $('#formE')[0].reset();
                    
                  
                }else{
                    $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                    swal ( "Oops" ,  "Something went wrong please try again !" ,  "error" );
                }
                $('.submitBtn').removeAttr("disabled");
                $('.modal-body').css('opacity', '50');
            }
        });
    }
}

</script>




<!-- Modal -->
<div class="modal fade" id="getForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
                      <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="">Close</span>
                </button>

           <div class="modal-header model-c">

    
        <h3 class="modal-title"><strong>GET A <span>FREE QUOTE</span>  WITH <span>25%OFF </span></strong></h3></div>

            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" id="formE">
            
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" id="inputName" placeholder="Enter your name"/>
                    </div>
                       <div class="form-group">
                        <label for="inputEmail">Phone No</label>
                        <input type="text" class="form-control" id="inputPhone" placeholder="Enter your Phone No"/>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="Enter your email"/>
                    </div>
                     <div class="form-group">
                        <label for="inputMessage">Address</label>
                        <textarea class="form-control" id="inputAddress" placeholder="Enter your Address"></textarea>
                    </div>
                
                </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
           <button type="button" class="btn  btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn  btn-success submitBtn" onclick="submitEnquiry()">SUBMIT</button>
            </div>
        </div>
    </div>
</div>

</body>


</body>


</html>
>>>>>>> 91f5bb18feb1771d4b0ae52d64c0c99cc2fe3498
