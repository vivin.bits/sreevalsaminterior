@extends('layouts.app')
@section('meta_tags')
@endsection
@section('layout_header')

@endsection
@section('content')
<div class="container summary reg-sucess">
<div class="container">
    @if(Session::get('message'))
          <h3 class="success-msg">{!! Session::get('message') !!}</h3>
          @else
           @endif
@if(!Auth::check())
<a href="{{ URL::to('/login') }}" class="btn-lnk">{{trans('authentication.title_login')}}</a>
<a href="{{ URL::to('/') }}"  class="btn-lnk">{{trans('home.title_home')}}</a>
@endif
</div>
</div>
@endsection
@section('additional_scripts')



@endsection