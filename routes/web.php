<?php
// dd('hh');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$domain = env('DOMAIN');


Route::get('email-reset/{id}/{confirmationCode}', [ 'uses' => 'ProfileController@confirmEmail']);

Route::get('account-activated', 'LoginController@accountActiveSuccess')->name('account-activated');


// ****************************************************************************
// *                           FRONTEND SECTION                                  *
// ****************************************************************************

Route::group(['domain' => "$domain"], function () {

	Route::get('/', 'website\HomeController@index');
	Route::get('about','website\HomeController@about');
	Route::get('aboutus/{slug}','website\HomeController@aboutDetails');
	Route::get('gallery','website\HomeController@gallery');
	Route::get('activity','website\HomeController@activity');



	Route::get('course/{slug}','website\HomeController@course');
	
	// Route::get('gallery','website\HomeController@gallery');
	Route::get('message','website\HomeController@message');
	Route::get('mission','website\HomeController@mission');
	Route::get('facility','website\HomeController@facility');
	Route::get('contact','website\HomeController@contact');
	Route::get('application','website\HomeController@application');
	Route::get('career','website\HomeController@career');
	Route::get('testimonial','website\HomeController@testimonial');
	Route::post('application/create','website\HomeController@applicationcreate');
	Route::post('career/create','website\HomeController@careercreate');



	Route::get('allFacility','website\HomeController@allFacility');


});





// ****************************************************************************
// *                           ADMIN SECTION                                  *
// ****************************************************************************


Route::group(['domain' => "admin.$domain"], function () {


	Route::get('login', 'Admin\AuthController@showLoginForm')->name('login');
	Route::post('login', 'Admin\AuthController@login');
	Route::get('logout', 'Admin\AuthController@logout');
	Route::get('register', 'Admin\AuthController@register');
	// Route::group(['middleware' => ['auth','Permission.required']], function () {
	Route::group(['middleware' => ['admin', 'admin.permissions']], function () {
		/********** Dashboard *********/
		Route::get('/', 'Admin\DashboardController@index');
		Route::get('change-password', 'Admin\DashboardController@changePassword');
		Route::post('change-password', 'Admin\DashboardController@updateChangePassword');

		/********** Users *********/
		Route::group(['module' => 'user'],function (){
			Route::get('users', ['permissions' => [], 'uses' => 'Admin\UserController@index']);
			Route::get('users/create', ['permission' => 'add', 'uses' =>  'Admin\UserController@createForm']);
			Route::post('users/create',  ['permission' => 'add', 'uses' =>  'Admin\UserController@create']);
			Route::get('users_data',  ['permissions' => ['Users.view'], 'uses' => 'Admin\UserController@getUsersData']);
			Route::get('users/edit/{id}', ['permission' => 'edit', 'uses' =>  'Admin\UserController@editForm']);
			Route::post('users/edit/{id}', ['permission' => 'edit', 'uses' =>  'Admin\UserController@edit']);

			Route::post('/ajax/changeUserStatus', 'Admin\AjaxController@changeUserStatus');
			Route::post('/ajax/changeBlockStatus', 'Admin\AjaxController@changeBlockStatus');
			Route::post('/ajax/changeEmailStatus', 'Admin\AjaxController@changeEmailStatus');
		});


		  // Roles management section
		Route::group(['module' => 'roles'],function (){
			Route::get('roles', 'Admin\RoleController@index');
			Route::get('permissions', 'Admin\RoleController@permissionIndex');
			Route::get('role/create', ['permission' => 'add', 'uses' => 'Admin\RoleController@createForm']);
			Route::post('role/create', ['permission' => 'add', 'uses' =>  'Admin\RoleController@create']);
			Route::get('role/edit/{id}', ['permission' => 'edit', 'uses' =>  'Admin\RoleController@updateForm']);
			Route::get('permission/edit/{id}', ['permission' => 'edit', 'uses' =>  'Admin\RoleController@updatePermissionForm']);
					Route::post('role/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\RoleController@updateRole']);
			Route::post('permission/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\RoleController@update']);
			Route::post('role/change_status/{id}',['permission' => 'edit', 'uses' => 'Admin\RoleController@changeStatus']);
			Route::post('role/delete/{id}',['permission' => 'delete', 'uses' => 'Admin\RoleController@delete']);
		});

        // Module management section
		Route::group(['module' => 'module'],function (){
			Route::get('module', 'Admin\ModuleController@index');
			Route::get('module/create', ['permission' => 'add', 'uses' => 'Admin\ModuleController@createForm']);
			Route::post('module/create', ['permission' => 'add', 'uses' => 'Admin\ModuleController@create']);
			Route::get('module/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\ModuleController@updateForm']);
			Route::post('module/edit/{id}', ['permission' => 'edit', 'uses' => 'Admin\ModuleController@update']);
		});

		/********** Settings *********/

		Route::get('settings', ['permissions' => ['Settings.view'],
			'uses' => 'Admin\SettingsController@index']);

		Route::get('social-media', ['permissions' => ['Settings.view'],
			'uses' => 'Admin\SettingsController@socialIndex']);

		Route::post('settings/update', ['permissions' => ['Settings.edit'],
			'uses' => 'Admin\SettingsController@update']);

		Route::post('settings/social-update', ['permissions' => ['Settings.edit'],
			'uses' => 'Admin\SettingsController@socialUpdate']);


		Route::get('smtp-settings', ['permissions' => ['Settings.view'],
			'uses' => 'Admin\SettingsController@smtpIndex']);

		Route::post('settings/smtp-update', ['permissions' => ['Settings.edit'],
			'uses' => 'Admin\SettingsController@smtpUpdate']);


		Route::get('googleapi-settings', ['permissions' => ['Settings.view'],
			'uses' => 'Admin\SettingsController@googleApiIndex']);

		Route::post('settings/googleapi-update', ['permissions' => ['Settings.edit'],
			'uses' => 'Admin\SettingsController@googleApiUpdate']);

		   // Category Management Section
		Route::group(['module' => 'category'],function (){
		Route::get('category', ['permissions' => [''], 'uses' => 'Admin\CategoryController@index']);
		Route::get('category/get-data', ['permissions' => ['category.view'], 'uses' => 'Admin\CategoryController@data']);
		 Route::get('category/create', ['permissions' => ['add'], 'uses' => 'Admin\CategoryController@create']);
        Route::post('category/create', ['permissions' => ['add'], 'uses' => 'Admin\CategoryController@save']);
         Route::get('category/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\CategoryController@edit']);
        Route::post('category/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\CategoryController@update']);
        Route::post('category/delete', ['permissions' => ['delete'], 'uses' => 'Admin\CategoryController@deleteCategory']);
        Route::post('category/delete-check', ['permissions' => ['delete'], 'uses' => 'Admin\CategoryController@checkCategoryDelete']);

             		});

			/********** Email templates *********/
						Route::group(['module' => 'email_template'],function (){
		Route::get('email-templates', ['permissions' => [''], 'uses' => 'Admin\EmailTemplatesController@index']);
		Route::get('email-templates/create', ['permissions' => ['add'], 'uses' => 'Admin\EmailTemplatesController@createForm']);
		Route::post('email-templates/create', ['permissions' => ['add'], 'uses' => 'Admin\EmailTemplatesController@create']);
		Route::get('email-templates/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\EmailTemplatesController@editForm']);
		Route::post('email-templates/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\EmailTemplatesController@edit']);
		Route::post('email-templates/delete', ['permissions' => ['delete'], 'uses' => 'Admin\EmailTemplatesController@delete']);

				});

		/* Manage CMS page Content */
		Route::get('pages', ['permissions' => ['Pages.view'], 'uses' => 'Admin\CmsController@index']);
		Route::get('pages/get-data', ['permissions' => ['Pages.view'], 'uses' => 'Admin\CmsController@cmsGetdata']);
		Route::get('pages/create', ['permissions' => ['Pages.create'], 'uses' => 'Admin\CmsController@create']);
		Route::post('pages/url-slug', 'Admin\Pages\CmsController@urlSlug');
		Route::get('pages/view/{id}', ['permissions' => ['Pages.view'], 'uses' => 'Admin\CmsController@view']);
		Route::post('pages/view/{id}', ['permissions' => ['Pages.view'], 'uses' => 'Admin\CmsController@edit']);
		Route::post('pages/create', ['permissions' => ['Pages.view'], 'uses' => 'Admin\CmsController@doCreate']);
		Route::post('/ajax/changePageStatus', 'Admin\AjaxController@changePageStatus');

		/********** Admin Profile *********/
		Route::get('profile', 'Admin\UserController@profile');
		Route::post('profile', 'Admin\UserController@updateProfile');
		Route::post('change-adminmail', 'Admin\UserController@updateEmail')->name('admin.updatemail');
		
	});

});



/********DEPARTMENT*************** */

	Route::group(['module' => 'department'],function (){
		Route::get('department', ['permissions' => [], 'uses' => 'Admin\DepartmentController@index']);
		Route::get('department/create', ['permission' => 'add', 'uses' =>  'Admin\DepartmentController@createForm']);
		Route::post('department/create',  ['permission' => 'add', 'uses' =>  'Admin\DepartmentController@create']);
		Route::get('department/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\DepartmentController@destroy']);
		Route::get('department/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\DepartmentController@editForm']);
		Route::post('department/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\DepartmentController@edit']);
				 });


/***********SUBJECT********** */


Route::group(['module' => 'subject'],function (){
	Route::get('subject', ['permissions' => [], 'uses' => 'Admin\SubjectController@index']);
	Route::get('subject/create', ['permission' => 'add', 'uses' =>  'Admin\SubjectController@createForm']);
	Route::post('subject/create',  ['permission' => 'add', 'uses' =>  'Admin\SubjectController@create']);
	Route::get('subject/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\SubjectController@destroy']);

			 });




/*********************SCHEME*********************** */		
Route::group(['module' => 'scheme'],function (){
	Route::get('scheme', ['permissions' => [], 'uses' => 'Admin\SchemeController@index']);
	Route::get('scheme/create', ['permission' => 'add', 'uses' =>  'Admin\SchemeController@createForm']);
	Route::post('scheme/create',  ['permission' => 'add', 'uses' =>  'Admin\SchemeController@create']);
	Route::get('scheme/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\SchemeController@destroy']);

			 }); 



/************NEWS******* */		

Route::group(['module' => 'news'],function (){
	Route::get('news', ['permissions' => [], 'uses' => 'Admin\NewsController@index']);
	Route::get('news/create', ['permission' => 'add', 'uses' =>  'Admin\NewsController@createForm']);
	Route::post('news/create',  ['permission' => 'add', 'uses' =>  'Admin\NewsController@create']);
	Route::get('news/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\NewsController@destroy']);
	Route::get('news/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\NewsController@editForm']);
	Route::post('news/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\NewsController@edit']);

			 });



/******************VISIT  & CAREER   ****************** */
Route::group(['module' => 'visit'],function (){
	Route::get('visit', ['permissions' => [], 'uses' => 'Admin\VisitController@index']);
	Route::get('career', ['permissions' => [], 'uses' => 'Admin\VisitController@careerindex']);

			 });



/*****************ADMISSION***********/
Route::group(['module' => 'admission'],function (){
	Route::get('admission', ['permissions' => [], 'uses' => 'Admin\AdmissionController@index']);
	//Route::get('news/create', ['permission' => 'add', 'uses' =>  'Admin\NewsController@createForm']);
	//Route::post('news/create',  ['permission' => 'add', 'uses' =>  'Admin\NewsController@create']);

			 });




/*********************** COURSE******************/
Route::group(['module' => 'course'],function (){
	Route::get('course', ['permissions' => [], 'uses' => 'Admin\CourseController@index']);
	Route::get('course/create', ['permission' => 'add', 'uses' =>  'Admin\CourseController@createForm']);
	Route::post('course/create',  ['permission' => 'add', 'uses' =>  'Admin\CourseController@create']);
	Route::get('courses/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\CourseController@destroy']);
	Route::get('courses/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\CourseController@editForm']);
	Route::post('courses/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\CourseController@edit']);
			 });





/***********************ALBUM*******************/	
Route::group(['module' => 'album'],function (){
	Route::post('album/change-status',['permission' => ['edit'], 'uses' => 'Admin\AlbumController@changeStatus']);
	
	Route::get('get-album', ['permissions' => [], 'uses' => 'Admin\AlbumController@getAlbum']);
	Route::get('album', ['permissions' => [], 'uses' => 'Admin\AlbumController@index']);
	Route::get('album/create', ['permission' => 'add', 'uses' =>  'Admin\AlbumController@createForm']);
	Route::post('album/create',  ['permission' => 'add', 'uses' =>  'Admin\AlbumController@create']);
	Route::post('album/delete', ['permission' => 'delete', 'uses' =>  'Admin\AlbumController@destroy']);
	Route::get('album/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AlbumController@editForm']);
	Route::post('album/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AlbumController@edit']);
	Route::get('album/upload/{id}', ['permissions' => ['add'], 'uses' => 'Admin\AlbumController@upload']);
	Route::post('album/upload/{id}', ['permissions' => ['add'], 'uses' => 'Admin\AlbumController@filestore']);
	Route::post('image/delete', ['permissions' => ['delete'], 'uses' => 'Admin\AlbumController@fileDestroy']);

	Route::post('upload/delete', ['permission' => 'delete', 'uses' =>  'Admin\AlbumController@Uploaddestroy']);

			 });	 
/***********************Activity*******************/
Route::post('activity/change-status',['permission' => ['edit'], 'uses' => 'Admin\ActivityController@changeStatus']);
	
	Route::get('get-activity', ['permissions' => [], 'uses' => 'Admin\ActivityController@getAlbum']);
	Route::get('activity', ['permissions' => [], 'uses' => 'Admin\ActivityController@index']);
	Route::get('activity/create', ['permission' => 'add', 'uses' =>  'Admin\ActivityController@createForm']);
	Route::post('activity/create',  ['permission' => 'add', 'uses' =>  'Admin\ActivityController@create']);
	Route::post('activity/delete', ['permission' => 'delete', 'uses' =>  'Admin\ActivityController@destroy']);
	Route::get('activity/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\ActivityController@editForm']);
	Route::post('activity/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\ActivityController@edit']);

/***********************GALLERY******************/
Route::group(['module' => 'gallery'],function (){
	Route::get('gallery', ['permissions' => [], 'uses' => 'Admin\GalleryController@index']);
	Route::get('gallery/create', ['permission' => 'add', 'uses' =>  'Admin\GalleryController@createForm']);
	Route::post('gallery/create',  ['permission' => 'add', 'uses' =>  'Admin\GalleryController@create']);
	Route::get('gallery/delete{id}', ['permission' => 'delete', 'uses' =>  'Admin\GalleryController@destroy']);
	Route::get('gallery/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\GalleryController@editForm']);
	Route::post('gallery/edit{id}', ['permissions' => ['edit'], 'uses' => 'Admin\GalleryController@edit']);

			 });

/***************************ABOUTUS********** */


Route::get('tab-heading-create',  'Admin\TabHeadingController@createForm');
Route::post('tab-heading-create-tab',  'Admin\TabHeadingController@create');
Route::get('tab-heading',  'Admin\TabHeadingController@index');
Route::get('/admin/get-tab',  'Admin\TabHeadingController@getTab');
Route::post('tab-heading/delete',  'Admin\TabHeadingController@delete');
Route::group(['module' => 'about'],function (){
	Route::get('about-view-page', ['permissions' => [], 'uses' => 'Admin\AboutController@aboutViewPage']);
	Route::get('about', ['permissions' => [], 'uses' => 'Admin\AboutController@index']);
	Route::get('about/create', ['permission' => 'add', 'uses' =>  'Admin\AboutController@createForm']);
	Route::post('about/create',  ['permission' => 'add', 'uses' =>  'Admin\AboutController@create']);
	Route::post('about/delete', ['permission' => 'delete', 'uses' =>  'Admin\AboutController@destroy']);
	Route::get('about/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@editForm']);
	Route::post('about/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@edit']);


                      /**************MISSION**********/
    Route::get('missionList',  'Admin\AboutController@missionList');
	Route::get('mission/create', ['permission' => 'add', 'uses' =>  'Admin\AboutController@missionForm']);
	Route::post('mission/create',  ['permission' => 'add', 'uses' =>  'Admin\AboutController@createmission']);
	Route::get('mission', ['permissions' => [], 'uses' => 'Admin\AboutController@indexmission']);
	Route::get('mission/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@missioneditForm']);
	Route::post('mission/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@missionedit']);
	Route::post('mission/delete', ['permission' => 'delete', 'uses' =>  'Admin\AboutController@missiondestroy']);

  /**************Why Choose**********/
    Route::get('getwhychoose', ['permissions' => [], 'uses' => 'Admin\WhyChooseController@getWhychoose']);
    Route::get('whyChoose', ['permissions' => [], 'uses' => 'Admin\WhyChooseController@index']);
	Route::get('whychoose/create', ['permission' => 'add', 'uses' =>  'Admin\WhyChooseController@missionForm']);
	Route::post('whychoose/create',  ['permission' => 'add', 'uses' =>  'Admin\WhyChooseController@createmission']);

	Route::get('whychoose/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\WhyChooseController@missioneditForm']);
	Route::post('whyChoose/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\WhyChooseController@missionedit']);
	Route::post('whychoose/delete', ['permission' => 'delete', 'uses' =>  'Admin\WhyChooseController@missiondestroy']);


                     /****************VISSION**********/

	Route::get('vission/create', ['permission' => 'add', 'uses' =>  'Admin\AboutController@vissionForm']);
	Route::post('vission/create',  ['permission' => 'add', 'uses' =>  'Admin\AboutController@createvission']);
	Route::get('vission', ['permissions' => [], 'uses' => 'Admin\AboutController@indexvission']);


                       /******************MESSAGE***********/
    Route::get('get-message', ['permissions' => [], 'uses' => 'Admin\AboutController@getMessage']);
	Route::get('message/create', ['permission' => 'add', 'uses' =>  'Admin\AboutController@messageForm']);
	Route::post('message/create',  ['permission' => 'add', 'uses' =>  'Admin\AboutController@createmessage']);
	Route::get('message', ['permissions' => [], 'uses' => 'Admin\AboutController@indexmessage']);
	Route::get('message/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@msgeditForm']);
	Route::post('message/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\AboutController@editmsg']);
	Route::post('message/delete', ['permission' => 'delete', 'uses' =>  'Admin\AboutController@msgdestroy']);



			 });


		
/***************************CONTACT************/

Route::group(['module' => 'contact'],function (){
	Route::get('contact', ['permissions' => [], 'uses' => 'Admin\ContactController@index']);
	Route::get('contact/create', ['permission' => 'add', 'uses' =>  'Admin\ContactController@createForm']);
	Route::post('contact/create',  ['permission' => 'add', 'uses' =>  'Admin\ContactController@create']);
	Route::get('contact/delete/{id}', ['permission' => 'delete', 'uses' =>  'Admin\ContactController@destroy']);
	Route::get('contact/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\ContactController@editForm']);
	Route::post('contact/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\ContactController@edit']);

			 });

	
/**************************SLIDER**************** */
Route::group(['module' => 'slider'],function (){
    Route::get('/get-slider', ['permissions' => [], 'uses' => 'Admin\SliderController@indexList']);
	Route::get('slider', ['permissions' => [], 'uses' => 'Admin\SliderController@index']);
	Route::get('slider/create', ['permission' => 'add', 'uses' =>  'Admin\SliderController@createForm']);
	Route::post('slider/create',  ['permission' => 'add', 'uses' =>  'Admin\SliderController@create']);
	
	Route::get('slide/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\SliderController@editForm']);
	Route::post('slider/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\SliderController@edit']);
	Route::post('slider/delete', ['permission' => 'delete', 'uses' =>  'Admin\SliderController@destroy']);

			 });



/**************************TESTIMONIAL****************/
Route::group(['module' => 'slider'],function (){
	Route::get('testimonial', ['permissions' => [], 'uses' => 'Admin\TestimonialController@index']);
	Route::get('testimonial/create', ['permission' => 'add', 'uses' =>  'Admin\TestimonialController@createForm']);
	Route::post('testimonial/create',  ['permission' => 'add', 'uses' =>  'Admin\TestimonialController@create']);
	Route::get('test/delete/{id}', ['permission' => 'delete', 'uses' =>  'Admin\TestimonialController@destroy']);
	Route::get('test/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\TestimonialController@editForm']);
	Route::post('test/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\TestimonialController@edit']);

			 });

/**************************SCHOOLSTUDENT****************/

Route::get('get-school', ['permissions' => [], 'uses' => 'Admin\SchoolStudentController@getSchool']);

Route::get('school/students', ['permissions' => [], 'uses' => 'Admin\SchoolStudentController@index']);
	Route::get('school/students/create', ['permission' => 'add', 'uses' =>  'Admin\SchoolStudentController@createForm']);
	Route::post('school/students/create',  ['permission' => 'add', 'uses' =>  'Admin\SchoolStudentController@create']);
	Route::post('school/students/delete', ['permission' => 'delete', 'uses' =>  'Admin\SchoolStudentController@destroy']);
	Route::get('school/students/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\SchoolStudentController@edit']);
	Route::post('school/students/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\SchoolStudentController@update']);
	Route::post('school/students/change-status',['permission' => ['edit'], 'uses' => 'Admin\SchoolStudentController@changeStatus']);
  

/************************FACILITY*******************/
Route::group(['module' => 'facility'],function (){
	Route::get('facility', ['permissions' => [], 'uses' => 'Admin\FacilityController@index']);
	Route::get('facility/create', ['permission' => 'add', 'uses' =>  'Admin\FacilityController@createForm']);
	Route::post('facility/create',  ['permission' => 'add', 'uses' =>  'Admin\FacilityController@create']);
	Route::post('facility/delete', ['permission' => 'delete', 'uses' =>  'Admin\FacilityController@destroy']);
	Route::get('facility/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\FacilityController@editForm']);
	Route::post('facility/edit/{id}', ['permissions' => ['edit'], 'uses' => 'Admin\FacilityController@edit']);
	Route::get('get-facility', ['permissions' => ['edit'], 'uses' => 'Admin\FacilityController@getFacility']);

			 });
