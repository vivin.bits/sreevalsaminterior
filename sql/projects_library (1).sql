-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2019 at 06:42 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projects_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` varchar(200) NOT NULL,
  `main_description` varchar(200) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `meta_tag` varchar(200) DEFAULT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `mission` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `main_description`, `slug`, `meta_tag`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `mission`, `created_at`, `updated_at`) VALUES
(1, 'iuiyi', '<p>yiuiyu</p>', '<p>yuiyui</p>', NULL, NULL, 'yiuiyu', 'yuiyu', 'yuiyui', 'iyuiyu', 'yuiyu', NULL, '2019-08-15 03:30:11', '2019-08-15 03:30:11');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` enum('facility','order','user','setting','role','classification','category','email','cms','module') DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', NULL, '2018-08-31 00:54:24', '2018-08-31 00:54:24'),
(2, 1, 1, 'user', NULL, '2018-08-31 00:54:48', '2018-08-31 00:54:48'),
(3, 1, 1, 'user', NULL, '2018-08-31 00:56:22', '2018-08-31 00:56:22'),
(4, 1, 1, 'user', NULL, '2018-08-31 00:56:33', '2018-08-31 00:56:33'),
(5, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:30:44', '2018-09-12 04:30:44'),
(6, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:31:03', '2018-09-12 04:31:03'),
(7, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 04:56:59', '2018-09-12 04:56:59'),
(8, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 05:55:38', '2018-09-12 05:55:38'),
(9, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:46', '2018-09-12 06:01:46'),
(10, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:57', '2018-09-12 06:01:57'),
(11, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:03', '2018-09-12 06:04:03'),
(12, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:23', '2018-09-12 06:04:23'),
(13, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:35', '2018-09-12 06:04:35'),
(14, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:32:50', '2018-09-12 06:32:50'),
(15, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:33:01', '2018-09-12 06:33:01'),
(16, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 22:59:25', '2018-09-12 22:59:25'),
(17, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:10', '2018-09-12 23:01:10'),
(18, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:20', '2018-09-12 23:01:20'),
(19, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:27', '2018-09-12 23:02:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:47', '2018-09-12 23:02:47'),
(21, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:15', '2018-09-12 23:03:15'),
(22, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:40', '2018-09-12 23:03:40'),
(23, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:54', '2018-09-12 23:03:54'),
(24, 1, 1, 'user', NULL, '2018-09-13 04:46:30', '2018-09-13 04:46:30'),
(25, 1, 1, 'user', NULL, '2018-09-13 04:47:20', '2018-09-13 04:47:20'),
(26, 1, 1, 'user', NULL, '2018-09-13 04:48:46', '2018-09-13 04:48:46'),
(27, 1, 1, 'user', NULL, '2018-09-13 04:52:00', '2018-09-13 04:52:00'),
(28, 1, 1, 'user', NULL, '2018-09-13 04:52:17', '2018-09-13 04:52:17'),
(29, 1, 18, 'module', 'New module has been created by:', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(30, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 02:56:44', '2019-07-29 02:56:44'),
(31, 1, 19, 'module', 'New module has been created by:', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(32, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:50:28', '2019-07-29 06:50:28'),
(33, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:53:26', '2019-07-29 06:53:26'),
(34, 1, 20, 'module', 'New module has been created by:', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(35, 1, 1, 'role', 'New role has been edited by:', '2019-07-30 22:58:34', '2019-07-30 22:58:34'),
(36, 1, 21, 'module', 'New module has been created by:', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(37, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 04:35:01', '2019-08-06 04:35:01'),
(38, 1, 22, 'module', 'New module has been created by:', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(39, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 05:32:55', '2019-08-06 05:32:55'),
(40, 1, 23, 'module', 'New module has been created by:', '2019-08-12 22:58:03', '2019-08-12 22:58:03'),
(41, 1, 1, 'role', 'New role has been edited by:', '2019-08-12 22:58:25', '2019-08-12 22:58:25'),
(42, 1, 24, 'module', 'New module has been created by:', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(43, 1, 1, 'role', 'New role has been edited by:', '2019-08-13 04:36:06', '2019-08-13 04:36:06'),
(44, 1, 25, 'module', 'New module has been created by:', '2019-08-14 00:16:42', '2019-08-14 00:16:42'),
(45, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 00:17:05', '2019-08-14 00:17:05'),
(46, 1, 26, 'module', 'New module has been created by:', '2019-08-14 05:56:10', '2019-08-14 05:56:10'),
(47, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 05:56:32', '2019-08-14 05:56:32'),
(48, 1, 27, 'module', 'New module has been created by:', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(49, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 07:52:44', '2019-08-14 07:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `perifix` varchar(200) NOT NULL,
  `type` enum('parent','child') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 'User', 'user', 'parent', '2018-03-26 05:44:20', '2018-03-26 05:44:20'),
(2, 0, 'Roles', 'roles', 'parent', '2018-03-27 01:07:21', '2018-03-27 01:07:21'),
(3, 0, 'Module', 'module', 'parent', '2018-03-27 01:07:42', '2018-03-27 01:07:42'),
(4, 0, 'Admin User', 'admin_user', 'parent', '2018-03-27 23:00:33', '2018-03-27 23:00:33'),
(5, 0, 'Classification', 'classification', 'parent', '2018-03-29 00:06:51', '2018-03-29 00:06:51'),
(6, 0, 'Category', 'category', 'parent', '2018-03-29 07:09:16', '2018-03-29 07:09:16'),
(7, 0, 'CMS', 'cms', 'parent', '2018-03-29 22:48:40', '2018-03-29 22:48:40'),
(8, 0, 'Facility Management', 'facility_management', 'parent', '2018-05-06 23:21:17', '2018-05-06 23:21:17'),
(9, 0, 'Order Management', 'order', 'parent', '2018-05-07 23:47:47', '2018-05-07 23:47:47'),
(10, 0, 'Email Setting', 'email_template', 'parent', '2018-05-08 23:49:01', '2018-05-08 23:49:01'),
(11, 0, 'User Report', 'user_report', 'parent', '2018-05-13 22:40:52', '2018-05-13 22:40:52'),
(12, 0, 'Facility Report', 'facility_report', 'parent', '2018-05-14 22:02:29', '2018-05-14 22:02:29'),
(13, 0, 'Order Report', 'order_report', 'parent', '2018-05-15 00:45:33', '2018-05-15 00:45:33'),
(14, 0, 'Settings', 'setting', 'parent', '2018-05-16 22:17:17', '2018-05-16 22:17:17'),
(15, 0, 'Admin User', 'admin_user', 'parent', '2018-05-22 05:50:07', '2018-05-22 05:50:07'),
(16, 0, 'Static Content', 'static_content', 'parent', '2018-05-30 03:38:32', '2018-05-30 03:38:32'),
(17, 0, 'Testimonial', 'testimonial', 'parent', '2018-06-01 05:45:03', '2018-06-01 05:45:03'),
(18, 0, 'department', 'department', 'parent', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(19, 0, 'subject', 'subject', 'parent', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(20, 0, 'news', 'news', 'parent', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(21, 0, 'scheme', 'scheme', 'parent', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(22, 0, 'course', 'course', 'parent', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(23, 0, 'gallery', 'gallery', 'parent', '2019-08-12 22:58:02', '2019-08-12 22:58:02'),
(24, 0, 'album', 'album', 'parent', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(25, 0, 'about', 'about', 'parent', '2019-08-14 00:16:41', '2019-08-14 00:16:41'),
(26, 0, 'contact', 'contact', 'parent', '2019-08-14 05:56:09', '2019-08-14 05:56:09'),
(27, 0, 'slider', 'slider', 'parent', '2019-08-14 07:51:16', '2019-08-14 07:51:16');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `course` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`id`, `name`, `address`, `phone`, `course`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth(h)', '9446395796', 'BCA', '2019-07-01 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `category` varchar(150) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `category`, `main_image`, `created_at`, `updated_at`) VALUES
(10, 'IT fest', '2019/8/computer9.jpg', '2019-08-13 07:02:29', '2019-08-13 07:02:29'),
(11, 'Onam cellebration', '2019/8/onam.jpg', '2019-08-13 22:30:10', '2019-08-13 22:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `slug` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1. Default Item',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `slug`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'THREE', 'Three', 'next2', 0, 0, '2018-09-10', '2018-09-11'),
(10, 'gg', '', 'dd', 9, 0, '2018-09-10', '2018-09-10'),
(13, 'fgdgdgdsfgs', '', 'fgdgdgdsfgs', 9, 0, '2018-09-10', '2018-09-10'),
(15, '4', '', 'two', 12, 0, '2018-09-10', '2018-09-11'),
(16, '222', 'test', 'thhree', 1, 0, '2018-09-11', '2018-09-12'),
(26, 'ONE', 'one', 'sdfsdfsd', 0, 0, '2018-09-11', '2018-09-11'),
(30, '11', NULL, 'dummy', 26, 0, '2018-09-11', '2018-09-11'),
(31, '44', NULL, '44', 12, 0, '2018-09-11', '2018-09-11'),
(32, '444', NULL, '444', 12, 0, '2018-09-11', '2018-09-11'),
(35, 'htyyt', 'tjytjyjyj', 'htyyt', 0, 0, '2019-08-01', '2019-08-01'),
(36, 'jyjytjyt', NULL, 'jyjytjyt', 35, 0, '2019-08-01', '2019-08-01'),
(37, 'jyutyuytj', NULL, 'jyutyuytj', 35, 0, '2019-08-01', '2019-08-01'),
(38, 'Bachelor of Computer Application', 'zdxczxv', 'bachelor-of-computer-application', 1, 0, '2019-08-01', '2019-08-01'),
(39, 'Bsc. Geology', 'fghfh', 'bsc-geology', 35, 0, '2019-08-01', '2019-08-01'),
(40, 'we', 'wewre', 'we', 0, 0, '2019-08-01', '2019-08-01'),
(41, 'vc', NULL, 'vc', 40, 0, '2019-08-01', '2019-08-01'),
(42, 'vm', NULL, 'vm', 40, 0, '2019-08-01', '2019-08-01'),
(43, 'vk', NULL, 'vk', 40, 0, '2019-08-01', '2019-08-01'),
(45, 'ffgfg', 'gfgfg', 'ffgfg', 0, 0, '2019-08-01', '2019-08-01'),
(46, 'cvcvcv', NULL, 'cvcvcv', 45, 0, '2019-08-01', '2019-08-01'),
(47, 'cvgfhgh', 'fgh', 'cv', 35, 0, '2019-08-01', '2019-08-14'),
(49, 'xfd', NULL, 'xfd', 0, 0, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(150) NOT NULL,
  `map` varchar(150) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `insta` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `phone`, `email`, `address`, `map`, `facebook`, `twitter`, `linkedin`, `insta`, `youtube`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `created_at`, `updated_at`) VALUES
(4, 'ghj', 'hfghgf', 'hfgh', '<p>ghjgfhj</p>', 'hfghf', 'hfghgf', 'hfghfg', 'hfgh', 'fghfg', 'hgfh', 'fghfg', 'hfgh', 'fghgf', 'fghfg', 'fghfg', '2019-08-15 03:55:04', '2019-08-15 03:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `subject_id` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `content` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL,
  `scheme` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `departments_id`, `subject_id`, `course`, `content`, `image`, `scheme`, `created_at`, `updated_at`) VALUES
(14, 12, '19', 'Bachelor of Computer Application', '<p>tyrtuytruyufyjgh</p>', '2019/8/commerce3.jpg', '2', '2019-08-12 05:45:09', '2019-08-12 05:45:09'),
(15, 10, '20', 'Bachelor of geology', '<p>study of earth</p>', '2019/8/geology3.jpg', '5', '2019-08-12 10:42:45', '2019-08-12 10:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'Department of PetroChecmicals', '2019-08-06 06:28:48', '2019-08-06 06:28:48'),
(10, 'Department of Geology', '2019-08-06 06:28:17', '2019-08-06 06:28:17'),
(12, 'Department of Commerce', '2019-08-06 06:29:45', '2019-08-06 06:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `eligibility` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(500) DEFAULT NULL,
  `to_name` varchar(500) DEFAULT NULL,
  `to_email` varchar(500) DEFAULT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_email` varchar(500) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email_body` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `key`, `from_name`, `from_email`, `to_name`, `to_email`, `cc_name`, `cc_email`, `subject`, `email_body`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Registration notification to admin', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.raju@gmail.com', 'Sanoop Saji', 'sanoopsaji@gmail.com', 'Registration notification', '<div>\r\n<h1>NEW USER REGISTRATION</h1>\r\n\r\n<h3 style=\"font-size:30px;font-weight:400;color:#000;\"><span style=\"font-size: 13px;\">Hi ,&nbsp;<br>\r\nNew user has been registered with following details:</span></h3><p>Name&nbsp; &nbsp;: [username]</p><p>Email&nbsp; &nbsp;: [email]</p><p>Phone&nbsp; : [phone]</p>\r\n<br>\r\nThanks,<br><strong>\r\n<a href=\"#\">Admin Library</a></strong>&nbsp;</div>', 1, 1, '2018-09-11 04:38:25', '2018-09-14 10:57:36'),
(2, 'Test', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.rk@acodez.co.in', 'Sanoop V', 'sanoop.v@acodez.in', 'Testing', '<p>This is just a <strong>dummy </strong>content...</p>', 0, 1, '2018-09-12 07:13:55', '2019-08-01 05:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `album_id`, `filename`, `created_at`, `updated_at`) VALUES
(10, 10, '2019/8/1565750958935it.jpg', '2019-08-13 21:19:19', '2019-08-13 21:19:19'),
(11, 10, '2019/8/1565750993356its.jpg', '2019-08-13 21:19:53', '2019-08-13 21:19:53'),
(23, 11, '2019/8/1565757499136onam2.jpg', '2019-08-13 23:08:19', '2019-08-13 23:08:19'),
(22, 11, '2019/8/1565757302324it.jpg', '2019-08-13 23:05:02', '2019-08-13 23:05:02'),
(24, 11, '2019/8/1565757649399onam.jpg', '2019-08-13 23:10:49', '2019-08-13 23:10:49'),
(25, 11, '2019/8/1565759217536it.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(26, 11, '2019/8/1565759217539its.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57'),
(27, 11, '2019/8/1565759217546onam.jpg', '2019-08-13 23:36:57', '2019-08-13 23:36:57');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(1, 'ghjgfh', '<p>gfhgfh</p>', '2019-08-15 04:41:23', '2019-08-15 04:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `mission` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `mission`, `created_at`, `updated_at`) VALUES
(1, '<p>fsdfsd</p>', '2019-08-14 22:43:40', '2019-08-14 22:43:40'),
(2, '<p>gfryu</p>', '2019-08-14 22:53:54', '2019-08-14 22:53:54'),
(3, '<p>trytry</p>', '2019-08-14 23:09:57', '2019-08-14 23:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(7, 'Admission opens', '<p>Admission opnes&nbsp;</p>', '2019/8/computer5.jpg', '2019-08-05 06:10:03', '2019-08-05 06:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `meta_key` text CHARACTER SET utf8,
  `meta_description` text CHARACTER SET utf8,
  `slug` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `meta_key`, `meta_description`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>This is just a dummy content</p>', NULL, NULL, 'about-us', 1, '2018-09-11 06:02:45', '2018-09-12 06:25:00'),
(2, 'Services', '<p>This is just a dummy content.</p>', NULL, NULL, 'services', 1, '2018-09-11 06:05:29', '2018-09-12 06:18:06'),
(3, 'contact', '<p>information</p>', 'contact', 'contact', 'contact', 1, '2019-08-13 05:01:33', '2019-08-13 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(1, 'browse_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(2, 'read_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(3, 'edit_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(4, 'add_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(5, 'delete_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(6, 'browse_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(7, 'read_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(8, 'edit_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(9, 'add_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(10, 'delete_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(11, 'browse_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(12, 'read_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(13, 'edit_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(14, 'add_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(15, 'delete_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(16, 'browse_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(17, 'read_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(18, 'edit_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(19, 'add_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(20, 'delete_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(21, 'browse_classification', 'classification', '2018-03-29 00:06:51', '2018-03-29 00:06:51', 5),
(22, 'read_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(23, 'edit_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(24, 'add_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(25, 'delete_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(26, 'browse_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(27, 'read_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(28, 'edit_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(29, 'add_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(30, 'delete_category', 'category', '2018-03-29 07:09:17', '2018-03-29 07:09:17', 6),
(31, 'browse_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(32, 'read_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(33, 'edit_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(34, 'add_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(35, 'delete_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(36, 'browse_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(37, 'read_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(38, 'edit_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(39, 'add_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(40, 'delete_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(41, 'browse_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(42, 'read_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(43, 'edit_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(44, 'add_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(45, 'delete_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(46, 'browse_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(47, 'read_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(48, 'edit_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(49, 'add_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(50, 'delete_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(51, 'browse_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(52, 'read_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(53, 'edit_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(54, 'add_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(55, 'delete_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(56, 'browse_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(57, 'read_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(58, 'edit_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(59, 'add_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(60, 'delete_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(61, 'browse_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(62, 'read_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(63, 'edit_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(64, 'add_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(65, 'delete_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(66, 'browse_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(67, 'read_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(68, 'edit_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(69, 'add_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(70, 'delete_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(71, 'browse_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(72, 'read_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(73, 'edit_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(74, 'add_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(75, 'delete_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(76, 'browse_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(77, 'read_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(78, 'edit_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(79, 'add_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(80, 'delete_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(81, 'browse_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(82, 'read_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(83, 'edit_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(84, 'add_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(85, 'delete_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(86, 'browse_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(87, 'read_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(88, 'edit_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(89, 'add_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(90, 'delete_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(91, 'browse_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(92, 'read_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(93, 'edit_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(94, 'add_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(95, 'delete_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(96, 'browse_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(97, 'read_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(98, 'edit_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(99, 'add_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(100, 'delete_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(101, 'browse_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(102, 'read_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(103, 'edit_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(104, 'add_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(105, 'delete_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(106, 'browse_gallery', 'gallery', '2019-08-12 22:58:02', '2019-08-12 22:58:02', 23),
(107, 'read_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(108, 'edit_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(109, 'add_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(110, 'delete_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(111, 'browse_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(112, 'read_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(113, 'edit_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(114, 'add_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(115, 'delete_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(116, 'browse_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(117, 'read_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(118, 'edit_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(119, 'add_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(120, 'delete_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(121, 'browse_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(122, 'read_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(123, 'edit_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(124, 'add_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(125, 'delete_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(126, 'browse_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(127, 'read_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(128, 'edit_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(129, 'add_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(130, 'delete_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'System Administrator', '2018-02-07 06:39:33', '2018-09-12 06:01:57'),
(2, 'user', 'Normal User', '2018-02-07 06:39:33', '2018-09-12 04:31:00'),
(3, 'sub_admin', 'Sub admin', '2018-03-28 01:38:44', '2018-03-28 01:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biology', '2019-08-06 04:42:18', '2019-08-06 04:42:18'),
(2, 'computer science', '2019-08-06 04:47:06', '2019-08-06 04:47:06'),
(3, 'humanities', '2019-08-06 04:47:15', '2019-08-06 04:47:15'),
(5, 'any', '2019-08-12 09:36:28', '2019-08-12 09:36:28'),
(6, 'commerce', '2019-08-12 09:36:43', '2019-08-12 09:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `content` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 'sdghfgsfsf', '<p>sdfsdjfhsdjf</p>', '2019/8/tulips.jpg', '2019-08-14 13:29:46', '2019-08-14 13:29:46');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `created_at`, `updated_at`) VALUES
(19, 'maths', '2019-08-09 05:53:37', '2019-08-09 05:53:37'),
(20, 'physcics', '2019-08-09 05:53:37', '2019-08-09 05:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `temp_phone` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `login_otp` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `otp_status` tinyint(2) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  `blocked` tinyint(2) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `temp_email`, `password`, `phone`, `temp_phone`, `otp`, `login_otp`, `role_id`, `avatar`, `remember_token`, `activation_token`, `active`, `otp_status`, `token`, `blocked`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin@admin.com', NULL, '$2y$10$j9iaCUOR3tQHAnOlh2TKquxESTmhCITukHeGQR6dnyHacKxBr1YS.', '94247760947', '+96698989898989', 4799, NULL, 1, '2018/8/screenshot-iytimgcom-2016-12-02-12-07-35.png', '1ccC2wlv2FFbb5zG1TSm9RliVxUSUhHgnMD9uF0zzt6pdlJW5sgZ7z2cVLpR', NULL, 1, 1, '', 0, 'active', '2019-08-14 10:08:14', '2018-03-23 06:50:19', '2019-08-14 04:38:14', '0000-00-00 00:00:00'),
(2, 'Vasanthan', 'R K', 'vasanthan.rk@acodez.co.in', NULL, '$2y$10$/2RMCfB7LDOmXrFPwtbwduntpqYtfDaY3qqUekozhdpBpUQWyz5/m', '15454545', NULL, NULL, NULL, 3, NULL, NULL, NULL, 1, 0, NULL, 0, 'inactive', NULL, '2018-08-29 00:44:20', '2019-07-29 04:51:14', NULL),
(3, 'Nandul', 'Das', 'nandul.das@acodez.co.in', NULL, '$2y$10$yElsIKQuA3i6TQuCW6OTyOd0F3S4KihLqepjXbw7masvm21UhKuyy', '123456789', NULL, NULL, NULL, 2, NULL, NULL, NULL, 1, 0, NULL, 0, 'inactive', NULL, '2018-08-30 05:54:53', '2019-07-29 04:51:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`id`, `name`, `address`, `phone`, `email`, `reason`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth (h)\r\nperinkary(po)\r\nkannur', '9061395796', 'subinakandoth@gmail.com', 'Admission', '2019-07-10', '03:25:00', '2019-07-29 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `vission`
--

CREATE TABLE `vission` (
  `id` int(11) NOT NULL,
  `vission` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vission`
--

INSERT INTO `vission` (`id`, `vission`, `created_at`, `updated_at`) VALUES
(1, '<p>ghhsasa</p>', '2019-08-15 04:26:57', '2019-08-15 04:26:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vission`
--
ALTER TABLE `vission`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vission`
--
ALTER TABLE `vission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
