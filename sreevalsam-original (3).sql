-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 06, 2020 at 11:32 PM
-- Server version: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sreevalsam-original`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `short_description` longtext NOT NULL,
  `main_description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `meta_tag` varchar(200) DEFAULT NULL,
  `meta_title` varchar(50) NOT NULL,
  `meta_content` varchar(50) NOT NULL,
  `meta_description` varchar(50) NOT NULL,
  `meta_key` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `mission` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title`, `short_description`, `main_description`, `image`, `slug`, `meta_tag`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `mission`, `created_at`, `updated_at`) VALUES
(6, 'xz', 'xz', 'Sreevalsam Interiors is a young and dynamic interior design company in south India offering comprehensive interior design solutions We have professional and the Best Interior Designers in Cochin to adopt a systematic and methodological approach to the design environment, identifying the client\'s requirement in context of the current condition, limitations, keeping in mind the aesthetic and durability aspects, be it for your home or office. Our professional expertise will be able to provide you with the best in Residential Interior Designs and Commercial Interior Designs There is nothing like custom made furniture just for your home or office. We undertake comprehensive contract furnishing services for modern interior design,office interior design,interior design decorating and commercial interior design. Our Best Interior Designers and furniture experts turn concepts and possibilities into reality. We work within your budget to execute a quality solution that is a distinctive expression of your home or business .', '2020/2/success-1.jpg', 'xz', NULL, 'xz', 'xz', 'xz', 'xz', 'xz', NULL, '2020-01-29 17:21:39', '2020-02-16 07:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  `type` enum('facility','order','user','setting','role','classification','category','email','cms','module') DEFAULT NULL,
  `activity` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `relation_id`, `type`, `activity`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'user', NULL, '2018-08-31 00:54:24', '2018-08-31 00:54:24'),
(2, 1, 1, 'user', NULL, '2018-08-31 00:54:48', '2018-08-31 00:54:48'),
(3, 1, 1, 'user', NULL, '2018-08-31 00:56:22', '2018-08-31 00:56:22'),
(4, 1, 1, 'user', NULL, '2018-08-31 00:56:33', '2018-08-31 00:56:33'),
(5, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:30:44', '2018-09-12 04:30:44'),
(6, 1, 2, 'role', 'New role has been edited by:', '2018-09-12 04:31:03', '2018-09-12 04:31:03'),
(7, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 04:56:59', '2018-09-12 04:56:59'),
(8, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 05:55:38', '2018-09-12 05:55:38'),
(9, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:46', '2018-09-12 06:01:46'),
(10, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:01:57', '2018-09-12 06:01:57'),
(11, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:03', '2018-09-12 06:04:03'),
(12, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:23', '2018-09-12 06:04:23'),
(13, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:04:35', '2018-09-12 06:04:35'),
(14, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:32:50', '2018-09-12 06:32:50'),
(15, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 06:33:01', '2018-09-12 06:33:01'),
(16, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 22:59:25', '2018-09-12 22:59:25'),
(17, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:10', '2018-09-12 23:01:10'),
(18, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:01:20', '2018-09-12 23:01:20'),
(19, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:27', '2018-09-12 23:02:27'),
(20, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:02:47', '2018-09-12 23:02:47'),
(21, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:15', '2018-09-12 23:03:15'),
(22, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:40', '2018-09-12 23:03:40'),
(23, 1, 1, 'role', 'New role has been edited by:', '2018-09-12 23:03:54', '2018-09-12 23:03:54'),
(24, 1, 1, 'user', NULL, '2018-09-13 04:46:30', '2018-09-13 04:46:30'),
(25, 1, 1, 'user', NULL, '2018-09-13 04:47:20', '2018-09-13 04:47:20'),
(26, 1, 1, 'user', NULL, '2018-09-13 04:48:46', '2018-09-13 04:48:46'),
(27, 1, 1, 'user', NULL, '2018-09-13 04:52:00', '2018-09-13 04:52:00'),
(28, 1, 1, 'user', NULL, '2018-09-13 04:52:17', '2018-09-13 04:52:17'),
(29, 1, 18, 'module', 'New module has been created by:', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(30, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 02:56:44', '2019-07-29 02:56:44'),
(31, 1, 19, 'module', 'New module has been created by:', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(32, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:50:28', '2019-07-29 06:50:28'),
(33, 1, 1, 'role', 'New role has been edited by:', '2019-07-29 06:53:26', '2019-07-29 06:53:26'),
(34, 1, 20, 'module', 'New module has been created by:', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(35, 1, 1, 'role', 'New role has been edited by:', '2019-07-30 22:58:34', '2019-07-30 22:58:34'),
(36, 1, 21, 'module', 'New module has been created by:', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(37, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 04:35:01', '2019-08-06 04:35:01'),
(38, 1, 22, 'module', 'New module has been created by:', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(39, 1, 1, 'role', 'New role has been edited by:', '2019-08-06 05:32:55', '2019-08-06 05:32:55'),
(40, 1, 23, 'module', 'New module has been created by:', '2019-08-12 22:58:03', '2019-08-12 22:58:03'),
(41, 1, 1, 'role', 'New role has been edited by:', '2019-08-12 22:58:25', '2019-08-12 22:58:25'),
(42, 1, 24, 'module', 'New module has been created by:', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(43, 1, 1, 'role', 'New role has been edited by:', '2019-08-13 04:36:06', '2019-08-13 04:36:06'),
(44, 1, 25, 'module', 'New module has been created by:', '2019-08-14 00:16:42', '2019-08-14 00:16:42'),
(45, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 00:17:05', '2019-08-14 00:17:05'),
(46, 1, 26, 'module', 'New module has been created by:', '2019-08-14 05:56:10', '2019-08-14 05:56:10'),
(47, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 05:56:32', '2019-08-14 05:56:32'),
(48, 1, 27, 'module', 'New module has been created by:', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(49, 1, 1, 'role', 'New role has been edited by:', '2019-08-14 07:52:44', '2019-08-14 07:52:44'),
(50, 1, 28, 'module', 'New module has been created by:', '2019-08-20 04:26:05', '2019-08-20 04:26:05'),
(51, 1, 1, 'role', 'New role has been edited by:', '2019-08-20 04:26:20', '2019-08-20 04:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_modules`
--

CREATE TABLE `admin_modules` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `perifix` varchar(200) NOT NULL,
  `type` enum('parent','child') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_modules`
--

INSERT INTO `admin_modules` (`id`, `parent_id`, `name`, `perifix`, `type`, `created_at`, `updated_at`) VALUES
(1, 0, 'User', 'user', 'parent', '2018-03-26 05:44:20', '2018-03-26 05:44:20'),
(2, 0, 'Roles', 'roles', 'parent', '2018-03-27 01:07:21', '2018-03-27 01:07:21'),
(3, 0, 'Module', 'module', 'parent', '2018-03-27 01:07:42', '2018-03-27 01:07:42'),
(4, 0, 'Admin User', 'admin_user', 'parent', '2018-03-27 23:00:33', '2018-03-27 23:00:33'),
(5, 0, 'Classification', 'classification', 'parent', '2018-03-29 00:06:51', '2018-03-29 00:06:51'),
(6, 0, 'Category', 'category', 'parent', '2018-03-29 07:09:16', '2018-03-29 07:09:16'),
(7, 0, 'CMS', 'cms', 'parent', '2018-03-29 22:48:40', '2018-03-29 22:48:40'),
(8, 0, 'Facility Management', 'facility_management', 'parent', '2018-05-06 23:21:17', '2018-05-06 23:21:17'),
(9, 0, 'Order Management', 'order', 'parent', '2018-05-07 23:47:47', '2018-05-07 23:47:47'),
(10, 0, 'Email Setting', 'email_template', 'parent', '2018-05-08 23:49:01', '2018-05-08 23:49:01'),
(11, 0, 'User Report', 'user_report', 'parent', '2018-05-13 22:40:52', '2018-05-13 22:40:52'),
(12, 0, 'Facility Report', 'facility_report', 'parent', '2018-05-14 22:02:29', '2018-05-14 22:02:29'),
(13, 0, 'Order Report', 'order_report', 'parent', '2018-05-15 00:45:33', '2018-05-15 00:45:33'),
(14, 0, 'Settings', 'setting', 'parent', '2018-05-16 22:17:17', '2018-05-16 22:17:17'),
(15, 0, 'Admin User', 'admin_user', 'parent', '2018-05-22 05:50:07', '2018-05-22 05:50:07'),
(16, 0, 'Static Content', 'static_content', 'parent', '2018-05-30 03:38:32', '2018-05-30 03:38:32'),
(17, 0, 'Testimonial', 'testimonial', 'parent', '2018-06-01 05:45:03', '2018-06-01 05:45:03'),
(18, 0, 'department', 'department', 'parent', '2019-07-29 00:33:33', '2019-07-29 00:33:33'),
(19, 0, 'subject', 'subject', 'parent', '2019-07-29 06:50:06', '2019-07-29 06:50:06'),
(20, 0, 'news', 'news', 'parent', '2019-07-30 22:58:10', '2019-07-30 22:58:10'),
(21, 0, 'scheme', 'scheme', 'parent', '2019-08-06 04:34:38', '2019-08-06 04:34:38'),
(22, 0, 'course', 'course', 'parent', '2019-08-06 05:32:04', '2019-08-06 05:32:04'),
(23, 0, 'gallery', 'gallery', 'parent', '2019-08-12 22:58:02', '2019-08-12 22:58:02'),
(24, 0, 'album', 'album', 'parent', '2019-08-13 04:35:50', '2019-08-13 04:35:50'),
(25, 0, 'about', 'about', 'parent', '2019-08-14 00:16:41', '2019-08-14 00:16:41'),
(26, 0, 'contact', 'contact', 'parent', '2019-08-14 05:56:09', '2019-08-14 05:56:09'),
(27, 0, 'slider', 'slider', 'parent', '2019-08-14 07:51:16', '2019-08-14 07:51:16'),
(28, 0, 'facility', 'facility', 'parent', '2019-08-20 04:26:05', '2019-08-20 04:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `street` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `course` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admission`
--

INSERT INTO `admission` (`id`, `fname`, `lname`, `street`, `city`, `pincode`, `state`, `country`, `phone`, `email`, `course`, `created_at`, `updated_at`) VALUES
(5, 'SUBINA', 'KS', 'IRITTY', 'KANNUR', '670706', 'Kerala', 'INDIA', '9061395796', 'subinakandoth@gmail.com', 'B.Sc.Petrochemical', '2019-08-19 03:56:58', '2019-08-19 03:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) DEFAULT NULL,
  `description` text,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `main_image1` varchar(191) DEFAULT NULL,
  `main_image2` varchar(191) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `title`, `slug`, `main_image`, `description`, `status`, `created_at`, `updated_at`, `main_image1`, `main_image2`) VALUES
(23, 'Residential Designing', 'test', '2020/2/services-12.jpg', 'Sreevalsam Interiors is recognized leader in residential interior design specializing in highly curated and customized residential interiors that appeal to a sophisticated and discerning clientele. Our Team of interior designers bring a wealth of experience and expertise that are committed to responding to our clients’ brief and lifestyle, and creating original, authentic and individual interiors that stand the test of time.\r\n\r\nWe offer a full residential designing service, from concept through to completion, delivering professional well-documented solutions that exceeds expectation and add value to projects. We are known to delivers bespoke house designs filled with fresh air, light and space to breathe. Across Kochi and internationally.\r\n\r\nStarting from ideal living room to bedroom and dining room to bathroom. We have everthing covered up to create unique architecture and interior design for your luxurious home experience every day.', 0, '2020-02-16 08:18:10', '2020-02-16 02:48:10', '2020/2/services-13.jpg', '2020/2/services-14.jpg'),
(24, 'Office Interior Designing', 'service', '2020/2/services-4-1.jpg', 'Your office says a lot about your company its mission and vision. So an attractive office design impresses the client and motivates employees. By completing these services efficnetly today we are one of the most eminent names in kochi for providing Corporate Interior Designing Solutions. Natural proportions and complex geometric patterns form the framework of our design process thus rendering a unique aesthetic commercial design language.\r\n\r\nHaving over 15 years of experience in innovative design and fitout of commercial interiors; It means you can continue running your business in the knowledge that your project will be of excellent quality & fully operational on time & to budget. Bearing in mind, the client’s requirement, consultants at Zed helps you to connect the dots of imagination and present it live on the structur', 1, '2020-02-16 08:19:05', '2020-02-16 02:49:05', '2020/2/services-10.jpg', '2020/2/services-11.jpg'),
(25, 'Architect & Interior Designing', 'xs', '2020/2/services-8.jpg', 'Sreevalsam Interiors. is a versatile Interior Designing Company based in Kochi, India that encompasses modern residential, commercial, retail and hospitality projects, with full service capabilities in ground-up architecture, site development, interior design, and fine art consulting services to our esteemed clients.\r\n\r\nWe create distinct luxury lifestyle experiences that reflect our clients’ personal needs and desires with sophisticated designs that exude order and artistic cool. Our Interior Designer Professionals tailor quality materials and state-of-the-art technology to suit the lifestyle of our clients which embody a luxurious experience every day.You can feel maximum benefits with just minimum alterations. As reliable firm we offer economical and innovative solutions to your requirements.', 1, '2020-02-16 08:16:27', '2020-02-16 02:46:27', '2020/2/services-6.jpg', '2020/2/services-5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `qualification` longtext NOT NULL,
  `experience` longtext NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(50) NOT NULL,
  `category` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`id`, `fname`, `lname`, `city`, `street`, `phone`, `email`, `qualification`, `experience`, `country`, `state`, `pincode`, `category`, `updated_at`, `created_at`) VALUES
(4, 'shijina', 'k', 'kannur', 'mattannur', '9446395796', 'shijina@gmail.com', 'MSc geology', 'fresher', 'INDIA', 'Kerala', '670709', 'Faculty', '2019-08-19 10:31:48', '2019-08-19 10:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `slug` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1. Default Item',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `slug`, `parent_id`, `status`, `created_at`, `updated_at`) VALUES
(3, 'THREE', 'Three', 'next2', 0, 0, '2018-09-10', '2018-09-11'),
(10, 'gg', '', 'dd', 9, 0, '2018-09-10', '2018-09-10'),
(13, 'fgdgdgdsfgs', '', 'fgdgdgdsfgs', 9, 0, '2018-09-10', '2018-09-10'),
(15, '4', '', 'two', 12, 0, '2018-09-10', '2018-09-11'),
(16, '222', 'test', 'thhree', 1, 0, '2018-09-11', '2018-09-12'),
(26, 'ONE', 'one', 'sdfsdfsd', 0, 0, '2018-09-11', '2018-09-11'),
(30, '11', NULL, 'dummy', 26, 0, '2018-09-11', '2018-09-11'),
(31, '44', NULL, '44', 12, 0, '2018-09-11', '2018-09-11'),
(32, '444', NULL, '444', 12, 0, '2018-09-11', '2018-09-11'),
(35, 'htyyt', 'tjytjyjyj', 'htyyt', 0, 0, '2019-08-01', '2019-08-01'),
(36, 'jyjytjyt', NULL, 'jyjytjyt', 35, 0, '2019-08-01', '2019-08-01'),
(37, 'jyutyuytj', NULL, 'jyutyuytj', 35, 0, '2019-08-01', '2019-08-01'),
(38, 'Bachelor of Computer Application', 'zdxczxv', 'bachelor-of-computer-application', 1, 0, '2019-08-01', '2019-08-01'),
(39, 'Bsc. Geology', 'fghfh', 'bsc-geology', 35, 0, '2019-08-01', '2019-08-01'),
(40, 'we', 'wewre', 'we', 0, 0, '2019-08-01', '2019-08-01'),
(41, 'vc', NULL, 'vc', 40, 0, '2019-08-01', '2019-08-01'),
(42, 'vm', NULL, 'vm', 40, 0, '2019-08-01', '2019-08-01'),
(43, 'vk', NULL, 'vk', 40, 0, '2019-08-01', '2019-08-01'),
(45, 'ffgfg', 'gfgfg', 'ffgfg', 0, 0, '2019-08-01', '2019-08-01'),
(46, 'cvcvcv', NULL, 'cvcvcv', 45, 0, '2019-08-01', '2019-08-01'),
(47, 'cvgfhgh', 'fgh', 'cv', 35, 0, '2019-08-01', '2019-08-14'),
(49, 'xfd', NULL, 'xfd', 0, 0, '2019-08-06', '2019-08-06');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `phone` longtext NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` longtext NOT NULL,
  `map` longtext NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `insta` varchar(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `meta_title` varchar(50) DEFAULT NULL,
  `meta_content` varchar(50) DEFAULT NULL,
  `meta_description` varchar(50) DEFAULT NULL,
  `meta_key` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `phone`, `email`, `address`, `map`, `facebook`, `twitter`, `linkedin`, `insta`, `youtube`, `meta_title`, `meta_content`, `meta_description`, `meta_key`, `author`, `created_at`, `updated_at`) VALUES
(6, 'B.B.H.S Nangiarkulangara', '+0479 241 0750', 'principal.aims1@gmail.com', '<p>Mavelikara Road, Nangiarkulangara, Kerala 690513</p>', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7868.548572652532!2d77.056052!3d9.571608!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbb65e73f9454c0f0!2sAyyappa+Institute+of+Management+Studies%2C+Peermade!5e0!3m2!1sen!2sin!4v1566212709483!5m2!1sen!2sin', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', 'B.B.H.S Nangiarkulangara', '2019-08-19 11:06:32', '2019-11-17 11:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  `course` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `departments_id`, `course`, `slug`, `content`, `description`, `image`, `created_at`, `updated_at`) VALUES
(17, 10, 'B.Sc. Geology', 'bsc-geology', '<p>Geology is an Earth science concerned with the solid earth, the rock of which it is composed and the processes by which they change over time. Geology can refer to the study of the solid features of any planet or natural satellite(Such as Mars or the Moon).</p><p>Geology describes the structure of the earth beneath its surface, and the processes that have shaped structure. An important of geology is the study of how earth’s materials, structures processes and organisms have changed overtime.\r\n\r\n</p><h3>JOB OPPORTUNITIES</h3><p></p><p>Variety of companies offer job to geologist like oil and natural gas corporation (ONGC), Geological survey of India (GSI), CPWD, TWAD, PWD, DGM, and many private and government organizations with interests in Natural Resource Exploitation.</p><p>The research degrees provide a higher level of training often in a geology specially areas such as paleontology mineralogy hydrology or volcano logy. Employment opportunities for geologists very good.</p>', '', '2019/8/dpm-8444-1-768x5124.jpg', '2019-08-16 05:16:01', '2019-08-15 23:46:01'),
(18, 11, 'B.Sc.Petrochemical', 'bscpetrochemical', '<p>BSC Petrochemicals is a UG course which prepares an aspiring student for a bright future in the petrochemical industry. This is a very rare course. AIMS, Ayyappa Institute of Management Studies is one of the top institutions providing this course under MG University Kottayam.</p><p>Petrochemical industries are at great importance in the context at present business scenario. Petrochemical products are obtained from refining crude oil and it is needless to mention that the entire procedure is quite extensive. This is why petrochemicals or petroleum products have a high price in the market. Newer ending opportunities are waiting for a petrochemical student. 1000’s of jobs are awaiting qualified candidates as the expansion of cochin Refineries is nearing completion.\r\n\r\n</p>', '<h3>Eligibility</h3>\r\n<p>\r\nA pass in Plus Two / Equivalent examination with Chemistry as one of the optional subjects</p><h3>B.Sc.Petrochemical + Advanced diploma in</h3><ol><li>Oil & Gas Safety Management</li><li>Process & Applications of Petroleum Products</li></ol><h3>JOB OPPORTUNITIES</h3><ul><li>Petroleum Refineries</li><li>Polymer Industries</li><li>Paint Industries</li><li>Food Industry</li><li>Pharmaceutical Industry</li><li>Auto Mobile Industry</li></ul><h3>Research Areas</h3><ul><li>Nanotechnology</li><li>Polymers</li><li>Research on fuel technology</li></ul>', '2019/8/dpm-8068.jpg', '2019-08-16 10:26:53', '2019-08-16 04:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `created_at`, `updated_at`) VALUES
(11, 'Department of PetroChecmicals', '2019-08-19 10:43:34', '2019-08-19 05:13:34'),
(10, 'Department of Geology', '2019-08-06 06:28:17', '2019-08-06 06:28:17'),
(13, 'Department of commerce', '2019-08-19 05:13:59', '2019-08-19 05:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `eligibility`
--

CREATE TABLE `eligibility` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `eligibility` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `from_name` varchar(50) DEFAULT NULL,
  `from_email` varchar(500) DEFAULT NULL,
  `to_name` varchar(500) DEFAULT NULL,
  `to_email` varchar(500) DEFAULT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_email` varchar(500) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email_body` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `title`, `key`, `from_name`, `from_email`, `to_name`, `to_email`, `cc_name`, `cc_email`, `subject`, `email_body`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Registration notification to admin', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.raju@gmail.com', 'Sanoop Saji', 'sanoopsaji@gmail.com', 'Registration notification', '<div>\r\n<h1>NEW USER REGISTRATION</h1>\r\n\r\n<h3 style=\"font-size:30px;font-weight:400;color:#000;\"><span style=\"font-size: 13px;\">Hi ,&nbsp;<br>\r\nNew user has been registered with following details:</span></h3><p>Name&nbsp; &nbsp;: [username]</p><p>Email&nbsp; &nbsp;: [email]</p><p>Phone&nbsp; : [phone]</p>\r\n<br>\r\nThanks,<br><strong>\r\n<a href=\"#\">Admin Library</a></strong>&nbsp;</div>', 1, 1, '2018-09-11 04:38:25', '2018-09-14 10:57:36'),
(2, 'Test', NULL, 'Sandeep', 'sandeep.kumar@acodez.co.in', 'Vasanthan', 'vasanthan.rk@acodez.co.in', 'Sanoop V', 'sanoop.v@acodez.in', 'Testing', '<p>This is just a <strong>dummy </strong>content...</p>', 0, 1, '2018-09-12 07:13:55', '2019-08-01 05:42:48');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `first_name` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `message` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `first_name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'ccc', 'cxc@gmail.com', 'cx', '2020-02-16 06:33:37', '2020-02-16 06:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(2, 'content not available', '<p>content not available</p>', '2019/8/img-not-available.png', '2019-08-20 10:32:39', '2019-08-20 10:41:54'),
(3, 'content not available', '<p>content not available</p>', '2019/8/img-not-available1.png', '2019-08-20 10:47:19', '2019-08-20 10:47:19');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `album_id`, `filename`, `created_at`, `updated_at`) VALUES
(47, 23, '2020/1/1580056346765img-not-available.jpg', '2020-01-26 11:02:26', '2020-01-26 11:02:26'),
(48, 23, '2020/1/1580056346769red-cross.jpg', '2020-01-26 11:02:26', '2020-01-26 11:02:26'),
(49, 23, '2020/1/1580056356467kids2.jpg', '2020-01-26 11:02:36', '2020-01-26 11:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `title`, `message`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Architect & Interior Designing', 'Sreevalsam Interiors. is a versatile Interior Designing Company based in Kochi, India that encompasses modern residential, commercial, retail and hospitality projects, with full service capabilities in ground-up architecture, site development, interior design, and fine art consulting services to our esteemed clients.', '2020/2/services-11.jpg', '2019-11-17 16:25:35', '2020-02-16 07:13:27'),
(4, 'Furniture Designing', 'Sometimes you need an extra edge to give your home or place of business beautiful, stylish, and comfortable.A house with a well-furnished wooden work gives as finishing touch and furniture matching with the shades of interiors of the house delivers a distinctive look to the interiors Decorators Designs.', '2020/2/services-4.jpg', '2020-01-25 10:29:16', '2020-02-16 07:12:24'),
(5, 'Residential Designing', 'Sreevalsam Interiors is recognized leader in residential interior design specializing in highly curated and customized residential interiors that appeal to a sophisticated and discerning clientele.We offer a full residential designing service, from concept through to completion, delivering professional well-documented solutions that exceeds expectation and add value to projects.', '2020/2/services-2.jpg', '2020-02-16 07:15:05', '2020-02-16 07:15:05'),
(6, 'Modular Kitchen', 'Whether your kitchen needs a minor facelift or a complete guttering, hiring a Sreevalsam Interiors kitchen designer or renovator could be a worthwhile investment. We have a heritage of 15 years as top kitchen designer influenced by modern and elegant design principles.With a team of trusted professionals, we can manage your entire kitchen makeover.', '2020/2/services-3.jpg', '2020-02-16 07:15:59', '2020-02-16 07:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id` int(11) NOT NULL,
  `vission` longtext NOT NULL,
  `mission` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `vission`, `mission`, `created_at`, `updated_at`) VALUES
(4, '<p>The Vision of BBHS for Girls is to form young generation who care , serve and love the downtrodden and the marginalized. Founder Mar Ivanios, the icon of the Teaching profession depicts as a “Friend & Guide” , one who devotes himself completely to the upliftment of the humanity.</p>', '<p>The Mission of BBHS for Girls is to continue the Teaching ministry of JESUS CHRIST in this world. BBHS aims at providing excellent academic formation along with artistic skills of International Standards, concentrates both in efficiency and value orientation and enable the students to face the emerging challenges.</p>', '2019-11-17 10:54:16', '2019-11-17 10:54:16');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_gallery`
--

CREATE TABLE `multiple_gallery` (
  `id` int(11) NOT NULL,
  `tab_id` int(11) NOT NULL,
  `content` text,
  `image` varchar(191) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_gallery`
--

INSERT INTO `multiple_gallery` (`id`, `tab_id`, `content`, `image`, `created_at`, `updated_at`) VALUES
(1, 3, 'xzx', '2020/3/screenshot-from-2020-02-28-21-12-30.png', '2020-03-05 16:40:00', '2020-03-05 16:40:00'),
(2, 5, 'dfdf', '2020/3/screenshot-from-2019-11-17-12-56-34.png', '2020-03-06 17:31:58', '2020-03-06 17:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) NOT NULL,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `image`, `created_at`, `updated_at`) VALUES
(8, 'Admission', 'admission', '<p>Admission \r\n\r\nStarted\r\n\r\n for the year 2019&nbsp;</p>', '2019/8/about-pic3.png', '2019-08-16 23:30:28', '2019-08-16 23:46:59'),
(9, 'Admission', 'admission-2', '<p>Admission Started for the year 2019&nbsp;</p>', '2019/8/about-pic4.png', '2019-08-16 23:48:21', '2019-08-16 23:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) DEFAULT NULL,
  `description` text,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `title`, `slug`, `main_image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(29, 'Bedroom', 'bedroom', '2020/2/g.jpg', 'A large portion of anyone\'s life is spent in a bedroom, as the best place to relieve from anxieties,worries and hurries. Family feels secure and free in this room and thus it need the best ambiance, facilities and provisions. Cot, side tables, wardrobes and dressing table mostly occupies almost all the space in a normal bedroom. How to arrange the furniture in style and what is the best suitable color theme, an interior designer can provide the solution. They can make and provide the bed room furniture in perfect size to fit the space. Come to Sreevalsam Interiors and explore the options for making the best comfortable bed room. Below are just a few samples of bed room interior furnishing that we provide anywhere in', 1, '2020-02-16 02:59:17', '2020-02-16 02:59:17'),
(25, 'kitchen', 'test-image-in-package', '2020/2/services-11.jpg', 'Custom-made modular kitchen exactly matches the space and fits perfectly as per actual measurement. An experienced designer from Sreevalsam Interiors team visits the site, takes measurement and discusses the requirements with client. He prepares a suitable detailed plan and design as per the shape of kitchen. Customized modular kitchen in Kerala can be either for a flat (apartment) or group villa or for an independent villa. This professional interior design company makes the kitchen in own factory at Ernakulam, as perapproved design and agreement. We have a team of experienced technicians to do the installation at site as part of the contract. Below are 5 shapes and styles that we follow while designing and implementing a kitchen for any place in Kerala.', 1, '2020-02-16 08:25:14', '2020-02-16 02:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `packages_media`
--

CREATE TABLE `packages_media` (
  `id` int(11) NOT NULL,
  `packages_id` int(11) NOT NULL,
  `filename` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages_media`
--

INSERT INTO `packages_media` (`id`, `packages_id`, `filename`, `created_at`, `updated_at`) VALUES
(17, 25, '2020/2/1581841474988sreevalsam-interiors-23.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(18, 25, '2020/2/1581841474992sreevalsam-interiors-22.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(7, 26, '2020/1/1580058895958navya-nair.jpg', '2020-01-26 11:44:56', '2020-01-26 11:44:56'),
(8, 26, '2020/1/15800588959533.jpg', '2020-01-26 11:44:56', '2020-01-26 11:44:56'),
(9, 26, '2020/1/1580058895962kids2.jpg', '2020-01-26 11:44:56', '2020-01-26 11:44:56'),
(10, 26, '2020/1/1580058895960red-cross.jpg', '2020-01-26 11:44:56', '2020-01-26 11:44:56'),
(11, 28, '2020/2/1580643777336babitha.jpg', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(12, 28, '2020/2/15806437773312.jpg', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(13, 28, '2020/2/1580643777338chithra.jpg', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(14, 28, '2020/2/1580643777340navya-nair.jpg', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(15, 28, '2020/2/1580643777347spc.jpg', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(16, 28, '2020/2/1580643777344footer-logo.png', '2020-02-02 06:12:57', '2020-02-02 06:12:57'),
(19, 25, '2020/2/1581841474997sreevalsam-interiors-13.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(20, 25, '2020/2/1581841474994sreevalsam-interiors-20.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(21, 25, '2020/2/1581841475002sreevalsam-interiors-12.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(22, 25, '2020/2/1581841475004sreevalsam-interiors-07.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(23, 25, '2020/2/1581841475007sreevalsam-interiors-04.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(24, 25, '2020/2/1581841475009sreevalsam-interiors-03.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(25, 25, '2020/2/1581841475011sreevalsam-interiors-02.jpg', '2020-02-16 02:54:35', '2020-02-16 02:54:35'),
(26, 29, '2020/2/1581841767689g.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27'),
(27, 29, '2020/2/1581841767693f.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27'),
(28, 29, '2020/2/1581841767695d.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27'),
(29, 29, '2020/2/1581841767697c.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27'),
(30, 29, '2020/2/1581841767700b.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27'),
(31, 29, '2020/2/1581841767702a.jpg', '2020-02-16 02:59:27', '2020-02-16 02:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `meta_key` text CHARACTER SET utf8,
  `meta_description` text CHARACTER SET utf8,
  `slug` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `content`, `meta_key`, `meta_description`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<p>This is just a dummy content</p>', NULL, NULL, 'about-us', 1, '2018-09-11 06:02:45', '2018-09-12 06:25:00'),
(2, 'Services', '<p>This is just a dummy content.</p>', NULL, NULL, 'services', 1, '2018-09-11 06:05:29', '2018-09-12 06:18:06'),
(3, 'contact', '<p>information</p>', 'contact', 'contact', 'contact', 1, '2019-08-13 05:01:33', '2019-08-13 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `module_id`) VALUES
(1, 'browse_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(2, 'read_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(3, 'edit_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(4, 'add_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(5, 'delete_user', 'user', '2018-03-26 05:44:20', '2018-03-26 05:44:20', 1),
(6, 'browse_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(7, 'read_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(8, 'edit_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(9, 'add_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(10, 'delete_roles', 'roles', '2018-03-27 01:07:21', '2018-03-27 01:07:21', 2),
(11, 'browse_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(12, 'read_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(13, 'edit_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(14, 'add_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(15, 'delete_module', 'module', '2018-03-27 01:07:42', '2018-03-27 01:07:42', 3),
(16, 'browse_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(17, 'read_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(18, 'edit_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(19, 'add_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(20, 'delete_admin_user', 'admin_user', '2018-03-27 23:00:33', '2018-03-27 23:00:33', 4),
(21, 'browse_classification', 'classification', '2018-03-29 00:06:51', '2018-03-29 00:06:51', 5),
(22, 'read_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(23, 'edit_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(24, 'add_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(25, 'delete_classification', 'classification', '2018-03-29 00:06:52', '2018-03-29 00:06:52', 5),
(26, 'browse_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(27, 'read_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(28, 'edit_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(29, 'add_category', 'category', '2018-03-29 07:09:16', '2018-03-29 07:09:16', 6),
(30, 'delete_category', 'category', '2018-03-29 07:09:17', '2018-03-29 07:09:17', 6),
(31, 'browse_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(32, 'read_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(33, 'edit_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(34, 'add_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(35, 'delete_cms', 'cms', '2018-03-29 22:48:40', '2018-03-29 22:48:40', 7),
(36, 'browse_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(37, 'read_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(38, 'edit_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(39, 'add_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(40, 'delete_facility_management', 'facility_management', '2018-05-06 23:21:17', '2018-05-06 23:21:17', 8),
(41, 'browse_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(42, 'read_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(43, 'edit_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(44, 'add_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(45, 'delete_order', 'order', '2018-05-07 23:47:47', '2018-05-07 23:47:47', 9),
(46, 'browse_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(47, 'read_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(48, 'edit_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(49, 'add_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(50, 'delete_email_template', 'email_template', '2018-05-08 23:49:01', '2018-05-08 23:49:01', 10),
(51, 'browse_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(52, 'read_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(53, 'edit_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(54, 'add_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(55, 'delete_user_report', 'report', '2018-05-13 22:40:52', '2018-05-13 22:40:52', 11),
(56, 'browse_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(57, 'read_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(58, 'edit_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(59, 'add_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(60, 'delete_facility_report', 'facility_report', '2018-05-14 22:02:29', '2018-05-14 22:02:29', 12),
(61, 'browse_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(62, 'read_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(63, 'edit_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(64, 'add_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(65, 'delete_order_report', 'order_report', '2018-05-15 00:45:33', '2018-05-15 00:45:33', 13),
(66, 'browse_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(67, 'read_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(68, 'edit_setting', 'setting', '2018-05-16 22:17:17', '2018-05-16 22:17:17', 14),
(69, 'add_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(70, 'delete_setting', 'setting', '2018-05-16 22:17:18', '2018-05-16 22:17:18', 14),
(71, 'browse_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(72, 'read_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(73, 'edit_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(74, 'add_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(75, 'delete_static_content', 'static_content', '2018-05-30 03:38:32', '2018-05-30 03:38:32', 16),
(76, 'browse_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(77, 'read_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(78, 'edit_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(79, 'add_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(80, 'delete_testimonial', 'testimonial', '2018-06-01 05:45:03', '2018-06-01 05:45:03', 17),
(81, 'browse_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(82, 'read_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(83, 'edit_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(84, 'add_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(85, 'delete_department', 'department', '2019-07-29 00:33:33', '2019-07-29 00:33:33', 18),
(86, 'browse_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(87, 'read_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(88, 'edit_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(89, 'add_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(90, 'delete_subject', 'subject', '2019-07-29 06:50:06', '2019-07-29 06:50:06', 19),
(91, 'browse_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(92, 'read_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(93, 'edit_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(94, 'add_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(95, 'delete_news', 'news', '2019-07-30 22:58:10', '2019-07-30 22:58:10', 20),
(96, 'browse_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(97, 'read_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(98, 'edit_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(99, 'add_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(100, 'delete_scheme', 'scheme', '2019-08-06 04:34:38', '2019-08-06 04:34:38', 21),
(101, 'browse_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(102, 'read_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(103, 'edit_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(104, 'add_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(105, 'delete_course', 'course', '2019-08-06 05:32:04', '2019-08-06 05:32:04', 22),
(106, 'browse_gallery', 'gallery', '2019-08-12 22:58:02', '2019-08-12 22:58:02', 23),
(107, 'read_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(108, 'edit_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(109, 'add_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(110, 'delete_gallery', 'gallery', '2019-08-12 22:58:03', '2019-08-12 22:58:03', 23),
(111, 'browse_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(112, 'read_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(113, 'edit_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(114, 'add_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(115, 'delete_album', 'album', '2019-08-13 04:35:50', '2019-08-13 04:35:50', 24),
(116, 'browse_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(117, 'read_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(118, 'edit_about', 'about', '2019-08-14 00:16:41', '2019-08-14 00:16:41', 25),
(119, 'add_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(120, 'delete_about', 'about', '2019-08-14 00:16:42', '2019-08-14 00:16:42', 25),
(121, 'browse_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(122, 'read_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(123, 'edit_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(124, 'add_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(125, 'delete_contact', 'contact', '2019-08-14 05:56:09', '2019-08-14 05:56:09', 26),
(126, 'browse_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(127, 'read_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(128, 'edit_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(129, 'add_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(130, 'delete_slider', 'slider', '2019-08-14 07:51:16', '2019-08-14 07:51:16', 27),
(131, 'browse_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(132, 'read_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(133, 'edit_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(134, 'add_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28),
(135, 'delete_facility', 'facility', '2019-08-20 04:26:05', '2019-08-20 04:26:05', 28);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(16, 3),
(17, 1),
(17, 2),
(17, 3),
(18, 1),
(18, 2),
(18, 3),
(19, 1),
(19, 2),
(19, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 1),
(21, 2),
(21, 3),
(22, 1),
(22, 2),
(22, 3),
(23, 1),
(23, 2),
(23, 3),
(24, 1),
(24, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(31, 3),
(32, 1),
(32, 2),
(32, 3),
(33, 1),
(33, 2),
(33, 3),
(34, 1),
(34, 2),
(34, 3),
(35, 1),
(35, 2),
(35, 3),
(36, 1),
(36, 2),
(36, 3),
(37, 1),
(37, 2),
(37, 3),
(38, 1),
(38, 2),
(38, 3),
(39, 1),
(39, 2),
(39, 3),
(40, 1),
(40, 2),
(40, 3),
(41, 1),
(41, 2),
(41, 3),
(42, 1),
(42, 2),
(42, 3),
(43, 1),
(43, 2),
(43, 3),
(44, 1),
(44, 2),
(44, 3),
(45, 1),
(45, 2),
(45, 3),
(46, 1),
(46, 2),
(46, 3),
(47, 1),
(47, 2),
(47, 3),
(48, 1),
(48, 2),
(48, 3),
(49, 1),
(49, 2),
(49, 3),
(50, 1),
(50, 2),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `image` varchar(191) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `image`, `created_at`, `updated_at`) VALUES
(6, '2020/2/158183761724905.jpg', '2020-02-16 07:20:17', '2020-02-16 07:20:17'),
(7, '2020/2/158183761724512.jpg', '2020-02-16 07:20:17', '2020-02-16 07:20:17'),
(8, '2020/2/158183761725101.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(9, '2020/2/158183761725501-4.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(10, '2020/2/158183761725801-3.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(11, '2020/2/158183761726101-2.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(12, '2020/2/158183761726301-10.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(13, '2020/2/158183761726601-9.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(14, '2020/2/158183761727101-8.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(15, '2020/2/158183761727401-7.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(16, '2020/2/158183761727701-6.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18'),
(17, '2020/2/158183761727901-1.jpg', '2020-02-16 07:20:18', '2020-02-16 07:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'System Administrator', '2018-02-07 06:39:33', '2018-09-12 06:01:57'),
(2, 'user', 'Normal User', '2018-02-07 06:39:33', '2018-09-12 04:31:00'),
(3, 'sub_admin', 'Sub admin', '2018-03-28 01:38:44', '2018-03-28 01:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biology', '2019-08-06 04:42:18', '2019-08-06 04:42:18'),
(2, 'computer science', '2019-08-06 04:47:06', '2019-08-06 04:47:06'),
(3, 'humanities', '2019-08-06 04:47:15', '2019-08-06 04:47:15'),
(5, 'any', '2019-08-12 09:36:28', '2019-08-12 09:36:28'),
(6, 'commerce', '2019-08-12 09:36:43', '2019-08-12 09:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `school_activitys`
--

CREATE TABLE `school_activitys` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `main_image` varchar(500) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_activitys`
--

INSERT INTO `school_activitys` (`id`, `title`, `slug`, `main_image`, `status`, `created_at`, `updated_at`) VALUES
(17, 'bloom', 'activity-2', '2020/2/7.png', 1, '2020-02-16 07:55:37', '2020-02-16 02:25:37'),
(19, 'kaff', 'kaff', '2020/2/4-1.png', 1, '2020-02-16 02:26:51', '2020-02-16 02:26:51'),
(18, 'virgo', 'activity-2', '2020/2/6.png', 1, '2020-02-16 07:56:00', '2020-02-16 02:26:00'),
(16, 'centuryLminates', 'activity-1', '2020/2/5.png', 1, '2020-02-16 07:56:27', '2020-02-16 02:26:27'),
(20, 'ebco', 'ebco', '2020/2/3.png', 1, '2020-02-16 02:27:09', '2020-02-16 02:27:09'),
(21, 'faber', 'faber', '2020/2/2.png', 1, '2020-02-16 02:27:20', '2020-02-16 02:27:20');

-- --------------------------------------------------------

--
-- Table structure for table `school_students`
--

CREATE TABLE `school_students` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `image` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_students`
--

INSERT INTO `school_students` (`id`, `name`, `status`, `image`, `created_at`, `updated_at`) VALUES
(20, 'Navya Nair', 1, '2019/11/navya-nair.jpg', '2019-11-17 16:55:17', '2019-11-17 16:55:17'),
(21, 'Dr. Chithra', 1, '2019/11/chithra1.jpg', '2019-11-17 16:59:51', '2019-11-17 16:59:51'),
(22, 'Babitha Jayan', 1, '2019/11/babitha.jpg', '2019-11-17 17:00:03', '2019-11-17 17:00:03');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(16, 'bethany balikamandhom', '2020/2/image-2.jpg', '2019-11-17 16:20:11', '2020-02-16 06:50:50'),
(18, 'tests', '2020/2/image-1.jpg', '2020-01-29 16:31:11', '2020-02-16 06:50:43'),
(19, 'third', '2020/2/image-3.jpg', '2020-01-29 16:31:26', '2020-02-16 06:50:34'),
(20, 'dcsds', '2020/3/screenshot-from-2020-02-27-20-48-47.png', '2020-03-06 17:31:15', '2020-03-06 17:31:15');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `name`, `created_at`, `updated_at`) VALUES
(24, 'Mathematics', '2019-08-15 05:49:56', '2019-08-15 05:49:56'),
(20, 'physcics', '2019-08-09 05:53:37', '2019-08-09 05:53:37'),
(21, 'Chemistry', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(22, 'Geology', '2019-08-15 05:47:35', '2019-08-15 05:47:35'),
(23, 'Biology', '2019-08-15 05:47:35', '2019-08-15 05:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `tab_heading`
--

CREATE TABLE `tab_heading` (
  `id` int(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `status` tinyint(6) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tab_heading`
--

INSERT INTO `tab_heading` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(3, 'Trip', 'trip', 1, '2019-03-08', '2019-03-08'),
(4, 'all', 'all', 1, '2019-03-08', '2019-03-08'),
(5, 'Office Memories', 'office', 1, '2019-03-12', '2019-03-14'),
(6, 'Guest Memories', 'guest-memories', 1, '2019-03-14', '2019-03-14');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `content`, `image`, `created_at`, `updated_at`) VALUES
(4, 'Nevin John', '\" Sreevalsam Interiors took the time to understand my requirements, helped to recommend and manage all the deals wanted, returned all of my calls and communication promptly and did a wonderful work of furnishing our space. It’s contemporary, warm and a great consign to “come home to. Greatly Recommended..\"', '2020/2/author-1s.jpg', '2020-01-25 10:39:53', '2020-02-16 07:22:34'),
(5, 'Vishnu', '\" I would really like to thank you for an amazing job. You have done a really good job and thanks for incorporating the changes as and when required. It was pleasure knowing and working with you. If there is anything in future we will definitely bug you. Take care and all the best for everything.\"', '2020/2/author-1ss.jpg', '2020-02-16 07:23:30', '2020-02-16 07:23:30'),
(6, 'ICSM International', '\"As we are in need of an educational and life-skill training centre which confirms with our vision, Sreevalsam Interiors offer as a perfect plan and executed the inner atmosphere in a way which is spell-bounding. Our Kochi Campus is one of the best due to Sreevalsam Interiors. Thanks.\"', '2020/2/author-1sss.png', '2020-02-16 07:24:14', '2020-02-16 07:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `temp_email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `temp_phone` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `login_otp` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `activation_token` varchar(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `otp_status` tinyint(2) NOT NULL DEFAULT '0',
  `token` varchar(60) DEFAULT NULL,
  `blocked` tinyint(2) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `temp_email`, `password`, `phone`, `temp_phone`, `otp`, `login_otp`, `role_id`, `avatar`, `remember_token`, `activation_token`, `active`, `otp_status`, `token`, `blocked`, `status`, `last_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'System', 'admin@admin.com', NULL, '$2y$10$j9iaCUOR3tQHAnOlh2TKquxESTmhCITukHeGQR6dnyHacKxBr1YS.', '94247760947', '+96698989898989', 4799, NULL, 1, '2018/8/screenshot-iytimgcom-2016-12-02-12-07-35.png', '3OsrBXb5tRhcfq1F51HON0KaixirDmOBRYRvlQhRZ7qwycuRp0kx1Uz3L72C', NULL, 1, 1, '', 0, 'active', '2020-02-16 06:20:35', '2018-03-23 06:50:19', '2020-02-16 00:50:35', '0000-00-00 00:00:00'),
(2, 'Vasanthan', 'R K', 'vasanthan.rk@acodez.co.in', NULL, '$2y$10$/2RMCfB7LDOmXrFPwtbwduntpqYtfDaY3qqUekozhdpBpUQWyz5/m', '15454545', NULL, NULL, NULL, 3, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-29 00:44:20', '2019-10-24 08:07:25', NULL),
(3, 'Nandul', 'Das', 'nandul.das@acodez.co.in', NULL, '$2y$10$yElsIKQuA3i6TQuCW6OTyOd0F3S4KihLqepjXbw7masvm21UhKuyy', '123456789', NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, 0, NULL, 0, 'inactive', NULL, '2018-08-30 05:54:53', '2019-10-24 08:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit`
--

INSERT INTO `visit` (`id`, `name`, `address`, `phone`, `email`, `reason`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 'subina', 'kandoth (h)\r\nperinkary(po)\r\nkannur', '9061395796', 'subinakandoth@gmail.com', 'Admission', '2019-07-10', '03:25:00', '2019-07-29 18:30:00', '2019-07-16 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `vission`
--

CREATE TABLE `vission` (
  `id` int(11) NOT NULL,
  `vission` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vission`
--

INSERT INTO `vission` (`id`, `vission`, `created_at`, `updated_at`) VALUES
(1, '<p>ghhsasa</p>', '2019-08-15 04:26:57', '2019-08-15 04:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `whychoose`
--

CREATE TABLE `whychoose` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `message` longtext NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `whychoose`
--

INSERT INTO `whychoose` (`id`, `title`, `message`, `image`, `created_at`, `updated_at`) VALUES
(7, 'test 2', 'description', '2020/2/sreevalsm.jpg', '2020-01-29 16:34:44', '2020-02-16 06:51:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_modules`
--
ALTER TABLE `admin_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eligibility`
--
ALTER TABLE `eligibility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_gallery`
--
ALTER TABLE `multiple_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages_media`
--
ALTER TABLE `packages_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_activitys`
--
ALTER TABLE `school_activitys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_students`
--
ALTER TABLE `school_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tab_heading`
--
ALTER TABLE `tab_heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vission`
--
ALTER TABLE `vission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whychoose`
--
ALTER TABLE `whychoose`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `admin_modules`
--
ALTER TABLE `admin_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `eligibility`
--
ALTER TABLE `eligibility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `multiple_gallery`
--
ALTER TABLE `multiple_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `packages_media`
--
ALTER TABLE `packages_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `school_activitys`
--
ALTER TABLE `school_activitys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `school_students`
--
ALTER TABLE `school_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tab_heading`
--
ALTER TABLE `tab_heading`
  MODIFY `id` int(191) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visit`
--
ALTER TABLE `visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vission`
--
ALTER TABLE `vission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `whychoose`
--
ALTER TABLE `whychoose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
