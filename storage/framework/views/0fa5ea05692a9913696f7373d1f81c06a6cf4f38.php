
<?php $__env->startSection('content'); ?>
   <!-- Breadcrumbs Start -->
   <div class="rs-breadcrumbs">
            <img src="<?php echo e(asset('website/images/bg-box.jpg')); ?>" alt="Breadcrumbs Image">
            <div class="container">
                <div class="breadcrumbs-content">
                    <h1 class="title">Our Gallery</h1>
                    <div class="page-path text-center">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Our Gallery</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
		<!-- Breadcrumbs End -->

        <!-- Services Start -->
        <div class="rs-gallery-section pt-90 pb-70">
            <div class="container">

                <div class="row grid">
                <?php if($gallery): ?>
                <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galleries): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter1 filter3">
                        <div class="single-gallery style-2">
                            <img src="<?php echo e(getImageByPath($galleries->main_image,'459x266.67','album')); ?>" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="<?php echo e(getImageByPath($galleries->main_image,'325x319','album')); ?>" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    <!-- <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter3">
                        <div class="single-gallery style-2">
                            <img src="<?php echo e(asset('website/images/gallery/img/2.jpg')); ?>" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="<?php echo e(asset('website/images/gallery/img/2.jpg')); ?>" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 mb-30 grid-item filter2">
                        <div class="single-gallery style-2">
                            <img src="<?php echo e(asset('website/images/gallery/img/3.jpg')); ?>" alt="Gallery Image" />
                            <div class="popup-icon">
                                <a class="image-popup" href="<?php echo e(asset('website/images/gallery/img/3.jpg')); ?>" title="Photo Title Here">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
       -->
        
                </div>
            </div>
        </div>
        <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>