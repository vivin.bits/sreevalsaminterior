<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0; maximum-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
        <title>Ayyappa Institute of Management Studies</title>
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/assets/bootstrap/css/bootstrap.min.css')); ?>">
        <!-- Reset CSS -->
        <link href="<?php echo e(asset('website/css/reset.css')); ?>" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="<?php echo e(asset('website/css/fonts.css')); ?>" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo e(asset('website/assets/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <!-- Select2 -->
        <link href="<?php echo e(asset('website/assets/select2/css/select2.min.css')); ?>" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo e(asset('website/assets/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
        <!-- Magnific Popup -->
        <link href="<?php echo e(asset('website/assets/magnific-popup/css/magnific-popup.css')); ?>" rel="stylesheet">
        <!-- Iconmoon -->
        <link href="<?php echo e(asset('website/assets/iconmoon/css/iconmoon.css')); ?>" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="<?php echo e(asset('website/assets/owl-carousel/css/owl.carousel.min.css')); ?>" rel="stylesheet">

   
        <!-- Animate -->
        <link href="<?php echo e(asset('website/css/animate.css')); ?>" rel="stylesheet">
        <!-- Custom Style -->
        <link href="<?php echo e(asset('website/css/custom.css')); ?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

             


               <!-- Start Preloader -->
        <div id="loading">
            <div class="element">
                <div class="sk-folding-cube">
                    <div class="sk-cube1 sk-cube"></div>
                    <div class="sk-cube2 sk-cube"></div>
                    <div class="sk-cube4 sk-cube"></div>
                    <div class="sk-cube3 sk-cube"></div>
                </div>
            </div>
        </div>
        <!-- End Preloader -->

        <!-- Start Header -->
        <header> 
            <!-- Start Header top Bar -->
            <div class="header-top">
                <div class="container clearfix">

                    <ul class="follow-us hidden-xs">
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>

             


                </div>
            </div>
            <!-- End Header top Bar --> 
            <!-- Start Header Middle -->
            <div class="container header-middle">
                <div class="row"> <span class="col-xs-6 col-sm-3"><a href="index.html"><img src="<?php echo e(asset('website/images/logo.png')); ?>" class="img-responsive" alt=""></a></span>
                    <div class="col-xs-6 col-sm-3"></div>
                    <div class="col-xs-6 col-sm-9">
                        <div class="contact clearfix">
                            <ul class="hidden-xs">
                                <li> <span>Email</span> <a href="mailto:info@ayyappacollege.org"><?php echo e($contact->email); ?></a> </li>
                                <li> <span>Call Us</span> <?php echo $contact->phone; ?></li>
                            </ul>
                         </div>
                    </div>
                </div>
            </div>
            <!-- End Header Middle --> 
            <!-- Start Navigation -->
              <!-- Start Navigation -->
              <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                  
                        <ul class="nav navbar-nav">
                            <li> <a href="<?php echo e(url('/')); ?>">Home</a></li>

                      
                             
                            <li class="dropdown"> <a data-toggle="dropdown" href="<?php echo e(url('about')); ?>">About <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu">
                               
                                  <li><a href="<?php echo e(url('message')); ?>"> Chairman's Message</a></li>
                                  <li><a href="<?php echo e(url('mission')); ?>">Vision & Mission </a></li>
                                   
                                </ul>
                            </li>
                   
                                 
                            <li class="dropdown"> <a data-toggle="dropdown" href="<?php echo e(URL('courses')); ?>">Our Courses <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu">
                                    <!-- <li><a href="course-listing.html">course Listing</a></li>
                                  
                                    <li><a href="mba-general.html">MBA General</a></li>
                                    <li><a href="mba-operations.html">MBA Operations</a></li> -->
                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <li><a href="<?php echo e(url('course')); ?>/<?php echo e($key); ?>"><?php echo e($value); ?></a></li>

                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </li>
                            <li> <a href="<?php echo e(url('facility')); ?>"> Facilities</a></li>
                            <li> <a href="<?php echo e(url('gallery')); ?>">Gallery</a></li>
                           
                            <li> <a href="<?php echo e(url('career')); ?>">Career</a></li>
                            <li> <a href="<?php echo e(url('application')); ?>">Apply Now</a></li>
                            



                            <li> <a href="<?php echo e(url('contact')); ?>">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navigation --> 
         
            </header>
        <!-- End Header --> 


<?php echo $__env->yieldContent('content'); ?>
        

        




            <!-- Start Footer Bottom -->
            <div class="bottom">
            
            </div>
            <!-- End Footer Bottom --> 
        </footer>
        <!-- End Footer --> 

        <!-- Scroll to top --> 
        <a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> 

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="<?php echo e(asset('website/js/jquery.min.js')); ?>"></script> 
        <script src="<?php echo e(asset('website/js/jquery.vticker.min.js')); ?>"  type="text/javascript"></script>
        <!-- Bootsrap JS --> 
        <script src="<?php echo e(asset('website/assets/bootstrap/js/bootstrap.min.js')); ?>"></script> 
        <!-- Select2 JS --> 
        <script src="<?php echo e(asset('website/assets/select2/js/select2.min.js')); ?>"></script> 
        <!-- Match Height JS --> 
        <script src="<?php echo e(asset('website/assets/matchHeight/js/matchHeight-min.js')); ?>"></script> 
        <!-- Bxslider JS --> 
        <script src="<?php echo e(asset('website/assets/bxslider/js/bxslider.min.js')); ?>"></script> 
        <!-- Waypoints JS --> 
        <script src="<?php echo e(asset('website/assets/waypoints/js/waypoints.min.js')); ?>"></script> 
        <!-- Counter Up JS --> 
        <script src="<?php echo e(asset('website/assets/counterup/js/counterup.min.js')); ?>"></script> 
        <!-- Magnific Popup JS --> 
        <script src="<?php echo e(asset('website/assets/magnific-popup/js/magnific-popup.min.js')); ?>"></script> 
        <!-- Owl Carousal JS --> 
        <script src="<?php echo e(asset('website/assets/owl-carousel/js/owl.carousel.min.js')); ?>"></script> 

        <!-- Modernizr JS --> 
        <script src="<?php echo e(asset('website/js/modernizr.custom.js')); ?>"></script> 
        <!-- Custom JS --> 
        <script src="<?php echo e(asset('website/js/custom.js')); ?>"></script>

    
<script>
$(document).ready(function() {
 
    $("#main-slider").owlCarousel({
 
 navigation : true, // Show next and prev buttons



 animateOut: 'slideOutDown',
    animateIn: 'flipInX',
   slideSpeed: 600,
   navigation : true,
 items : 1, 
 loop: true,
 autoplay: true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: true
 

});

});


</script>




        <script type="text/javascript">


            $(function() {
          $('#newstick').vTicker();
        });
        
        </script>
    </body>
</html>