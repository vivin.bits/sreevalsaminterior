

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main_content'); ?>

<section class="content-header">
    <h1>
        NEWS 

    </h1>
  
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">
<?php if(can('add_module')): ?>
                    <a class="btn btn-primary pull-right" href="<?php echo e(url('news/create')); ?>" >Create</a>
                      <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">

    <?php if(can('browse_module')): ?>
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Description</th>

                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                     <?php else: ?>
        <?php echo $__env->make('admin.no-access-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   
<?php $__env->stopSection(); ?>

    <?php $__env->startSection('js'); ?>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery.dataTables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/dataTables.bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('admin/js/custom/news.js')); ?>"></script>
    <script src="<?php echo e(asset('admin/js/custom/delete.js')); ?>"></script>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>