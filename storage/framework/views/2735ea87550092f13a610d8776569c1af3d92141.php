<!doctype html>
<html lang="en">
<head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>Bethany Balikamadhom</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <?php echo $__env->yieldContent('css'); ?>
        <link rel="shortcut icon" type="<?php echo e(asset('website/image/x-icon')); ?>" href="<?php echo e(asset('website/images/fav.png')); ?>">
        <!-- bootstrap v4 css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/bootstrap.min.css')); ?>">
        <!-- Hover CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/hover-min.css')); ?>">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/font-awesome.min.css')); ?>">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/animate.css')); ?>">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/owl.carousel.css')); ?>">
		<!-- slick css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/slick.css')); ?>">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/magnific-popup.css')); ?>">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/fonts/flaticon.css')); ?>">
        <!-- rsmenu CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/rsmenu-main.css')); ?>">
        <!-- rsmenu transitions CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/rsmenu-transitions.css')); ?>">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/style.css')); ?>">
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('website/css/responsive.css')); ?>">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    
    </head>
    <body>

        <!-- Start Preloader -->
        <div id="loading">
            <div class="element">
                <div class="sk-folding-cube">
                    <div class="sk-cube1 sk-cube"></div>
                    <div class="sk-cube2 sk-cube"></div>
                    <div class="sk-cube4 sk-cube"></div>
                    <div class="sk-cube3 sk-cube"></div>
                </div>
            </div>
        </div>
        <!-- End Preloader -->
 <!--Full width header Start-->
 <div class="full-width-header">
        <!-- Start Header -->
        <header id="rs-header" class="rs-header style-1">
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-12">
                                <div class="logo-area">
                                    <a href="index.html"><img src="<?php echo e(asset('website/images/logo.png')); ?>" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-12">
                                <div class="main-menu clearfix">
                                    <a class="rs-menu-toggle"><i class="fa fa-bars"></i>Menu</a>
                                    <nav class="rs-menu">
                                        <ul class="nav-menu">
                                            <!-- Home -->
                                            <li class="current-menu-item current_page_item menu-item-has-children"> <a href="<?php echo e(url('/')); ?>">Home</a>
                                               
                                            </li>
                                            <!-- End Home --> 

                                            <!--About Start-->
                                            <li><a href="<?php echo e(url('about')); ?>">About Us</a></li>
                                            <!--About End-->

                                            <!--Teacher Menu Start -->
                                            <li class="menu-item-has-children"> <a href="<?php echo e(url('allFacility')); ?>">FACILITIES</a>
                                            
                                            </li>

                                             <!--Teacher Menu Start -->
                                            <li class="menu-item-has-children"> <a href="<?php echo e(url('gallery')); ?>">GALLERY</a>
                                            
                                            </li>

                                                  <li><a href="<?php echo e(url('activity')); ?>">ACTIVITIES
</a></li>
                                            <!--Teacher Menu End -->

                                         
                                            <!--Class Menu End -->

                            

                                     

                                            <!--Blog Menu End-->

                                            <!--Contact Menu Start-->
                                            <li><a href="<?php echo e(url('contact')); ?>">Contact</a></li>
                                            <!--Contact Menu End-->
                                        </ul> <!-- //.nav-menu -->
                                    </nav>
                                </div> <!-- //.main-menu -->
                                <div class="right-bar-icon text-right">
                                    <a class="rs-search" data-target=".search-modal" data-toggle="modal" href="#"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                <!-- Menu End -->
            </header>
          <!-- Header End -->
       
  <!-- Toolbar Start -->

<?php echo $__env->yieldContent('content'); ?>
        

        






   <!-- Footer Start -->
   <footer id="rs-footer" class="rs-footer footer-2 footer-bg2  pt-100">
			<!-- Footer Top -->
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-12">
                            <div class="footer-contact">
                                <div class="footer-logo-part">
                                    <img src="<?php echo e(asset('website/images/footer-logo.png')); ?>" alt="Footer Logo">
                                </div>
                                <div class="footer-contact-part">
                                    <div class="contact-item mb-15">
                                        <div class="icon-part kinder-pulm">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="address-part">
                                            <span>Mavelikara Road, Nangiarkulangara, Kerala 690513</span>
                                        </div>
                                    </div>
                                    <div class="contact-item mb-15">
                                        <div class="icon-part rotate-icon kinder-orange">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="number-part">
                                            <a href="tel+04792410750">+0479 241 0750</a><br>
                                      
                                        </div>
                                    </div>
                                    <div class="contact-item">
                                        <div class="icon-part kinder-blue">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="email-part">
                                            <a href="mailto:infoname@gmail.com">35046alapuzha@gmail.com</a><br>
                                            <a href="#">www.bethanybluebells.org</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-12 mb-md-30">
                            <h4 class="footer-title">Useful Links</h4>
                            <ul class="sitemap-widget clearfix">
                                <li class="active"><a href="index.html"><i class="fa fa-angle-right"></i>Home</a></li>
                                <li ><a href=""><i class="fa fa-angle-right"></i>About</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Mission</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Vision</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>Facilities</a></li>
                                <li><a href=""><i class="fa fa-angle-right"></i>News & Events</a></li>                                

                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-12 mb-md-30">
                            <h4 class="footer-title">Google Map</h4>
                            <div class="recent-post-widget">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15751.323416431123!2d76.46604!3d9.2594054!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1e801fd21c858dfe!2sBethany+Balikamadhom+Higher+Secondary+School!5e0!3m2!1sen!2sin!4v1564673612649!5m2!1sen!2sin" width="300" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-md-12">
                            <h4 class="footer-title">Social Media</h4>
                            <div class="newsletter-text">
                           
                             
                                <div class="footer-share">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>    
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div>                                
                </div>
            </div>

            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="container">
                    <div class="copyright">
                        <p>© 2019 <a href="#">Bethany Balikamadhom</a>. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->

        <!-- start scrollUp  -->
        <div id="scrollUp">
            <i class="fa fa-rocket"></i>
        </div>
        
        <!-- Search Modal Start -->
        <div aria-hidden="true" class="modal fade search-modal" role="dialog" tabindex="-1">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true" class="fa fa-close"></span>
	        </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here.." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll to top --> 
        <a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> 
        <?php echo $__env->yieldContent('js'); ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="<?php echo e(asset('website/js/modernizr-2.8.3.min.js')); ?>"></script>
        <!-- jquery latest version -->
        <script src="<?php echo e(asset('website/js/jquery.min.js')); ?>"></script>
        <!-- bootstrap js -->
        <script src="<?php echo e(asset('website/js/bootstrap.min.js')); ?>"></script>
        <!-- owl.carousel js -->
        <script src="<?php echo e(asset('website/js/owl.carousel.min.js')); ?>"></script>
		<!-- slick.min js -->
        <script src="<?php echo e(asset('website/js/slick.min.js')); ?>"></script>
        <!-- isotope.pkgd.min js -->
        <script src="<?php echo e(asset('website/js/isotope.pkgd.min.js')); ?>"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="<?php echo e(asset('website/js/imagesloaded.pkgd.min.js')); ?>"></script>
        <!-- wow js -->
        <script src="<?php echo e(asset('website/js/wow.min.js')); ?>"></script>
        <!-- counter top js -->
        <script src="<?php echo e(asset('website/js/waypoints.min.js')); ?>"></script>
        <script src="<?php echo e(asset('website/js/jquery.counterup.min.js')); ?>"></script>
        <!-- magnific popup -->
        <script src="<?php echo e(asset('website/js/jquery.magnific-popup.min.js')); ?>"></script>
        <!-- Water Ripples Effect -->
        <script src="<?php echo e(asset('website/js/jquery.ripples-min.js')); ?>"></script>
        <!-- rsmenu js -->
        <script src="<?php echo e(asset('website/js/rsmenu-main.js')); ?>"></script>
        <!-- plugins js -->
        <script src="<?php echo e(asset('website/js/plugins.js')); ?>"></script>
		 <!-- main js -->
        <script src="<?php echo e(asset('website/js/main.js')); ?>"></script>


  <script src="<?php echo e(asset('website/js/jquery.vticker.min.js')); ?>"  type="text/javascript"></script>
<script type="text/javascript">
    $(function() {
  $('#newstick').vTicker();
});

</script>

    
    </body>
</html>