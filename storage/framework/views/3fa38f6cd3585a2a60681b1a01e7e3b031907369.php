<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="<?php echo Session::token(); ?>" name="csrf-token">
  <meta name="csrf_token" content="<?php echo e(csrf_token()); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!--     Fonts and icons     -->
<!--    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin/css/main.css')); ?>">-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin/css/styles.css')); ?>">
  <link rel="stylesheet" href="<?php echo asset('admin/css/front.css'); ?>">
  <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo asset('admin/bower_components/font-awesome/css/font-awesome.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo asset('admin/plugins/datatables/dataTables.bootstrap.css'); ?>">
  <link rel="stylesheet" href="<?php echo asset('admin/css/pretty-checkbox.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo asset('admin/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo asset('admin/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo asset('admin/dist/css/skins/_all-skins.min.css'); ?>">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/morris.js/morris.css'); ?>">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/jvectormap/jquery-jvectormap.css'); ?>">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css'); ?>">
   <link rel="stylesheet" href="<?php echo asset('admin/css/iziToast.min.css'); ?>">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?php echo asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="<?php echo e(asset('admin/css/alertify.css')); ?>" >


   <!-- Favicon-->
   <link rel="shortcut icon" href="<?php echo asset('img/favicon.ico'); ?>">
<style type="text/css">
  .leftpad{
    padding-left: 0;
  }
   .rightpad{
    padding-right: 0;
  }
</style>

   <?php echo $__env->yieldContent('additional_styles'); ?>

 </head>
 <body class="hold-transition skin-blue sidebar-mini" style="background-color: #222d32;">
  <?php
  $user = \App\User::where('role_id',1)->first();
  ?>
  <header class="main-header">
    <!-- Logo -->
    <a  class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>L</span>
      <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b> Sreevalsam Interiors</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

      <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

          <img src="<?php echo e(asset('media/profile-images/'.$user->avatar)); ?>" class="user-image" alt="San Fran">
          <span class="hidden-xs"><?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
            <img src="<?php echo e(asset('media/profile-images/'.$user->avatar)); ?>" class="img-circle" alt="User Image">

            <p>
              <?php echo e($user->firstname); ?> <?php echo e($user->lastname); ?>

              <small>Administrator</small>
            </p>
          </li>
          <!-- Menu Body -->

          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
             <a href="<?php echo url('admin/profile'); ?>" class="btn btn-default btn-flat">Profile</a>
           </div>
           <div class="pull-right">
            <a href="<?php echo url('admin/change-password'); ?>" class="btn btn-default btn-flat">Change Password</a>
          </div>
        </li>
      </ul>
    </li>

    <li class="dropdown tasks-menu">
      <a href="<?php echo url('admin/logout'); ?>">Logout <i class="fa fa-sign-out" aria-hidden="true"></i></a>


    </a>

  </li>
  <!-- Control Sidebar Toggle Button -->

</ul>
</div>
</nav>
</header>
<!-- Side Navbar -->

 <?php echo $__env->make('admin.layout.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 <div class="content-wrapper">
  <?php echo $__env->yieldContent('main_content'); ?>
</div>

<!-- JavaScript files-->

<script>
  var base_url = '<?php echo url('/'); ?>/';
  var admin_base_url = '<?php echo url('/'); ?>/';
</script>


<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="<?php echo e(asset('admin/bootstrap/js/bootstrap.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(asset('admin/bower_components/moment/moment.js')); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(asset('admin/bower_components/raphael/raphael.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/bower_components/morris.js/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(asset('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
   <script src="<?php echo e(asset('admin/js/alertify.js')); ?>"></script>
<!-- daterangepicker -->

<script src="<?php echo e(asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(asset('admin/bower_components/fastclick/lib/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(asset('admin/dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->



<script src="<?php echo e(asset('admin/js/jquery.dataTables.js')); ?>"></script>

<!--<script src="<?php echo e(asset('admin/js/plugins/datatables/jquery.dataTables.min.js')); ?>"></script>-->
<!--<script src="<?php echo e(asset('admin/js/plugins/datatables/dataTables.bootstrap.min.js')); ?>"></script>-->
<script src="<?php echo e(asset('admin/js/mark.js(jquery.mark.min.js)')); ?>"></script>
<script src="<?php echo e(asset('admin/js/datatables.mark.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/iziToast.min.js')); ?>"></script>
      <script src="<?php echo e(asset('admin/js/plugins/sweetalert.min.js')); ?>"></script>
      <script src="<?php echo e(asset('admin/js/plugins/alertify.min.js')); ?>"></script>


      <script>
          var base_url = '<?php echo e(url('admin/dashboard')); ?>';

      </script>
      <script>
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
      </script>


      <?php echo $__env->yieldContent('js'); ?>

</body>
</html>
