<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin/redactor/redactor/redactor.css')); ?>" />

<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
            Why Choose

        </h1>
 
    </section>

<section class="content">
        <div class="row">
            <div class="col-xs-12">


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                        <a href="<?php echo URL::to('admin/whyChoose'); ?>" class="btn btn-primary pull-right">Back </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">



            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>

           <?php endif; ?>
           <div class="col-md-10 p-left-0">

                    <form action="<?php echo e(url('admin/whychoose/create/')); ?>" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <div class="box-body">

                            <label for="title">Title</label>
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="<?php echo old('title'); ?>">
                                <?php if($errors->first('title')): ?>
                                <span class="error"><?php echo e($errors->first('title')); ?></span>
                                <?php endif; ?>

                            </div>

                            <div>

                                <div class="form-group <?php if($errors->first('content')): ?> has-danger <?php endif; ?>">
                                    <label for="description">Message</label>
                                    <textarea class="form-control <?php if($errors->first('message')): ?> is-invalid <?php endif; ?>" id="message" name="message"
                                              placeholder="Enter Message" autocomplete="off" value="<?php echo e(old('message')); ?>" maxlength="3000"
                                              rows="10" style="height: 100px;"></textarea>
                                    <?php if($errors->first('content')): ?>
                                    <label class="error"><?php echo e($errors->first('message')); ?></label>
                                    <?php endif; ?>
                                </div>

                            <div  class="form-group <?php if($errors->first('image')): ?> has-danger <?php endif; ?>">

                            <label  for="image">Image</label><br>

                            <input   id="image" onchange="PreviewImage();"  name="image"  type="file" >

                            <?php if($errors->first('image')): ?><span  class="alert-danger"><?php echo e($errors->first('image')); ?></span>
                             <?php endif; ?>
                           
                            <div>
<br>
                            <div class="main-image" style="display: none;">
                                                <img id="uploadPreview" style="width: 200px; height: 200px; " />
                                            </div>

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/redactor.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/filemanager/filemanager.js')); ?>"></script> 
<script src="<?php echo e(asset('admin/js/additional-methods.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/imagemanager/imagemanager.js')); ?>"></script> 

<script src="<?php echo e(asset('redactor/langs/ar.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/jquery-migrate-3.0.0.min.js')); ?>"></script>


<!-- place in header of your html document -->
<script>

 

$(document).ready(function () {
$R('.content', {
    alert('ga');
                maxHeight:"200px",
                minHeight:"200px",
                plugins: ['fontsize','imagemanager'],
                imageUpload: '<?php echo asset('admin/redactor/demo/scripts/image_upload.php'); ?>',
                imageResizable: true,
                imagePosition: true
            });
});


function PreviewImage() {
   
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("image").files[0]);

    oFReader.onload = function (oFREvent) {
        $(".main-image").show();
        document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
};
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>