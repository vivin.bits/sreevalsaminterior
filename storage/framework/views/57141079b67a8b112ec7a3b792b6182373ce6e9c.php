<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('admin/redactor/redactor/redactor.css')); ?>" />

<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
            Product Create
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Photo Category </h3>
                        <a href="<?php echo URL::to('packagess'); ?>" class="btn btn-primary pull-right">Back</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>

           <?php endif; ?>
           <div class="col-md-10 p-left-0">
              
                    <form action="<?php echo URL::to('package/create'); ?>" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <div class="box-body">
                            
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Title</label>
                                <input type="text" name="title" id="name" class="form-control" placeholder="Enter Title" value="<?php echo old('category'); ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>

                            <div class="form-group <?php if($errors->first('slug')): ?> has-danger <?php endif; ?>">
                                <label for="slug">Slug</label>
                                <input class="form-control <?php if($errors->first('slug')): ?> is-invalid <?php endif; ?>" id="slug" name="slug" type="text" placeholder="Enter slug"  value="<?php echo e(old('slug')); ?>">
                                <?php if($errors->first('slug')): ?>
                                    <label class="error"><?php echo e($errors->first('slug')); ?></label>
                                <?php endif; ?>
                            </div>     
                     
                            <!-- <div class="form-group <?php if($errors->first('description')): ?> has-danger <?php endif; ?>">
                                            <label for="title"> Desciption</label>
                                            <textarea name="description" id="description" cols="30" rows="10" maxlength ="40" ><?php echo e(old('description')); ?></textarea>
                                            <?php if($errors->first('description')): ?>
                                                <span class="form-control-feedback"><?php echo e($errors->first('description')); ?></span>
                                            <?php endif; ?>
                                        </div> -->
                                        <div class="form-group email_temp <?php if($errors->first('content')): ?> has-error <?php endif; ?>">
                                            <label for="description">Description</label>
                                        <textarea class="form-control <?php if($errors->first('message')): ?> is-invalid <?php endif; ?>" id="message" name="description"
                                                  placeholder="Enter description" autocomplete="off" value="<?php echo e(old('message')); ?>" 
                                                  rows="10" style="height: 100px;"></textarea>
                                        </div>
                            <div  class="form-group <?php if($errors->first('image')): ?> has-danger <?php endif; ?>">

                            <label  for="image">Image</label><br>

                            <input   id="image"  name="image"  type="file" >

                            <?php if($errors->first('image')): ?><span  class="alert-danger"><?php echo e($errors->first('image')); ?></span>
                             <?php endif; ?>
                           
                            <div>
                         
                </div>

    

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/redactor/redactor/redactor.js')); ?>"></script>
    <script src="<?php echo e(asset('admin/redactor/plugins/fontsize/fontsize.js')); ?>"></script>
    <script src="<?php echo e(asset('admin/redactor/plugins/imagemanager/imagemanager.js')); ?>"></script>
 <script src="<?php echo e(asset('admin/js/custom/slug.js')); ?>"></script> 
<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '<?php echo asset('redactor/scripts/file_upload.php'); ?>',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '<?php echo asset('redactor/scripts/image_upload.php'); ?>',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});


$R('#description', {
                maxHeight:"400px",
                minHeight:"400px",
                plugins: ['fontsize','imagemanager'],
                imageUpload: '<?php echo asset('admin/redactor/demo/scripts/image_upload.php'); ?>',
                imageResizable: true,
                imagePosition: true
            });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>