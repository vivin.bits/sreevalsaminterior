
<?php $__env->startSection('content'); ?>
<div class="rs-breadcrumbs">
            <img src="<?php echo e(asset('website/images/bg-box.jpg')); ?>" alt="Breadcrumbs Image">
            <div class="container">
                <div class="breadcrumbs-content">
                    <h1 class="title">Our Activities</h1>
                    <div class="page-path text-center">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li>Our Activities</li>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
		<!-- Breadcrumbs End -->

        <!-- Services Start -->
        <div class="rs-gallery-section pt-90 pb-70">
            <div class="container">
                                      <div class="row">


<?php if($activity): ?>
<?php $__currentLoopData = $activity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activities): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-lg-4 mb-50">
                                                    <div class="event-item blue-color">
                                                        <div class="event-img">
                                                            <a href="">   <img src="<?php echo e(getImageByPath($activities->main_image,'370x230','school-activity')); ?>" alt=""></a>
                                                         
                                                         
                                                        </div>
                                                           <div class="middle-part">
                                            
                                                    <h3 class="title"><a class="title-act" href="#"><?php echo e($activities->title); ?></a></h3>
                                     
                                                </div>
                                                    </div>
                                                </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>



                        <!-- <div class="col-lg-4 mb-50">
                            <div class="event-item blue-color">
                                <div class="event-img">
                                    <a href="">   <img src="<?php echo e(asset('website/images/img_not_available.jpg')); ?>" alt=""></a>
                                 
                                 
                                </div>
                                   <div class="middle-part">
                    
                            <h3 class="title"><a class="title-act" href="#">Guiding</a></h3>
             
                        </div>
                            </div>
                        </div> -->

                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="<?php echo e(asset('website/images/img_not_available.jpg')); ?>" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">DCL</a></h3>
                 
                            </div>
                                </div>
                            </div> -->

                            
                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="<?php echo e(asset('website/images/activity/kcsl.jpg')); ?>" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">KCSL</a></h3>
                 
                            </div>
                                </div>
                        </div> -->

<!--                             
                        <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="<?php echo e(asset('website/images/img_not_available.jpg')); ?>" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">NCC</a></h3>
                 
                            </div>
                                </div>
                            </div> -->

                            
                        <!-- <div class="col-lg-4 mb-50">
                                <div class="event-item blue-color">
                                    <div class="event-img">
                                        <a href="">   <img src="<?php echo e(asset('website/images/activity/spc.jpg')); ?>" alt=""></a>
                                     
                                     
                                    </div>
                                       <div class="middle-part">
                        
                                <h3 class="title"><a class="title-act" href="#">SPC</a></h3>
                 
                            </div>
                                </div>
                        </div> -->

                            



                    </div>    
            </div>
        </div>
        <!-- Services End -->
        
       
        <?php $__env->stopSection(); ?>




       
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>