
<?php $__env->startSection('content'); ?>


        <!-- Start Banner -->
        <div class="inner-banner contact">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-lg-9">
                        <div class="content">
                            <h1>Contact Us</h1>
                            <p><?php echo $contact->address; ?></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3"> <a href="<?php echo e(url('application')); ?>" class="apply-online clearfix">
                        <div class="left clearfix"> <span class="icon"><img src="images/apply-online-sm-ico.png" class="img-responsive" alt=""></span> <span class="txt">Apply Online</span> </div>
                        <div class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </a></div>
                </div>
            </div>
        </div>
        <!-- End Banner --> 

      

        <!-- Start Google Map -->
        <section class="google-map">
            <div id="map"><iframe src="<?php echo e($contact->map); ?>" style="border:none;"></iframe></div>
            <div class="container">
                <div class="contact-detail">
                    <div class="address">
                        <div class="inner">
                            <h3>Ayyappa College </h3>
                            <p><?php echo $contact->address; ?></p>
                        </div>
                        <div class="inner">
                            <h3><?php echo $contact->phone; ?></h3>
                        </div>
                        <div class="inner"> <a href="mailto:info@edumart.com"><?php echo e($contact->email); ?></a> </div>
                    </div>
                    <div class="contact-bottom">
                        <ul class="follow-us clearfix">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Google Map --> 

       

      
        <!-- End Footer --> 

        <!-- Scroll to top --> 
        <a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> 

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
        <script src="js/jquery.min.js"></script> 
        <!-- Bootsrap JS --> 
        <script src="assets/bootstrap/js/bootstrap.min.js"></script> 
        <!-- Select2 JS --> 
        <script src="assets/select2/js/select2.min.js"></script> 
        <!-- Match Height JS --> 
        <script src="assets/matchHeight/js/matchHeight-min.js"></script> 
        <!-- Google Map API JS --> 
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpe2spShgjCJMPyokNNktU9g0-Lcyv2A4&sensor=false"></script> 
        <!-- Jquery Validate JS --> 
        <script src="js/jquery.validate.min.js"></script> 
        <!-- Custom JS --> 
        <script src="js/custom.js"></script>
    </body>
</html>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>