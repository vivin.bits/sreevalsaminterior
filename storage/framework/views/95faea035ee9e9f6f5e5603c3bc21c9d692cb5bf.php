<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" href="<?php echo asset('redactor/redactor.css'); ?>" />
<link rel="stylesheet" href="<?php echo asset('redactor/plugins/filemanager/filemanager.css'); ?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
            GALLERY
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $packages->category; ?> </h3>
                        <a href="<?php echo URL::to('packagess'); ?>" class="btn btn-primary pull-right">Back</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>
           
           <?php endif; ?>
           <div class="col-md-10 p-left-0">
              
                    <form action="<?php echo e(url('packages/media/upload/'.$packages->id)); ?>" method="post"  enctype="multipart/form-data" class="dropzone" id="dropzone">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <input type="hidden" name="albumid" value="<?php echo e($packages->id); ?>">





    
                     </form>
                </div><!-- /.box -->
            
   </div>
   <section class="content-header">
    <h1>
        Uploaded Images

    </h1>
    <div class = "row">
   



            <?php $__currentLoopData = $media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $images): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         
               
            <div class = "col-sm-6 col-md-3">
                    <div class = "thumbnail">
                    <img src = "<?php echo e(getImageByPath($images->filename,'440x290','packages-media')); ?>" alt = "Generic placeholder thumbnail"> 
                    </div>
                    
                    <div class = "caption">
                       
                       
                       <p>
                          <a   class = "btn btn-primary delete" data-content="upload" data-id=<?php echo e($images->id); ?> role = "button">
                             Delete
                          </a> 
                        
                       </p>
                       
                    </div>
                 </div>
                   
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    





        
      
           
           
       
        
     </div>
    
  
</section>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->
    <script type="text/javascript">
        Dropzone.options.dropzone =
         {
            maxFilesize: 12,
            renameFile: function(file) {
                var dt = new Date();
                var time = dt.getTime();
               return time+file.name;
            },
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            timeout: 5000,
            removedfile: function(file) 
            {
                var name = file.upload.filename;
                $.ajax({
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            },
                    type: 'POST',
                    url: '<?php echo e(url("image/delete")); ?>',
                    data: {filename: name},
                    success: function (data){
                        console.log("File has been successfully removed!!");
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                    var fileRef;
                    return (fileRef = file.previewElement) != null ? 
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
       
            success: function(file, response) 
            {
                console.log(response);
            },
            error: function(file, response)
            {
               return false;
            }
};
</script>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/js/custom/delete.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/redactor.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/filemanager/filemanager.js')); ?>"></script> 
<script src="<?php echo e(asset('admin/js/additional-methods.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/imagemanager/imagemanager.js')); ?>"></script> 

<script src="<?php echo e(asset('redactor/langs/ar.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/jquery-migrate-3.0.0.min.js')); ?>"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '<?php echo asset('redactor/scripts/file_upload.php'); ?>',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '<?php echo asset('redactor/scripts/image_upload.php'); ?>',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>