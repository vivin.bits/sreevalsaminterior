
<!DOCTYPE html>
<html>


<head>
  <meta charset="UTF-8">
<meta name="description" content="Sreevalsam Interiors is one of classic & the best interior designers in Cochin. We have spread up our interior designing business across the nation including Cochin,Thiruvalla, Thiruvanthapuram,Pathanamthitta,Kollam etc.">
  <meta name="keywords" content="interior in kottayam,pathanamthitta,interior in ernakulam,interior design kerala">
  <meta name="author" content="Sreevalsam Interiors">


<title>sreevalsaminteriors</title>
<!-- Stylesheets -->
<link href="<?php echo e(asset('website/css/bootstrap.css')); ?>" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>


 <!--Main Footer-->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
<link href="<?php echo e(asset('website/plugins/revolution/css/settings.css')); ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="<?php echo e(asset('website/plugins/revolution/css/layers.css')); ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="<?php echo e(asset('website/plugins/revolution/css/navigation.css')); ?>" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="<?php echo e(asset('website/css/style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('website/css/responsive.css')); ?>" rel="stylesheet">

<!--Color Switcher Mockup-->
<link href="<?php echo e(asset('website/css/color-switcher-design.css')); ?>" rel="stylesheet">
<?php echo $__env->yieldContent('head'); ?>
<!--Color Themes-->
<link id="theme-color-file" href="<?php echo e(asset('website/css/color-themes/default-theme.css')); ?>" rel="stylesheet">

<link rel="shortcut icon" href="<?php echo e(asset('website/images/favicon.png')); ?>" type="image/x-icon">
<link rel="icon" href="<?php echo e(asset('website/images/favicon.png')); ?>" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120473367-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120473367-1');
</script>
</head>

<body>
<div class="contact-toggle-main" style="display: block;">
      <div class="contact-toggle-btn" id="contact-toggle-btn"></div>
      <div class="contact-course newAdd" id="contact-course">
        Get in Touch<br>
        <!-- <i class="fa fa-phone" aria-hidden="true"></i>  --><a href="tel:919061645457" style="color:#fff;">+91 9349 429 212</a><br>
      </div>

<div class="offers">
<a>
    <img src="<?php echo e(asset('website/images/get_quote_free.png')); ?>" width="10%" class="pull-right" data-toggle="modal" data-target="#getForm"></a>
<div class="clear"></div>   

</div>


    </div>





<div class="page-wrapper">
 	
    <!-- Preloader -->

 	
    <!-- Main Header-->
    <header class="main-header">
    
    	<!--Header Top-->
    	<div class="header-top">
            <div class="auto-container">
                <div class="clearfix">
                    <!--Top Left-->
                    <div class="top-left pull-left">
                    	<ul class="clearfix">
                        	<li>Welcome to our Home Interiors</li>
                        </ul>
                    </div>
                    <!--Top Right-->
                    <div class="top-right pull-right">
                    	<ul class="social-nav">
                        	<li><a href="https://www.facebook.com/sreevalsaminteriors/"><span class="fa fa-facebook-f"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                            <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                        </ul>
                    	<ul class="list">
                        	<li>Have any question? +91 9349 429 212</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="auto-container">
            	<div class="clearfix">
                	
                	<div class="pull-left logo-box">
                    	<div class="logo">
                            <svg><rect></rect></svg>
                            <a href="index.html"><img src="<?php echo e(asset('website/images/logo.png')); ?>" alt="" title=""></a></div>

                    </div>
                    
                    <div class="pull-right upper-right clearfix">
                    	
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-home-1"></span></div>
                            <ul>
                            	<li><strong>49/1638-A6 Thykoodam</strong></li>
                                <li>Vyttila P.O, Ernakulam</li>
								 <li>India-82019</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-note"></span></div>
                            <ul>
                            	<li><strong>Send your mail at</strong></li>
								<li>info@sreevalsaminteriors</li>
								<li>.com</li>
                            </ul>
                        </div>
                        
                        <!--Info Box-->
                        <div class="upper-column info-box">
                        	<div class="icon-box"><span class="flaticon-clock-1"></span></div>
                            <ul>
                            	<li><strong>Working Hours</strong></li>
                                <li>Mon-Sat:9.30am to 7.00pm</li>
                            </ul>
                        </div>
                        
                    </div>

                    
                </div>

            </div>
        </div>

        <!--End Header Upper-->
        
        <!--Header Lower-->
        <div class="header-lower">
            
        	<div class="auto-container">
            	<div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="<?php echo e(Request::segment(1) === 'categories' ? 'active' : null); ?>"><a href="<?php echo e(url('/')); ?>">Home</a>
                                   
                                </li>
                                <li><a href="<?php echo e(url('about')); ?>">About us</a></li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                        <?php if(service()): ?>
                                        <?php $__currentLoopData = service(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serviceList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url('service/'.$serviceList->slug)); ?>"><?php echo e($serviceList->title); ?></a></li>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Products</a>
                                     <ul>
                                     <?php if(product()): ?>
                                     <?php $__currentLoopData = product(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url('products',$products->slug)); ?>"><?php echo e($products->title); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        
                                    </ul>
                                </li>
								<li><a href="<?php echo e(url('all-gallery-images')); ?>">Gallery</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Blog</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Packages</a>
                                    <ul>
                                        <li><a href="<?php echo e(url('package-list')); ?>">INTERIOR 3.99 LAKHS</a></li>
                                        <!-- <li><a href="interior4.39lakhs.html">INTERIOR 4.39 LAKHS</a></li>
                                        <li><a href="interior4.99lakhs.html">INTERIOR 4.99 LAKHS</a></li>
										<li><a href="interior5.39lakhs.html">INTERIOR 5.39 LAKHS</a></li>
                                        <li><a href="interior5.99lakhs.html">INTERIOR 5.99 LAKHS</a></li>
                                        <li><a href="interior6.49lakhs.html">INTERIOR 6.49 LAKHS</a></li> -->
                                    </ul>
                                </li>
                                <li><a href="<?php echo e(url('contact')); ?>">Contact us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->
                    <div class="outer-box clearfix">
                    	
                        <!--Search Box-->
                   
                        
                        <div class="advisor-box">
                        	<a href="<?php echo e(url('contact')); ?>" class="theme-btn advisor-btn">Book Now <span class="fa fa-long-arrow-right"></span></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->
        
        <!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
              
                	<a href="index.html" class="img-responsive"><img src="<?php echo e(asset('website/images/logo-small.png')); ?>" alt="" title=""></a>
                </div>
                
                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="<?php echo e(Request::segment(1) === 'categories' ? 'active' : null); ?>"><a href="<?php echo e(url('/')); ?>">Home</a>
                                    
                                </li>
                                <li><a href="<?php echo e(url('about')); ?>">About us</a></li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                    <?php if(service()): ?>
                                        <?php $__currentLoopData = service(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serviceList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url('service/'.$serviceList->slug)); ?>"><?php echo e($serviceList->title); ?></a></li>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Products</a>
                                   <ul>
                                   <?php if(product()): ?>
                                     <?php $__currentLoopData = product(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $products): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><a href="<?php echo e(url('products',$products->slug)); ?>"><?php echo e($products->title); ?></a></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
								<li d><a href="<?php echo e(url('all-gallery-images')); ?>">Gallery</a>
                                <li class="dropdown"><a href="">Blog</a>
                                    
                                </li>
                                <li class="dropdown"><a href="#">Packages</a>
                                    <ul>
                                        <li><a href="interior3.99lakhs.html">INTERIOR 3.99 LAKHS</a></li>
                                        <!-- <li><a href="interior4.39lakhs.html">INTERIOR 4.39 LAKHS</a></li>
                                        <li><a href="interior4.99lakhs.html">INTERIOR 4.99 LAKHS</a></li>
										<li><a href="interior5.39lakhs.html">INTERIOR 5.39 LAKHS</a></li>
                                        <li><a href="interior5.99lakhs.html">INTERIOR 5.99 LAKHS</a></li>
                                        <li><a href="interior6.49lakhs.html">INTERIOR 6.49 LAKHS</a></li> -->
                                    </ul>
                                </li>
                                <li><a href="<?php echo e(url('contact')); ?>">Contact us</a></li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
                
            </div>
        </div>
        <!--End Sticky Header-->
    
    </header>
    <!--End Main Header -->

<?php echo $__env->yieldContent('content'); ?>
        

        






   <!--Main Footer-->
   <footer class="main-footer" style="background-image:url(<?php echo e(asset('website/images/background/3.jpg')); ?>)">
    	<div class="auto-container">
        
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
                	
                    <!--big column-->
                    <div class="big-column col-lg-7 col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        
                            <!--Footer Column-->
                            <div class="footer-column col-md-7 col-sm-6 col-xs-12">
                                <div class="footer-widget logo-widget">
									<div class="logo">
                                    	<a href="index.html"><img src="<?php echo e(asset('website/images/footer-logo.png')); ?>" alt="" /></a>
                                    </div>
                                    <div class="text">Sreevalsam Interiors is one of classic &amp; the best interior designers in Cochin. We have spread up our interior designing business across the nation including Cochin,Thiruvalla, Thiruvanthapuram,Pathanamthitta,Kollam etc. </div>
                                    <ul class="social-icon-one">
                                        <li><a href="https://www.facebook.com/sreevalsaminteriors/"><span class="fa fa-facebook-f"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                        <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                        <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-5 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Services</h2>
                                    </div>
                                    <ul class="footer-lists">
                                        <li><a href="<?php echo e(url('about')); ?>">About Us</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                        <li><a href="architectinteriordesigning.html">Architect & Interior Designing</a></li>
                                        <li><a href="residentialdesigning.html">Residential Designing</a></li>
                                        <li><a href="officeinteriordesigning.html">Office Interior Designing</a></li>
                                        <li><a href="commercialhospitality.html">Commercial & Hospitality</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!--big column-->
                    <div class="big-column col-lg-5 col-md-6 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                        	
                            <!--Footer Column-->
                            <div class="footer-column col-md-5 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Extra Links</h2>
                                    </div>
                                    <ul class="footer-lists-two">
                                        <li><a href="faq.html">FAQ</a></li>
                                        <li><a href="#">Blogs</a></li>
                                        <li><a href="modularkitchen.html">Modular Kitchen</a></li>
                                        <li><a href="furnituredesigning.html">Furniture Designing</a></li>
                                        <li><a href="vastushastrainteriordesigning.html">Vastu Shastra Interior Designing</a></li>
                                        <li><a href="turnkeyprojects.html">Turnkey Projects</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-7 col-sm-6 col-xs-12">
                                 <div class="footer-widget links-widget">
                                	<div class="footer-title">
                                		<h2>Extra Links</h2>
                                    </div>
                                    <ul class="footer-lists-two">
                                        <li><a href="retailstore.html">Retail Store / Shopping Mall Interior Design</a></li>
                                        <li><a href="resortsinteriordesign.html">Resorts Interior Design</a></li>
                                        <li><a href="hotelinteriordesign.html">Hotel Interior Design</a></li>
                                        <li><a href="conventioncentreinteriordesign.html">Convention Centre Interior Design </a></li>
                                        <li><a href="requestquote.html">Request Quote</a></li>
                                        <li><a href="testimonals.html">Testimonals</a></li>
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
        <!--Footer Bottom-->
        <div class="footer-bottom">
        	<div class="auto-container">
            	<div class="row clearfix">
                	
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	<div class="copyright">Copyright © 2018 <a href="#">Sreevalsam Interiors,</a> All Right Reserved</div>
                    </div>
                    <div class="column col-md-6 col-sm-12 col-xs-12">
                    	 <p>Designed by <a href="http://verywebby.com/">Very Webby Design & Technologies</a>. All rights reserved</p>

                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
    <!--End Main Footer-->
    
</div>
<!--End pagewrapper-->

<!--Scroll to top-->

<!--<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>-->

<!-- Color Palate / Color Switcher -->

<!-- /.End Of Color Palate -->
<meta content="<?php echo csrf_token(); ?>" name="csrf-token">

<script> var base_url ='<?php echo e(url('')); ?>'</script>
<script src="<?php echo e(asset('website/js/jquery.js')); ?>"></script> 
<!--Revolution Slider-->

<script src="<?php echo e(asset('website/js/jquery.slides.min.js')); ?>"></script>



<script src="<?php echo e(asset('website/js/bootstrap.min.js')); ?>"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="<?php echo e(asset('website/js/jquery.mCustomScrollbar.concat.min.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/jquery.fancybox.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/owl.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/mixitup.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/wow.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/jquery-ui.js')); ?>"></script>
<script src="<?php echo e(asset('website/js/script.js')); ?>"></script>
<!--Google Map APi Key-->

<script src="<?php echo e(asset('website/js/map-script.js')); ?>"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
<!--End Google Map APi-->

<!--Color Switcher-->
  <script>
    $(function() {
      $('#slides').slidesjs({
      
        play: {
     
          auto: true,
          interval: 10000
   
        }
      });
    });
  </script>

<script>

$.ajaxSetup({ headers: { 'csrftoken' : '<?php echo e(csrf_token()); ?>' } });

$(document).ready(function() {

    base_url = "<?php echo e(url('')); ?>";

});



$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

</script>
	






<script type="text/javascript">
    
  $(document).ready(function(){
$(".contact-toggle-btn").click(function(){
if($("#contact-course").hasClass( "newAdd" )){
$("#contact-course").removeClass("newAdd");
}
else{
$("#contact-course").addClass("newAdd");
}
});
});
$(document).ready(function(){
$(".contact-toggle-btn").click(function(){
if($("#contact-toggle-btn").hasClass( "btn-r" )){
$("#contact-toggle-btn").removeClass("btn-r");
}
else{
$("#contact-toggle-btn").addClass("btn-r");
}
});
});

$(function(){
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  $('.menu-a').click( function() {
    //$(".navbar-nav").toggleClass("hide-nav");
    $('.navbar-toggle').click();
  });
}
});


</script>
<?php echo $__env->yieldContent('js'); ?>
<script>
    $(document).ready(function(){


 //This JQuery code will Reset value of Modal item when modal will load for create new records


 //This JQuery code is for Click on Modal action button for Create new records or Update existing records. This code will use for both Create and Update of data through modal
 
 
 
 
 $('#action').click(function(){
  var name = $('#name').val();
  var phone = $('#phone').val();
  var email = $('#email').val();
  var address = $('#address').val(); 
    var pack = $('#pack').val(); 
      var message = $('#message').val(); 


   $.ajax({
    url : "_email/in22.php",   
    data:{name:name, phone:phone,email:email,pack:pack,address:address,message:message}, 
    success:function(data){
     //alert(data);  
                          if(data == 'No')  
                          {  
                               alert("Wrong Data");  
                          }  
                          else  
                          {  
                               
                               
                          }  
     
    }
   });
  

 });




});
</script>


<script>
function submitEnquiry(){

    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

    var name = $('#inputName').val();
    var phone = $('#inputPhone').val();
    var email = $('#inputEmail').val();
    var address = $('#inputAddress').val();

    if(name.trim() == '' ){
    
        $('.statusMsg').html('<span style="color:red;">Please enter your name.</span>');
        $('#inputName').focus();
        return false;
    }else if(phone.trim() == '' ){
       
         $('.statusMsg').html('<span style="color:red;">Please enter your Phone No.</span>');
        $('#inputMessage').focus();
        return false;
    }
    else if(email.trim() == '' ){
        
        $('.statusMsg').html('<span style="color:red;">Please enter your email.</span>');
        $('#inputEmail').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
    
           $('.statusMsg').html('<span style="color:red;">Please enter valid email.</span>');
        $('#inputEmail').focus();
        return false;
    }else if(address.trim() == '' ){
          $('.statusMsg').html('<span style="color:red;">Please enter your Address Or Place.</span>');
        $('#inputMessage').focus();
        return false;
    }else{
     
        $.ajax({
            type:'POST',
            url:'_email/submit_enquiry.php',
            data:'name='+name+'&phone='+phone+'&email='+email+'&address='+address,
            beforeSend: function () {
                $('.submitBtn').attr("disabled","disabled");
                $('.modal-body').css('opacity', '.5');
            },
            success:function(msg){
                if(msg == 'ok'){
                    $('#getForm').modal('hide');
                   swal("Submitted Successfully.", "We Will Contact You Soon.", "success");
        
            $('#formE')[0].reset();
                    
                  
                }else{
                    $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                    swal ( "Oops" ,  "Something went wrong please try again !" ,  "error" );
                }
                $('.submitBtn').removeAttr("disabled");
                $('.modal-body').css('opacity', '50');
            }
        });
    }
}

</script>




<!-- Modal -->
<div class="modal fade" id="getForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
                      <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="">Close</span>
                </button>

           <div class="modal-header model-c">

    
        <h3 class="modal-title"><strong>GET A <span>FREE QUOTE</span>  WITH <span>25%OFF </span></strong></h3></div>

            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" id="formE">
            
                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control" id="inputName" placeholder="Enter your name"/>
                    </div>
                       <div class="form-group">
                        <label for="inputEmail">Phone No</label>
                        <input type="text" class="form-control" id="inputPhone" placeholder="Enter your Phone No"/>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control" id="inputEmail" placeholder="Enter your email"/>
                    </div>
                     <div class="form-group">
                        <label for="inputMessage">Address</label>
                        <textarea class="form-control" id="inputAddress" placeholder="Enter your Address"></textarea>
                    </div>
                
                </form>
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
           <button type="button" class="btn  btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn  btn-success submitBtn" onclick="submitEnquiry()">SUBMIT</button>
            </div>
        </div>
    </div>
</div>

</body>


</body>


</html>
