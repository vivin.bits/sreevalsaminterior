
<?php $__env->startSection('content'); ?> 

<div class="rs-toolbar hidden-md">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="rs-toolbar-left">
                                <div class="welcome-message">
                                    <i class="fa fa-map-marker"></i>
                                    <span>Mavelikara Road, Nangiarkulangara, Kerala 690513</span> 
                                </div>
                                <div class="welcome-message">
                                    <i class="fa fa-phone"></i>
                                    <span><a href="tel:+04792410750">+0479 241 0750</a></span> 
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="rs-toolbar-right">
                                <div class="toolbar-share-icon">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a class="hidden-xs rs-search" data-target=".search-modal" data-toggle="modal" href="#"><i class="fa fa-search"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Toolbar End -->
       
            </div>
            </div>
			
		<!-- Slider Area Start -->
        <div id="rs-slider" class="slider-overlay-2">
        <?php if($banner): ?>
                    <?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bannerImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($bannerImage->image): ?>
                <div id="home-slider">
        
    
    
    
                    <!-- Item 2 -->
                    <div class="item active">
                   

                        <img src="<?php echo e(getImageByPath($bannerImage->image,'459x192','banner')); ?>" alt="Slide2" />
                   
                        <div class="slide-content">
                            <div class="display-table">
                                <div class="display-table-cell">
                                    <div class="container text-left">
                                        <div class="sliter-title-text">
                                            <h2 class="slider-title small-text slider-title white-layer" data-animation-in="fadeInLeft" data-animation-out="animate-out">Welcome To</h2>
                                            <br>
                                            <h1 class="slider-title slider-title white-layer" data-animation-in="fadeInLeft" data-animation-out="animate-out">Bethany BalikaMadhom</span></h1>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
      <?php endif; ?>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                                    <!-- Item 2 -->
                    <!-- <div class="item">
                        <img src="<?php echo e(asset('website/images/slider/home1/h1-s1.jpg')); ?>" alt="Slide2" />
                        <div class="slide-content">
                            <div class="display-table">
                                <div class="display-table-cell">
                                    <div class="container text-left">
                                           <div class="sliter-title-text">
                                            <h2 class="slider-title small-text slider-title white-layer" data-animation-in="fadeInLeft" data-animation-out="animate-out">Welcome To </h2>
                                            <br>
                                            <h1 class="slider-title slider-title white-layer" data-animation-in="fadeInLeft" data-animation-out="animate-out">Bethany BalikaMadhom</span></h1>
                                        </div>
    
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
    
    
    
    
    
    
    
                </div>         
            </div>
            <!-- Slider Area End -->
		
		<!-- CTA Start -->
        <div class="rs-kid-cta primary-bg pt-50 pb-50">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-9 news">
                            <div class="cta-text white-color text-md-left">
                                <h2 class="margin-0 white-color ns">News & Events</h2>
                            </div>
                            <div id="newstick" class="span12">
      <ul class="">
  

       <li>
            <h3>August 15th</h3>
           <p>Celebrations and house wise competitions of Independence Day held on August 15th</p>
       </li>

       <li>
            <h3>Onam celebration</h3>
           <p>Onam celebration and house wise competetion related to Onam will be conducted soon</p>
       </li>

       <li>
            <h3>August 22nd</h3>
           <p>First Terminal Examination starts on August 22nd</p>
       </li>
    
    
      </ul>
    </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            <!-- CTA End -->

         <!-- Services Start -->
         <div class="rs-services pt-90 pb-50">
            <div class="container">
                <div class="sec-title mb-100 text-center">
                    <h2 class="sec-title mb-10 title-sec">Why Choose Us</h2> 
                    <p class="sec-subtitle normal body-color"></p>
                </div>
                <?php if($whychoose): ?>

                <div class="row">
                <?php $__currentLoopData = $whychoose; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $choose): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-3 col-md-6 col-sm-12 mb-md-70">
                        <div class="single-service style-3 activity-shape text-center">
                            <div class="service-icon">
                                <i class="flaticon-classroom"></i>
                            </div>
                            <div class="service-text">
                                <h3 class="service-title"><a class="title-color" href="#"><?php echo e($choose->title); ?></a></h3>
                                <p class="service-desc"><?php echo $choose->message; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                        </div>
                   
                    </div>
                    
                </div>
               
            </div>
        </div>

        <?php if($about): ?>
        <div id="rs-about" class="rs-about align-items-center gray-bg sec-spacer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-12 mb-md-30">
                        <div class="about-img text-center">
                            <img src="<?php echo e(getImageByPath($about->image,'429x369','profile-images')); ?>" alt="img02"/>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 mpl-15 pl-40">
                        <div class="about-tab style-1 extra-padding ">
                            <div class="sec-title margin-0 text-left">

                                <h2 class="section-title primary-color"><span class="title-sec"></span><?php echo e($about->title); ?> </h2> 

                                <h4 class="section-subtitle-right">A  journey  in  moulding</h4>  
                                <p>
                                <?php echo $about->short_description; ?>

                                </p>   
                                <!-- <p class="section-desc mb-15">Bethany Balikamadhom High School for Girls was established in the year 1931 May 18 by the Servant of God, Arch Bishop Mar Ivanios for the Sisters of the Imitation of Christ, 
                                    Pathanamthitta Province with a vision of women empowerment.</p>
                                    <p>Bethany Sisters , the gem of Syro Malankara Catholic Church, 
                                        moulding the young generation with a view to reach out to the oppressed and the needy and to liberate them through education for the better citizenship.</p> -->
 <a class="readon hvr-ripple-out kinder-orange readon-btn uppercase" href="<?php echo e(url('aboutus/'.$about->slug)); ?>">Read More</a>
                           
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!-- About Us End -->



           <!-- Services Start -->
           <div class="r-section pt-20 pb-20">
         
         <div class="row">
                <div class="container">
                   <div class="row">
             <div class="col-lg-6 col-md-6 col-sm-12 mb-md-30 pt-50">
                 <div class="single-service text-center">
                     <div class="grid-style">
                         <div class="icon-style2">
                             <div class="shape"><i class="flaticon-bullseye"></i></div>
                         </div>
                         <div class="service-text">
                             <h3 class="service-title"><a class="yt" href="#">Our Vision</a></h3>
                             <p class="service-desc">The Vision of BBHS for Girls is to form young generation who care , serve and  love  the  downtrodden and the marginalized. Founder Mar Ivanios, the  
                                 icon of the Teaching profession depicts as a “Friend & Guide” , 
                                 one who devotes himself completely to the upliftment of the humanity. </p>
                         </div>
                          <a class="readon hvr-ripple-out kinder-orange readon-btn uppercase sp" href="#">Read More</a>
                     </div>
                 </div>
             </div>
             <div class="col-lg-6 col-md-6 col-sm-12 mb-md-30 pt-50">
                 <div class="single-service text-center">
                     <div class="grid-style">
                         <div class="icon-style2">
                             <div class="shape"><i class="flaticon-school-1"></i></div>
                         </div>
                         <div class="service-text">
                             <h3 class="service-title"><a class="yt" href="#">Our Mission</a></h3>
                             <p class="service-desc">The Mission of BBHS for Girls is to continue the Teaching ministry of JESUS CHRIST in this world. 
                                 BBHS aims at providing excellent academic formation along with artistic skills of International Standards, 
                                 concentrates both in efficiency and value orientation and enable the students to face the emerging challenges.</p>
                         </div>
                          <a class="readon hvr-ripple-out kinder-orange readon-btn uppercase sp2" href="#">Read More</a>
                     </div>
                 </div>
             </div>
  
     </div>
         </div>
     </div>
 </div>
 <!-- Services End -->
 
<div class="p-panel pt-50 pb-50">
     <div class="container">
         <div class="row">
                        <div class="col-lg-12 col-md-6 col-sm-12 mb-sm-30">
                 <div class="single-service text-center">
                     <div class="grid-style">
                         <div class="icon-style2">
                             <div class="shape_p"><i class="flaticon-tutor"></i></div>
                         </div>
                         <div class="service-text">
                             <h3 class="service-title"><a class="title-sec" href="#">Principal's Message</a></h3>
                             <p class="service-desc"><b>“Little dreams can lead to greatness, Little victories to success. ‘it’s the little things that bring the greatest happiness”</b>

                                     It’s my earnest wish to convey the feelings of appreciation, gratitude and joy as I write this message. 
                                     Bethany Balikamadhom High School steps in to the 89th year of its establishment and service to the humanity with the Motto and Charism of women empowerment.
                                     
                                     First, I would like to express my sincere gratitude to Almighty God, who strengthen, guide this Institution and instill the grace of Holy Spirit.

                                     BBHS,  while admiring and keeping its motto and charism, always aspiring to aim High in its quest for Excellence in imparting human, intellectual, spiritual and moral formation to the students.

                             </p>
                           
                         </div>
                     </div>
                 </div>
             </div>


          </div>
      </div>

</div>

 <!-- Classes Start -->
 <div id="rs-classes" class="rs-classes style-1 class-bg sec-spacer2">
     <div class="container">
         <div class="icon-image">
             <div class="sec-title text-center">
                
                 <h2 class="section-subtitle title-sec">Gallery</h2>
             </div>
             <?php if($gallery): ?>
             
             <div class="row">
             <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $galleryPic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <div class="col-lg-4 mb-50">
                     <div class="event-item blue-color">
                         <div class="event-img">
                             <a href="">   <img src="<?php echo e(getImageByPath($galleryPic->main_image,'429x286','album')); ?>" alt=""></a>
                          
                          
                         </div>
                         <div class="events-details gallery-title-bg">
                             <h3 class="gallery-title"><a href=""><?php echo e($galleryPic->title); ?></a></h3>
                          
                         </div>
                     </div>
                 </div>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                


             </div>
             <?php endif; ?>
             <div class="viewmore-btn text-center">
                 <a class="readon hvr-ripple-out kinder-orange readon-btn uppercase" href="<?php echo e(url('gallery')); ?>">View All Gallery</a>
             </div>
             <div class="right-bottom-image animated pulse infinite bottom-175">
                 <img src="<?php echo e(asset('website/images/bg/ball3.png')); ?>" alt="Hand Image">
             </div>
         </div>
     </div>
 </div>
 <!-- Classes End -->
 <!-- Counter Up Section Start-->
 <div class="rs-counter style-2 counter-bg2 pt-100 pb-100">
     <div class="container">
         <div class="row">
             <div class="col-lg-3 col-md-6 col-sm-6 mb-md-30">
                 <div class="rs-counter-list">
                     <div class="counter-number plus">60</div>                  
                     <h3 class="counter-desc">Teachers</h3>
                 </div>
             </div>
             <div class="col-lg-3 col-md-6 col-sm-6 mb-md-30">
                 <div class="rs-counter-list blue-color">
                     <div class="counter-number plus">1500</div>
                     <h3 class="counter-desc">Happy Girls</h3>
                 </div>
             </div>
             <div class="col-lg-3 col-md-6 col-sm-6 mbs-mb-30">
                 <div class="rs-counter-list orange-color">
                     <div class="counter-number plus">1400</div>                  
                     <h3 class="counter-desc">Teaching Hours</h3>
                 </div>
             </div>
                      <div class="col-lg-3 col-md-6 col-sm-6 mbs-mb-30">
                 <div class="rs-counter-list orange-color">
                     <div class="counter-number plus">180</div>                  
                     <h3 class="counter-desc">Meal Per Year</h3>
                 </div>
             </div>
     
         </div>
     </div>
 </div>
 <!-- Counter Down Section End -->


  <!-- Classes Start -->
  <div id="rs-classes" class="rs-classes style-2 pt-90 pb-100">
            <div class="container mb-50">
                <div class="sec-title text-center">
                    <h2 class="section-subtitle title-sec">Our Activities</h2> 
                    <p class="sec-subtitle"></p>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true" data-md-device-dots="true">
                   <?php if($activity): ?>
                   <?php $__currentLoopData = $activity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activities): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="classes-grid">
                        <div class="top-part">
                            <img src="<?php echo e(getImageByPath($activities->main_image,'429x267','school-activity')); ?>" alt="image">
                        </div>
                        <div class="middle-part">
                
                            <h3 class="title"><a class="title-act" href="#"><?php echo e($activities->title); ?></a></h3>
                       
                        </div>
                  
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

               

                 <?php endif; ?>
                </div>
            </div>
                     <div class="viewmore-btn text-center">
                        <a class="btn_2 btn uppercase" href="<?php echo e(url('activity')); ?>">View All Activities</a>
                    </div>
        </div>
        <!-- Classes End -->



 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>