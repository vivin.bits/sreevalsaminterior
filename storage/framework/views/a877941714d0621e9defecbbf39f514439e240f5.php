

<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" href="<?php echo asset('redactor/redactor.css'); ?>" />
<link rel="stylesheet" href="<?php echo asset('redactor/plugins/filemanager/filemanager.css'); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
        Advertisement Create
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                        <a href="<?php echo e(url('admin/activity')); ?>" class="btn btn-primary pull-right">Back </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>

           <?php endif; ?>
           <div class="col-md-10 p-left-0">
              
                    <form action="<?php echo e(url('admin/activity/create/')); ?>" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <div class="box-body">
                            
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="name">Name</label>
                                <input type="text" name="title"  id="name" class="form-control" placeholder="Enter name" value="<?php echo old('name'); ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>
                            <div class="form-group <?php if($errors->first('slug')): ?> has-danger <?php endif; ?>">
                                <label for="slug">Slug</label>
                                <input class="form-control <?php if($errors->first('slug')): ?> is-invalid <?php endif; ?>" id="slug" name="slug" type="text" placeholder="Enter slug"  value="<?php echo e(old('slug')); ?>">
                                <?php if($errors->first('slug')): ?>
                                    <label class="error"><?php echo e($errors->first('slug')); ?></label>
                                <?php endif; ?>
                            </div>    
                     
                           
                         
                            
                            
                        </div>

    
                            <div  class="form-group <?php if($errors->first('image')): ?> has-danger <?php endif; ?>">

                            <label  for="image">Image</label><br>

                            <input   id="image"  name="image"  type="file" >

                            <?php if($errors->first('image')): ?><span  class="alert-danger"><?php echo e($errors->first('image')); ?></span>
                             <?php endif; ?>
                           
                            <div>

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/redactor.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/filemanager/filemanager.js')); ?>"></script> 
<script src="<?php echo e(asset('admin/js/additional-methods.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/imagemanager/imagemanager.js')); ?>"></script> 

<script src="<?php echo e(asset('redactor/langs/ar.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/jquery-migrate-3.0.0.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/custom/slug.js')); ?>"></script>

<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '<?php echo asset('redactor/scripts/file_upload.php'); ?>',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '<?php echo asset('redactor/scripts/image_upload.php'); ?>',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>