

<?php $__env->startSection('content'); ?>

        <!-- Start apply online -->
        <section class="login-wrapper register">
            <div class="inner">
                <div class="regiter-inner">
                    <div class="login-logo"> <a href="index.html"><img src="images/login-logo.png" class="img-responsive" alt=""></a> </div>
                    <div class="head-block">
                        <h1>Apply Now</h1>
                    </div>
                    <div class="cnt-block">
                    <form action="<?php echo URL::to('application/create'); ?>" method="post"  enctype="multipart/form-data" class="form-outer">
                    <?php echo e(csrf_field()); ?>

                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="fname" type="text" placeholder="First Name">
                                </div>
                                <div class="col-sm-6">
                                    <input name="lname" type="text" placeholder="Last Name">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="street" type="text" placeholder="Street">
                                </div>
                                <div class="col-sm-6">
                                    <input name="city" type="text" placeholder="City">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="pincode" type="text" placeholder="Postal Code">
                                </div>
                                <div class="col-sm-6">
                                    <select class="custom_select" name="state">
                                        <option value="State">State</option>
                                        <option value="Kerala">Kerala</option>
                                        <option value="Tamilnadu">Tamilnadu</option>
                                        <option value="Karnataka">Karnataka</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <select class="custom_select" name="country">
                                        <option value="Country">Country</option>
                                        <option value="INDIA">INDIA</option>
                                        <option value="UK">UK</option>
                                        <option value="USA">USA</option>
                                        <option value="UAE">UAE</option>
                                    </select>
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <input name="country code" type="text" placeholder="+91" class="country-code">
                                    <input name="phone" type="text" placeholder="000-000-0000" class="phone-no">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input name="email" type="text" placeholder="Email">
                                </div>
                                <div class="col-sm-6">
                                    <select class="custom_select" name="course">
                                    
                                  
                                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <option ><?php echo e($value); ?></option>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="button-outer">
                                <!-- <button class="btn">Apply <span class="icon-more-icon"></span></button> -->
                                
                        <button type="submit" class="pull-right btn btn-primary margin-top">Apply</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- End apply online --> 
        
       
        <?php $__env->stopSection(); ?>




       
<?php echo $__env->make('layouts.apply', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>