<?php $__env->startSection('content'); ?>
<!--Page Title-->
<section class="page-title" style="background-image:url(<?php echo e(asset('website/images/background/8.jpg')); ?>)">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                	<h1>Gallery</h1>
                </div>
                <div class="pull-right">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
            <div class="contact-number text-center"><span class="icon flaticon-phone-call"></span>Call Us: +91 9349 429 212</div>
        </div>
    </section>
    <!--End Page Title-->
<!--Project Section-->
<section class="project-section">
    	<div class="auto-container">
        	<div class="sec-title centered">
            	<div class="title">Our Best Works</div>
                <h2><span class="theme_color">Our</span> Gallery</h2>
            </div>
            
            <!--MixitUp Galery-->
            <div class="mixitup-gallery">
                
                <!--Filter-->
                <div class="filters clearfix">
                <div class="filter-links" style="display:block">
                    <ul class="filter-tabs filter-btns clearfix " >
                    <?php if(sizeof($gallery) > 0): ?>  
                    <input type="hidden" id="gallery_image_id" value="<?php echo e($gallery[0]->id); ?>">

                           <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$gallerz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <!-- <li class="<?php if($key == 0): ?>active-border <?php endif; ?> gallery" id="gallery_<?php echo e($gallerz->id); ?>" data-id="<?php echo e($gallerz->id); ?>"><?php echo e($gallerz->name); ?> </li> -->
                        <li class=" filter <?php if($key == 0): ?>active-border <?php endif; ?> gallery" id="gallery_<?php echo e($gallerz->id); ?>" data-id="<?php echo e($gallerz->id); ?>" data-filter="all"><?php echo e($gallerz->name); ?></li>
                         <!-- <li class="filter" data-role="button" data-filter=".kitchen">Modular Kitchen</li>
                        <li class="filter" data-role="button" data-filter=".wardrobes">Modern Wardrobes</li>
                        <li class="filter" data-role="button" data-filter=".home">Home and Hospitality Interiors</li>
                        <li class="filter" data-role="button" data-filter=".ceiling">Kerala Traditional Pop Wood Ceiling</li>
						<li class="filter" data-role="button" data-filter=".chappell">Chappell & Temple Designing</li> -->
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </ul>
                </div>
                </div>
                
               
					
				

                                                      <!--Gallery Item-->
                    
                         <!-- <div class="filter-list row clearfix gallery-thumbs">
                                                                   
                    <div class="gallery-item mix all kitchen col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="<?php echo e(asset('website/images/gallery/small/1/01_7.jpg')); ?>" alt="">
                               
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                         
                                            <li><a href="<?php echo e(asset('website/images/gallery/big/1/01_7.jpg')); ?>" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Modulasssr Kitchend</a></h3>
                                           
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                                                      </div> -->
                                                                   <!--Gallery Item-->
             

                                                                   <!--Gallery Item-->
                    

                                                                   <!--Gallery Item-->
                   

                              
                
            </div>
            
    	</div>
       
    </section>
<?php $__env->startSection('js'); ?>
    <script>
       $('body').on('click', '.gallery', function () {

var id = $(this).data("id") ;
var url ='/all/gallery/image';
$(".gallery").removeClass('active-border');
           var gallery_ = "#gallery_"+id;
           $(gallery_).addClass('active-border');
        $.ajax({
            url: url,
                            type: 'POST',  
                            data:{
                                'id':id,
                            },
                            
                            beforeSend: function(){
                                $("#if_loading").html("loading");
                                
                            },
                            success: function(success) {
                                // $('#show_gallery').html(success);

                              $('.gallery-thumbs').empty();
                              $( success).insertAfter( ".filter-links" );
                            },
                            
                            complete: function(){
                                $('#if_loading').html("");
                                
                            }
                            
                        });
    
                   
                });
                
        
    
    </script>
    <!-- Parallax -->
    <script>
        jQuery(document).ready(function () {
            $(window).scroll(function (e) {
                parallaxScroll();
            });

            function parallaxScroll() {
                var scrolled = $(window).scrollTop();
                $('.gallery-bg-parallax').css('top', (0 - (scrolled * .08)) + 'px');
            }


            $.ajax({
                url:'/all/gallery/image',
                type: 'POST',
                data:{
                    'id':$("#gallery_image_id").val(),
                },

                beforeSend: function(){
                    $("#if_loading").html("loading");

                },
                success: function(success) {
                     //$('#show_gallery').html(success);
                    var clear = '';
                    $( clear).insertAfter( ".filter-links" );
                  $( success).insertAfter( ".filter-links" );
                },

                complete: function(){
                    $('#if_loading').html("");

                }

            });
    
                   
            

        }); 
    </script>
    <?php $__env->stopSection(); ?>
    <!--End Project Section-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>