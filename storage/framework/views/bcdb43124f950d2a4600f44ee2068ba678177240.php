<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Sreevalsam Interiors</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo asset('admin/bower_components/font-awesome/css/font-awesome.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo e(asset('admin/plugins/datatables/dataTables.bootstrap.css')); ?>">
  <link rel="stylesheet" href="<?php echo asset('admin/css/pretty-checkbox.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo asset('admin/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo asset('admin/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="<?php echo asset('admin/dist/css/skins/_all-skins.min.css'); ?>">
   <!-- Morris chart -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/morris.js/morris.css'); ?>">
   <!-- jvectormap -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/jvectormap/jquery-jvectormap.css'); ?>">
   <!-- Date Picker -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="<?php echo asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css'); ?>">
   <link rel="stylesheet" href="<?php echo asset('admin/css/iziToast.min.css'); ?>">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="<?php echo asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'); ?>">
       <link rel="stylesheet" href="<?php echo asset('css/loginstyle.css'); ?>">
   <style type="text/css">
     .form-check, .form-check{
      padding-left: 20px;
     }
     .error{
    color: #f00;
  }

   </style>
      </head>
      <body class="hold-transition login-page bodystyle">


<div class="form-box">
    <div class="head">Sreevalsam Interiors</div>
   <form id="login-form" action="<?php echo e(url('admin/login')); ?>" method="post">
              <?php echo e(csrf_field()); ?>

        <div class="form-group">
          <label class="label-control">
            <span class="label-text">Email</span>
          </label>
        
          <input type="email" name="email" class="form-control">
                     <?php if($errors->has('email')): ?> 
                  <span class="help-block">
                    <?php echo e($errors->first('email')); ?>

                  </span>
                  <?php endif; ?>
        </div>
        <div class="form-group">
          <label class="label-control">
            <span class="label-text">Password</span>
          </label> 
       <input type="password" name="password" class="form-control" autocomplete="off">
                     <?php if($errors->has('password')): ?>
                  <span class="help-block">
                    <?php echo e($errors->first('password')); ?>

                  </span>
                  <?php endif; ?>
        </div>
        <div class="form-group has-error">
  
<div class="g-recaptcha" data-sitekey=""></div>
<?php if($errors->has('g-recaptcha-response')): ?> 
                        <span class="help-block" style="white-space: nowrap;">
                            <?php echo e($errors->first('g-recaptcha-response')); ?>

                        </span>
                        <?php endif; ?>
</div>
         <div class="form-group error">
                  <?php if(Session::has('message')): ?>
                  <?php echo Session::get('message'); ?>

                <?php endif; ?></div>
        <input type="submit" value="Login" class="btn" />
        <!-- <p class="text-p">Don't have an account? <a href="#">Sign up</a> </p> -->
    </form>
    
  </div>
      <script src="<?php echo asset('js/jquery.min.js'); ?>"></script>
<script type="text/javascript">
$(window).load(function(){
  $('.form-group input').on('focus blur', function (e) {
      $(this).parents('.form-group').toggleClass('active', (e.type === 'focus' || this.value.length > 0));
  }).trigger('blur');
});
</script> 

  <script src='https://www.google.com/recaptcha/api.js'></script>
      </body>
      </html>
