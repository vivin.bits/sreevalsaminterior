

<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" href="<?php echo asset('redactor/redactor.css'); ?>" />
<link rel="stylesheet" href="<?php echo asset('redactor/plugins/filemanager/filemanager.css'); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
          Contact Edit
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Add Contact </h3>
                        <a href="<?php echo URL::to('admin/contact'); ?>" class="btn btn-primary pull-right">Back</a>
                    </div><!-- /.box-header -->
                    <div class="box-body">


            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>

           <?php endif; ?>
           <div class="col-md-10 p-left-0">
              
                    <form action="<?php echo e(url('contact/edit',$contact->id)); ?>" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <div class="box-body">
                            
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="<?php echo $contact->title; ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>

                            
                           
                         
                            <div class="form-group email_temp <?php if($errors->first('content')): ?> has-error <?php endif; ?>">
                                <label for="description">Address</label>
                                <textarea class="form-control <?php if($errors->first('message')): ?> is-invalid <?php endif; ?>" id="message" name="content"
                                          placeholder="Enter Description" autocomplete="off" value="<?php echo e(old('phone')); ?>" maxlength="3000"
                                          rows="10" style="height: 100px;"> <?php echo e($contact->address); ?></textarea>
                                <span><?php echo e($errors->first('content')); ?></span>




                            </div>
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Phone</label>
                                <input type="text" name="phone" class="form-control" placeholder="Enter Title"  value="<?php echo $contact->phone; ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>

                            
                        </div>
                        <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Location Map</label>
                                <input type="text" name="map" class="form-control" placeholder=""  value="<?php echo $contact->map; ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>
                        <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Facebook</label>
                                <input type="text" name="facebook" class="form-control" placeholder="Enter Facebook Link"  value="<?php echo $contact->facebook; ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>

                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                    <label for="title">Linkedin</label>
                                    <input type="text" name="linkedin" class="form-control" placeholder="Enter Linkedin Link"  value="<?php echo $contact->linkedin; ?>">
                                    <span><?php echo e($errors->first('title')); ?></span>
                                </div>
                                <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                        <label for="title">Twiter</label>
                                        <input type="text" name="twiter" class="form-control" placeholder="Enter Twiter Link"  value="<?php echo $contact->twitter; ?>">
                                        <span><?php echo e($errors->first('title')); ?></span>
                                    </div>
                                    <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                            <label for="title">Youtube</label>
                                            <input type="text" name="youtube" class="form-control" placeholder="Enter youtube Link"  value="<?php echo $contact->youtube; ?>">
                                            <span><?php echo e($errors->first('title')); ?></span>
                                        </div>
                                    <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                            <label for="title">Insta</label>
                                            <input type="text" name="insta" class="form-control" placeholder="Enter Insta Link"  value="<?php echo $contact->insta; ?>">
                                            <span><?php echo e($errors->first('title')); ?></span>
                                        </div>
                                         <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                                <label for="title">Email</label>
                                                <input type="text" name="email" class="form-control" placeholder="Enter Email"  value="<?php echo $contact->email; ?>">
                                                <span><?php echo e($errors->first('title')); ?></span>
                                            </div>
                                            <div class="form-group <?php if($errors->first('meta_title')): ?> has-danger <?php endif; ?>">
                                                    <label for="meta_title">Meta Title</label>
                                                    <input class="form-control <?php if($errors->first('meta_title')): ?> is-invalid <?php endif; ?>"
                                                           id="meta_title" name="meta_title" type="text" placeholder="Enter meta name"
                                                           autocomplete="off"  value="<?php echo $contact->meta_title; ?>">
                                                    <?php if($errors->first('meta_title')): ?>
                                                    <label class="alert-danger"><?php echo e($errors->first('meta_title')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="form-group <?php if($errors->first('meta_title')): ?> has-danger <?php endif; ?>">
                                                        <label for="meta_title">Meta Content</label>
                                                        <input class="form-control <?php if($errors->first('meta_title')): ?> is-invalid <?php endif; ?>"
                                                               id="meta_title" name="meta_content" type="text" placeholder="Enter meta name"
                                                               autocomplete="off" value="<?php echo $contact->meta_content; ?>">
                                                        <?php if($errors->first('meta_title')): ?>
                                                        <label class="alert-danger"><?php echo e($errors->first('meta_title')); ?></label>
                                                        <?php endif; ?>
                                                    </div>
                                                <div class="form-group <?php if($errors->first('meta_key')): ?> has-danger <?php endif; ?>">
                                                    <label for="meta_key">Meta Key</label>
                                                    <input class="form-control <?php if($errors->first('meta_key')): ?> is-invalid <?php endif; ?>"
                                                           id="meta_key" name="meta_key" type="text" placeholder="Enter meta key"
                                                           autocomplete="off"  value="<?php echo $contact->meta_key; ?>">
                                                    <?php if($errors->first('meta_key')): ?>
                                                    <label class="alert-danger"><?php echo e($errors->first('meta_key')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                
                                                <div class="form-group <?php if($errors->first('meta_description')): ?> has-danger <?php endif; ?>">
                                                    <label for="meta_description">Meta Description</label>
                                                    <textarea
                                                            class="form-control <?php if($errors->first('meta_description')): ?> is-invalid <?php endif; ?>"
                                                            id="meta_description" name="meta_description" type="text"
                                                            placeholder="Enter meta description" autocomplete="off"><?php echo e($contact->meta_description); ?></textarea>
                                                    <?php if($errors->first('meta_description')): ?>
                                                    <label class="alert-danger"><?php echo e($errors->first('meta_description')); ?></label>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="form-group <?php if($errors->first('meta_key')): ?> has-danger <?php endif; ?>">
                                                        <label for="meta_key">Author</label>
                                                        <input class="form-control <?php if($errors->first('meta_key')): ?> is-invalid <?php endif; ?>"
                                                               id="meta_key" name="author" type="text" placeholder="Enter author"
                                                               autocomplete="off"  value="<?php echo $contact->author; ?>">
                                                        <?php if($errors->first('meta_key')): ?>
                                                        <label class="alert-danger"><?php echo e($errors->first('meta_key')); ?></label>
                                                        <?php endif; ?>
                                                    </div>

                                        
    
                            

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/redactor.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/filemanager/filemanager.js')); ?>"></script> 
<script src="<?php echo e(asset('admin/js/additional-methods.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/imagemanager/imagemanager.js')); ?>"></script> 

<script src="<?php echo e(asset('redactor/langs/ar.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/jquery-migrate-3.0.0.min.js')); ?>"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '<?php echo asset('redactor/scripts/file_upload.php'); ?>',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '<?php echo asset('redactor/scripts/image_upload.php'); ?>',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('additional_scripts'); ?>
<script>
    setTimeout(function(){ $('.clsAlert').fadeOut(); }, 3000);
    $('.fa-close').click(function(){
        $('.clsAlert').fadeOut();
    });
    
        // Add Repeater Field : start
        
        var speci_field_index = 0;
        $(document).ready(function () {

    var MaxInputsAudio = 50; //maximum input boxes allowed
    var InputsWrapper = $("#AudioWrapperr_two input"); //Input boxes wrapper ID
    var AddButton = "#AddMoreAudio_two"; //Add button ID

    var x = InputsWrapper.length; //initlal text box count
    var FieldCount = 1; //to keep track of text box added
    var count = 0;
    $(AddButton).click(function (e) //on add input button click
    {
        count++;

        //console.log(count);

        e.preventDefault();
        InputsWrapper = $("#AudioWrapperr_two input");
        x = InputsWrapper.length;
        //console.log(x + '  ' + MaxInputsAudio);
        if (count < MaxInputsAudio) //max input box allowed
        {
            speci_field_index++;
            FieldCount++; //text box added increment
            //add input box
            $(InputsWrapper).parents('#AudioWrapperr_two').append('<div class="form-group extra_two form-group-bg-grey clearfix"><div class="col-md-12"><div class="col-md-6 p-l-0" style="padding-left:0;"><input type="text" required name="items['+speci_field_index+']" class="form-control cls-sub-cat-title" id="items' + speci_field_index + '" placeholder="Title" ><input type="hidden" name="items_type['+speci_field_index+']" id="items_type' + speci_field_index + '" value=""></div><div class="col-md-5"></div><div class="col-md-1"><span class="remove_now_two close" style="margin-top:5px;"><i class="fa fa-close"></i></span></div></div></div>');
            //x++; //text box increment
        }

    });
    
    $("body").on("click", ".remove_now_two", function (e) {
        if (count >= 1){
            $(this).parents('.extra_two').remove();
            count--;
        }
        return false;
    });
});

// ********* Category Change ********
$('.cls-bic-prnt-cat').on('change',function(){
    if($(this).val()==""){
        $('.cls-sub-cat-title').attr('disabled',false);
        $('.cls-cat-parent-empty').show();
    }else{
        $('.cls-sub-cat-title').attr('disabled',true);
        $('.cls-cat-parent-empty').hide();
    }
});

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>