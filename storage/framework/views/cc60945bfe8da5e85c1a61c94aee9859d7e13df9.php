

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main_content'); ?>

<section class="content-header">
    <h1>
        contacts

    </h1>
  
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
<?php if(can('add_module')): ?>
                    <!-- <a class="btn btn-primary pull-right" href="<?php echo e(url('contact/create')); ?>" >Create</a> -->
                      <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">

    <?php if(can('browse_module')): ?>
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Email</th>
                            
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        <?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $count++;?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <td><?php echo e($contact->title); ?></td>
                            <td><?php echo $contact->address; ?></td>
                            <td><?php echo $contact->phone; ?></td>
                            <td><?php echo e($contact->email); ?></td>
                            
                    
                            <td>
                               
                                <a href="<?php echo e(url('contact/edit',$contact->id)); ?>" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            </td>
                            
                              
                            
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                     <?php else: ?>
        <?php echo $__env->make('admin.no-access-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/dataTables.bootstrap.min.js')); ?>"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>