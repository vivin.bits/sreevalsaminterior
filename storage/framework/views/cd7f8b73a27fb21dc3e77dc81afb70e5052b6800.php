

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main_content'); ?>

<section class="content-header">
    <h1>
        Testimonial 

    </h1>
  
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
<?php if(can('add_module')): ?>
                    <a class="btn btn-primary pull-right" href="<?php echo e(url('testimonial/create')); ?>" >Create</a>
                      <?php endif; ?>
                </div><!-- /.box-header -->
                <div class="box-body">

    <?php if(can('browse_module')): ?>
    
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>S#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count=0;?>
                        <?php $__currentLoopData = $test; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $test): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $count++;?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <td><?php echo e($test->title); ?></td>
                            <td><?php echo $test->content; ?></td>
                            <td> <img src="<?php echo e(asset('media/profile-images/'.$test->image)); ?>" 
                                     alt="San Fran" style="width:120px;"></td>
                    
                            <td>
                               
                                <a href="<?php echo e(url('test/edit/'.$test->id)); ?>" title="Edit Module"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              
                            
                            
                            
                               
                                <a href="<?php echo e(url('test/delete/'.$test->id)); ?>" title="Delete  Module"><i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              
                              </td>
                            
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                     <?php else: ?>
        <?php echo $__env->make('admin.no-access-content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
   
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/dataTables.bootstrap.min.js')); ?>"></script>
<script type="text/javascript">

    $('#sampleTable').DataTable({
        bPaginate: false,
        bSort: false,
        bFilter: false,
        bInfo: false
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>