<?php $__env->startSection('content'); ?>

<section class="price-section">
    	<div class="auto-container">
        	<div class="sec-title centered">
            	<div class="title">Basic</div>
                <h2><span class="theme_color">Interior</span>  Rs 3.99* Lakhs Package</h2>
				<h2><span class="theme_color">Free: </span>  Hood & Hob</h2>
            </div>
            <div class="row clearfix">
                
               
              
                
                <!--Price Block-->
                <div class="price-block col-md-12 col-sm-12 col-xs-12">
                	<div class="inner-box">
                    	<div class="upper-box">
                        	<div class="title">Basic</div>
                             <div class="price">Offer Price Rs 3.99* Lakhs </div>
                            <div class="months">Actual Price Rs 4.99* LAKHS PACKAGE</div>
                        </div>
                        <ul>
                        	<li>Modular Kitchen(360 x 90 cm, 360 x 60 cm) - 50 Sqft + 9 Accessories Hettich or Ebco</li>
                            <li>Accessories and Utilities: a) Cutlery tray, b) Cup and Saucer, c) Plate Basket, d) GTPT, e) Pull Out, f) Detergent Holder, g) Waste Bin, h) Plain Basket, i) Bottle Basket</li>
                            <li>Bed 1: 3 Door Wardrobe (125 x 210 cm) + Queen Size Cot + 1 Side Table</li>
                            <li>Bed 2: 3 Door Wardrobe (125 x 210 cm) + Queen Size Cot + 1 Side Table</li>
                            <li>Bed 3: 3 Door Wardrobe (125 x 210 cm) + Family Size Cot + 1 Side Table</li>
							<li>Dining:Table with Glass Top + 6 Chairs + Wash Counter</li>
							<li>Living: LCD TV Display Unit</li>
							<li>Plywood: 710 Gurgen BWP Marine Plywood(10 to 15 Years Warranty)</li>
							<li>Multiwood : Power, Thomason, Supreme (18mm life time guarantee)</li>
							<li>Lamination: Green or Merino</li>
							<li><h2>Hood & Hob: <span class="theme_color" style="font-weight: 900">Faber or kaff or Hafele(4 burner) - Free</span></h2></li>
                        </ul>
                       
                        <button type="button" id=""  class="theme-btn btn-style-five" data-toggle="modal" data-target="#modalForm">Book Now</button>
                    </div>
                </div>
                
                <!--Price Block-->
              
                
            </div>
        </div>
    </section>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>