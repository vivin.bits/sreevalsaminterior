

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('main_content'); ?>

<section class="content-header">
    <h1>
        Slider 

    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-12 admin-users">
            <div class="box">
                <div class="box-header">

                    <a class="btn btn-primary pull-right" href="<?php echo e(url('activity/create')); ?>" >Create</a>

                </div><!-- /.box-header -->
                <div class="box-body">

                    <table class="table table-hover table-bordered " id="sample">
                        <thead>
                        <tr>
                            <th>SL#</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('admin/js/plugins/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/custom/status-change.js')); ?>"></script> 
    <script src="<?php echo e(asset('admin/js/custom/module/schoolActivity.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/custom/delete.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>