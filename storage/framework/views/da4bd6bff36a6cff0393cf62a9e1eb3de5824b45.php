

<?php $__env->startSection('additional_styles'); ?>
<link rel="stylesheet" href="<?php echo asset('redactor/redactor.css'); ?>" />
<link rel="stylesheet" href="<?php echo asset('redactor/plugins/filemanager/filemanager.css'); ?>" />
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head_title'); ?>
Email Templates
<?php $__env->stopSection(); ?>
<?php $__env->startSection('main_content'); ?>
 <section class="content-header">
        <h1>
                About Us Edit
            
        </h1>
   
    </section>
   
<section class="content">
        <div class="row">
            <div class="col-xs-12">
          

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> </h3>
                        <a href="<?php echo URL::to('admin/about'); ?>" class="btn btn-primary pull-right">Back </a>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        


            <?php if(Session::get('message')): ?>

            <div class="alert alert-success msgalt">
             <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a><?php echo Session::get('message'); ?>

           </div>

           <?php endif; ?>
           <div class="col-md-10 p-left-0">
              
                    <form action="<?php echo e(url('about/edit',$about->id)); ?>" method="post"  enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <div class="box-body">
                          
                            <div class="form-group <?php if($errors->first('title')): ?> has-error <?php endif; ?>">
                                <label for="title">Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Enter Title" value="<?php echo $about->title; ?>">
                                <span><?php echo e($errors->first('title')); ?></span>
                            </div>

                            <div class="form-group email_temp <?php if($errors->first('content')): ?> has-error <?php endif; ?>">
                                    <label for="description">short description</label>
                                    <textarea name="short_desc" id="content"><?php echo e($about->short_description); ?></textarea>
                                    <span><?php echo e($errors->first('content')); ?></span>
                                   
                                    
                                </div>
                                <div class="form-group email_temp <?php if($errors->first('content')): ?> has-error <?php endif; ?>">
                                    <label for="description">Main description</label>
                                    <textarea name="main_desc" id="content"><?php echo e($about->main_description); ?></textarea>
                                    <span><?php echo e($errors->first('content')); ?></span>
                                   
                                    
                                </div>
                                <div class="form-group <?php if($errors->first('meta_title')): ?> has-danger <?php endif; ?>">
                                    <label for="meta_title">Meta Title</label>
                                    <input class="form-control <?php if($errors->first('meta_title')): ?> is-invalid <?php endif; ?>"
                                           id="meta_title" name="meta_title" type="text" placeholder="Enter meta name"
                                           autocomplete="off"  value="<?php echo $about->meta_title; ?>">
                                    <?php if($errors->first('meta_title')): ?>
                                    <label class="alert-danger"><?php echo e($errors->first('meta_title')); ?></label>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group <?php if($errors->first('meta_title')): ?> has-danger <?php endif; ?>">
                                        <label for="meta_title">Meta Content</label>
                                        <input class="form-control <?php if($errors->first('meta_title')): ?> is-invalid <?php endif; ?>"
                                               id="meta_title" name="meta_content" type="text" placeholder="Enter meta name"
                                               autocomplete="off" value="<?php echo $about->meta_content; ?>">
                                        <?php if($errors->first('meta_title')): ?>
                                        <label class="alert-danger"><?php echo e($errors->first('meta_title')); ?></label>
                                        <?php endif; ?>
                                    </div>
                                <div class="form-group <?php if($errors->first('meta_key')): ?> has-danger <?php endif; ?>">
                                    <label for="meta_key">Meta Key</label>
                                    <input class="form-control <?php if($errors->first('meta_key')): ?> is-invalid <?php endif; ?>"
                                           id="meta_key" name="meta_key" type="text" placeholder="Enter meta key"
                                           autocomplete="off"  value="<?php echo $about->meta_key; ?>">
                                    <?php if($errors->first('meta_key')): ?>
                                    <label class="alert-danger"><?php echo e($errors->first('meta_key')); ?></label>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group <?php if($errors->first('meta_description')): ?> has-danger <?php endif; ?>">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea
                                            class="form-control <?php if($errors->first('meta_description')): ?> is-invalid <?php endif; ?>"
                                            id="meta_description" name="meta_description" type="text"
                                            placeholder="Enter meta description" autocomplete="off"> <?php echo e($about->meta_description); ?></textarea>
                                    <?php if($errors->first('meta_description')): ?>
                                    <label class="alert-danger"><?php echo e($errors->first('meta_description')); ?></label>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group <?php if($errors->first('meta_key')): ?> has-danger <?php endif; ?>">
                                        <label for="meta_key">Author</label>
                                        <input class="form-control <?php if($errors->first('meta_key')): ?> is-invalid <?php endif; ?>"
                                               id="meta_key" name="author" type="text" placeholder="Enter author"
                                               autocomplete="off"  value="<?php echo $about->author; ?>">
                                        <?php if($errors->first('meta_key')): ?>
                                        <label class="alert-danger"><?php echo e($errors->first('meta_key')); ?></label>
                                        <?php endif; ?>
                                    </div>

                     
                           
                         
                        
                            
                        
                            <div>
                                    <div class="form-group <?php if($errors->first('image')): ?> has-danger <?php endif; ?>">
                                            <label for="image">Image</label><br>
                                            <input   id="image" name="image" type="file" accept="image/x-png,image/gif,image/jpeg">
                                            <?php if($errors->first('image')): ?><span class="alert-danger"><?php echo e($errors->first('image')); ?></span> <?php endif; ?>
                                        </div>
                                        <img src="<?php echo e(asset('media/profile-images/'.$about->image)); ?>" id="uploadPreview" width="150px">

                        <button type="submit" class="pull-right btn btn-primary margin-top">Save</button>
                    </form>
                </div><!-- /.box -->
   </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    </section><!-- /.content -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="<?php echo e(asset('admin/js/jquery.validate.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/redactor.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/table/table.min.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/filemanager/filemanager.js')); ?>"></script> 
<script src="<?php echo e(asset('admin/js/additional-methods.js')); ?>"></script>
<script src="<?php echo e(asset('redactor/plugins/imagemanager/imagemanager.js')); ?>"></script> 

<script src="<?php echo e(asset('redactor/langs/ar.js')); ?>"></script>

<script src="<?php echo e(asset('admin/js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(asset('admin/js/jquery-migrate-3.0.0.min.js')); ?>"></script>


<!-- place in header of your html document -->
<script>

 $R('#content', {
  plugins: ['filemanager','table','imagemanager'],
   // fileUpload: '<?php echo asset('redactor/scripts/file_upload.php'); ?>',
  //  fileManagerJson: '/your-folder/files.json' ,
  imageUpload: '<?php echo asset('redactor/scripts/image_upload.php'); ?>',
 //   imageManagerJson: '/your-folder/images.json',
 // lang: 'ar' ,
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>