<?php $__env->startSection('content'); ?>
 <?php if($package): ?>
     <!--Page Title-->
     <section class="page-title" style="background-image:url(<?php echo e((asset('website/images/background/8.jpg'))); ?>)">
    	<div class="auto-container">
        	<div class="clearfix">
            	<div class="pull-left">
                	<h1><?php echo e($package->title); ?></h1>
                </div>
                <div class="pull-right">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li><?php echo e($package->title); ?></li>
                    </ul>
                </div>
            </div>
            <div class="contact-number text-center"><span class="icon flaticon-phone-call"></span>Call Us: +91 9349 429 212</div>
        </div>
    </section>
   
  
   <!--Project Section-->
   <section class="project-section">
    	<div class="auto-container">
        	<div class="sec-title centered">
            	<div class="title">Our Best Works</div>
                <h2><span class="theme_color">Custom Made </span> <?php echo e($package->title); ?></h2>
				<div class="text"><?php echo e($package->description); ?></div>
            </div>
            
            <!--MixitUp Galery-->
            <div class="mixitup-gallery">
                
                <!--Filter-->
                <div class="filters clearfix">
                   
                </div>
                
                <div class="filter-list row clearfix">
					<!--Gallery Item-->
                   <?php if($package->media): ?>
                 
                   <?php $__currentLoopData = $package->media; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                       
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="<?php echo e(getImageByPath($image->filename,'354x198','packages-media')); ?>" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                                <!--Overlay Box-->
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="<?php echo e(getImageByPath($image->filename,'354x198','packages-media')); ?>" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made <?php echo e($package->title); ?></a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    <?php endif; ?>
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_02.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                                
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_02.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#"> Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_04.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_04.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_05.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_05.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_06.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_06.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_07.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                                
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_07.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_08.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                              
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_08.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure> -->
                        <!-- </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_09.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                               
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_09.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_10.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                               
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_10.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_11.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum">
                               
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_11.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_12.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_12.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_13.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_13.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_14.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_14.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_15.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_15.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
					<!--Gallery Item-->
                    <!-- <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="images/products/hospitalityinteriors/sreevalsam_interiors_03.jpg" alt="sreevalasam_interior_kottayam_pathanamthitta_ernakulam_trivandrum"> -->
                                <!--Overlay Box-->
                                <!-- <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                            
                                            <li><a href="images/products/hospitalityinteriors/sreevalsam_interiors_03.jpg" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#">Custom Made Hospitality Interiors</a></h3>
                                        
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div> -->
                
                   
                    
                </div>                    
                
            </div>
            
            <!--Styled Pagination-->
             
            <!--End Styled Pagination-->
            
        </div>
    </section>
    <!--End Project Section-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>