



<div class="filter-list row clearfix gallery-thumbs">
    <?php if($galler_images): ?>
<?php $__currentLoopData = $galler_images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<div class="gallery-item mix all kitchen col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <figure class="image-box">
                                <img src="<?php echo e(getImageByPath($gallery->image,'354.03x198.35','tab-images')); ?>" alt="">
                                <!--Overlay Box-->
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <ul>
                                         
                                            <li><a href="<?php echo e(getImageByPath($gallery->image,'2000x1000','tab-images')); ?>" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                        </ul>
                                        <div class="content">
                                            <h3><a href="#"><?php echo e($gallery->content); ?></a></h3>
                                           
                                        </div>
                                    </div>
                                </div>
                            </figure>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                    </div>  
                    
                  
                   