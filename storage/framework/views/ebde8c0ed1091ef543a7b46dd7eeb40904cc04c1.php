<?php $menus =  \Illuminate\Support\Facades\Config::get('backend-menu.menus');?>
    
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
    

        <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      
                    <?php $flag = false;
                    $new_str = substr(Request::path(), ($pos = strpos(Request::path(), '/')) !== false ? $pos + 1 : 0);
                    ?>

                          <li
                          <?php if(is_array($value['child'])): ?>
                                  <?php $__currentLoopData = $value['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <?php if($new_str == $v['slug']): ?> <?php $flag = true;?> <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <?php if($flag): ?> class="active current-menu treeview" <?php else: ?> class="treeview" <?php endif; ?>
                          <?php endif; ?>
                                  >
                            <a href="<?php echo URL::to($value['slug']); ?>">
                              <i class="<?php echo $value['icon-class']; ?>"></i> <span><?php echo $value['Name']; ?></span>
                               <?php if($value['child']): ?>
                                    <i class="fa fa-angle-left pull-right"></i>
                                   <?php endif; ?>
                            </a>
                              <?php if(is_array($value['child'])): ?>

                                        <ul class="treeview-menu">
                                        <?php $__currentLoopData = $value['child']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                         
                                                    <li <?php if(Request::path() == $v['slug']): ?> class="active current-menu treeview" <?php endif; ?>><a href="<?php echo URL::to($v['slug']); ?>"><i class="<?php echo $v['icon-class']; ?>"></i> <?php echo $v["Name"]; ?></a></li>
                                          
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                              <?php endif; ?>
                          </li>
          

           
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>