<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?> 

  <!--Main Slider-->
  <section class="main-slider">



<div id="slides">
<?php if($banner): ?>
<?php $__currentLoopData = $banner; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banners): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <img src="<?php echo e(getImageByPath($banners->image,'1284x722','banner')); ?>">
  <!-- <img src="<?php echo e(asset('website/images/main-slider/image-2.jpg')); ?>" alt="">
  <img src="<?php echo e(asset('website/images/main-slider/image-3.jpg')); ?>" alt=""> -->
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
</div>


  
</section>
<!--End Main Slider-->




<section class="blog-section fluid-section-one">
     <div class="blog-picture left">
 
                        <?php if($whychoose): ?>     
        <img src="<?php echo e(getImageByPath($whychoose->image,'642x556','banner')); ?>" alt="blog-picture" class="width-100">
     
     </div>

         <div class="content-column">
            <div class="inner-box-1">
                <div class="sec-title">
                    <div class="title">We mean expertise</div>
                    <h2><span class="theme_color">WHY  </span> CHOOSE US</h2>
                </div>
                <div class="text"><?php echo e($whychoose->message); ?></div>
                <ul class="list-style-one clearfix">
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-briefcase-1"></span>15 years of Experience</li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-diamond-1"></span>Exclusive Partnerships</li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-bank-building"></span>Individual Approach</li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>Global Best Solutions</li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>Best Quality</li>
                    <li class="col-md-6 col-sm-6 col-xs-12"><span class="icon flaticon-two-fingers-up"></span>24/7 Online Support</li>
                </ul>
            </div>
        </div>
  </section>
  <?php endif; ?>



<!--Services Section-->
<section class="services-section">

    <div class="row">
        <div class="sec-title">
                    <div class="auto-container">
            <div class="title">We Build Everything</div>
            <h2><span class="theme_color">Our</span> Services</h2>

        </div>

    </div>

<div class="auto-container">
        <div class="three-item-carousel owl-carousel owl-theme">
            <?php if($service): ?>
            <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <!--Services Block-->
            <div class="services-block">
                <div class="inner-box">
                    <div class="image">
                        <a href=""><img src="<?php echo e(getImageByPath($services->image,'364x236','profile-images')); ?>" alt="" /></a>
                    </div>
                    <div class="lower-content">
                        <div class="upper-box">
                            <div class="icon-box">
                                <span class="icon flaticon-drawing"></span>
                            </div>
                            <h3><a href=""><?php echo $services->title; ?></a></h3>
                        </div>
                        <div class="text"><?php echo str_limit($services->message,400); ?></div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            
           </div>
            
           
            
        </div>
    </div>
</section>
<!--End Services Section-->

     
<!--End Fluid Section One-->

<!--Project Section-->
<section class="project-section">

        
    <div class="row">
        <div class="sec-title centered">
                    <div class="auto-container">
         <div class="title">Our Best Works</div>
            <h2><span class="theme_color">Our</span> Recent Projects</h2>

        </div>
         
    </div>

        <!--MixitUp Galery-->
        <div class="mixitup-gallery">
             <div class="auto-container">
            <!--Filter-->
      
            
            <div class="filter-list row clearfix">
                

<?php if($project): ?>
<?php $__currentLoopData = $project; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $projects): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <!--Gallery Item-->
                <div class="gallery-item mix all agriculture col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box">
                            <img src="<?php echo e(getImageByPath($projects->image,'354x198','project')); ?>" alt="">
                            <!--Overlay Box-->
                            <div class="overlay-box">
                                <div class="overlay-inner">
                                    <ul>
                                     
                                        <li><a href="<?php echo e(getImageByPath($projects->image,'900x500','project')); ?>" data-fancybox="images" data-caption="" class="link"><span class="icon flaticon-picture-gallery"></span></a></li>
                                    </ul>
                                    <div class="content">
                                        <h3><a href="#">Home and Hospitality Interiors</a></h3>
                                       
                                    </div>
                                </div>
                            </div>
                        </figure>
                    </div>
                </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>        
            </div>
                
            
        </div>
        
    </div>
</section>
<!--End Project Section-->



    <section class="price-section">


    <div class="row">
        <div class="sec-title centered">
                    <div class="auto-container">
         <div class="title">Choose Packages</div>
            <h2><span class="theme_color">Our</span> Best Price</h2>

        </div>
    </div>

        <div class="row clearfix">
             <div class="auto-container">
            <!--Price Block-->
            <div class="price-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="title">Basic</div>
                        <div class="price">Offer Price Rs 3.99* Lakhs </div>
                        <div class="months">Actual Price Rs 4.99* LAKHS PACKAGE</div>
                        <div class="months">Free:  <span class="theme_color" style="font-weight: 900"> Hood & Hob</span></div>
                    </div>
                    
                    <a href="interior3.99lakhs.html" class="theme-btn btn-style-five">Book Now</a>
                </div>
            </div>
            
            <!--Price Block-->
            <div class="price-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="title">Standard</div>
                        <div class="price">Offer Price Rs 4.39* Lakhs</div>
                        <div class="months">Actual Price Rs 5.39* LAKHS PACKAGE</div>
                        <div class="months">Free:   <span class="theme_color" style="font-weight: 900">Hood & Hob</span></div>
                    </div>
                  
                    <a href="interior4.39lakhs.html" class="theme-btn btn-style-five">Book Now</a>
                </div>
            </div>
            
            <!--Price Block-->
            <div class="price-block col-md-4 col-sm-6 col-xs-12">
                <div class="inner-box">
                    <div class="upper-box">
                        <div class="title">Super</div>
                        <div class="price">Offer Price Rs 4.99* Lakhs </div>
                        <div class="months">Actual Price Rs 5.99* LAKHS PACKAGE</div>
                        <div class="months">Free:   <span class="theme_color" style="font-weight: 900">Sofa Set(3+2) </span></div>
                    </div>
                   
                    <a href="interior4.99lakhs.html" class="theme-btn btn-style-five">Book Now</a>
                </div>
            </div>
            
            
            
            
            
            
            </div>
            
        </div>
    </div>
</section>
<!--End Price Section-->
    <!--Call To Action-->
<section class="call-to-action-section" style="background-image:url(<?php echo e(asset('website/images/background/2.jpg')); ?>)">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Text Column-->
            <div class="text-column col-md-9 col-sm-12 col-xs-12">
                <div class="text">Sreevalsam Interiors Service & <span class="theme_color">Care Committed to Serve.</span> For Immediate Service Care.</div>
            </div>
            <!--Btn Column-->
            <div class="btn-column col-md-3 col-sm-12 col-xs-12">
                <a href="#" class="theme-btn btn-style-three">+91 9349 429 212</a>
            </div>
        </div>
    </div>
</section>
<!--End Call To Action-->
<!--Testimonial Section-->
<section class="testimonial-section" style="background-image:url(<?php echo e(asset('website/images/background/patern-1.png')); ?>)">
    <div class="auto-container">
        <div class="row clearfix">
            
            <!--Image Column-->
            <div class="image-column col-md-5 col-sm-5 col-xs-12">
                <div class="image">
                    <img src="<?php echo e(asset('website/images/resource/testimonial.jpg')); ?>" alt="" />
                </div>
            </div>
            <!--Carousel Column-->
            <div class="carousel-column col-md-7 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="sec-title">
                        <div class="title">Testimonial</div>
                        <h2><span class="theme_color">Clients </span> Say</h2>
                    </div>
                    
                    <div class="single-item-carousel owl-carousel owl-theme">
                        <?php if($testmonial): ?>
                        <?php $__currentLoopData = $testmonial; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $testmonials): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <!--Testimonial Block-->
                        <div class="testimonial-block">
                            <div class="inner-box">
                                <div class="text"><?php echo e($testmonials->content); ?></div>
                                <div class="author-info">
                                    <div class="author-inner">
                                        <div class="image">
                                            <img src="<?php echo e(getImageByPath($testmonials->image,'354x198','profile-images')); ?>" alt="" />
                                        </div>
                                        <h3><?php echo e($testmonials->title); ?></h3>
                                        <!-- <div class="designation">Kottayam</div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                      
                        
                     
                        
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</section>
<!--End Testimonial Section-->





<!--Clients Section-->
<section class="clients-section">
    <div class="auto-container">
        
        <div class="sponsors-outer">
            <!--Sponsors Carousel-->
            <ul class="sponsors-carousel owl-carousel owl-theme">
            <?php if($activity): ?>
            <?php $__currentLoopData = $activity; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $activitys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="slide-item"><figure class="image-box"><a href="#"><img src="<?php echo e(getImageByPath($activitys->main_image,'190x105','school-activity')); ?>" alt=""></a></figure></li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endif; ?>
            </ul>
        </div>
        
    </div>
</section>
<!--End Clients Section-->

<!--Contact Info Section-->
<section class="contact-info-section">
    <!--Map Section-->
    <div class="map-section">
        <!--Map Outer-->
        <div class="map-outer">
            <!--Map Canvas-->
            <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3929.691798835347!2d76.32512821460683!3d9.95957999287887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s49%2F1638-A6+Thykoodam+Vyttila+P.O%2C+Ernakulam+India-82019!5e0!3m2!1sen!2sin!4v1525144531287" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <div class="map-canvas"
                data-zoom="15"
                data-lat="9.959580"
                data-lng="76.327317"
                data-type="roadmap"
                data-hue="#ffc400"
                data-title="Sreevalsam Interiors"
                data-icon-path="images/icons/map-marker.png"
                data-content="49/1638-A6 Thykoodam
Vyttila P.O, Ernakulam
India-82019<br><a href='mailto:info@youremail.com'>Mail: info@sreevelsaminteriors.com</a>">
            </div>
        </div>
    </div>
    <!--Map Section-->
    <div class="auto-container">
        <div class="inner-container">
            <div class="row clearfix">
                <!--Info Column-->
                <div class="info-column col-md-4 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <ul class="list-style-two">
                            <li><span class="icon flaticon-home-1"></span><strong>49/1638-A6 Thykoodam</strong>Vyttila P.O, Ernakulam, India-82019</li>
                            <li><span class="icon flaticon-envelope-1"></span><strong>Send your mail at</strong>info@sreevalsaminteriors.com</li>
                            <li><span class="icon flaticon-technology-2"></span><strong>Have Any Question</strong>+91 9349 429 212, 0484 2306 221</li>
                            <li><span class="icon flaticon-clock-1"></span><strong>Working Hours</strong>Mon-Sat:9.30am to 7.00pm</li>
                        </ul>
                    </div>
                </div>
                <!--Form Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <h2>Contact Us</h2>
                        <!--Contact Form-->
                        <div class="contact-form">
                            <form method="post" action="<?php echo e(url('contact-store')); ?>">
                                <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                                <div class="row clearfix">
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="text" name="name" value="" placeholder="Name" required>
                                    </div>
                                    <div class="form-group col-md-6 col-sm-6 co-xs-12">
                                        <input type="email" name="email" value="" placeholder="Email" required>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 co-xs-12">
                                        <textarea name="message" placeholder="Your Massage"></textarea>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 co-xs-12">
                                        <button type="submit" class="theme-btn btn-style-one">Send Message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--End Contact Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Contact Info Section-->


<section id="contact-btns">
<div class="container-fluid">
    <a href="tel:+919349429212" class="btn btn-primary">Call Us Now</a>
    <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#getForm">GET A FREE QUOTE</a>
</div>
</section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>